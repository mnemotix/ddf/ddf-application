"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports["default"] = void 0;

var path = require("path");

exports.default = function(_ref) {
  var t = _ref.types;
  return {
    visitor: {
      ImportDeclaration: {
        enter: function ImportDeclaration(nodePath) {
          var node = nodePath.node;

          if (node && node.source && node.source.value && node.source.type === 'StringLiteral') {
            var sourceValue = node.source.value;
            if ([".svg", ".png", ".jpg", ".jpeg", ".md"].indexOf(path.extname(sourceValue)) > -1) {
              // Replace variable style imports by transparent images
              if (node.specifiers && node.specifiers.length > 0) {
                var id = t.identifier(node.specifiers[0].local.name);
                nodePath.replaceWith(t.variableDeclaration('var', [t.variableDeclarator(id, t.stringLiteral("data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\n" +
                  "\n"))]));
              } else {
                nodePath.remove();
              }
            }
          }
        }
      }
    }
  };
};