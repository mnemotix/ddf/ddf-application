const fontbasesize = 16;

const Config = {
  theme: {
    breakpoint: 'lg'
  },
  widths: {
    desktopColumn: 500 //desktop-column-width
  },
  heights: {
    desktopHeader: 100,
    mobileHeader: 60, // mobile-header-default-height
    footerLinksBanner: 83,
    footerPartnersLinks: 70,
    footerStoresLinks: 100,
    footerImproveButton: 100,
    footerFooterExplanationText: 182,
    improveDdfButton: 60,
  },
  mediaQueries: {
    desktopMinWidth: 1024,
    mobileMaxWidth: 1023
  },
  colors: {
    white: "#ffffff",
    purple: "#8200a0",
    darkpurple: "#600C88",

    pink: "#ff2882",
    weirdpink: "#C7B8CC",
    yellow: "#ffb400",

    verylightgray: "#f6f6f6",
    lightgray: "#f4f4f4",
    mediumgray: "#e6e6e6",
    darkgray: "#828282",
    gray: "#828282",
    verydarkgray: "#6c6969",

    black: "#000000",
    orange: "#ff6923",
    green: "#00ebd2",
    loading_placeholder: "#f4f4f4",
  },
  spacings: {
    verytiny: 3,
    tiny: 5,
    small: 10,
    medium: 20,
    large: 30,
    huge: 40,
  },
  fontSizes: {
    xsmall: fontbasesize * 0.625,
    small: fontbasesize * 0.75,
    sm: fontbasesize * 0.875,
    medium: fontbasesize,
    ml: fontbasesize * 1.1,
    large: fontbasesize * 1.25,
    xlarge: fontbasesize * 1.5,
    xxlarge: fontbasesize * 2.125,
  },
  fontWeights: {
    light: 200,
    normal: 400,
    medium: 500,
    semibold: 600,
    bold: 700,
  },
  cookie: {
    name: 'ddf-disclaimer-cookies',
    duration: 365
  }
};
export default Config;
