import React, {Suspense} from 'react'
import {Route, Redirect, Switch} from 'react-router-dom';
import {ROUTES} from './routes';
import {lazily} from 'react-lazily'
import {DdfLoadingSplashScreen} from './components/general/DdfLoadingSplashScreen';
import {MetaSEO} from "./components/widgets/MetaSEO";
import {EnvironmentContext} from './hooks/useEnvironment';
import {createTheme, MuiThemeProvider} from '@material-ui/core/styles';
import {PlaceContextProvider} from "./hooks/PlaceContext";
import Config from "./Config";

/**
 * https://webpack.js.org/api/module-methods/
 *  webpackPrefetch: true 
 *  webpackPreload: true 
 * 
 */

const {NotificationsDisplay} = lazily(() => import(/* webpackChunkName: "utilities" */ './components/general/NotificationsDisplay'));
const {Index} = lazily(() => import('./components/routes/Index'));
const {Form} = lazily(() => import('./components/routes/Form'));
const {ExploreAggs} = lazily(() => import('./components/routes/ExploreForm/ExploreAggs'));
const {ExploreLocalisationForms} = lazily(() => import('./components/routes/ExploreForm/ExploreLocalisationForms'));
const {ExploreConceptForms} = lazily(() => import('./components/routes/ExploreForm/ExploreConceptForms'));
const {ProjectPresentation} = lazily(() => import('./components/routes/static/ProjectPresentation'));
const {Gcu} = lazily(() => import('./components/routes/static/Gcu'));
const {Partners} = lazily(() => import('./components/routes/static/Partners'));
const {Credits} = lazily(() => import('./components/routes/static/Credits'));
const {HelpIndex} = lazily(() => import(/* webpackChunkName: "utilities" */'./components/routes/static/HelpIndex'));

const {PlanSite} = lazily(() => import(/* webpackChunkName: "utilities" */'./components/routes/static/PlanSite'));
const {Accessibilite} = lazily(() => import(/* webpackChunkName: "utilities" */'./components/routes/static/Accessibilite'));
const {MentionsLegales} = lazily(() => import(/* webpackChunkName: "utilities" */'./components/routes/static/MentionsLegales'));
const {PolitiqueConfidentialite} = lazily(() => import(/* webpackChunkName: "utilities" */'./components/routes/static/PolitiqueConfidentialite'));

const {HowToSearch} = lazily(/* webpackChunkName: "help" */() => import('./components/routes/help/HowToSearch'));
const {HowToRead} = lazily(/* webpackChunkName: "help" */() => import('./components/routes/help/HowToRead'));
const {HowToContribute} = lazily(/* webpackChunkName: "help" */() => import('./components/routes/help/HowToContribute'));
const {HelpDdfIrl} = lazily(/* webpackChunkName: "help" */() => import('./components/routes/help/HelpDdfIrl'));
const {FormDemo} = lazily(/* webpackChunkName: "help" */() => import('./components/routes/FormDemo'));
const {Subscribe} = lazily(() => import('./components/routes/Subscribe'));
const {Login} = lazily(() => import('./components/routes/Login'));
const {MyAccount} = lazily(() => import('./components/routes/users/MyAccount'));
const {PasswordForgotten} = lazily(() => import('./components/routes/PasswordForgotten'));
const {CreateLexicalSense} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/CreateLexicalSense'));
const {EditLexicalSense} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/EditLexicalSense'));
const {EditLexicalSenseUsageExemples} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseUsageExemples'));
const {EditLexicalSenseDomains} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseDomains'));
const {EditLexicalSenseTemporalities} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseTemporalities'));
const {EditLexicalSenseRegisters} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseRegisters'));
const {EditLexicalSenseFrequencies} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseFrequencies'));
const {EditLexicalSenseTextualGenres} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseTextualGenres'));
const {EditLexicalSenseGrammaticalconstraints} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseGrammaticalconstraints'));
const {EditLexicalSenseSociolects} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseSociolects'));
const {EditLexicalSenseConnotations} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseConnotations'));
const {EditLexicalSenseGlossaries} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseGlossaries'));
const {EditLexicalSensePlaces} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSensePlaces'));
const {EditLexicalSenseSemanticRelations} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseSemanticRelations'));
const {AddLexicalSenseSemanticRelation} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/AddLexicalSenseSemanticRelation'));


const {Yasguisparql} = lazily(() => import(/* webpackChunkName: "yasguisparql" */ './components/sparql/Yasguisparql'));
const {ContributionsReview} = lazily(() => import(/* webpackChunkName: "admin" */'./components/routes/admin/contributions/ContributionsReview'));
const {Users} = lazily(() => import(/* webpackChunkName: "admin" */'./components/routes/admin/users/Users'));
const {Reportings} = lazily(() => import(/* webpackChunkName: "admin" */'./components/routes/admin/reportings/Reportings'));
const {DumpFiles} = lazily(() => import(/* webpackChunkName: "admin" */'./components/routes/admin/DumpFiles'));
const {Imports} = lazily(() => import(/* webpackChunkName: "admin" */'./components/routes/admin/imports/Imports.js'));
const {DataIngestMonitoring} = lazily(() => import(/* webpackChunkName: "admin" */'./components/routes/admin/monitoring/DataIngestMonitoring.js'));
const {AdminIndex} = lazily(() => import(/* webpackChunkName: "admin" */'./components/routes/admin/AdminIndex'));
const {UnderMaintenance} = lazily(() => import(/* webpackChunkName: "UnderMaintenance" */'./components/routes/UnderMaintenance'));


const theme = createTheme({
  palette: {
    primary: {
      main: Config.colors.purple
    },
    secondary: {
      main: Config.colors.yellow
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1024,
      xl: 1920,
    },
  },
});


/**
 * protected route who require logged user or admin, if not redirect to login page
 * @param {*} props
 */
function ProtectedRoute(props) {
  const {component: Component, isLoadingUser, forAdmin, isAdmin, isLogged, path, ...otherProps} = props;
  // don't redirect while loading user
  if (!isLogged && !isLoadingUser) {
    //return <Redirect to={{ pathname: ROUTES.LOGIN }} />;
    return <Route path={path}> <Redirect to={{pathname: ROUTES.LOGIN}} /></Route>
  } else {
    // if a page is dedicated to admin
    if (!isLoadingUser && forAdmin && !isAdmin) {
      // return <Redirect to={{pathname: '/'}} />;
      return <Route path={path}>   <Redirect to={{pathname: '/'}} /></Route>
    }
    return <Route path={path}> <Component {...otherProps} /></Route>
  }
}

// isLogged is fetched in DDFApplication or mocked  to avoid test failure
// isLoadingUser  is true, prevent redirect
export function MainView(props) {
  const {envData, user, isLoadingUser} = props;

  let isLogged = user?.isLogged;
  let isAdmin = user?.userAccount?.isAdmin || user?.userAccount?.isOperator;
  let underMaintenance = envData?.environment?.UNDER_MAINTENANCE === "1" || envData?.environment?.UNDER_MAINTENANCE === 1;


  return (
    <MuiThemeProvider theme={theme}>
      <Suspense fallback={<DdfLoadingSplashScreen />}>
        <PlaceContextProvider >
          <EnvironmentContext.Provider value={envData?.environment}>
            <MetaSEO title="Dictionnaire des francophones" description="Comprendre et partager les mots avec plusieurs dictionnaires du français parlé partout dans le monde. Participer à l'enrichissement collectif" />
            <NotificationsDisplay />
            {underMaintenance ?
              <UnderMaintenance />
              :
              <Switch>
                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_USAGEEXAMPLES}
                  component={EditLexicalSenseUsageExemples}
                />
                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_DOMAINS}
                  component={EditLexicalSenseDomains}
                />
                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_EDIT_TEMPORALITIES}
                  component={EditLexicalSenseTemporalities}
                />
                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_EDIT_REGISTERS}
                  component={EditLexicalSenseRegisters}
                />
                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_EDIT_FREQUENCIES}
                  component={EditLexicalSenseFrequencies}
                />

                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_EDIT_TEXTUALGENRES}
                  component={EditLexicalSenseTextualGenres}
                />

                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_EDIT_GRAMMATICAL_CONSTRAINTS}
                  component={EditLexicalSenseGrammaticalconstraints}
                />

                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_EDIT_SOCIOLECTS}
                  component={EditLexicalSenseSociolects}
                />

                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_EDIT_CONNOTATIONS}
                  component={EditLexicalSenseConnotations}
                />

                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_EDIT_GLOSSARIES}
                  component={EditLexicalSenseGlossaries}
                />

                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_EDIT_SEMANTIC_RELATIONS}
                  component={EditLexicalSenseSemanticRelations}
                />
                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_ADD_SEMANTIC_RELATION}
                  component={AddLexicalSenseSemanticRelation}
                />

                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_PLACES}
                  component={EditLexicalSensePlaces}
                />
                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.FORM_LEXICAL_SENSE_EDIT}
                  component={EditLexicalSense}
                />

                <Route path={ROUTES.FORM_SEARCH} component={Form} />
                <Route path={ROUTES.EXPLORE} component={ExploreAggs} exact />
                <Route path={ROUTES.EXPLORE_LOCALISATION_FORM} component={ExploreLocalisationForms} exact />
                <Route path={ROUTES.EXPLORE_CONCEPT_FORM} component={ExploreConceptForms} exact />

                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  path={ROUTES.CREATE_LEXICAL_SENSE}
                  component={CreateLexicalSense}
                />
                <Route exact path={ROUTES.PRESENTATION} component={ProjectPresentation} />
                <Route exact path={[ROUTES.GCU, ROUTES.GCU_LEGACY]} component={Gcu} />
                <Route exact path={[ROUTES.PARTNERS, ROUTES.PARTNERS_LEGACY]} component={Partners} />
                <Route exact path={ROUTES.CREDITS} component={Credits} />

                <Route exact path={ROUTES.PLAN_DU_SITE} component={PlanSite} />
                <Route exact path={ROUTES.ACCESSIBILITE} component={Accessibilite} />
                <Route exact path={ROUTES.MENTIONS_LEGALES} component={MentionsLegales} />
                <Route exact path={ROUTES.POLITIQUE_CONFIDENTIALITE} component={PolitiqueConfidentialite} />

                <Route exact path={ROUTES.HELP_INDEX} component={HelpIndex} />
                <Route exact path={ROUTES.HELP_HOW_TO_SEARCH} component={HowToSearch} />
                <Route exact path={ROUTES.HELP_HOW_TO_READ} component={HowToRead} />
                <Route exact path={ROUTES.HELP_HOW_TO_CONTRIBUTE} component={HowToContribute} />
                <Route exact path={ROUTES.HELP_DDF_IRL} component={HelpDdfIrl} />
                <Route exact path={ROUTES.SUBSCRIBE} component={Subscribe} />
                <Route exact path={ROUTES.LOGIN} component={Login} />
                <Route exact path={ROUTES.PASSWORD_FORGOTTEN} component={PasswordForgotten} />
                <ProtectedRoute isLogged={isLogged} isLoadingUser={isLoadingUser} path={ROUTES.MY_ACCOUNT} component={MyAccount} />
                <Route exact path={ROUTES.FORM_DEMO} component={FormDemo} />

                <Route exact path={ROUTES.DUMP_FILES} component={DumpFiles} />
                <Route exact path={ROUTES.IMPORT_SOURCE} component={Imports} />
                <Route exact path={ROUTES.MONITOR_DATA_INGEST} component={DataIngestMonitoring} />

                <ProtectedRoute
                  forAdmin
                  isAdmin={isAdmin}
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.ADMIN}
                  component={AdminIndex}
                />
                <ProtectedRoute
                  forAdmin
                  isAdmin={isAdmin}
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.ADMIN_USERS}
                  component={Users}
                />
                <ProtectedRoute
                  forAdmin
                  isAdmin={isAdmin}
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.ADMIN_REPORTING}
                  component={Reportings}
                />
                <ProtectedRoute
                  isLogged={isLogged}
                  isLoadingUser={isLoadingUser}
                  exact
                  path={ROUTES.CONTRIBUTIONS_REVIEW}
                  component={ContributionsReview}
                />
                <Route exact path={ROUTES.SPARQL} component={Yasguisparql} />
                <Route path="/" component={Index} />
              </Switch>
            }
          </EnvironmentContext.Provider>
        </PlaceContextProvider>
      </Suspense>
    </MuiThemeProvider>
  );
}
