// dumb function to check if event correspond to space or enter key
export function checkIfEnterOrSpace(event) {
  return (event?.code === "Enter" || event?.code === "NumpadEnter" || event?.code === "Space" || event?.keyCode === 23 || event?.keyCode === 13)
}

// reverse the case of the first letter
// for example when user search 'paris', this propose "Paris" to search too.
export function otherCaseFirstLetter(word) {
  if (word.charAt(0) === word.charAt(0).toUpperCase()) {
    return word.charAt(0).toLowerCase() + word.substr(1);
  } else {
    return word.charAt(0).toUpperCase() + word.substr(1);
  }
}

export function upperCaseFirstLetter(word) {
  try {
    if (typeof "word" === 'string') {
      // return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
      return word.charAt(0).toUpperCase() + word.substr(1);
    }
  } catch (error) {
    console.log(error)
  }
  return ""
}


// lodash.uniq ne marche pas sur les array d'object malgré leur doc 
export function unique(array, key) {

  return [...new Map(array.map(item => [item[key], item])).values()];

}

// decoupe un tableau en tableau de size elements
export function chunk(arr, size) {
  var arrTmp = [];

  for (var i = 0; i < arr.length; i += size) {
    arrTmp.push(arr.slice(i, i + size));
  }
  return arrTmp;
}

// if word length is longer than maxLength, cut the rest and add ...
export function cutLongWord(word, maxLength = 60) {
  if (word && word.length > 60) {
    return word.slice(0, 60) + "...";
  } else {
    return word
  }
}


/**
* get the type from the id
 examples:
 "ddfa:domaine/brasserie" return "domain"
 "ddfa:temporality/outdatedUsage" return "temporality"
...
* @returns 
*/
export function getTypeFromId(id) {
  const mapping = {
    "domaine": "domain", // les vocabulaires controllés presentes qlq typo
    "temporality": "temporality",
    "grammatical-constraint": "grammatical-constraint",
    "sociolect": "sociolect",
    "register": "register",
    "textual-genre": "register",// les vocabulaires controllés presentes qlq typo
    "frequency": "frequency",
    "connotation": "connotation",
    "glossary": "glossary"
  }


  try {
    let type = id?.split("/")?.[0].split(":")?.[1];
    return mapping?.[type];
  } catch (error) {
    console.log(error);
    return false;
  }
}


export function getTypeFromSchemes(data) {
  return data?.schemes?.edges?.[0]?.node?.id ? data?.schemes?.edges?.[0]?.node?.id.replace("ddfa:", "") : false;
}

// add space each 3 number to better reading of large number
export function prettifyNumber(num) {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '&nbsp;');
}

function isEncoded(uri) {
  uri = uri || '';
  try {
    return uri !== decodeURIComponent(uri);
  } catch (error) { }
  return false;
}

export function decodeURIComponentIfNeeded(uri) {
  // decode recursivly because uri can be encoded multiple times
  while (isEncoded(uri)) {
    uri = decodeURIComponent(uri);
  }

  return uri;
}


export function encodeURIComponentIfNeeded(uri) {

  uri = decodeURIComponentIfNeeded(uri)
  if (!isEncoded(uri)) {
    uri = encodeURIComponent(uri);
  }

  return uri;
}
