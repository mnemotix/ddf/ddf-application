import {getPlaceRegionInfo} from '../places';

describe("getPlaceRegionInfo()", () => {
  it("Returns the state and country name with a comma separation", () => {
    let place = {
      "id": "geonames:6454071",
      "__typename": "City",
      "name": "Grenoble",
      "countryCode": "FR",
      "countryId": "geonames:geonames:3017382",
      "countryName": "France",
      "stateName": "Auvergne-Rhône-Alpes"
    };
    let res = getPlaceRegionInfo(place);
    expect(res).toEqual("Auvergne-Rhône-Alpes, France");
  });


  it("Returns only the country name", () => {
    let place = {
      "id": "geonames:11071625",
      "__typename": "State",
      "name": "Auvergne-Rhône-Alpes",
      "countryCode": "FR",
      "countryId": "geonames:geonames:3017382",
      "countryName": "France"
    };
    let res = getPlaceRegionInfo(place);
    expect(res).toEqual("France");
  });

  it("Returns an empty string", () => {
    let place = {
      "id": "geonames:3017382",
      "__typename": "Country",
      "name": "France",
      "countryCode": "FR"
    };
    let res = getPlaceRegionInfo(place);
    expect(res).toEqual("");
  });
});
