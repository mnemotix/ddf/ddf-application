import get from 'lodash/get';
import groupBy from 'lodash/groupBy';
import {gql} from "@apollo/client";

/**
 * Returns the list of "ddf:SemanticRelation" of a ontolex:LexicalSense grouped by the ddf:semanticRelationType.
 *
 * @see into the ontology `ontolex:LexicalSense > ddf:SemanticRelation/ddf:semanticRelationType`
 *
 * @param lexicalSense
 * @return {object} An object with semanticRelationType as key and semanticRelation[] as value
 */
export function getGroupedLexicalSenseSemanticRelationList(lexicalSense) {

  return groupBy(
    lexicalSense?.semanticRelations?.edges.map(({node}) => node),
    (semanticRelation) => semanticRelation.semanticRelationTypeLabel
  );

}

/**
 * Returns semanticRelations
 *  @param {bool} excludeDistinctWrittenRep:  return only relations between definitions of same form/writtenRep
 *  @param {bool} filterOnDistinctWrittenRep: return only relations of a definitions of other form/writtenRep  
 * @type {gql}
 */
export const gqlLexicalSenseSemanticRelationFragment = gql`
  fragment LexicalSenseSemanticRelationFragment on LexicalSense {
    semanticRelations(
      filterOnDistinctWrittenRep: $filterOnDistinctWrittenRep
      excludeDistinctWrittenRep: $excludeDistinctWrittenRep
    ) {
      edges {
        node {
          id
          gatheredLexicalSenses{
            edges {
              node {
                canonicalFormWrittenRep
                id
              }
            }
          }
          semanticRelationTypeLabel
          semanticRelationTypeScopeNote
          canUpdate @include(if: $includeCreator)
        }
      }
    }
  }
`;

/** 
 * filter semanticRelations to exclude distinctWrittenRep, 
 * meaning it return only relations between definitions of same form/writtenRep
 * @type {gql}
 **/
export const gqlLexicalSenseSemanticRelationTypeFragment = gql`
  fragment LexicalSenseSemanticRelationTypeFragment on LexicalSense{
    semanticRelations(excludeDistinctWrittenRep: true){
      edges{
        node{
          id
          lexicalSenses {
            edges {
              node {
                id
                canonicalFormWrittenRep
              }
            }
          }
          semanticRelationTypeLabel
          semanticRelationTypeScopeNote  
          canUpdate @include(if: $includeCreator) 
        }
      }
    }
  }
`;