import {Fragment} from 'react';

/**
 * Usage :
 *
 * <ParseNewlines text={"My text with newlines\nA second line"} />
 *
 * Will render :
 *
 * My text with newlines<br/>A second line
 */
export function ParseNewlines(props) {
  let items = props.text.split('\n');
  return items.map((item, key) => {
    let last = (key == items.length - 1);
    return (
      <Fragment key={key}>
        {item}
        <If condition={!last}>
          <br/>
        </If>
      </Fragment>
    );
  });
}
