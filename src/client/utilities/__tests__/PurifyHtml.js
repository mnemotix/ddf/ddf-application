import React from 'react';
import { cleanup, fireEvent } from '@testing-library/react';
import { PurifyHtml } from '../PurifyHtml';
import { renderWithMocks } from '../../../../jest/utilities/renderWithMocks';

afterEach(() => {
  cleanup();
});

describe('injections sanitization', () => {
  it('avoids <style> tags', async () => {
    const html = 'Some text <style>executeMalicious()</style>and next text.';
    const { findByTestId } = renderWithMocks({ element: <PurifyHtml html={html} /> });

    let containerElement = await findByTestId('purified-html-container');
    expect(containerElement.innerHTML).toEqual('Some text and next text.');
  });

  it('avoids <iframe>', async () => {
    const html = 'Some text <iframe src="https://myiframe.com"></iframe>and next text.';
    const { findByTestId } = renderWithMocks({ element: <PurifyHtml html={html} /> });

    let containerElement = await findByTestId('purified-html-container');
    expect(containerElement.innerHTML).toEqual('Some text and next text.');
  });

  it('avoids <h1> tag but keeps its content', async () => {
    const html = 'Some text <h1>Title</h1> and next text.';
    const { findByTestId } = renderWithMocks({ element: <PurifyHtml html={html} /> });

    let containerElement = await findByTestId('purified-html-container');
    expect(containerElement.innerHTML).toEqual('Some text Title and next text.');
  });

  it('avoids href attribute with javascript', async () => {
    const html = 'Some text <a href="javascript:executeMalicious()">link</a>and next text.';
    const { findByTestId } = renderWithMocks({ element: <PurifyHtml html={html} /> });

    let containerElement = await findByTestId('purified-html-container');
    expect(containerElement.innerHTML).toEqual('Some text <a>link</a>and next text.');
  });

  it('allows <i>, <b>, <em>, <strong> and <a> with valid href', async () => {
    const html = 'Some text <b>bold</b>, <i>italic</i>, <em>re italic</em>, <strong>re bold</strong>, <a href="/locallink">link</a>';
    const { findByTestId } = renderWithMocks({ element: <PurifyHtml html={html} /> });

    let containerElement = await findByTestId('purified-html-container');
    expect(containerElement.innerHTML).toEqual(
      'Some text <b>bold</b>, <i>italic</i>, <em>re italic</em>, <strong>re bold</strong>, <a href="/locallink">link</a>'
    );
  });

  describe('anchors', () => {
    it('prevents default event for clicks on local links and push target to history', async () => {
      const historyMock = { push: jest.fn() };
      const html = 'Please click <a href="/my/local/path/">my link</a>';
      const { findByText } = renderWithMocks({ element: <PurifyHtml html={html} history={historyMock} /> });

      let link = await findByText('my link');
      fireEvent.click(link);
      expect(historyMock.push.mock.calls[0][0]).toEqual('/my/local/path/');
    });

    it("doesn't prevent default events for click on external links", async () => {
      const historyMock = { push: jest.fn() };
      const html = 'Please click <a href="http://external.com">my link</a>';
      const { findByText } = renderWithMocks({ element: <PurifyHtml html={html} history={historyMock} /> });

      let link = await findByText('my link');
      fireEvent.click(link);
      expect(historyMock.push.mock.calls.length).toEqual(0);
    });

    it('adds target _blank to external links', async () => {
      const html = 'Please click <a href="http://external.com">my link</a>';
      const { findByTestId } = renderWithMocks({ element: <PurifyHtml html={html} /> });

      let containerElement = await findByTestId('purified-html-container');
      expect(containerElement.innerHTML).toEqual('Please click <a href="http://external.com" target="_blank" rel="noopener noreferrer">my link</a>');
    });
  });
});
