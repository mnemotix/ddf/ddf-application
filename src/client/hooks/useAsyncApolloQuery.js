
import {getDDFApolloClient} from '../services/DDFApolloClient';

export default async function useAsyncApolloQuery(query, variables, extractor = null) {
  const apolloClient = getDDFApolloClient();

  try {
    let {data} = await apolloClient.query({
      query, variables
    });
    if(extractor){
      return extractor(data);
    }
    return data;
  } catch (error) {
    console.log(error)
    return null;
  }

}