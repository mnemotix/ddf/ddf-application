/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {flatten} from 'lodash';
import upperFirst from 'lodash/upperFirst';
import {useQuery, gql} from "@apollo/client";

import {ShowWhereIAM} from '../widgets/ShowWhereIAM';
import PinkChip from "../widgets/PinkChip";
import {ROUTES} from '../../routes';

import {
  getGroupedLexicalSenseSemanticRelationList,
  gqlLexicalSenseSemanticRelationFragment
} from '../../utilities/helpers/lexicalSenseSemanticRelationList';

import lexicalSenseLoaderUseStyle from './LexicalSenseLoaderStyle.js';

import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  semanticRelationTitle: {
    color: Config.colors.pink,
    fontSize: Config.fontSizes.medium,
    fontWeight: Config.fontWeights.bold
  },

  semanticRelation: {
    "& div": {
      marginTop: Config.spacings.small,
      cursor: "pointer",
      fontStyle: "italic",
    }
  },

  section: {
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
    marginBottom: Config.spacings.medium,
    backgroundColor: Config.colors.white,

    "&.definitionSection": {
      [theme.breakpoints.up(Config.theme.breakpoint)]: {
        paddingTop: Config.spacings.medium,
      }
    }
  }
}));




export const LexicalSenseSemanticRelationListQuery = gql`
  query LexicalSenseSemanticRelationList_Query($lexicalSenseId: ID!,$excludeDistinctWrittenRep:Boolean, $filterOnDistinctWrittenRep: Boolean, $includeCreator: Boolean = false) {
    lexicalSense(id: $lexicalSenseId){
      id
      ...LexicalSenseSemanticRelationFragment 
    }
  }
  ${gqlLexicalSenseSemanticRelationFragment}
`;

const gqlLexicalSenseSemanticRelationListQueryWithGlossaries = gql`
  query LexicalSenseSemanticRelationList_Query($lexicalSenseId: ID!, $excludeDistinctWrittenRep:Boolean, $filterOnDistinctWrittenRep: Boolean, $includeCreator: Boolean = false, $includeGlossaries: Boolean = true) {
    lexicalSense(id: $lexicalSenseId){
      id
      ...LexicalSenseSemanticRelationFragment
      glossaries @include(if: $includeGlossaries){
        edges {
          node {
            id
            prefLabel             
          }          
        }
      }
    }
  }
  ${gqlLexicalSenseSemanticRelationFragment}
`;




/**
 * This component render all distinctWrittenRep semantic relation of a lexicalSense
 * so between a definition and a definition of another writtenRep
 *
 * @param {string} lexicalSenseId 
 
 * @param {function} [setConfirmToDelete]  : optional, if not null :enable delete icon next to each item (only if user is allowed to remove it)

 * @param {bool} [showGlossaries]
 * @param {function} [setDataInParent] : optional, if you need this data in the parent. use to prevent user to add multiple time same relation between definition
 */
export function LexicalSenseDistinctSemanticRelationList({lexicalSenseId, formQuery, showGlossaries = true, setConfirmToDelete, setDataInParent}) {
  const classes = useStyles();
  const lexicalSenseLoaderStyle = lexicalSenseLoaderUseStyle();
  const [groupedSemanticRelations, setGroupedSemanticRelations] = useState(false);

  const {data: {lexicalSense} = {}, loading} = useQuery(gqlLexicalSenseSemanticRelationListQueryWithGlossaries, {
    variables: {
      lexicalSenseId,
      filterOnDistinctWrittenRep: true,
      excludeDistinctWrittenRep: false,
      includeCreator: !!setConfirmToDelete,
      includeGlossaries: showGlossaries
    },
    fetchPolicy: 'cache-and-network'
  });


  useEffect(() => {
    if (lexicalSense) {

      let ngroupedSemanticRelations = getGroupedLexicalSenseSemanticRelationList(lexicalSense);
      // clean and format each sub array
      for (const i in ngroupedSemanticRelations) {
        ngroupedSemanticRelations[i] = formatSemanticRelations(ngroupedSemanticRelations[i], formQuery);
      }
      setGroupedSemanticRelations(ngroupedSemanticRelations)
      if (setDataInParent) {
        setDataInParent(ngroupedSemanticRelations);
      }
    }
    return () => {
      setGroupedSemanticRelations(false);
    };
  }, [lexicalSense]);


  if (loading) {
    return renderLoader();
  }
  if (!groupedSemanticRelations) {
    return null;
  }

  return <ShowWhereIAM path='src/client/components/LexicalSense/LexicalSenseDistinctSemanticRelationList.js'>
    {showGlossaries && renderGlossaries()}
    {Object.entries(groupedSemanticRelations).map(([semanticRelationLabel, semanticRelations], idx) =>
      <RenderSemanticRelationSection semanticRelationLabel={semanticRelationLabel} semanticRelations={semanticRelations} key={idx} />
    )}
  </ShowWhereIAM>

  function renderGlossaries() {
    const glossaries = lexicalSense?.glossaries?.edges?.map(({node}) => node);
    if (glossaries?.length > 0) {
      return (<ShowWhereIAM path={"in renderGlossaries"}>
        <div className={classes.section}>
          <h3 className={classes.semanticRelationTitle}>
            Lexiques
          </h3>
          {glossaries.map((glossary, index) => {
            return <Link
              className={classes.semanticRelation} key={"glossary" + index} role="link"
              aria-label={"Lancer une recherche sur " + glossary.prefLabel}
              to={formatRoute(ROUTES.EXPLORE_CONCEPT_FORM, {type: "glossary", id: glossary.id})}
            >
              <PinkChip label={glossary.prefLabel} />
            </Link>
          })}
        </div>
      </ShowWhereIAM>)
    } else {
      return null;
    }
  }

  /**
   * @param semanticRelationLabel {string}    The title of the group of semantic relations, for example "synonymes", to use as a title to the section
   * @param semanticRelations {Array<Object>}  The array of semantic relation objects (see the interface SemanticRelation in the graphQL API)
   */
  function RenderSemanticRelationSection({semanticRelationLabel, semanticRelations}) {

    if (!semanticRelations || semanticRelations.length < 1) {
      return null;
    }

    return (
      <ShowWhereIAM path={"in RenderSemanticRelationSection " + semanticRelationLabel}>
        <div className={classes.section}>
          <h3 className={classes.semanticRelationTitle}>
            {upperFirst(semanticRelationLabel)}
          </h3>
          {setConfirmToDelete ?
            semanticRelations.map((form, index) => {
              return <span className={classes.semanticRelation} key={index}>
                <PinkChip label={form.writtenRep}
                  isMine={form?.canUpdate === true}
                  onDelete={() => {setConfirmToDelete(form)}}
                />
              </span>
            })
            :
            semanticRelations.map((form, index) => {
              const to = form?.defId ? formatRoute(ROUTES.FORM_LEXICAL_SENSE, {formQuery: form.writtenRep, lexicalSenseId: form.defId}) :
                formatRoute(ROUTES.FORM_SEARCH, {formQuery: form.writtenRep})
              return <Link
                className={classes.semanticRelation} key={index} role="link"
                aria-label={"Lancer une recherche sur " + form.writtenRep}
                to={to}
              >
                <PinkChip label={form.writtenRep} />
              </Link>
            }
            )
          }
        </div>
      </ShowWhereIAM>
    );
  }

  function renderLoader() {
    return (
      <div className={lexicalSenseLoaderStyle.placeholder}>
        <div className={lexicalSenseLoaderStyle.fakeTextLine}>
          &nbsp;
          <div className={lexicalSenseLoaderStyle.textPlaceholder} />
        </div>

        <div className={lexicalSenseLoaderStyle.fakeTextLine}>
          &nbsp;
          <div className={lexicalSenseLoaderStyle.textPlaceholder} />
        </div>
      </div>
    );
  }
}


function formatSemanticRelations(semanticRelations, formQuery) {
  const foundRelatedLexicalSenses = [];
  const formatedSemanticRelations = flatten(
    semanticRelations.map(({id, gatheredLexicalSenses: lexicalSenses, canUpdate}) => {
      return lexicalSenses.edges.reduce((acc, {node: lexicalSense}) => {
        if (!foundRelatedLexicalSenses.includes(lexicalSense.id) && lexicalSense.canonicalFormWrittenRep && lexicalSense.canonicalFormWrittenRep !== formQuery) {
          acc.push({id, writtenRep: lexicalSense.canonicalFormWrittenRep, defId: lexicalSense.id, canUpdate});
          foundRelatedLexicalSenses.push(lexicalSense.id)
        }
        return acc;
      }, []);
    })
  );
  return formatedSemanticRelations.sort((a, b) => (a.writtenRep > b.writtenRep ? 1 : -1));
}