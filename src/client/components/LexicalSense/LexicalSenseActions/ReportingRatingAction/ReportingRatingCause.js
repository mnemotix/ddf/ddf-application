import {upperCaseFirstLetter} from "../../../../utilities/helpers/tools";

export const _reasonLists = [
  'MeaninglessContentReporting',
  'CopyrightInfringementReporting',
  'DiscriminationInducementReporting',
  'ViolenceThreatReporting',
  'ContributorBullyingReporting',
  'PrivacyInfringementReporting',
  'SlanderReporting',
  'AdvertisementReporting',
  'LayoutReporting',
];
export const _otherReasonLists = ['OtherContentReporting', 'OtherPersonReporting', 'OtherWebsiteReporting'];
export const _UncategorizedReporting = 'UncategorizedReporting';

// id is like 'meaningless-content-reporting', to be used in mutation need to be 'MeaninglessContentReporting'
export function convertIdToReason(id) {
  const arr = id.split("/")[0].split("-");

  for (let i = 0; i < arr.length; i++) {
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1).toLowerCase();
  }
  return arr.join("");
}

/*
export const _reasonLists = [
  { k: 'MeaninglessContentReporting', v: "Test de débutant, essai, contenu dénué de sens en l'état" },
  { k: 'CopyrightInfringementReporting', v: "Problème de respect du droit d'auteur ou de plagiat" },
  {
    k: 'DiscriminationInducementReporting',
    v:
      'Incitation à la haine ou à la discrimination de personne en raison de leurs origines, de leur sexe, de leur genre, de leur orientation sexuelle ou de leur handicap',
  },
  {
    k: 'ViolenceThreatReporting',
    v:
      "Menaces, apologie d'actes criminels ou incistations à la violence, incitation à commettre des actes criminels, incluant par exemple pédophilie, vol, meurtre, escroquerie, trafic illicite.",
  },
  { k: 'ContributorBullyingReporting', v: 'Harcèlement contre une personne participant au Dictionnaire des francophones' },
  {
    k: 'PrivacyInfringementReporting',
    v: "Divulgation d'informations personnelles ou propriétés d'un autre utilisateur, personne, ou organisation",
  },
  { k: 'SlanderReporting', v: 'Injure, dénigrement, diffamation, moquerie' },
  { k: 'AdvertisementReporting', v: 'Publipostage, message inutile ou publicitaire' },
  { k: 'LayoutReporting', v: "Problème de mise en forme ou de présentation de l'information pouvant être dû à l'importation des données" },
];
// autre utilise UncategorizedReporting


export const _otherReasonLists = [
  { k: 'OtherContentReporting', v: 'Problème lié au conteu' },
  { k: 'OtherPersonReporting', v: 'Problème lié aux personnes' },
  { k: 'OtherWebsiteReporting', v: 'Problème lié au fonctionnement du site' },
];
*/
