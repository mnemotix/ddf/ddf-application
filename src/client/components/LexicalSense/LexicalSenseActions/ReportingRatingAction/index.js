/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import {useMutation} from '@apollo/client';

import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Modal from '@material-ui/core/Modal';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import CloseIcon from '@material-ui/icons/Close';
import {makeStyles} from '@material-ui/core/styles';
import {useMediaQueries} from '../../../../layouts/MediaQueries';
import clsx from 'clsx';
import {ActionButton} from '../../../widgets/ActionButton';
import {PurpleRadio} from '../../../widgets/PurpleRadio';
import {PurpleButton} from '../../../widgets/PurpleButton';
import {Textarea} from '../../../widgets/forms/Textarea';
import {_reasonLists, _otherReasonLists, _UncategorizedReporting} from './ReportingRatingCause';
import {useSnackbar} from 'notistack';
import {gqlReportingRatingFragment, gqlAddReportingRatingMutation, gqlRemoveReportingRatingMutation} from './ReportingRating.gql';
import Config from "../../../../Config";


const _purpleColor = '#8200a0';
const useStyles = makeStyles((theme) => ({
  textarea200: {
    height: "200px",
    fontSize: "20px",
    border:`1px solid ${Config.colors.lightgray}`
  },
  center: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formControl: {
    margin: theme.spacing(1),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  content: {
    margin: 'auto',
    backgroundColor: 'white',
    width: 'min(65vw,1100px)',
    minHeight: '50vh',
    maxHeight: '85vh',
    boxShadow: 'none',
    overflowY: 'auto',
    // outline: 'none',
  },
  contentMobile: {
    margin: 'auto',
    backgroundColor: 'white',
    width: '100vw',
    height: '100vh',
    boxShadow: 'none',
    overflowY: 'auto',
    // outline: 'none',
  },
  topYellow: {
    backgroundColor: '#FFB400',
    //height: "50px"
    padding: '15px',
  },

  nopadding: {padding: '0px !important'},
  padding15: {padding: '15px'},
  padding25: {padding: '25px'},
  padding35: {padding: '35px'},
  paddingTB15: {paddingTop: '15px', paddingBottom: '15px'},
  rlabel: {
    paddingTop: '7px',
    paddingBottom: '7px',
    fontSize: '15px',
  },
  hoverPurple: {
    '&:hover': {
      backgroundColor: _purpleColor,
      color: 'white',
    },
  },
  textAlign: {
    textAlign: 'justify',
  },
  mainTitle: {
    fontSize: '25px',
  },
  subTitle: {
    fontSize: '20px',
    paddingBottom: '20px',
  },
  subTitleMob: {
    fontSize: '15px',
    paddingBottom: '20px',
  },

  purpleText: {
    color: _purpleColor,
  },
  allText: {
    textShadow: 'none',
    lineHeight: '35px',
    fontWeight: 'normal',
  },
}));


// disable for now, show other reason list
const enableOtherReasonList = false;

export function ReportingRatingAction({lexicalSense, actionButtonShowText, onActionError, notistackService} = {}) {
  const {enqueueSnackbar} = notistackService || useSnackbar();
  let {t} = useTranslation();
  const {isMobile} = useMediaQueries();
  const classes = useStyles();

  let [addReportingRating, {loading: addMutationLoading}] = useMutation(gqlAddReportingRatingMutation, {
    onError: onActionError,
    onCompleted(data) {
      if (data?.createReportingRating?.createdObject?.id !== '-1') {
        enqueueSnackbar(t('LEXICAL_SENSE_DETAIL.ACTIONS.REPORT_SUCCESS'), {variant: 'success'});
      } else {
        enqueueSnackbar(t('LEXICAL_SENSE_DETAIL.ACTIONS.REPORT_ERROR'), {variant: 'error'});
      }
    },
  });
  let [removeReportingRating, {loading: removeMutationLoading}] = useMutation(gqlRemoveReportingRatingMutation, {
    onError: onActionError,
  });

  // open or not the modal to make a reporting
  const [openModal, setOpenModal] = useState(false);
  // will contain the checked item
  const [reportingRatingType, setReportingRatingType] = useState(null);

  // will contain the selected 'other reason' from select list
  const [otherReasonValue, setOtherReasonValue] = useState('');
  const [uncategorizedReportingMessage, setUncategorizedReportingMessage] = useState('');

  function reset() {
    setReportingRatingType(null);
    setOpenModal(false);
    // setShowOtherReason(false);
    setOtherReasonValue('');
    setUncategorizedReportingMessage('');
  }

  const handleSelectChange = (event) => {
    setOtherReasonValue(event.target.value);
  };

  const handleRadioChange = (event) => {
    // on peut décocher une case
    if (event.target.value === reportingRatingType) {
      setReportingRatingType(null);
    } else {
      setReportingRatingType(event.target.value);
    }
    if (event.target.value === _UncategorizedReporting) {
      // force refresh,
      setOpenModal(false);
      setOpenModal(true);
    }
  };

  // check if form is valid or not
  function isFormValid() {
    let res;
    if (reportingRatingType === _UncategorizedReporting) {
      if (enableOtherReasonList) {
        res = otherReasonValue !== '' && otherReasonValue && uncategorizedReportingMessage.length > 5;
      } else {
        res = uncategorizedReportingMessage.length > 5;
      }
    } else {
      res = !!reportingRatingType;
    }
    return res;
  }
  // affiche le formulaire simple ou bien celui pour "autre" avec le textarea
  function renderContentForm() {
    const disabledbtn = !isFormValid();
    return (
      <FormControl component="fieldset">
        <RadioGroup id="form-title" name="reason" value={reportingRatingType}>
          {reportingRatingType !== _UncategorizedReporting &&
            _reasonLists.map((elem, index) => (
              <FormControlLabel
                className={classes.rlabel}
                key={index}
                value={elem}
                control={<PurpleRadio onClick={handleRadioChange} />}
                aria-label={t(`LEXICAL_SENSE_DETAIL.MODAL.REASONS.${elem}`)}
                label={t(`LEXICAL_SENSE_DETAIL.MODAL.REASONS.${elem}`)}
              />
            ))}
          <FormControlLabel
            className={classes.rlabel}
            key={_UncategorizedReporting}
            value={_UncategorizedReporting}
            control={<PurpleRadio onClick={handleRadioChange} />}
            aria-label={t(`LEXICAL_SENSE_DETAIL.MODAL.REASONS.UncategorizedReporting`)}
            label={t(`LEXICAL_SENSE_DETAIL.MODAL.REASONS.UncategorizedReporting`)}
          />
          {reportingRatingType == _UncategorizedReporting && (
            <div>
              {enableOtherReasonList && (
                <FormControl className={classes.formControl}>
                  <Select
                    className={classes.selectEmpty}
                    value={otherReasonValue}
                    displayEmpty
                    onChange={handleSelectChange}
                    inputProps={{'aria-label': 'autre raison'}}
                  >
                    <MenuItem value="" disabled>
                      <span className={classes.purpleText}>{t(`LEXICAL_SENSE_DETAIL.MODAL.WHATISPRB`)}</span>
                    </MenuItem>
                    {_otherReasonLists.map((elem, index) => (
                      <MenuItem key={index} value={elem} className={clsx(classes.rlabel, classes.hoverPurple)}>
                        {t(`LEXICAL_SENSE_DETAIL.MODAL.REASONS.${elem}`)}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              )}
              <br />
              <div className={clsx(classes.textAlign, isMobile ? classes.subTitleMob : classes.subTitle)}>
                {t(`LEXICAL_SENSE_DETAIL.MODAL.OTHERANONYMDETAILS`) + ' '}
                <a accessKey="9" role="link" aria-label="Nous contacter par email" href="mailto:contact&#64;dictionnairesdesfrancophones.org" className={classes.purpleText}>
                  contact&#64;dictionnairesdesfrancophones.org
                </a>
              </div>
              <br />

              <Textarea
                name="reason"
                rows="20"
                placeholder="Votre message"
                className={classes.textarea200}
                onChange={(event) => setUncategorizedReportingMessage(event.target.value)}
              />
              <br />
              <br />
            </div>
          )}

          <div className={classes.center}>
            <PurpleButton disabled={disabledbtn} onClick={handleAddReportingRating} aria-label={t(`LEXICAL_SENSE_DETAIL.MODAL.SEND`)}>
              {t(`LEXICAL_SENSE_DETAIL.MODAL.SEND`)}
            </PurpleButton>
          </div>
        </RadioGroup>
      </FormControl>
    );
  }

  return (
    <>
      <Choose>
        <When condition={lexicalSense.reportingRating?.id}>
          <ActionButton
            text={t('LEXICAL_SENSE_DETAIL.ACTIONS.UNREPORT')}
            iconName={"exclamation"}
            color={"orange"}
            onClick={handleRemoveReportingRating}
            loading={removeMutationLoading}
            unactive={false}
            showText={actionButtonShowText}
          />
        </When>
        <Otherwise>
          <ActionButton
            text={t('LEXICAL_SENSE_DETAIL.ACTIONS.REPORT')}
            iconName={"exclamation"}
            color={"orange"}
            //onClick={handleAddReportingRating}
            onClick={() => setOpenModal(true)}
            loading={addMutationLoading}
            unactive={true}
            showText={actionButtonShowText}
          />
        </Otherwise>
      </Choose>

      <Modal
        open={openModal}
        onClose={() => reset()}
        className={classes.center}
        role="alertdialog"
        aria-modal="true"
        aria-labelledby="Signaler cette définition"
        aria-describedby="Les causes sont définies dans la liste ci dessous"
      >
        <div className={clsx(classes.allText, isMobile ? classes.contentMobile : classes.content)}>
          <Grid container direction="row" justifyContent="space-between" alignItems="stretch" className={classes.topYellow}>
            <Grid item></Grid>
            <Grid item>
              <div id="modal-title" className={clsx(classes.center, classes.mainTitle)}>
                Signaler
              </div>
            </Grid>
            <Grid item>
              <IconButton onClick={() => reset()} className={classes.nopadding}>
                <CloseIcon />
              </IconButton>
            </Grid>
          </Grid>

          <div className={isMobile ? classes.padding25 : classes.padding35}>
            <div aria-labelledby="form-title" className={clsx(classes.purpleText, isMobile ? classes.subTitleMob : classes.subTitle)}>
              Quelle est la nature de l'abus que vous souhaitez signaler ?
            </div>
            {renderContentForm()}
          </div>
        </div>
      </Modal>
    </>
  );

  async function handleAddReportingRating() {
    if (!addMutationLoading && !removeMutationLoading) {
      let variables = {
        reportingRatingType,
        lexicalSenseId: lexicalSense.id,
      };
      if (reportingRatingType === _UncategorizedReporting) {
        variables['message'] = uncategorizedReportingMessage;
      }
      reset();
      await addReportingRating({
        variables,
        optimisticResponse: {
          __typename: 'Mutation',
          createReportingRating: {
            __typename: 'CreateReportingRatingPayload',
            createdObject: {
              __typename: 'ReportingRating',
              //fake ID while we wait for the actual value to return from server. Be sure that no action can be done on this ID before the actual value returns
              id: '-1',
            },
          },
        },
        update: (
          cache,
          {
            data: {
              createReportingRating: {createdObject},
            },
          }
        ) => {
          cache.writeFragment({
            id: `LexicalSense:${lexicalSense.id}`,
            fragment: gqlReportingRatingFragment,
            data: {
              reportingRatingsCount: lexicalSense.reportingRatingsCount + 1,
              reportingRating: createdObject,
              __typename: 'LexicalSense',
            },
          });
        },
      });
    }
  }

  async function handleRemoveReportingRating() {
    if (!addMutationLoading && !removeMutationLoading) {
      await removeReportingRating({
        variables: {reportingRatingId: lexicalSense.reportingRating.id},
        optimisticResponse: {
          __typename: 'Mutation',
          removeEntity: {
            __typename: 'RemoveEntityPayload',
            deletedId: lexicalSense.reportingRating.id,
          },
        },
        update: (cache) => {
          cache.writeFragment({
            id: `LexicalSense:${lexicalSense.id}`,
            fragment: gqlReportingRatingFragment,
            data: {
              reportingRatingsCount: lexicalSense.reportingRatingsCount - 1,
              reportingRating: null,
              __typename: 'LexicalSense',
            },
          });
        },
      });
    }
  }
}
