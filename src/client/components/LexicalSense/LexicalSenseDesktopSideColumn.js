import React from 'react';

import {useTranslation} from 'react-i18next';
import {useQuery, gql} from '@apollo/client';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {ParseNewlines} from "../../utilities/ParseNewlines";
import {apolloErrorHandler} from '../../utilities/apolloErrorHandler';
import {ROUTES} from '../../routes';
import {DesktopDiscussionPreview} from '../Form/DesktopDiscussionPreview';
import clsx from "clsx";
import {gqlLexicalSenseActionsFragment, LexicalSenseActions} from '../LexicalSense/LexicalSenseActions';
import {ShowWhereIAM} from '../../components/widgets/ShowWhereIAM';
import checkmarkGreen from '../../assets/images/signs/checkmark_green.svg';
import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";
import {useIsReadOnly} from "../../hooks/useIsReadOnly";

const _boxHeight = "60px";
const _arrowHeight = Config.spacings.small;
const _arrowBase = 1.15 * _arrowHeight;

const useStyles = makeStyles((theme) => ({
  fontSize: Config.fontSizes.xxlarge,
  color: Config.colors.purple,
  fontWeight: Config.fontWeights.semibold,
  marginBottom: Config.spacings.medium,

  likeCount: {
    position: "relative",
    padding: `${Config.spacings.small}px ${Config.spacings.medium}px`,
    backgroundColor: Config.colors.white,
    border: `1px solid ${Config.colors.mediumgray}`,
    marginBottom: Config.spacings.small,
    "&::after": {
      content: "''",
      display: "block",
      position: "absolute",
      top: ((_boxHeight - _arrowBase) / 2),
      left: -(_arrowHeight),
      width: 0,
      height: 0,
      borderRight: `${_arrowHeight} solid ${Config.colors.mediumgray}`,
      borderTop: `${(_arrowBase / 2)} solid transparent}`,
      borderBottom: `${(_arrowBase / 2)} solid transparent}`
    }
  },
  discussionsSection: {
    marginTop: Config.spacings.medium,
  },
  center: {
    height: _boxHeight,
    textAlign: "center",
    display: "flex",
    alignItems: "center"
  },
  textPurle: {
    color: Config.colors.purple
  },
  textGreen: {
    color: Config.colors.green
  },
  icon: {
    height: "25px",
    width: "25px",
    marginRight: Config.spacings.small,
  },

  source: {
    display: "block",
    fontSize: Config.fontSizes.small,
    color: Config.colors.purle,
    marginTop: Config.spacings.small,
    marginBottom: Config.spacings.medium
  },
  title: {
    fontWeight: Config.fontWeights.bold,
    textDecoration: "none",
    color: Config.colors.darkgray
  }
}));




const LexicalSenseQueryActions = gql`
  query LexicalSenseActions_Query($lexicalSenseId: ID!,$formQs: String) {
    lexicalSense(id: $lexicalSenseId) {
      id
      validationRatingsCount
      lexicographicResourceName
      ...LexicalSenseActionsFragment      
    }        
    topPostAboutEtymology: topPostAboutEtymologyForAccurateWrittenForm(formQs: $formQs){
      id
      content
    }
    topPostAboutForm: topPostAboutFormForAccurateWrittenForm(formQs: $formQs){
      id
      content
    }
  } 
  ${gqlLexicalSenseActionsFragment}
`;


/**
 * Usage :
 *
 * <LexicalSenseDesktopSideColumn
 *    lexicalSenseId={lexicalSenseId}
 *    editButton={<div>...</div>}
 *    formQuery="currentFormQuery"
 * />
 *
 */
export function LexicalSenseDesktopSideColumn(props) {
  const classes = useStyles();
  const {formQuery, lexicalSenseId, editButton, onRemoveSuccess} = props;
  const isReadOnly = useIsReadOnly();

  const {t} = useTranslation();
  const {data, loading, error} = useQuery(LexicalSenseQueryActions, {
    variables: {
      formQs: formQuery,
      lexicalSenseId
    },
  });

  if (loading || !data?.lexicalSense) {
    return null;
  }

  apolloErrorHandler(error, 'log');
  if ((loading && !data) || error) return null;


  const {topPostAboutForm, topPostAboutEtymology, lexicalSense} = data;
  const topPostAboutSense = lexicalSense?.topPostAboutSense;
  const likeCount = lexicalSense?.validationRatingsCount;

  return (
    <ShowWhereIAM path="components/LexicalSense/LexicalSenseDesktopSideColumn.js">
      <div>

        <ValidOrNotCount likeCount={likeCount} classname={classes.likeCount} />

        <LexicalSenseActions lexicalSense={lexicalSense} formQuery={formQuery} onRemoveSuccess={onRemoveSuccess} />

        <LexicalSenseSource
          formQuery={formQuery}
          source={lexicalSense?.lexicographicResourceName}
          id={lexicalSense.id}
        />

        <div className={classes.discussionsSection}>
          {renderTopPost({title: t('FORM.TOP_POSTS.DESKTOP.FORM_HEADER'), post: topPostAboutForm})}
          {renderTopPost({title: t('FORM.TOP_POSTS.DESKTOP.ETYMOLOGY_HEADER'), post: topPostAboutEtymology})}
          {renderTopPost({title: t('LEXICAL_SENSE_DETAIL.DESKTOP.TOP_POSTS.SENSE_HEADER'), post: topPostAboutSense})}
        </div>

        { !isReadOnly && editButton}

      </div>
    </ShowWhereIAM>
  );

  function renderTopPost({title, post}) {
    if (post) {
      return <DesktopDiscussionPreview title={title} content={post.content} commentCount={12} />;
    }
  }
}


export function ValidOrNotCount({likeCount, classname}) {
  const classes = useStyles();
  const countNotNull = likeCount > 0;
  const {t} = useTranslation();
  return <div id='ValidOrNotCount' className={clsx(classname, classes.center, countNotNull ? classes.textGreen : classes.textPurle)}>
    {countNotNull &&
      <img className={classes.icon} src={checkmarkGreen} alt="Action" />
    }
    <ParseNewlines
      text={
        countNotNull
          ? t('LEXICAL_SENSE_DETAIL.DESKTOP.LIKE_COUNT', {count: likeCount})
          : t('LEXICAL_SENSE_DETAIL.DESKTOP.LIKE_COUNT_zero')
      }
    />
  </div>
}

export function LexicalSenseSource({formQuery, id, source}) {
  const classes = useStyles();
  const {t} = useTranslation();

  return <If condition={source}>
    <Link role="link" aria-label="Détail sur la source"
      to={formatRoute(ROUTES.FORM_LEXICAL_SENSE_SOURCES, {formQuery, lexicalSenseId: id})}
      className={classes.source}
    >
      <span className={classes.title}>{t('LEXICAL_SENSE_DETAIL.SOURCE')}</span>
      <span>{source}</span>
    </Link>
  </If>
}