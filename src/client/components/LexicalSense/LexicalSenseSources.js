import React from 'react';
import PropTypes from 'prop-types';
import {useQuery, gql} from "@apollo/client";
import {formatRoute} from 'react-router-named-routes';
import {useTranslation} from 'react-i18next';
import {useHistory} from "react-router-dom";
import {makeStyles} from '@material-ui/core/styles';
import {ROUTES} from '../../routes';
import {PurifyHtml} from '../../utilities/PurifyHtml';
import {SimpleModalPage} from '../../layouts/mobile/SimpleModalPage';
import {ShowWhereIAM} from '../../components/widgets/ShowWhereIAM';
import Config from "../../Config";
import {LexicalSenseQueryActions} from "../../components/LexicalSense/LexicalSenseMobileActions"


/**
 * A lexicographic resource object as returned by the graphQL API (property of a LexicalEntry object). Must contain
 */

LexicalSenseSources.propTypes = {
  formQuery: PropTypes.string.isRequired,
  lexicalSenseId: PropTypes.string.isRequired
};


export const LexicalSenseQuery = gql`
  query LexicalSenseSources_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id      
      lexicographicResourceName
      lexicographicResourceShortName
      lexicographicResourceDefinition
    } 
  }
 
`;

export function LexicalSenseSources({formQuery, lexicalSenseId}) {
  const {t} = useTranslation();
  const classes = useStyles();
 

  const {data, loading} = useQuery(LexicalSenseQuery, {
    variables: {
      lexicalSenseId
    },
  });


  let formQueryURI = encodeURIComponent(formQuery);
  let wiktionaryLink = `https://fr.wiktionary.org/w/index.php?title=${formQueryURI}&action=history`;

  if (loading || !data?.lexicalSense) {
    return null;
  }

  const {lexicographicResourceName, lexicographicResourceDefinition} = data?.lexicalSense;

  return (
    <ShowWhereIAM path="src/client/components/LexicalSense/LexicalSenseSources">
      <div className={classes.section}>
        <p>{lexicographicResourceName}</p>
        <If condition={lexicographicResourceName.toLowerCase() === 'wiktionnaire francophone'}>
          <br />
          <p data-testid="wiktionary-link">
            <a role="link" aria-label="lien vers la page source du wiktionnaire" href={wiktionaryLink} target="_blank">
              {t('SOURCES_PAGE.WIKTIONARY_LINK', {formQuery})}
            </a>
          </p>
        </If>
        <If condition={lexicographicResourceDefinition}>
          <div className={classes.definition}>
            <PurifyHtml as="div" html={lexicographicResourceDefinition} />
          </div>
        </If>
      </div>
    </ShowWhereIAM >
  );
}

/**
 * A lexicographic resource object as returned by the graphQL API (property of a LexicalEntry object). Must contain
 */
LexicalSenseSourcesMobilePage.propTypes = {
  lexicalSenseId: PropTypes.string.isRequired,
  formQuery: PropTypes.string.isRequired,
};
export function LexicalSenseSourcesMobilePage({lexicalSenseId, formQuery}) {
  const {t} = useTranslation();
  let history = useHistory();

  function closeButtonHandler() {
    let backPath = formatRoute(ROUTES.FORM_LEXICAL_SENSE, {lexicalSenseId, formQuery});
    history.replace({
      pathname: backPath,
    });
  }

  return (
    <SimpleModalPage
      title={t('SOURCES_PAGE.TITLE')}
      content={<LexicalSenseSources formQuery={formQuery} lexicalSenseId={lexicalSenseId} />}
      controls={['close']}
      onClose={() => closeButtonHandler()}
    />
  );
}

const useStyles = makeStyles((theme) => ({
  definition: {
    marginBottom: Config.spacings.large,
    marginTop: Config.spacings.large
  },
  section: {
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
    marginBottom: Config.spacings.medium,
    backgroundColor: Config.colors.white,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      paddingTop: Config.spacings.medium
    }
  }
}));