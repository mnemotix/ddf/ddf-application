/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useContext} from 'react';
import {useTranslation} from 'react-i18next';
import {gql} from "@apollo/client";
import {GraphQLErrorHandler} from '../../services/GraphQLErrorHandler';
import {notificationService as appNotificationService} from '../../services/NotificationService';
import {gqlValidationRatingFragment, ValidationRatingAction} from './LexicalSenseActions/ValidationRatingAction';
import {ReportingRatingAction} from './LexicalSenseActions/ReportingRatingAction/index';
import {gqlReportingRatingFragment} from './LexicalSenseActions/ReportingRatingAction/ReportingRating.gql';
import {gqlSuppressionRatingFragment, SuppressionRatingAction} from './LexicalSenseActions/SuppressionRatingAction';
import {RemoveAction} from './LexicalSenseActions/RemoveAction';
import {UserContext} from "../../hooks/UserContext";
import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";
import {useIsReadOnly} from "../../hooks/useIsReadOnly";

const useStyles = makeStyles((theme) => ({
  actions: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap",
    marginBottom: Config.spacings.small,
    justifyContent: "flex-start"
  }
}));




export let gqlLexicalSenseActionsFragment = gql`
  fragment LexicalSenseActionsFragment on LexicalSense{
    validationRatingsCount
    suppressionRatingsCount
    ...LexicalSenseValidationRatingFragment
    ...LexicalSenseReportingRatingFragment
    ...LexicalSenseSuppressionRatingFragment
  }
  ${gqlValidationRatingFragment}
  ${gqlReportingRatingFragment}
  ${gqlSuppressionRatingFragment}
`;

export function LexicalSenseActions(props) {
  const classes = useStyles();
  const {lexicalSense, onRemoveSuccess} = props;
  let {t} = useTranslation();
  let notificationService = props.notificationService || appNotificationService;
  const {user} = useContext(UserContext);
  const isReadOnly = useIsReadOnly();

  if(isReadOnly){
    return null;
  }

  return (
    <div className={classes.actions}>
      <If condition={user?.isLogged}>
        <ValidationRatingAction
          lexicalSense={lexicalSense}
          onActionError={onActionError}
          actionButtonShowText={true}
        />

        <ReportingRatingAction
          lexicalSense={lexicalSense}
          onActionError={onActionError}
          notistackService={props?.notistackService}
          actionButtonShowText={!(user?.userAccount?.isOperator || user?.userAccount?.isAdmin)}
        />
      </If>

      <If condition={user?.userAccount?.isOperator && !user?.userAccount?.isAdmin}>
        <SuppressionRatingAction
          lexicalSense={lexicalSense}
          onActionError={onActionError}
          actionButtonShowText={!user?.userAccount?.isAdmin}
        />
      </If>

      <If condition={user?.userAccount?.isAdmin}>
        <RemoveAction
          lexicalSense={lexicalSense}
          onActionError={onActionError}
          onRemoveSuccess={onRemoveSuccess}
          actionButtonShowText={false}
        />
      </If>
    </div>
  );

  async function onActionError(error) {
    let {globalErrorMessage} = GraphQLErrorHandler(error, {t});
    await notificationService.error(globalErrorMessage);
  }
}
