import React from 'react';

import {makeStyles} from '@material-ui/core/styles';
import {useQuery, gql} from '@apollo/client';
import globalStyles from '../../assets/stylesheets/globalStyles';
import clsx from "clsx";
import Config from "../../Config";
import {useTranslation} from 'react-i18next';
import {PurifyHtml} from "../../utilities/PurifyHtml";


const useStyles = makeStyles((theme) => ({
  mobileBottomSpace: {
    [theme.breakpoints.down(Config.mediaQueries.mobileMaxWidth)]: {
      marginBottom: "50px"
    }
  }
}));

const LexicalSenseTopPostAbout = gql`
  query LexicalSenseTopPostAbout_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      topPostAboutSense{
        id
        content
      }  
    }
  }
`;


export function LexicalSenseTopPostAboutSense({lexicalSenseId}) {

  const classes = useStyles();
  const gS = globalStyles();
  const {t} = useTranslation();

  const {data, loading} = useQuery(LexicalSenseTopPostAbout, {
    variables: {
      lexicalSenseId
    },
  });

  const topPostAboutSenseContent = data?.lexicalSense?.topPostAboutSense?.content;

  if (loading || !topPostAboutSenseContent) {
    return null;
  }

  return (
    <div className={clsx(gS.topPostAboutSense, classes.mobileBottomSpace)} data-testid="about-sense">
      <h5 className={gS.topPostTitle}>{t('LEXICAL_SENSE_DETAIL.ABOUT_SENSE_TITLE')}&nbsp;:&nbsp;</h5>
      <PurifyHtml as="div" html={topPostAboutSenseContent} />
    </div>
  )
}

