
import React from 'react';
import {makeStyles} from '@material-ui/core/styles';

import interrogationIcon from '../../assets/images/signs/interrogation_gray.svg';
import checkmark_green from '../../assets/images/signs/checkmark_green.svg';
import checkmarkPurpleIcon from '../../assets/images/signs/checkmark_purple.svg';
import exclamationPinkIcon from '../../assets/images/signs/exclamation_pink.svg';


const useStyles = makeStyles((theme) => ({

  status: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },

  icon: {
    height: "20px"
  }
}));


/**
 * Determine the status of an entry :
 *
 * - 'notReviewed' :
 *    No operator nor admin has rated this entry (be it a validation, asking for suppression, or setting it as processed).
 *    `lexicalSense.reviewed === false`
 * - 'validated' :
 *    The entry is not processed, it was rated by at least one operator or admin, and has no resquest for suppression
 *   `lexicalSense.processed === false && lexicalSense.reviewed === true && lexicalSense.suppressionRatingsCount === 0`
 * - 'askedForSuppression' :
 *    The entry is not processed, it was rated by at least one operator or admin, and it has at least one request for suppression
 *    `lexicalSense.processed === false && lexicalSense.reviewed === true && lexicalSense.suppressionRatingsCount === 0`
 * - 'processed' :
 *    The entry is marked as processed
 */
export default function LexicalSenseStatus({lexicalSense}) {

  const classes = useStyles();

  let status;
  if (lexicalSense?.reviewerGroupsCount === 0) {
    status = 'notReviewed';
  } else if (lexicalSense?.processed) {
    status = 'processed';
  } else if (lexicalSense?.suppressionRatingsCount > 0) {
    status = 'askedForSuppression';
  } else {
    status = 'validated';
  }

  let icons = {
    notReviewed: interrogationIcon,
    validated: checkmarkPurpleIcon,
    askedForSuppression: exclamationPinkIcon,
    processed: checkmark_green,
  };

  let tooltips = {
    notReviewed: 'Non relu',
    validated: 'Relu',
    askedForSuppression: 'Au moins un opérateur a fait une demande de suppression',
    processed: 'Traité',
  };

  return (
    <div className={classes.status} title={tooltips[status]}>
      <img className={classes.icon} src={icons[status]} alt={tooltips[status]} />
    </div>
  );
}