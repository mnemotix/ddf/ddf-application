
import {gql} from "@apollo/client";
import {gqlLexicalSenseCountriesFragment} from '../../utilities/helpers/countries';

const gqlLexicalSenseFragment = gql`
  fragment LexicalSenseFragment on LexicalSense {
    definition
    canonicalFormWrittenRep
    usageExamples {
      edges {
        node {
          id
          bibliographicalCitation
          value
        }
      }
    }    
    ...LexicalSenseCountriesFragment
    lexicalEntry {
      id
      __typename
      ... on WordInterface {
        partOfSpeech {
          id
          prefLabel
        }
      }
      ... on InflectablePOS {
        number {
          id
          prefLabel
        }
        gender {
          id
          prefLabel
        }
      }
      ... on Verb {
        partOfSpeech {
          id
          prefLabel
        }
        transitivity {
          id
          prefLabel
        }
      }
      ... on VerbalInflection {
        partOfSpeech {
          id
          prefLabel
        }
        transitivity {
          id
          prefLabel
        }
        mood {
          id
          prefLabel
        }
        tense {
          id
          prefLabel
        }
        person {
          id
          prefLabel
        }
      }
      ... on MultiWordExpression {
        multiWordType {
          id
          prefLabel
        }
        number {
          id
          prefLabel
        }
        gender {
          id
          prefLabel
        }
      }
      ... on Affix {
        termElement {
          id
          prefLabel
        }
      }
    }
  }
  ${gqlLexicalSenseCountriesFragment}
`

export const gqlLexicalSenseQuery = gql`
  query LexicalSenseDefinition_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      lexicalEntryGrammaticalPropertyLabels
      ...LexicalSenseFragment
    }
  }
  ${gqlLexicalSenseFragment}
`;