import React from 'react';
import {MUIAutocomplete} from "./forms/MUIAutocomplete";
import {gql} from "@apollo/client";

import {unique} from "../../utilities/helpers/tools";

const gqlFormSuggestionsQuery = gql`
  query Suggestion_Query($first: Int, $qs: String) {   
    forms:formsStartWith(startWith: $qs, first: $first)
  }
`;


export function FormSearchInputAutocomplete(props) {

  return (
    <MUIAutocomplete
      id="form-autocomplete"
      {...props}
      addForm={props?.addForm}
      inputAriaLabel="Saisir le terme à rechercher"
      inputAccessKey="4"
      gql={gqlFormSuggestionsQuery}
      dataExtractor={(data) => {
        const forms = JSON.parse(data.forms);
        const r = unique(forms?.edges?.map(({node: {writtenRep, id}}) => {
          if(Array.isArray(writtenRep)){
            writtenRep = writtenRep[0];
          }
          return ({label: writtenRep, id})
        }), 'label')
          .sort((a, b) => (a.label > b.label ? 1 : -1));
        return r;
      }}
    />
  );
}
