import React, {useState} from 'react';
import classNames from 'classnames';
import clsx from "clsx";
import PropTypes from 'prop-types';

import rightArrow from '../../assets/images/right_arrow_pink.svg';
import bottomArrow from '../../assets/images/bottom_arrow_white.svg';


import Config from '../../Config';

import {makeStyles} from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  content: {
    backgroundColor: Config.colors.white,
    paddingTop: Config.spacings.tiny,
    paddingBottom: Config.spacings.tiny,
    paddingRight: Config.spacings.small,
    paddingLeft: Config.spacings.small
  },

  titleBar: {
    "display": "flex",
    "justifyContent": "space-between",
    "alignItems": "center",
    "paddingTop": Config.spacings.tiny,
    "paddingBottom": Config.spacings.tiny,
    "paddingRight": Config.spacings.small,
    "cursor": "pointer",
    "userSelect": "none",
  },

  titleBarOpen: {
    backgroundColor: Config.colors.pink,
    color: Config.colors.white,
    paddingLeft: Config.spacings.small,
  },
  rightArrow: {
    height: "12px",
  },
  displayNone: {
    display: "none"
  },

  titleBarClosed: {
    color: Config.colors.pink,
  },
  bottomArrow: {
    height: "8px"
  },

}));


FoldableContainer.propTypes = {
  /**
   * Render prop, render content to display inside the foldable container. The function takes an option objects as argument with the
   * following propertise :
   * - close: function, call this function to close the foldable container
   * - open: function, call this function to open the foldable container
   */
  children: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,

};

export function FoldableContainer({title, children}) {
  const classes = useStyles();

  const [isOpen, setIsOpen] = useState(false);

  return (
    <div >
      <div role="button"
        aria-label={isOpen ? "Fermer le panneau" : "Ouvrir le panneau"}
        className={clsx(classes.titleBar, isOpen ? classes.titleBarOpen : classes.titleBarClosed)}
        onClick={() => toggle()}
      >
        <div >{title}</div>
        <img className={clsx(classes.rightArrow, isOpen && classes.displayNone)} src={rightArrow} alt="Ouvrir" />
        <img className={clsx(classes.bottomArrow, !isOpen && classes.displayNone)} src={bottomArrow} alt="Fermer" />
      </div>
      <If condition={isOpen}>
        <div className={classes.content}>{children({close, open})}</div>
      </If>
    </div>
  );

  function toggle() {
    setIsOpen(!isOpen);
  }

  function close() {
    setIsOpen(false);
  }

  function open() {
    setIsOpen(true);
  }
}
