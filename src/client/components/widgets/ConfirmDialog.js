import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';
import {BracketButton} from './BracketButton';

import Config from '../../Config';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import {makeStyles} from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  spaceBetween: {
    display: "flex",
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'space-evenly'
  },
  p30: {
    padding: '30px'
  },
  colorPurple: {
    color: Config.colors.darkpurple
  },
  buttonText: {
    fontSize: '0.875rem'
  }
}));


function ConfirmDialog({isOpen, onClose, onConfirm,confirmText, children , loading}) {
  const classes = useStyles();

  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      aria-label="alert-dialog-title"

    >
      <div className={classes.p30}>
        <DialogContent>
          <DialogContentText>
            {children}
          </DialogContentText>
        </DialogContent>
        <div className={classes.spaceBetween}>
          <Button onClick={onClose} className={clsx(classes.colorPurple, classes.buttonText)}>
            Annuler
          </Button>
          <BracketButton
            greyBg={false}
            classNameText={classes.buttonText}
            text={confirmText}
            onClick={onConfirm}
            loading={loading}
          />
        </div>
      </div>
    </Dialog>
  );
};


ConfirmDialog.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  confirmText: PropTypes.string
};


export default ConfirmDialog;
