import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import backArrowPurple from '../../assets/images/back_arrow_purple.svg';

import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import Config from '../../Config';
import globalStyles from '../../assets/stylesheets/globalStyles.js';

BackArrowLink.propTypes = {
  /** The link text */
  text: PropTypes.string,
  /** The link target */
  to: PropTypes.string,
};

const useStyles = makeStyles((theme) => ({
  backLink: {
    display: "flex",
    alignItems: "center",
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    backgroundColor: Config.colors.white,
    color: Config.colors.purple,
    textDecoration: "inherit",
    fontSize: Config.fontSizes.small,
    fontFamily: "Raleway",
    fontWeight: Config.fontWeights.medium,

    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      backgroundColor: 'transparent',
      paddingBottom: '10px'
    }
  },
  img: {
    height: '8px'
  },
  span: {
    marginLeft: Config.spacings.tiny,
    whiteSpace: 'nowrap'
  }
}));

const defaultText = "Revenir à l’étape précédente";

// <BackArrowLink text={backLinkText} to={backLinkTo} />
export function BackArrowLink({text=defaultText, to}) {
  const classes = useStyles();
  const gS = globalStyles();

  return (
    <Link role="link" aria-label={defaultText} className={clsx(classes.backLink, gS.clickable)} to={to}>
      <img className={classes.img} src={backArrowPurple} alt="Retour" />
      <span className={classes.span} >{text}</span>
    </Link>
  );
};

