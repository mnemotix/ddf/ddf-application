/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import clsx from 'clsx';
import gif from '../../assets/images/ddf.gif'
import {makeStyles} from "@material-ui/core/styles";



const useStyles = makeStyles((theme) => ({
  loadingContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  img: {
    height: "35px"
  },
  imgSmall: {
    height: "20px"
  }
}));

// show a button with loading gif when action in progress
export default function LoadingGif({small = false, className, addContainer = true}) {
  const classes = useStyles();


  return (
    <div className={clsx(addContainer && classes.loadingContainer)}>
      <img src={gif} alt="Chargement..." className={clsx(classes.img, small && classes.imgSmall, className)} />
    </div>
  );
}
