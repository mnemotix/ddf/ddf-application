import React from 'react';

import Tooltip from '@material-ui/core/Tooltip';

const colors = [
  '#EFC58C', '#008000', '#800080', '#F0F8FF', "#FF7F50", "#B22222", "#FF69B4", "#FFFACD",

  '#f44336', '#3f51b5', '#009688', '#00bfa5', '#ff9800', '#212121', '#607d8b',
  '#dd2c00', '#00c853', '#01579b', '#e91e63'];
function randMax(max) {
  return Math.floor(Math.random() * (max + 1));
}


/**
 * composant uniquement utilisé pendant le debug pour savoir quel composant est affiché 
 * @param {*} param0 
 * @returns 
 */
export function ShowWhereIAM({path, children, ...props}) {
  const enable = false;
  if (enable) {
    const _data = path.split('/');
    const title = _data[_data.length - 1];
    const color = colors[randMax(colors.length - 1)];

    return (
      <div
        id="disable_ShowWhereIAM_for_test_or_snapshots_fail"
        style={{
          borderStyle: 'dotted',
          borderWidth: 2,
          borderRadius: 5,
          borderColor: color,
          backgroundColor: color + '19', // add 10% opacity
          margin: 2,
          padding: 2,
          width: '100%',
        }}
        {...props}
      >
        <Tooltip title={path} style={{fontStyle: 'italic', color}}>
          <div>{title}</div>
        </Tooltip>

        {children}
      </div>
    );
  } else {
    return children;
  }
}
