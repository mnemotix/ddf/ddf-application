
import React from 'react';
import clsx from 'clsx';
import Tooltip from '@material-ui/core/Tooltip';
import {checkIfEnterOrSpace} from "../../utilities/helpers/tools";
import globalStyles from '../../assets/stylesheets/globalStyles.js';


/**
 * create a button with OnClick and gS.outlineFocus, gS.clickable classnames
 * @param {bool} disabled : disable onClick
 * @param {string} tooltipTitle : if passed, use tooltip 
 * 
 * @returns 
 */
export default function DivButton({children, classesName, onClick, tooltipTitle = null, disabled = false, selected = false, ...props}) {
  const gS = globalStyles();
 
  const content = <div
    onClick={() => {!disabled && onClick()}}
    onKeyDown={event => {
      if (checkIfEnterOrSpace(event)) {
        onClick()
      }
    }}
    aria-disabled={disabled}
    role="button" tabIndex={0}
    className={clsx(!disabled && gS.outlineFocus, !disabled && gS.clickable, selected && gS.selectedDiv, classesName)}
    {...props}
  >
  {children}
  </div >;



  if (tooltipTitle) {
    return <Tooltip title={tooltipTitle}>
      {content}
    </Tooltip>
  } else {
    return content
  }

}


