import React from 'react';
import {useRouteMatch} from 'react-router-dom';
import {ROUTES} from '../../routes';
import {makeStyles} from '@material-ui/core/styles';
import {ParseNewlines} from "../../utilities/ParseNewlines";
import {useTranslation} from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  center: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontFamily: "Raleway",
    fontSize: "20px",
    [theme.breakpoints.down('sm')]: {
      fontSize: "15px"
    },
  },
  separatorTop: {
    backgroundColor: '#FFB400',
    width: '20px',
    height: '3px',
    marginTop: '15px',
    marginBottom: '25px',
  },
  separatorBottom: {
    backgroundColor: '#FFB400',
    width: '20px',
    height: '3px',
    marginTop: '25px',
    marginBottom: '15px',
  },
}));

export function FooterExplanationText() {
  const classes = useStyles();
  const {t} = useTranslation();

  if (displayFooterExplanationText()) {
    return (
      <div className={classes.center}>
        <div className={classes.separatorTop} />

        <ParseNewlines text={t('INDEX.FOOTER')} />

        <div className={classes.separatorBottom} />
      </div>
    );
  } else {
    return null;
  }
}


/**
 * use indexPageContent only in some routes, like index 
 */
export function displayFooterExplanationText() {
  const matched = useRouteMatch(ROUTES.INDEX);
  return (matched && matched.isExact)
}
