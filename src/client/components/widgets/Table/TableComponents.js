/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import Config from '../../../Config';
import {checkIfEnterOrSpace} from "../../../utilities/helpers/tools";
import globalStyles from '../../../assets/stylesheets/globalStyles.js';

const useStyles = makeStyles((theme) => ({
  table: {
    width: "100%",
    fontSize: Config.fontSizes.small,
    background: Config.colors.white,
    textAlign: "left",
    "& tbody tr:nth-child(odd)": {
      background: Config.colors.white
    },

    "& tbody tr:nth-child(even)": {
      background: Config.colors.verylightgray
    }
  },

  col: {
    height: "65px",
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
    paddingLeft: Config.spacings.tiny
  },
  heading: {
    backgroundColor: "transparent",
    "& .col": {
      backgroundColor: Config.colors.white,
      color: Config.colors.verydarkgray,
      borderSpacing: "10px",
    }
  }
}));

export function Table(props) {
  const classes = useStyles();

  return (
    <table className={clsx(classes.table, props.className)} {...props}>
      <tbody className='tbody'>
        {props.children}
      </tbody>
    </table>
  )
}

export function Row(props) {
  const classes = useStyles();
  const gS = globalStyles();
  return (
    <tr key={props.idkey} className={clsx(classes.row, props.className, props.heading && classes.heading, props.heading && gS.outlineFocus)}>
      {props.children}
    </tr >
  );
}

export function Col(props) {
  const classes = useStyles();
  const {idkey, className, children, ...otherProps} = props;
  return (
    // need "col" as className to match "heading &col" css 
    <td className={clsx(classes.col, "col", className)} key={idkey} {...otherProps}>
      {children}
    </td>
  );
}

export function ColHeader(props) {
  const classes = useStyles();
  const {className, idkey, children, ...otherProps} = props;
  return (
    <th key={idkey} className={clsx(classes.col, className)}
      onKeyDown={event => {if (props?.onClick && checkIfEnterOrSpace(event)) {props.onClick();} }}
      {...otherProps}
    >
      {children}
    </th>
  );
}

