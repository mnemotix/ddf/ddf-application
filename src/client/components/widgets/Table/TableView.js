import React, {forwardRef, useImperativeHandle, useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {useLazyQuery} from '@apollo/client';
import clsx from 'clsx';
import {makeStyles} from "@material-ui/core/styles";
import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@material-ui/core/Grid';
import get from "lodash/get";
import {useTranslation} from 'react-i18next';
import Pagination from '@material-ui/lab/Pagination';
import Checkbox from '@material-ui/core/Checkbox';
import globalStyles from '../../../assets/stylesheets/globalStyles.js';
import {Col, ColHeader, Row, Table} from './TableComponents';
import Config from '../../../Config.js';
import LoadingGif from "../LoadingGif";


const useStyles = makeStyles((theme) => ({
  sortIcon: {
    cursor: "pointer",
    marginLeft: Config.spacings.tiny,
  },
  noResults: {
    textAlign: "center",
    color: Config.colors.darkgray
  },
  center: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  marginTop: {"marginTop": "20px"}
}));


/**
 * @param ref = to reload TableView from parent on event change, use ref.current.reload(); in parent
 * @param gqlQuery   gql query to request
 * @param {string} gqlCountPath    path to count
 * @param {string} gqlConnectionPath  path to nodes
 * @param {array} columns  list of columns to show with field, sort , render
 * @param {bool} showCheckbox show checkbox to select or delete table item
 * @param {string} ariaDescribedby
 * @param {int} pageSize 
 * @param {int} colIndexForLoader: when component load nodes, we display should display a gif or text in the middle, 
 *                              as column width and number vary for each table we cannot just
 *                              display it in the middle column. so we use this index instead
 * @param {function} onSelectedIDsUpdate callback to update selected Id in table in parent component
 * @param {array} filters 
 */
export const TableView = forwardRef(({
  gqlQuery, gqlCountPath, gqlConnectionPath,
  columns, showCheckbox = false,
  ariaDescribedby,
  pageSize = 10,
  onSelectedIDsUpdate = () => { },
  filters = {},
  colIndexForLoader = -1,
  pollInterval,
  defaultSortBy = 'createdAt'
}, ref
) => {

  const {t} = useTranslation();
  const classes = useStyles();
  const gS = globalStyles();

  const [selectAllChecked, setSelectAllChecked] = useState(false);
  const [selectedIDs, setSelectedIDs] = useState([]);

  const [sortBy, setSortBy] = useState(defaultSortBy);
  const [isSortDescending, setIsSortDescending] = useState(true);

  const [currentPage, setCurrentPage] = useState(1);

  const [fetchData, {data, loading}] = useLazyQuery(gqlQuery, {
    variables: getQueryVariables(),
    fetchPolicy: 'cache-and-network',
    pollInterval
  });

  const count = get(data, gqlCountPath, get(data, `${gqlConnectionPath}.totalCount`));
  const pageCount = Math.ceil(count / pageSize);
  const nodes = get(data, `${gqlConnectionPath}.edges`, []).map(({node}) => node)

  useEffect(() => {
    resetSelectedRowsIndexes();
  }, [data]);

  useEffect(() => {
    onSelectedIDsUpdate(selectedIDs);
  }, [selectedIDs])

  useEffect(() => {
    setCurrentPage(1);
  }, [sortBy, isSortDescending, filters]);

  useEffect(() => {
    fetchData();
  }, [currentPage]);

  // for calling resetInput from parent
  useImperativeHandle(ref, () => ({
    reload() {
      fetchData();
    }
  }));

  // pour éviter le changement de taille des colonnes on fix leur tailles depuis la création de columns dans le composant parent
  let columnsWidth = columns.reduce(function (accumulateur, valeurCourante) {
    return accumulateur + (valeurCourante?.width || 2);
  }, showCheckbox ? 1 : 0);

  return (
    <div>
      <Table aria-describedby={ariaDescribedby}>
        {renderTableHeaders()}
        {renderTableContent()}
      </Table>
      {renderPagination()}
    </div>
  );


  function renderTableHeaders() {
    return (
      <Row heading key={"heading"}>
        {showCheckbox && <ColHeader key={"checkbox"}>
          <Checkbox
            checked={selectAllChecked}
            onChange={handleSelectAll}
            inputProps={{"aria-label": 'Effectuer une action sur tout ces élèments'}}
            style={{width: computeWidthPercent(1)}}
          />
        </ColHeader>}
        {columns.map(({label, width = 2, ariaLabel, title, icon, sortFor, tooltip}, index) => {
          const idkey = "colheader" + index;
          const widthStyle = {width: computeWidthPercent(width)}

          if (sortFor) {
            function tmp() {
              return <div className={gS.flexStart}>
                {label || icon}
                <span className={classes.sortIcon}>
                  {sortBy === sortFor && isSortDescending ? "▼" : "▲"}
                </span>
              </div>
            }
            return <ColHeader
              tabIndex={0}
              aria-label={ariaLabel}
              title={title}
              className={clsx(gS.clickable, gS.outlineFocus)}
              onClick={() => setSortingFor(sortFor)}
              key={idkey}
              style={widthStyle}
            >
              {(tooltip) ? (<Tooltip title={tooltip}>{tmp()}</Tooltip>) : tmp()}

            </ColHeader>
          } else {
            return <Col key={idkey} style={widthStyle}>{label}</Col>
          }
        })}
      </Row>
    );
  }

  function renderEmptyTable(message) {
    function renderLine(index, insertLoader = false) {
      return <Row key={index.toString()} even={index % 2 === 1}>
        {showCheckbox && <Col key={"checkbox"} className={classes.noResults} style={{width: computeWidthPercent(1)}}>
        </Col>}
        {columns.map(({label, width = 2, ariaLabel, title, icon, sortFor, tooltip}, indexC) => {
          const idkey = "col" + indexC;
          let show = false;

          if (insertLoader && colIndexForLoader === indexC) {
            show = message || <LoadingGif />
          }
          return <Col key={idkey} className={classes.noResults} style={{width: computeWidthPercent(width)}}>
            {show}
          </Col>
        })}
      </Row>
    }

    let rows = [];
    for (let index = 0; index < pageSize; index++) {
      rows.push(renderLine(index, inMiddle(index, pageSize - 1)));
    }
    return rows;
  }

  function inMiddle(i, size) {
    return i === Math.floor(size / 2)
  }

  function computeWidthPercent(width) {
    return (width * 100 / columnsWidth) + '%'
  }

  function renderTableContent() {
    if (loading) {
      return renderEmptyTable(false);
    }
    if (!nodes && !loading) {
      return renderEmptyTable(t('CONTRIBUTIONS_LIST.TABLE.ERROR_RESULTS'));
    }
    if (!loading && !nodes?.length) {
      return renderEmptyTable(t('CONTRIBUTIONS_LIST.TABLE.NO_RESULTS'));
    }

    return nodes.map((node, dindex) => {
      const isItemSelected = selectedIDs.includes(node?.id);

      return (
        <Row key={node?.id || dindex} even={dindex % 2 === 1}>
          {showCheckbox &&
            <Col>
              <Checkbox
                checked={isItemSelected}
                onChange={() => handleSelectRow(node?.id)}
                inputProps={{"aria-labelledby": dindex}}
                style={{width: computeWidthPercent(1)}} />
            </Col>
          }
          {columns.map(({customRender, colContentClassName, width, ariaLabel, label}, cindex) => {
            const idkey = "col" + (label || ariaLabel || cindex);
            return <Col className={clsx(colContentClassName)} key={idkey} style={{width: computeWidthPercent(width)}}>
              {customRender(node)}
            </Col>
          })}
        </Row>
      )
    });


  }

  /*
 * The query variables are factorized in a function, because they are used in several places (query and cache update) and we
 * need to ensure the that variables are exactly the same in all occurences
 */
  function getQueryVariables() {
    return {
      first: pageSize,
      after: currentPage > 1 ? `offset:${((pageSize) * (currentPage - 1)) - 1}` : null,
      ...filters,
      sortings: [
        {
          sortBy,
          isSortDescending,
        },
      ],
    };
  }


  function resetSelectedRowsIndexes() {
    setSelectedIDs([]);
    setSelectAllChecked(false);
  }

  /***
   * handle click on top checkbox, select all or unselect all lines
   */
  function handleSelectAll() {
    let newSelectedRowsID = [];
    if (selectAllChecked === false) {
      nodes.map(({id}) => {
        newSelectedRowsID.push(id)
      })
    }
    setSelectedIDs(newSelectedRowsID);
    setSelectAllChecked(!selectAllChecked);
  }

  /**
   * handle click on one Row
   */
  function handleSelectRow(selectedId) {
    setSelectAllChecked(false);
    let newSelectedRowsID = [...selectedIDs];
    if (newSelectedRowsID.includes(selectedId)) {
      newSelectedRowsID.splice(newSelectedRowsID.indexOf(selectedId), 1);
    } else {
      newSelectedRowsID.push(selectedId);
    }
    setSelectedIDs(newSelectedRowsID);
  }

  function renderPagination() {
    return (
      <If condition={pageCount > 1}>
        <Grid
          key="pagination"
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
          className={classes.marginTop}
        >
          <Grid item xs={12} md={4} className={classes.center}>
            <i>{count} résultats.</i>
          </Grid>
          <Grid item xs={12} md={4} className={classes.center} >
            <Pagination size="large" siblingCount={0} page={currentPage} count={pageCount} onChange={(event, page) => setCurrentPage(page)} />
          </Grid>
          <Grid item xs={false} md={4}></Grid>
        </Grid>
      </If>
    );
  }

  /**
 * On click on a column header, in order to set the sorting for that property. If the property is already selected for sorting,
 * this function toggles the sorting order. If the property isn't already selected, the function sets the property as a sorting filter
 * and sets the sorting order to 'descending'.
 *
 * @param {string} propertyName 
 */
  function setSortingFor(propertyName) {
    if (sortBy === propertyName) {
      setIsSortDescending((descending) => !descending);
    } else {
      setSortBy(propertyName);
      setIsSortDescending(true);
    }
  }
});


TableView.propTypes = {
  filters: PropTypes.object,
};