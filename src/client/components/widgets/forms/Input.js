import React from 'react';
import PropTypes from 'prop-types';
import formStyles from './FormStyles.js';
import Config from '../../../Config.js';


Input.propTypes = {
  /**
   * Flag to indicate if the input is required
   */
  isRequired: PropTypes.bool,
  /**
   * Default color is purple
   */
  colorTheme: PropTypes.string,
};

export function Input({placeholder, isRequired, colorTheme, ...otherProps}) {
  const formStyle = formStyles({color: colorTheme});

  if (placeholder && isRequired) {
    placeholder += ' *';
  }
  const style = colorTheme ? {backgroundColor: Config.colors[colorTheme]} : {};

  return (
    <input
      className={formStyle.input}
      type="text"
      required={isRequired}
      placeholder={placeholder}

      style={style}
      {...otherProps}
    />
  );
} 