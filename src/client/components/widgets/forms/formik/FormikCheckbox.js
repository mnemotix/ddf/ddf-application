import React from 'react';
import {connect, getIn} from 'formik';
import {Checkbox as RawCheckbox} from '../Checkbox';
import {isRequiredField} from '../../../../utilities/FormikIsRequiredField';
import formStyles from '../FormStyles.js';


const FormikCheckbox = ({formik, ...otherProps}) => {

  const formStyle = formStyles();

  const {handleChange, handleBlur} = formik;
  const {name} = otherProps;
  const isRequired = typeof otherProps.isRequired !== 'undefined' ? otherProps.isRequired : isRequiredField(formik, name);

  const error = getIn(formik.errors, name);
  /** input value if no value prop is provided to this component */
  let inputValue = getIn(formik.values, name) !== undefined ? getIn(formik.values, name) : getIn(formik.initialValues, name);

  return (
    <React.Fragment>
      <If condition={error}>
        <p className={formStyle.inputErrorMessage} data-testid={`${name}-error-msg`}>
          {error}
        </p>
      </If>
      <RawCheckbox
        onChange={handleChange}
        onBlur={handleBlur}
        {...otherProps}
        isRequired={isRequired}
        value={otherProps.value || inputValue}
      />
    </React.Fragment>
  );
};

export default connect(FormikCheckbox);



 