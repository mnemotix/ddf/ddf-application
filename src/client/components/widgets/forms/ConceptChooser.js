import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

import {makeStyles} from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import CircularProgress from "@material-ui/core/CircularProgress";
import SearchIcon from '@material-ui/icons/Search'
import {useQuery, gql} from "@apollo/client";
import {useFormikContext} from "formik";
import {uniq} from 'lodash';
import clsx from 'clsx';
import {upperCaseFirstLetter} from "../../../utilities/helpers/tools";
import DivButton from '../../widgets/DivButton';
import Config from '../../../Config';



const gqlGrammaticalProperties = gql`
  query GrammaticalProperties($startWith: String, $first: Int, $scheme: String!, $scopeNote: String, $isTopInScheme: Boolean) {
    grammaticalPropertiesStartWith(
      startWith: $startWith
      scheme: $scheme
      first: $first
      isTopInScheme: $isTopInScheme
      scopeNote : $scopeNote
      sortings: [{sortBy: "prefLabel"}]
    )
  } 
`

/*
  query GrammaticalProperties($qs: String!, $first: Int, $filters: [String!]){
    grammaticalProperties(qs: $qs, filters: $filters, first: $first, sortings: [{sortBy: "prefLabel"}]) {
      edges {
        node {
          id
          prefLabel
          definition
          scopeNote
        }
      }
    }
  }
*/

const useStyles = makeStyles((theme) => ({
  conceptChooserRoot: {
    paddingBottom: Config.spacings.large,
  },
  input: {
    width: "100%",
    //  height: "100%",
    color: "inherit",
    border: "none",
    fontFamily: "Raleway",
    textOverflow: "ellipsis",
    backgroundColor: Config.colors.white,
    marginBottom: Config.spacings.large,
  },
  button: {
    width: "100%",
    height: "100%",
    border: `1px solid ${Config.colors.mediumgray}`,
    color: Config.colors.black,
    fontSize: `${Config.fontSizes.large - 2}px`,
    backgroundColor: Config.colors.white,
    padding: Config.spacings.small,
    minHeight: '70px'
  },
  disabledButton: {
    backgroundColor: Config.colors.mediumgray,
  },
  definition: {
    color: Config.colors.darkgray,
    fontSize: Config.fontSizes.sm,
    paddingTop: Config.spacings.tiny,
  },

  error: {
    width: '100%',
    color: 'red',
    padding: Config.spacings.small,
    textAlign: 'center'
  }
}));

// Default way to display a concept entry
// text is prefLabel
// subText is Definition
export function DisplayEntry({text, subText}) {
  const classes = useStyles();
  return <div>{upperCaseFirstLetter(text)}
    {subText && <div className={classes.definition}> {upperCaseFirstLetter(subText)}</div>}
  </div>
}

function dataExtractor(grammaticalPropertiesStartWith, addIDontKnowConcept) {
  let res = uniq(
    grammaticalPropertiesStartWith?.edges?.map(({node: {id, prefLabel, definition}}) => (
      {id, prefLabel, definition}
    ))
    , 'prefLabel')
  //  .sort((a, b) => (a.prefLabel > b.prefLabel ? 1 : -1));

  if (addIDontKnowConcept) {
    res.push(iDontKnowConcept);
  }
  return res;
}

ConceptChooser.propTypes = {
  /*
   name for formikInput
  */
  name: PropTypes.string.isRequired,
  // field to query 
  schemeId: PropTypes.string.isRequired,
  // input placeHolder
  placeHolder: PropTypes.string.isRequired,

  maxSuggestionsLength: PropTypes.number,
  required: PropTypes.bool,
  // input can be hided when only few results are expected
  hideInput: PropTypes.bool,
  // we can pass static data that can be used instead of useQuery result for concept that don't have data in grapqh yet ( like for infinitif )
  staticData: PropTypes.object
};


const iDontKnowConcept = {
  "id": 'idontknow',
  "prefLabel": "Je ne sais pas",
  "definition": "Si vous n'êtes pas sûr"
};


/**
 * This component display list of Concept with filter input and user can choose one of them
 * @param {string} name : choosed concept wil be stored in formik values[name]
 * @param {string} schemeId : get the concept of this list scheme
 * @param {array} customFilters : add custom filters to filters, for example "scopeNote:distinctWrittenRep"
 * @param {int} maxSuggestionsLength 
 * @param {string} placeHolder 
 * @param {classes} inputClassName 
 * @param {boolean} hideInput 
 * @param {array} staticData : some concept are not in Graph, so we use staticData instead
 * @param {boolean} addIDontKnowConcept : add or not the concept "i Don't Know" to fetched ones, you can also use overloadData to add/remove the data you want (used mainly to replace prefLabel and definition and keep the same ID). 
 * @param {function} overloadData : optional, function that parse option to overload them (filter or add entries), used in StepCategory to rename "ddfa:part-of-speech-type/unspecifiedPOS"
 * @param {function} onSelectCallBack : callback function that can be called when a concept is choosed
 * @param {array}  persistedData : array of persisted data to prevent user to choose already persisted data
 * @returns 
 */
export function ConceptChooser({name, schemeId, maxSuggestionsLength = 14, placeHolder, inputClassName = null, customFilters = [],
  hideInput = false, staticData = null, addIDontKnowConcept = false, overloadData = false, onSelectCallBack = false, persistedData}) {



  const _persistedData = persistedData?.map((elem) => elem?.prefLabel) || [];

  const classes = useStyles();

  const formikContext = useFormikContext();
  const {errors, values} = formikContext;

  // this should be always a string, contain the QS query
  const [qs, setQs] = useState('');
  // contain the AC query result's
  const [options, setOptions] = useState([]);
  // this should be always be an object, contain the selected value from AC ( label + id )
  const [selectedOption, setSelectedOption] = useState({});

  useEffect(() => {
    if (values?.[name]) {
      setSelectedOption(values?.[name]);
    }
  }, [values]);

  // pas possible d'utiliser le lazyLoading, j'ai plein d'effet de bord et les données qui ne ce chargent pas notament 
  // si on est sur une étape, qu'on va à la suivante et qu'on revienne, la seconde requete ne part pas avec lazyloading
  const {loading} = useQuery(gqlGrammaticalProperties, {
    variables: getVariables(),
    fetchPolicy: 'cache-and-network',
    onCompleted: (data) => {
      let src = staticData ? staticData?.grammaticalProperties : JSON.parse(data?.grammaticalPropertiesStartWith);
      let newData = dataExtractor(src, addIDontKnowConcept);
      if (overloadData) {
        newData = overloadData(newData);
      }
      setOptions(newData);
    }
  });

  function onSelect(params) {
    if (selectedOption?.id === params.id) {
      setSelectedOption({});
      formikContext.setFieldValue(name, undefined);
      onSelectCallBack && onSelectCallBack({name, value: false});
    } else {
      setSelectedOption(params);
      formikContext.setFieldValue(name, params);
      onSelectCallBack && onSelectCallBack({name, value: params});
    }
  }

  return (
    <div className={classes.conceptChooserRoot}>
      {createInput()}
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="stretch"
        spacing={1}
      >
        {renderButtons()}
      </Grid>
      {errors?.[name] && (
        <div className={classes.error}>
          {errors?.[name]}
        </div>
      )}
    </div >
  );

  // render the buttons with available options, if selectedOption is not include in this buttons it will be added 
  function renderButtons() {

    const createButton = (elem, index) => {
      const disabled = _persistedData?.includes(elem?.prefLabel);
      // don't check on ID because multiple items can have same id...
      // example in <StepCategory/> "locution" and "Je ne sais pas" (used as default here)
      const selected = !!selectedOption?.prefLabel && !!elem?.prefLabel ? selectedOption?.prefLabel === elem?.prefLabel : selectedOption?.id === elem.id;


      return <Grid item key={index} xs={12} md={6} xl={4}>
        <DivButton onClick={() => onSelect(elem)} aria-label={elem?.prefLabel}
          classesName={clsx(classes.button, disabled && classes.disabledButton)} selected={selected}
          disabled={disabled}
          tooltipTitle={disabled && "Déjà ajouté"}
        >
          <DisplayEntry text={elem?.prefLabel} subText={elem?.definition} />
        </DivButton>
      </Grid>
    }

    let buttons = [];
    let found = false;
    options.map((elem, index) => {
      if (selectedOption?.id === elem.id) {
        found = true;
      }
      buttons.push(createButton(elem, index))
    })

    /* if (addIDontKnowConcept) {    
       buttons.push(createButton(iDontKnowConcept));
     }
     */

    if (!found && selectedOption?.id) {
      buttons.unshift(createButton(selectedOption, 'selectedelem'))
    }
    return buttons
  }

  function createInput() {
    if (hideInput) {return null}
    return <TextField
      variant="standard"
      id={"id" + name}
      fullWidth={true}
      autoCorrect="off"
      className={clsx(classes.input, inputClassName)}
      label={placeHolder}
      onChange={event => setQs(event.target.value)}
      InputProps={{
        startAdornment: <InputAdornment position="start"><SearchIcon /></InputAdornment>,
        endAdornment: loading && <InputAdornment position="end"><CircularProgress size={20} className={classes.marginRight} /></InputAdornment>
      }}
    />
  }

  function getVariables() {
    // customFilters can be like {"scopeNote":"distinctWrittenRep"}
    if (qs?.length > 0) {
      return {
        first: maxSuggestionsLength,
        startWith: qs,
        scheme: schemeId,
        ...customFilters
      }
    } else {
      return {
        first: maxSuggestionsLength,
        scheme: schemeId,
        ...customFilters,
        isTopInScheme: true
      }
    }
  }
}

