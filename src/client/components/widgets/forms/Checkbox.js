import React from 'react';
 
import PropTypes from 'prop-types'; 
import formStyles from './FormStyles.js';

Checkbox.propTypes = {
  label: PropTypes.node,
  /**
   * Flag to indicate if the input is required 
   */
  isRequired: PropTypes.bool,
};
export function Checkbox(props) {
  const formStyle = formStyles();
  const {label, isRequired, ...otherProps} = props;
  const id = `checkboxid-${props.name}`;
  
  return (
    <div className={formStyle.checkboxRow}>
      <input 
        id={id}
        className={formStyle.customCheckbox} 
        type="checkbox" 
        required={isRequired}
        {...otherProps} 
      />
      <label htmlFor={id}>{label}</label>
    </div>
  );
}

