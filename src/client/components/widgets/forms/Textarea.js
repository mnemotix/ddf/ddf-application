import React from 'react';
import PropTypes from 'prop-types';
import clsx from "clsx";
import Config from '../../../Config.js';
import formStyles from './FormStyles.js';


Textarea.propTypes = {
  /**
   * Flag to indicate if the input is required
   */
  isRequired: PropTypes.bool,
  /**
   * Default color is purple
   */
  colorTheme: PropTypes.string,
};
export function Textarea({placeholder, isRequired, colorTheme, className, ...otherProps}) {
  const formStyle = formStyles({color: colorTheme});
  if (placeholder && isRequired) {
    placeholder += ' *';
  }
  const style = colorTheme ? {backgroundColor: Config.colors[colorTheme]} : {};
  return (
    <textarea
      className={clsx(formStyle.textarea, className)}
      required={isRequired}
      placeholder={placeholder}
      style={style}
      {...otherProps}
    />
  );
}

