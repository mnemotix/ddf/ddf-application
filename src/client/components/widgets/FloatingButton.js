import React from 'react';
import Fab from '@material-ui/core/Fab';
import {makeStyles} from "@material-ui/core/styles";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'fixed',
    bottom: theme.spacing(8),
    right: theme.spacing(2),
  },
}));


/**
 * Display a floatting button on bottom right
 * 
 * @param {string} Icon : which icon is displayed as child ?  "AddIcon" ,you can pass directly a children 
 * @param {boolean} locked : disable the button
 */
export function FloatingButton({icon, locked, classname, component = Link, ...props} = {}) {
  const classes = useStyles();
  const Icon = icon;
  return (
    <Fab color="inherit" aria-label="add" disabled={locked} component={component} className={clsx(classes.fab, classname)}  {...props}>
      <Icon />
    </Fab>
  );
};

FloatingButton.propTypes = {
  locked: PropTypes.bool.isRequired,
  // icon: PropTypes.string.isRequired
};


