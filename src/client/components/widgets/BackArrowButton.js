import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';
import backArrowPurple from '../../assets/images/back_arrow_purple.svg';

import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import Config from '../../Config';
import globalStyles from '../../assets/stylesheets/globalStyles.js';

BackArrowButton.propTypes = {
  /** The link text */
  text: PropTypes.string,
  'aria-label': PropTypes.string,
  tooltip: PropTypes.bool, // if true use aria-label as title
  /** action to do on click */
  onClick: PropTypes.func,
};

const useStyles = makeStyles((theme) => ({
  backButton: {
    display: "flex",
    alignItems: "center",
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    color: Config.colors.purple,
    textDecoration: "inherit",
    fontSize: Config.fontSizes.small,
    fontFamily: "Raleway",
    fontWeight: Config.fontWeights.medium,

    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      // backgroundColor: Config.colors.white
    },
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      backgroundColor: 'transparent',
      paddingBottom: '10px'
    }
  },
  img: {
    height: '8px'
  },
  span: {
    marginLeft: Config.spacings.tiny,
    whiteSpace: 'nowrap'
  }
}));
const defaultText = "Revenir à l’étape précédente";

// <BackArrowButton text={backLinkText} onClick={fct} />
export function BackArrowButton({text = defaultText, tooltip = false, onClick, ...props}) {
  const classes = useStyles();
  const gS = globalStyles();

  const content = <div className={clsx(classes.backButton, gS.clickable)} onClick={onClick}>
    <img className={clsx(classes.img, gS.clickable)} src={backArrowPurple}
      alt={defaultText}
      aria-label={defaultText}
      role="button"
      {...props}
    />
    <span className={classes.span} >{text}</span>
  </div>

  /*if (tooltip) {
     return <Tooltip title={props['aria-label']}>
      {content}
    </Tooltip>
  } else {
    return content
  }*/
  return content;

};

