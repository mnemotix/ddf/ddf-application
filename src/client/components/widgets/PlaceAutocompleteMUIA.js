import React from 'react';
import {filter} from 'lodash';
import {MUIAutocomplete} from "./forms/MUIAutocomplete";
import {SearchPlacesQuery} from '../../services/GeolocService';



export function PlaceAutocompleteMUIA(props) {
  return (
    <MUIAutocomplete
      {...props}
      inputName={props?.inputName || "placeId"}
      inputAriaLabel="Choisir une localisation"
      gql={SearchPlacesQuery}
      addForm={false}
      dataExtractor={(data) => {
        return data?.places?.edges?.map(({node}) => ({
          label:
            filter([
              filter([node?.name, node?.stateName]).join(' ') // filter remove empty values to avoid concat undefined 
              , node?.countryName
            ]).join(', ')
          , ...node
        }))
      }}
    />
  );
}
