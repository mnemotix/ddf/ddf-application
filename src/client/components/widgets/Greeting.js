

import React from 'react';
import Config from '../../Config';
import clsx from 'clsx';

import greetingSmileIcon from '../../assets/images/greeting_smile.svg';

import {makeStyles} from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  text: {
    color: Config.colors.orange
  },
  icon: {
    height: "50px",
    margin: Config.spacings.small
  },
}));


export default function Greeting() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <img className={classes.icon} src={greetingSmileIcon} alt=";)" />
      <span className={classes.text}>
        Merci d'avoir<br />
        enrichi le DDF !
      </span>
    </div>
  );
};



