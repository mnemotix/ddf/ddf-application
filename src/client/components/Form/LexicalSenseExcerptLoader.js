import React from 'react';
import clsx from "clsx";
import {useStyles} from "./LexicalSenseExcerptStyle";
import globalStyles from '../../assets/stylesheets/globalStyles.js';

export function LexicalSenseExcerptLoader({className, hideRightEnd = false}) {
  const classes = useStyles();
  const gS = globalStyles();
  return (
    <div className={clsx(classes.container, gS.loadingOverlay, className)}>
      <div className={classes.spaceBetween}>
        <div className={classes.countriesRow}>
          <div className={clsx(classes.countries, classes.placeholder)}>
            <div className={classes.countrySourcePlaceholder}>&nbsp;</div>
          </div>
        </div>
        <div className={clsx(classes.sources, classes.placeholder)}>
          <div className={classes.countrySourcePlaceholder}>&nbsp;</div>
        </div>
      </div>

      <div className={classes.pos}>
        <div className={classes.posPlaceholder} />
      </div>

      <div className={classes.description}>
        <div className={classes.descriptionPlaceholder} />
      </div>

      <div className={classes.patchHeight} />

      {!hideRightEnd &&
        <div className={classes.rightEnd}>
          <div className={classes.countrySourcePlaceholder}>&nbsp;</div>
        </div>
      }
    </div>
  );
}