import React, {useMemo} from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {useQuery, gql} from '@apollo/client';
import upperFirst from "lodash/upperFirst";
import {ROUTES} from '../../routes';
import {makeStyles} from "@material-ui/core/styles";

import {apolloErrorHandler} from '../../utilities/apolloErrorHandler';
import {FormSeeAlsoLoader} from "./FormSeeAlsoLoader";
import globalStyles from '../../assets/stylesheets/globalStyles.js';
import Config from '../../Config';

export let gqlFormSeeAlsoQuery = gql`
  query FormSeeAlso_Query($filters: [String]) {
    semanticRelations(filters: $filters) {
      totalCount
      edges {
        node {
          id
          lexicalEntryFormWrittenRep
          lexicalSenseFormWrittenRep
          semanticRelationTypeLabel
        }
      }
    }
  }
`;

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: Config.colors.lightgray,
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
    "& h4": {
      marginBottom: Config.spacings.small,
      fontSize: Config.fontSizes.medium,
      color: Config.colors.purple
    },
    "& p": {
      fontSize: Config.fontSizes.small,
      color: Config.colors.darkgray,
      "& a": {
        color: "inherit",
        fontStyle: "italic"
      }
    },
    "& p:not(:last-child)": {
      marginBottom:  Config.spacings.small,
    },
    [theme.breakpoints.up(Config.mediaQueries.mobileMaxWidth)]: {
      backgroundColor: Config.colors.white,
      "& .title": {
        display: "none"
      },
      "& p": {        
        fontSize: Config.fontSizes.medium,
        color: Config.colors.black,
        "& a": {          
          fontStyle: "normal !important",
          color: `${Config.colors.purple} !important`         
        }
      }
    }
  },
  inlineTitle: {
    fontWeight: Config.fontWeights.bold,
    [theme.breakpoints.up(Config.mediaQueries.mobileMaxWidth)]: {
      fontWeight: Config.fontWeights.normal
    },
  }
}));



FormSeeAlso.propTypes = {
  formQuery: PropTypes.string.isRequired,
};

export function FormSeeAlso(props) {
  const classes = useStyles();
  const {t} = useTranslation();
  const {formQuery} = props;
  const {data : {semanticRelations} = {}, loading, error} = useQuery(gqlFormSeeAlsoQuery, {
    variables: {
      filters: [`gathetedFormWrittenRep = ${formQuery}`, `semanticRelationTypeScopeNote = distinctWrittenRep`]
    },
    fetchPolicy: 'cache-and-network',
  });

  apolloErrorHandler(error, 'log');

  const semanticRelationsCategories = useMemo(() => {
    let semanticRelationsCategories = {};

    (semanticRelations?.edges || []).map(({node: semanticRelation}) => {
      const {
        id,
        lexicalEntryFormWrittenRep,
        lexicalSenseFormWrittenRep,
        semanticRelationTypeLabel
      } = semanticRelation;

      if(!semanticRelationsCategories[semanticRelationTypeLabel]){
        semanticRelationsCategories[semanticRelationTypeLabel] = new Set();
      }

      const labels =  [...lexicalEntryFormWrittenRep, ...lexicalSenseFormWrittenRep].filter((formWrittenRep) => formWrittenRep !== formQuery)

      if(labels.length > 0){
        labels.map(label => semanticRelationsCategories[semanticRelationTypeLabel].add(label))
      }
    });

    return semanticRelationsCategories;
  }, [semanticRelations]);


  if (loading) {
    return (
      <SeeAlsoContainer>
        <FormSeeAlsoLoader />
      </SeeAlsoContainer>
    )
  }


  if (semanticRelations?.totalCount === 0) {
    return null;
  }

  return (
    <SeeAlsoContainer>
      {Object.entries(semanticRelationsCategories).map(([categoryLabel, semanticRelations]) => {
        return (
          <p key={categoryLabel}>
            <span className={classes.inlineTitle}>{upperFirst(categoryLabel)}&nbsp;:&nbsp;</span>
            {renderSemanticRelations(semanticRelations)}
          </p>
        )
      })}
    </SeeAlsoContainer>
  );

  function renderSemanticRelations(semanticRelations) {
    return [...semanticRelations.values()].map((label, index) => {
      return (
        <React.Fragment key={label}>
          <Link role="link" aria-label={"Lancer une recherche sur " + label} to={formatRoute(ROUTES.FORM_SEARCH, {formQuery: label})}>{label}</Link>
          <If condition={index < semanticRelations.size - 1}>
            <span>, </span>
          </If>
        </React.Fragment>
      );
    });
  }
}


export function SeeAlsoContainer({children}) {
  const classes = useStyles();
  const {t} = useTranslation();
  const gS = globalStyles();
  return <React.Fragment>
    <div className={gS.secondaryTitle}>{t('FORM.SEE_ALSO.TITLE')}</div>
    <div className={classes.container}>
      <h4 className={"title"}>{t('FORM.SEE_ALSO.TITLE')}</h4>
      {children}
    </div>
  </React.Fragment>
}