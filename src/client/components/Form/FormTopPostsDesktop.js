import React from 'react';
import { useQuery,gql} from "@apollo/client";
import {useTranslation} from 'react-i18next';

import {apolloErrorHandler} from '../../utilities/apolloErrorHandler';
import {DesktopDiscussionPreview} from './DesktopDiscussionPreview';
import {FormTopPostsQuery} from './FormTopPosts';

export function FormTopPostsDesktop(props) {
  const {t} = useTranslation();
  const {formQuery} = props;
  const {data, loading, error} = useQuery(FormTopPostsQuery, {
    variables: {
      formQs: formQuery
    }
  });
  apolloErrorHandler(error, 'log');
  if ((loading && !data) || error) return null;

  const {topPostAboutForm, topPostAboutEtymology} = data;

  return (
    <React.Fragment>
      <If condition={topPostAboutForm}>
        <DesktopDiscussionPreview 
          title={t('FORM.TOP_POSTS.DESKTOP.FORM_HEADER')}
          content={topPostAboutForm.content}
          commentCount={12}
        />
      </If>
      <If condition={topPostAboutEtymology}>
        <DesktopDiscussionPreview 
          title={t('FORM.TOP_POSTS.DESKTOP.ETYMOLOGY_HEADER')}
          content={topPostAboutEtymology.content}
          commentCount={8}
        />
      </If>
    </React.Fragment>
  );
}
