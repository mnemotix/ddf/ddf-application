import React, {useState, useEffect, useMemo} from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {gql, useLazyQuery, useQuery} from '@apollo/client';
import {Paginator} from '../widgets/Paginator';
import {ShowWhereIAM} from '../widgets/ShowWhereIAM';
import {MetaSEO} from '../widgets/MetaSEO';
import {LexicalSenseExcerpt} from './LexicalSenseExcerpt';
import {FormLexicalSenseListLoader} from './FormLexicalSenseListLoader';
import {FormNoResults} from './FormNoResults';
import {FormSeeAlso} from './FormSeeAlso';
import {
  getLocalizedEntityData,
  getPlaceCountryId,
  gqlLexicalSenseCountriesFragment,
} from '../../utilities/helpers/countries';


import globalStyles from '../../assets/stylesheets/globalStyles.js';
import {FormLexicalSenseListUseStyles} from "./FormLexicalSenseListStyles";

export const gqlFormQuery = gql`
  query Form_Query($first: Int, $after: String, $formQs: String, $filterByPlaceId: ID, $filters: [String]) {
    lexicalSenses: lexicalSensesForAccurateWrittenForm(formQs: $formQs, first: $first, after: $after, filterByPlaceId: $filterByPlaceId, filters: $filters) {
      totalCount
      edges {
        node {
          id
          definition
          lexicalEntryGrammaticalPropertyLabels
          lexicographicResourceShortName
          ...LexicalSenseCountriesFragment
        }
      }
    }
  }
  ${gqlLexicalSenseCountriesFragment}
`;

FormLexicalSenseList.propTypes = {
  formQuery: PropTypes.string.isRequired,
  /**
   * The geoname place the user is currently located at, used to customize the results order. 
   */
  currentPlace: PropTypes.object,
  selectedDefId: PropTypes.string,
};

/**
 * @param formQuery
 * @param currentPlace
 * @param {function} onItemClickCallBack = in /form/pomme , for each senses we have a link to go to it's page details .
 *    But in edit mode we need to select a sense to link it another sense or form ( semanticRelation ), this callback function is used her to get the sense clicked instead of routing.
 * @param {string} selectedDefId  = Id of the selected definition from parent, selected with onItemClickCallBack, used to add css to one Sense
 * @param {bool} enableAdd = in case of no result, enable the user to add new definition with "Ajouter une entrée" button
 * @param {string} [excludeLexicalSenseId] Exclude a lexicalSense from the list
 * @returns
 */
export function FormLexicalSenseList({formQuery, currentPlace = {}, onItemClickCallBack = false, selectedDefId = null, enableAdd = true, excludeLexicalSenseId}) {
  const {t} = useTranslation();
  const classes = FormLexicalSenseListUseStyles();
  const gS = globalStyles();
  const pageSize = 6;
  const [currentPage, setCurrentPage] = useState(1);
  const [lexicalSensesCount, setLexicalSensesCount] = useState(0);

  function getGqlVariables() {
    const after = currentPage > 1 ? `offset:${((pageSize) * (currentPage - 1)) - 1}` : null;
    let variables = {
      first: pageSize,
      after,
      formQs: formQuery
    };
    const countryId = getPlaceCountryId(currentPlace);
    if (countryId) {
      variables.filterByPlaceId = countryId;
    }
    if (excludeLexicalSenseId) {
      variables.filters = [`id != ${excludeLexicalSenseId}`]
    }
    return variables;
  }

  const {data: { lexicalSenses: lexicalSensesConnection } = {}, loading} = useQuery(gqlFormQuery, {
    variables: getGqlVariables()
  });

  const lexicalSenses = (lexicalSensesConnection?.edges || []).map(({node}) => node);
  const pageCount = Math.ceil(lexicalSensesCount / pageSize);

  useEffect(() => {
    if(lexicalSensesConnection){
      setLexicalSensesCount(lexicalSensesConnection?.totalCount || 0);
    }
  }, [lexicalSensesConnection]);

  useEffect(() => {
    if (currentPlace) {
      setCurrentPage(1);
    }
  }, [currentPlace, formQuery]);

  let metaDefinition;
  if (!loading && lexicalSensesCount && lexicalSenses) {
    metaDefinition = "";
    if (lexicalSensesCount && lexicalSensesCount === 0) {
      metaDefinition = "Pas de définition pour ce mot.";
    } else {
      if (lexicalSensesCount > 1) {
        metaDefinition = `${lexicalSensesCount} définitions pour ce mot dont: `
      }
      metaDefinition += lexicalSenses?.[0]?.definition
    }
  }

  return (
    <ShowWhereIAM path='FormLexicalSenseList'>
      <div className={classes.width100}>
        {!onItemClickCallBack &&
          <MetaSEO
            title={t('DOCUMENT_TITLES.FORM_SEARCH', {formQuery})}
            description={metaDefinition}
            name={formQuery}
            addScript={true}
          />
        }

        {renderContent()}
        {!onItemClickCallBack &&
          <FormSeeAlso formQuery={formQuery} />
        }
      </div>
    </ShowWhereIAM>
  );

  function renderContent() {
    if (lexicalSenses && !loading && lexicalSensesCount === 0) {
      return <div>{renderNoResults()}</div>
    } else {
      return (<>
        <div className={gS.secondaryTitle}>{lexicalSensesCount > 1 ? (lexicalSensesCount + ' ' + t('FORM.DEFINITIONS')) : t('FORM.DEFINITION')}</div>
        {!loading ?
          <div className={classes.lexicalSenses}>
            {lexicalSenses.map((lexicalSense, index) => renderLexicalSense(lexicalSense, index))}
          </div>
          :
          <FormLexicalSenseListLoader pageSize={pageSize} hideRightEnd={!!onItemClickCallBack} />
        }
        <Paginator currentPage={currentPage} pageCount={pageCount} setCurrentPage={setCurrentPage} />
      </>)
    }
  }

  function renderLexicalSense(lexicalSense, index) {
    let partOfSpeechList = lexicalSense?.lexicalEntryGrammaticalPropertyLabels || [];
    let source = lexicalSense?.lexicographicResourceShortName;
    let countryData = getLocalizedEntityData(lexicalSense?.places);

    return (
      <LexicalSenseExcerpt
        key={index}
        className={classes.lexicalSense}
        sense={lexicalSense}
        countryData={countryData}
        source={source}
        partOfSpeechList={partOfSpeechList}
        disableLinkByCallBack={onItemClickCallBack}
        selectedDefId={selectedDefId}
      />
    );
  }

  function renderNoResults() {
    if (enableAdd) {
      return <FormNoResults formQuery={formQuery} />;
    } else {
      return <div className={classes.noResultsText}>
        {t('FORM.NO_RESULTS')}
      </div>
    }
  }
}
