
import React from 'react';

import {LexicalSenseExcerptLoader} from './LexicalSenseExcerptLoader';
import {FormLexicalSenseListUseStyles} from "./FormLexicalSenseListStyles";

export function FormLexicalSenseListLoader({pageSize = 10, hideRightEnd = false}) {
  const classes = FormLexicalSenseListUseStyles();
  let rows = [];
  for (let index = 0; index < pageSize; index++) {
    rows.push(<LexicalSenseExcerptLoader
      key={index.toString()}
      hideRightEnd={hideRightEnd}
      className={classes.lexicalSense}
    />)
  }

  return <div className={classes.lexicalSenses}>
    {rows}
  </div>

}
