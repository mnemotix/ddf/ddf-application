import {gqlFormQuery} from '../../FormLexicalSenseList';
import {FormTopPostsQuery} from '../../../Form/FormTopPosts';

const formQueryMock = {
  request: {
    query: gqlFormQuery,
    variables: {
      formQs: "affairé",
      first: 6,
      after:null,
      filterByPlaceId: "geonames:6614192"
    }
  },
  result: {
    "data": {
      "lexicalSensesCount":"12",
      "lexicalSensesCountForAccurateWrittenForm":12,
      "lexicalSenses": {
        "edges": [{
          "node": {
            "id": "inv/lexical-sense/1234",
            "definition": "Affairiste",
            "places": {
              "edges": [{
                "node": {
                  "id": "place-1",
                  "name": "RDC",
                  "color": "jaune",
                  "__typename": "Country"
                },
                "__typename": "PlaceInterfaceEdge"
              }],
              "__typename": "PlaceInterfaceConnection"
            },
            "lexicalEntryGrammaticalPropertyLabels": ["nom"],
            "lexicographicResourceShortName": "Inv",
            "lexicographicResourceDefinition": "Le wiki ....",
            "__typename": "LexicalSense"
          },
          "__typename": "LexicalSenseEdge"
        },
          {
            "node": {
              "id": "inv/lexical-sense/45689",
              "definition": "Grand séducteur, coureur de jupons <a href=\"/test\">test</a>",
              "places": {
                "edges": [{
                  "node": {
                    "id": "place-2",
                    "name": "Cameroun",
                    "color": "jaune",
                    "__typename": "Country"
                  },
                  "__typename": "PlaceInterfaceEdge"
                }],
                "__typename": "PlaceInterfaceConnection"
              },
              "lexicalEntryGrammaticalPropertyLabels": ["nom"],
              "lexicographicResourceShortName": "Inv",
              "lexicographicResourceDefinition": "Le wiki ....",
              "__typename": "LexicalSense"
            },
            "__typename": "LexicalSenseEdge"
          }, {
            "node": {
              "id": "wikt/entry/1234/en/1/ls/1",
              "definition": "Qui a bien des affaires, qui est occupé.",
              "places": {
                "edges": [],
                "__typename": "PlaceInterfaceConnection"
              },
              "lexicalEntryGrammaticalPropertyLabels": ["adjectif"],
              "lexicographicResourceShortName": "Wikt",
              "lexicographicResourceDefinition": "Le wiki ....",
              "__typename": "LexicalSense"
            },
            "__typename": "LexicalSenseEdge"
          }],
        "pageInfo": {
          "startCursor": "YXJyYXljb25uZWN0aW9uOjA=",
          "endCursor": "YXJyYXljb25uZWN0aW9uOjM=",
          "hasNextPage": false,
          "__typename": "PageInfo"
        },
        "__typename": "LexicalSenseConnection"
      },
      "__typename": "Query"
    }
  }
};

const formTopPostsMock = {
  request: {
    query: FormTopPostsQuery,
    variables: {
      formQs: "affairé",
    }
  },
  result: {
    "data": {
      "topPostAboutEtymology": {
        "content": "Dérivé de affaire",
        "id": 'inv/lexical-sense/12346',
        "__typename": "Post"
      },
      "topPostAboutForm": {
        "content": "Semble s'être dit d'abord de domestiques, d'intendants, etc.",
        "id": 'inv/lexical-sense/12345', 
        "__typename": "Post"
      }
    }
  }

};

export const formGqlMocks = [formQueryMock, formTopPostsMock];
