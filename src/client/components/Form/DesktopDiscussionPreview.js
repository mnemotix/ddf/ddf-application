import React from 'react';
import PropTypes from 'prop-types';
import {PurifyHtml} from '../../utilities/PurifyHtml';
import Config from '../../Config';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  fontSize: Config.fontSizes.xxlarge,
  color: Config.colors.purple,
  fontWeight: Config.fontWeights.semibold,

  discussion: {
    position: "relative",
    backgroundColor: Config.colors.white,
    border: `1px solid ${Config.colors.mediumgray}`,
    fontSize: Config.fontSizes.small,
    marginBottom: Config.spacings.huge,
    /* triangle */
    "&::after": {
      content: '""',
      display: "block",
      position: "absolute",
      right: "15px",
      width: "0",
      height: "0",
      borderTop: `15px solid ${Config.colors.white}`,
      borderLeft: "23px solid transparent",
      filter: "drop-shadow(1px 1px 0 rgba(0,0,0,.1))"
    }
  },
  header: {
    backgroundColor: Config.colors.orange,
    color: Config.colors.white,
    fontWeight: Config.fontWeights.bold,
    paddingLeft: Config.spacings.small,
    paddingRight: Config.spacings.small,
    paddingTop: Config.spacings.tiny,
    paddingBottom: Config.spacings.tiny
  },
  content: {
    padding: Config.spacings.small,
    paddingBottom: Config.spacings.large
  }

}));


DesktopDiscussionPreview.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

export function DesktopDiscussionPreview({title, content}) {
  const classes = useStyles();

  return (
    <div className={classes.discussion}>
      <div className={classes.header}>{title}</div>
      <PurifyHtml className={classes.content} html={content} />
    </div>
  );
}
