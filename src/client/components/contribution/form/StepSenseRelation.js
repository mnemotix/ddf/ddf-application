import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';

const help = ``;
export function StepSenseRelation({onChangeCallBack = false, customFilters = {}, persistedData = [], placeHolder = 'Contrainte grammmaticale'}) {
  return (
    <StepTemplate help={help}>
      <ConceptChooser
        persistedData={persistedData}
        onSelectCallBack={onChangeCallBack}
        hideInput={false}
        customFilters={customFilters}
        name={"senseRelation"}
        placeHolder={placeHolder}
        maxSuggestionsLength={24}
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + "sense-relation"}
      />
    </StepTemplate>
  );
}

StepSenseRelation.propTypes = {
  values: PropTypes.shape({
    senseRelation: PropTypes.object
  })
};