


import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';

const fieldName = "register";

const help =
  `Le registre est la manière de s'exprimer, en fonction de la personne à qui l'on s'adresse (l'interlocuteur). Il s'agit d'une distinction sociale, qui peut dépendre du lieu où est parlée la langue, ou de la communauté au sein de laquelle la langue est parlée. Le registre change donc en fonction de la personne à laquelle on s'adresse puisqu'il convient de s'adresser différemment à un supérieur hiérarchique et à un ami par exemple. L'absence d'indication de registre indique qu'il s'agit du registre courant ou neutre.

Si l'interlocuteur est une personne proche, le registre sera plutôt familier alors que si l'interlocuteur est un supérieur hiérarchique, il sera plutôt soutenu.

Le registre familier correspond au registre informel, c'est celui qui est utilisé dans une conversation entre deux proches ou en tout cas, sans la barrière de l'officialité ou de la hiérarchie. En 1985, avec la refonte du Grand Robert est apparu le marqueur très familier qui vient apporter un niveau de familiarité supérieur (plus familier que familier). Ce marqueur a commencé à être utilisé pour limiter l'usage du marqueur populaire qui est vu aujourd'hui comme péjoratif. Nous avons donc suivi cette tendance lexicographique et l'avons fusionné tantôt avec familier, tantôt avec très familier, en fonction de la situation. Par exemple, le mot bifton qui signifie « billet de banque » sera marqué comme familier et le mot clamser qui signifie « mourir » sera marqué comme très familier .

Le registre vulgaire est un type de registre familier. Il est cependant plus fort que très familier puisqu'il rend compte d'insultes ou d'injures.

Le marqueur langage enfantin désigne les mots employés par l'enfant ou avec des enfants. Par exemple dada pour le cheval.

Le marqueur traditionnel désigne les termes employés dans un contexte très marqué culturellement, comme les cérémonies de mariage ou certains rites.`


export default function StepRegister({persistedData}) {
  return (
    <StepTemplate title='Je rajoute un registre' help={help}>
      <ConceptChooser
        persistedData={persistedData}
        hideInput={true}
        name={fieldName}
        placeHolder='Registre'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + fieldName}
      />
    </StepTemplate>
  );
}


StepRegister.propTypes = {
  values: PropTypes.shape({
    register: PropTypes.object
  })
};