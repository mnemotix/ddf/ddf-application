
import React, {useState, useEffect} from 'react';
import {useLocation} from 'react-router-dom';
import queryString from 'query-string';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Formik, Form} from 'formik';
import invariant from 'invariant';
import {formatRoute} from "react-router-named-routes";
import {useApolloClient} from "@apollo/client";
import {ShowWhereIAM} from '../../widgets/ShowWhereIAM';
import {FormBottom} from '../../widgets/forms/FormBottom';
import {BracketButton} from "../../widgets/BracketButton";
import DivButton from "../../widgets/DivButton";
import {notificationService as appNotificationService} from '../../../services/NotificationService';
import {LexicalSenseDefinition} from "../../LexicalSense/LexicalSenseDefinition";
import {GraphQLErrorHandler} from '../../../services/GraphQLErrorHandler';
import {BackArrowButton} from '../../widgets/BackArrowButton';
import {BackArrowLink} from '../../widgets/BackArrowLink';
import {LexicalSenseCopyright} from '../../widgets/LexicalSenseCopyright';
import globalStyles from '../../../assets/stylesheets/globalStyles';
import {ROUTES} from '../../../routes';

import StepForm from './StepForm';
import StepDefinition from './StepDefinition';
import StepPlace from './StepPlace';
import StepCategory from './StepCategory';
import StepGenderNumber from './StepGenderNumber';
import {StepTransitivity, infinitifFieldName, Verb, VerbalInflection} from './StepTransitivity';
import StepTenseMoodPerson from './StepTenseMoodPerson';

import {GoTo} from './GoTo';

import {
  mapping, gqlGetLexicalEntryInfo, decisionTree, validationSchemas, multiWordExpressionValidationSchemas,
  _FORM, _DEFINITION, _ADDNEWDEFINITION, _PLACE, _CATEGORY, _GENDERNUMBER,
  _TRANSITIVITY, _TENSEMOODPERSON, _USAGEEXAMPLE, _SUBMIT, _EDIT_USAGEEXAMPLES
} from "./LexicalSenseEditorConst";



/**
 * The LexicalSenseEditor is a component responsible for entering the fields formWrittenRep, grammaticalCategory, definition, place, gender&number, tense&mood, register. 
 * It handles the fact that the user can edit the form written representation, and that the grammatical categories suggested can differ depending on
 * the type of the form. 
 * 
 * this component can be used in 2 differents ways depending on url (and ?allsteps or ?step=XXX query param)
 * 
 * 1) if no ?allsteps or ?step=XXX is given in url, user have to fill all the step according to the decision tree
 * 2) if ?allsteps is given the component show a list of the step that can be added/eddited, when user choose one it go to ?step=XXX
 * 3) if ?step=XXX is given the component show only this step in the form
 * 
 */
LexicalSenseEditor.propTypes = {
  /* Initial value for the form written rep. This value should not change */
  formWrittenRep: PropTypes.string,
  grammaticalCategory: PropTypes.object,
  definition: PropTypes.string,
  // will contain mood tense person transitivity gender
  lexicalEntry: PropTypes.object,
  /* Default true. If 'false', the first step is not available because it is already filled with the form and not editable */
  canEditFormWrittenRep: PropTypes.bool,

  // show the stepPlace ? should be true for adding a new definition, false for editing a definition.
  // if places already saved , the place form is not displayed in edit. places are edited from "edit places" interface instead
  showStepPlace: PropTypes.bool,

  // is the current user the creator of this sense ? if false it can only add data to it (like place or example) but should not be able to edit definition 
  canUpdate: PropTypes.bool,
  /* Handler called on click on the "cancel" button" */
  onCancel: PropTypes.func,
  isSubmitting: PropTypes.bool,
  /* 
   * Handler called on click on the submit button. The function signature is `submitHandler(formikValues, formikOptions) => void`.
   * First parameter is the `formikValues` object which contains the following properties : 
   *   - formWrittenRep
   *   - grammaticalCategory
   *   - definition
   *   - lexicalEntryTypeName
   * Second parameter is the `formikOptions` object which contains all helpers necessary to interact with the Formik form, for example
   * setFieldError() or setSubmitting()
   */
  onSubmit: PropTypes.func
};

export function LexicalSenseEditor({
  canEditFormWrittenRep = true, formWrittenRep = '',
  showStepPlace = false, canUpdate = false,
  lexicalSenseId = "", grammaticalCategory, definition = '',
  lexicalEntry = {},
  isSubmitting = false,
  onCancel, onSubmit, notificationService = appNotificationService
}) {

  invariant(canEditFormWrittenRep || formWrittenRep, "If prop 'canEditFormWrittenRep' is false, the prop 'formWrittenRep' must be provided");

  const gS = globalStyles();
  const location = useLocation();

  /* Hooks */
  const {t} = useTranslation();
  const apolloClient = useApolloClient();
  const [lexicalEntryTypeName, updateLexicalEntryTypeName] = useState();
  // if we have multiWordExpression we need to use another rules for formik validationSchemas to disable required number & gender
  const formikValidationSchema = "multiwordexpression" === lexicalEntryTypeName?.toLowerCase() ? multiWordExpressionValidationSchemas : validationSchemas;
  const [grammaticalCategorySchemeId, updateGrammaticalCategorySchemeId] = useState();

  // predefined step from url
  const {allsteps, step} = queryString.parse(location.search);

  /*
   * the order, adding 
   * word or expression   = formWrittenRep
   * definition
   * place (lieu d'usage)
   * grammaticalCategory 
   * gender & number
   * tense & mood
   * registre (courant) 
   * exemple(s) + source
   */
  const [currentStep, setCurrentStep] = useState(null); // formWrittenRep ? _DEFINITION : _FORM
  // it's easier to store previous step than computing it from decisionTree
  const [previousSteps, setPreviousSteps] = useState([]);
  // for using only one Step , disable decisionTree 
  const [useOnlyOneStep, setUseOnlyOneStep] = useState(false);
  const [showStepChooser, setShowStepChooser] = useState(allsteps === true || allsteps === "true");

  /*
  some fields of formikValues need to be removed before submitting, 
  for example a user create a definition with "conjugated verb" and save it. so it contain mood/tens/person
  then he edit it, and choose "infinitif verb" and save it. formikValues still contain mood/tens/person in submit step because user cannot remove them 
  we need to remove the fields not been used in formik based on treeDecision
  */
  let [unusedFields, setUnusedFields] = useState({...mapping})


  useEffect(() => {
    // display or not the <StepChooser/> or a specific step of the form or the whole form 
    if (allsteps === true || allsteps === "true") {
      setShowStepChooser(true);
      setUseOnlyOneStep(false);
      setCurrentStep(null);
    } else if (step) {
      setShowStepChooser(false);
      // if step is different than _FORM, _CATEGORY or _DEFINITION we use only it step
      // for _FORM , _CATEGORY or _DEFINITION we use decisionTree   
      setUseOnlyOneStep(!(step === _FORM || step === _CATEGORY || step === _DEFINITION));
      setCurrentStep(step);
    } else {
      setCurrentStep(_FORM);
      //setCurrentStep(formWrittenRep ? _DEFINITION : _FORM);
    }
    // console.log({allsteps, step, currentStep});
  }, [allsteps, step]);


  useEffect(() => {
    setUnusedFields({
      ...unusedFields,
      [currentStep]: false
    });
  }, [currentStep]);


  if (!(currentStep === null && showStepChooser === false)) { // component is not finishing initialised yet, because of useEffect
    if (!canUpdate) {
      if (!(
        showStepChooser ||
        [_PLACE].includes(currentStep)
      )) {
        if (onCancel) {
          console.log("redirected");
          console.log({allsteps, step, currentStep, showStepChooser});
          onCancel();
        } else {
          invariant(
            [_PLACE].includes(currentStep),
            'User not allowed to edit this :' + currentStep
          );
        }
      }
    } else {
      invariant(
        showStepChooser || [_FORM, _DEFINITION, _PLACE, _CATEGORY, _GENDERNUMBER, _TRANSITIVITY, _TENSEMOODPERSON].includes(currentStep),
        'Invalid value for currentStep :' + currentStep
      );
    }
  }

  /* Constants */
  const {partOfSpeech, __typename} = lexicalEntry;
  // if grammaticalCategory is not set we fill it with partOfSpeech because it is the same data
  if (!grammaticalCategory?.id && partOfSpeech?.id) {
    grammaticalCategory = partOfSpeech;
  };
  let initialValues = {formWrittenRep, grammaticalCategory, definition, ...lexicalEntry};
  const initialStatus = {autocompleteInputValues: {grammaticalCategory}};
  // if transitivity is not set use __typename to fill it
  if (!initialValues?.[infinitifFieldName] && __typename) {
    if (__typename === 'VerbalInflection') {
      initialValues[infinitifFieldName] = VerbalInflection
    } else if (__typename === 'Verb') {
      initialValues[infinitifFieldName] = Verb
    }
  }
  if (!initialValues?.grammaticalCategory?.id && __typename && (__typename === 'VerbalInflection' || __typename === 'Verb')) {
    console.warn('forcing grammaticalCategory, remove it');
    initialValues.grammaticalCategory = {
      "id": "lexinfo:verb",
      "prefLabel": "verbe",
      "__typename": "GrammaticalProperty"
    }
  }


  if (showStepChooser) {
    return <ShowWhereIAM path="src/client/components/contribution/form/LexicalSenseEditor showStepChooser ">
      <div>
        {renderFormWrittenRep({currentFormWrittenRep: formWrittenRep, originalFormWrittenRep: formWrittenRep})}
        <LexicalSenseDefinition
          formQuery={formWrittenRep}
          lexicalSenseId={lexicalSenseId}
          hideUsageExamples={true}
        />
        <GoTo formQuery={formWrittenRep} lexicalSenseId={lexicalSenseId} showAddOnly={!canUpdate} />
        <LexicalSenseCopyright />
      </div>
    </ShowWhereIAM>
  } else {
    return (
      <ShowWhereIAM path="src/client/components/contribution/form/LexicalSenseEditor formik">
        <Formik
          initialValues={initialValues}
          initialStatus={initialStatus}
          enableReinitialize
          validationSchema={formikValidationSchema[currentStep]}
          validateOnChange={false}
          validateOnBlur={false}
          onSubmit={handleSubmit}>
          {({values, status, isValid, errors}) => {
            // console.log({values, status, isValid, errors});

            const cancelLabel = t('CONTRIBUTION.CREATE_LEXICAL_SENSE.CANCEL');
            const submitLabel = t(nextStep(values) === _SUBMIT ? 'CONTRIBUTION.CREATE_LEXICAL_SENSE.SUBMIT' : 'CONTRIBUTION.CREATE_LEXICAL_SENSE.NEXT');

            return (
              <Form noValidate aria-label="Ce formulaire permet de rajouter un mot ou une expression au DDF">
                {renderFormWrittenRep({currentFormWrittenRep: values.formWrittenRep, originalFormWrittenRep: formWrittenRep})}

                {showThisStep(_FORM) && <StepForm editMode={!!formWrittenRep} values={values} />}
                {showThisStep(_DEFINITION) && <StepDefinition />}
                {showThisStep(_PLACE) && <StepPlace values={values} />}
                {showThisStep(_CATEGORY) && <StepCategory values={values} schemeId={grammaticalCategorySchemeId} />}
                {showThisStep(_TRANSITIVITY) && <StepTransitivity values={values} />}
                {showThisStep(_GENDERNUMBER) && <StepGenderNumber values={values} />}
                {showThisStep(_TENSEMOODPERSON) && <StepTenseMoodPerson values={values} />}
                {/*showThisStep(_USAGEEXAMPLE) && <StepUsageExample values={values} />*/}

                <FormBottom>
                  <div className={gS.buttonRow}>
                    <DivButton
                      type="button"
                      onClick={onCancel}
                      classesName={gS.cancelButton}
                      aria-label={cancelLabel}
                    >
                      {cancelLabel}
                    </DivButton>

                    <BracketButton
                      greyBg={true}
                      aria-label={submitLabel}
                      text={submitLabel}
                      type="submit"
                      disabled={isSubmitting}
                      loading={isSubmitting}
                    />
                  </div>
                </FormBottom>
              </Form>
            )
          }}
        </Formik>
        <br />
        <br />
        <LexicalSenseCopyright addMarginTop={true} />
      </ShowWhereIAM>
    );
  }

  /**
   * render the form and the back button if needed 
   */
  function renderFormWrittenRep({originalFormWrittenRep, currentFormWrittenRep}) {

    if (currentStep !== _FORM || currentFormWrittenRep) {

      return <div className={gS.formWrittenRep}>
        {currentFormWrittenRep}
        {((previousSteps.length > 0) && ((currentStep === _DEFINITION && canEditFormWrittenRep) || (currentStep !== _DEFINITION))) ?
          <BackArrowButton
            tooltip={true}
            onClick={() => {
              const [last, ...others] = previousSteps.reverse();
              setCurrentStep(last);
              setPreviousSteps(others.reverse());
            }}
            data-testid="edit-form-written-rep-button"
          />
          :
          <BackArrowLink to={(currentStep === null) || (notInNewForm()) ?
            formatRoute(ROUTES.FORM_LEXICAL_SENSE, {formQuery: originalFormWrittenRep, lexicalSenseId}) :
            (formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT, {formQuery: originalFormWrittenRep, lexicalSenseId}) + "?allsteps=true")} />
        }
      </div>
    } else {
      return null
    }
  }

  function notInNewForm() {
    return window?.location?.href.includes("new?form=");
  }

  async function handleSubmit(formikValues, formikOptions) {
    const {setSubmitting} = formikOptions;

    setPreviousSteps([...previousSteps, currentStep]);

    const nStep = nextStep(formikValues);
    if (nStep === _SUBMIT) {
      // remove unused (old) data from fieldsToRemove
      for (const [key, value] of Object.entries(unusedFields)) {
        if (value !== false) {
          const fieldsToRemove = unusedFields[key].split(";")
          fieldsToRemove.forEach(field => {
            delete formikValues[field];
          });
        }
      }

      onSubmit({...formikValues, lexicalEntryTypeName}, formikOptions);
    } else if (nStep === _CATEGORY) {
      setSubmitting(false);
      await toStepCategory({formWrittenRep: formikValues.formWrittenRep, formikOptions, previousLexicalEntryTypeName: lexicalEntryTypeName});
    } else {
      setCurrentStep(nStep);
      setSubmitting(false);
    }
  }


  /**
   * go to grammatical category step: 
   *
   * - fetch lexicalEntryContribInputsFromFormWrittenRep to get the type of the lexical entry, and the scheme ID for the grammatical categories
   * - If the lexical entry type has changed, reset the grammatical category value in formik state
   * - switch to step 3
   *
   *
   * @param {string} formWrittenRep
   * @param {Object} formikOptions Pass the formik options object if this is the user triggered transition from STEP1 to STEP2.
   *                                       If this is the initialization to STEP2 from the URL query param, don't pass any object
   * @param {string} previousLexicalEntryTypeName Pass the value of lexicalEntryTypeName when this function is called. This is used 
   *                                                      to compare with the new value of lexicalEntryTypeName that will be fetched during
   *                                                      the funtion call to know if the entry type has changed.
   */
  async function toStepCategory({formWrittenRep, formikOptions, previousLexicalEntryTypeName}) {
    let result = await apolloClient.query({
      query: gqlGetLexicalEntryInfo,
      variables: {formWrittenRep}
    });


    let success = !!result.data.lexicalEntryContribInputsFromFormWrittenRep;
    let {
      data: {
        lexicalEntryContribInputsFromFormWrittenRep: {
          lexicalEntryTypeName,
          grammaticalCategorySchemeId
        }
      },
      error
    } = result;

    if (error) {
      let errorMessage = GraphQLErrorHandler(getLexicalEntryInfoError, {t});
      notificationService.error(errorMessage);
    }

    if (success) {
      if (previousLexicalEntryTypeName !== lexicalEntryTypeName) {
        updateLexicalEntryTypeName(lexicalEntryTypeName);
        if (previousLexicalEntryTypeName && formikOptions) {
          resetGrammaticalCategory(formikOptions);
        }
      }
      updateGrammaticalCategorySchemeId(grammaticalCategorySchemeId);
      setCurrentStep(_CATEGORY);
    }
  }

  /**
   * @param {Object} formikOptions
   */
  function resetGrammaticalCategory({setStatus, setFieldValue, status}) {
    setFieldValue('grammaticalCategory', null, false);
    if (!status) status = {};
    if (!status.autocompleteInputValues) status.autocompleteInputValues = {};
    status.autocompleteInputValues.grammaticalCategory = '';
    setStatus(status);
  }

  // did we show this step ? 
  function showThisStep(step) {
    return currentStep === step
  }

  function nextStep(formikValues) {
    if (useOnlyOneStep) {
      return _SUBMIT;
    }
    if (currentStep) {
      return decisionTree[currentStep]({lexicalEntryTypeName, formikValues, showStepPlace});
    }
    return null;
  }
}
