import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';

const fieldName = "connotation";

const help = ``

export default function StepConnotation({persistedData}) {
  return (
    <StepTemplate title='Je rajoute une connotation' help={help}>
      <ConceptChooser
        persistedData={persistedData}
        hideInput={false}
        name={fieldName}
        placeHolder='Connotation'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + fieldName}
      />
    </StepTemplate>
  );
}


StepConnotation.propTypes = {
  values: PropTypes.shape({
    connotation: PropTypes.object
  })
};