import React from 'react';
import PropTypes from 'prop-types';
import {TextAreaField} from "../../widgets/FormV2";
import StepTemplate from './StepTemplate';

const fieldName = "definition"
const help = "Une définition présente le sens d'un mot. Elle est courte et peut intégrer des liens vers d'autres mots qui pourraient être nécessaires pour définir le sens."
/*
 in this step we add the definition 

 TextAreaField for definition
*/

export default function StepDefinition() {
  return (
    <StepTemplate title='Je rédige une nouvelle définition' help={help}>
      <TextAreaField
        name={fieldName}
        required={true}
        autoFocus
        placeholder="Définition" 
        aria-label="Saisir ici la définition du mot ou de l'expression"
      />
    </StepTemplate>
  );
}

StepDefinition.propTypes = {
  values: PropTypes.shape({
    formWrittenRep: PropTypes.string.isRequired,
    definition: PropTypes.string     
  }),
};

 