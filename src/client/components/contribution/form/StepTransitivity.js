


import React from 'react';
import PropTypes from 'prop-types';
import globalStyles from '../../../assets/stylesheets/globalStyles.js';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';
import clsx from "clsx";


const tFieldName = "verb-frame";
export const infinitifFieldName = "infinitif"

const help = null;

export const Verb = {
  "id": true,
  "prefLabel": "Infinitif",
  "definition": "C'est la forme de base du verbe"
};
export const VerbalInflection = {
  "id": false,
  "prefLabel": "Conjugué",
  "definition": "C'est une forme particulière du verbe"
};

const staticDataInfinitif = {
  "grammaticalProperties": {
    "edges": [{"node": Verb}, {"node": VerbalInflection}],
    "__typename": "GrammaticalPropertyConnection"
  }
}

export function StepTransitivity({values}) {
  const gS = globalStyles();
  return (
    <StepTemplate title='Je rajoute la transitivité ...' help={help}>
      <ConceptChooser
        hideInput={true}
        //initialValue={values?.transitivity}
        addIDontKnowConcept={true}
        name={"transitivity"}
        placeHolder='Transitivité'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + tFieldName}
      />

      <div className={clsx(gS.stepTemplateSpacer, gS.stepTemplateTitle)}>... c'est bien la forme de base du verbe ?</div>

      <ConceptChooser
        hideInput={true}
        // initialValue={values?.[infinitifFieldName]}
        name={infinitifFieldName}
        placeHolder='Infinitif ou conjugué'
        staticData={staticDataInfinitif}
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + infinitifFieldName}
      />

    </StepTemplate>
  );
}


StepTransitivity.propTypes = {
  values: PropTypes.shape({
    formWrittenRep: PropTypes.string.isRequired,
    definition: PropTypes.string,
    place: PropTypes.object,
    grammaticalCategory: PropTypes.object.isRequired,
    transitivity: PropTypes.object,
    infinitif: PropTypes.object
  })
};