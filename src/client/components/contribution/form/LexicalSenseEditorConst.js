import * as Yup from 'yup';
import {gql} from "@apollo/client";

export const gqlGetLexicalEntryInfo = gql`
  query GetLexicalEntryInfo($formWrittenRep: String!) {
    lexicalEntryContribInputsFromFormWrittenRep(formWrittenRep: $formWrittenRep) { 
      lexicalEntryTypeName
      grammaticalCategorySchemeId
    }
  }
`;

// to avoid syntax error
export const _FORM = "_FORM";
export const _DEFINITION = "_DEFINITION";
export const _ADDNEWDEFINITION = "_ADDNEWDEFINITION";
export const _CATEGORY = "_CATEGORY";
export const _GENDERNUMBER = "_GENDERNUMBER";
export const _TRANSITIVITY = "_TRANSITIVITY";
export const _TENSEMOODPERSON = "_TENSEMOODPERSON";

export const _SUBMIT = "_SUBMIT";
// for one place
export const _PLACE = "_PLACE";
// for showing the list of all places to edit/delete one 
export const _EDIT_PLACES = "_EDIT_PLACES";

// for adding one usage and example
export const _USAGEEXAMPLE = "_USAGEEXAMPLE";
// for showing the list of all usage and example to edit one
export const _EDIT_USAGEEXAMPLES = "_EDIT_USAGEEXAMPLES";

export const _DOMAIN = "_DOMAIN";
export const _EDIT_DOMAINS = "_DOMAINS";

export const _TEMPORALITY = "_TEMPORALITY";
export const _EDIT_TEMPORALITIES = "_EDIT_TEMPORALITIES";

export const _REGISTER = "_REGISTER";
export const _EDIT_REGISTERS = "_EDIT_REGISTERS";

export const _CONNOTATION = "_CONNOTATION";
export const _EDIT_CONNOTATIONS = "_EDIT_CONNOTATIONS";

export const _FREQUENCY = "_FREQUENCY";
export const _EDIT_FREQUENCIES = "_EDIT_FREQUENCIES";

export const _GRAMMATICAL_CONSTRAINT = "_GRAMMATICAL_CONSTRAINT";
export const _EDIT_GRAMMATICAL_CONSTRAINTS = "_EDIT_GRAMMATICAL_CONSTRAINTS";

export const _TEXTUALGENRE = "_TEXTUALGENRE";
export const _EDIT_TEXTUALGENRES = "_EDIT_TEXTUALGENRES";

export const _SOCIOLECT = "_SOCIOLECT";
export const _EDIT_SOCIOLECTS = "_EDIT_SOCIOLECTS";

export const _GLOSSARY = "_GLOSSARY";
export const _EDIT_GLOSSARIES = "_EDIT_GLOSSARIES";

export const _SEMANTIC_RELATION = "_SEMANTIC_RELATIONS";
export const _EDIT_SEMANTIC_RELATIONS = "_EDIT_SEMANTIC_RELATIONS";

const _REQUIRED_FIELD = "Champ obligatoire";


export const validationSchemas = {
  _FORM: Yup.object().shape({
    formWrittenRep: Yup.string().required(_REQUIRED_FIELD)
  }),
  _DEFINITION: Yup.object().shape({
    definition: Yup.string().required(_REQUIRED_FIELD)
  }),
  _PLACE: Yup.object().shape({
    place: Yup.object().required(_REQUIRED_FIELD)
  }),
  _CATEGORY: Yup.object().shape({
    grammaticalCategory: Yup.object().required(_REQUIRED_FIELD),
  }),
  _GENDERNUMBER: Yup.object().shape({
    gender: Yup.object().required(_REQUIRED_FIELD),
    number: Yup.object().required(_REQUIRED_FIELD),
  }),
  _TRANSITIVITY: Yup.object().shape({
    transitivity: Yup.object().required(_REQUIRED_FIELD),
    infinitif: Yup.object().required(_REQUIRED_FIELD)
  }),
  _TENSEMOODPERSON: Yup.object().shape({
    tense: Yup.object().required(_REQUIRED_FIELD),
    mood: Yup.object().required(_REQUIRED_FIELD),
    person: Yup.object().required(_REQUIRED_FIELD)
  }),
  _REGISTER: Yup.object().shape({
    register: Yup.object().required(_REQUIRED_FIELD)
  }),
  _USAGEEXAMPLE: Yup.object().shape({
    value: Yup.string(),
    bibliographicalCitation: Yup.string()
  }),
  _DOMAIN: Yup.object().shape({
    domain: Yup.object().required(_REQUIRED_FIELD)
  }),
  _TEMPORALITY: Yup.object().shape({
    temporality: Yup.object().required(_REQUIRED_FIELD)
  }),
  _FREQUENCY: Yup.object().shape({
    frequency: Yup.object().required(_REQUIRED_FIELD)
  }),
  _GRAMMATICAL_CONSTRAINT: Yup.object().shape({
    grammaticalConstraint: Yup.object().required(_REQUIRED_FIELD)
  }),
  _TEXTUALGENRE: Yup.object().shape({
    textualGenre: Yup.object().required(_REQUIRED_FIELD)
  }),
  _CONNOTATION: Yup.object().shape({
    connotation: Yup.object().required(_REQUIRED_FIELD)
  }),
  _SOCIOLECT: Yup.object().shape({
    sociolect: Yup.object().required(_REQUIRED_FIELD)
  }),
  _GLOSSARY: Yup.object().shape({
    glossary: Yup.object().required(_REQUIRED_FIELD)
  }),
  _SEMANTIC_RELATION: Yup.object().shape({
    formWrittenRep: Yup.string().required(_REQUIRED_FIELD),
    semanticRelation: Yup.object().required(_REQUIRED_FIELD)
  }),
};

export const multiWordExpressionValidationSchemas = {
  ...validationSchemas,
  // overload gender and number to be not required
  _GENDERNUMBER: Yup.object().shape({
    gender: Yup.object(),
    number: Yup.object(),
  }),

};


export const mapping = {
  _DEFINITION: "definition",
  _PLACE: "place",
  _DOMAIN: "domain",
  _GENDERNUMBER: "gender;number",
  _TRANSITIVITY: "transitivity",
  _TENSEMOODPERSON: "tense;mood;person",
  _REGISTER: "register",
  _USAGEEXAMPLE: "usageExample",
}


/**
 * the keys of this object are the current step.
 * each key value return the next step according to other parameters ( often choosed by user )
 * _SUBMIT is the end of the tree
 */
export const decisionTree = {
  _FORM: () => _DEFINITION,
  _DEFINITION: ({showStepPlace}) => {
    if (showStepPlace) {
      return _PLACE;
    } else {
      return _CATEGORY;
    }
  },
  _PLACE: () => _CATEGORY,
  _CATEGORY: ({lexicalEntryTypeName, formikValues}) => {
    /*
    console.log({
      a: lexicalEntryTypeName.toLowerCase(),
      b: formikValues?.grammaticalCategory?.id?.toLowerCase(),
      formikValues
    }) 
    */

    switch (lexicalEntryTypeName.toLowerCase()) {
      case "word":
        switch (formikValues?.grammaticalCategory?.id?.toLowerCase()) {
          case "lexinfo:verb":
            return _TRANSITIVITY;
          case "lexinfo:adverb":
          case "lexinfo:conjunction":
          case "lexinfo:interjection":
          case "lexinfo:particle":
            return _SUBMIT;
          case "lexinfo:possessiveAdjective":
          case "lexinfo:definiteArticle":
          case "lexinfo:partitiveArticle":
          case "lexinfo:demonstrativePronoun":
          case "lexinfo:indefinitePronoun":
          case "lexinfo:interrogativePronoun":
          case "lexinfo:personalPronoun":
          case "lexinfo:possessivePronoun":
          case "lexinfo:relativePronoun":
          case "lexinfo:adjective":
          case "lexinfo:article":
          case "lexinfo:noun":
          case "lexinfo:properNoun":
          case "lexinfo:preposition":
          case "lexinfo:pronoun":
          case "lexinfo:verb":
          default:
            return _GENDERNUMBER;
        }
      case "multiwordexpression": // locution
        return _GENDERNUMBER; // NEW 03/03/23 , avant multiwordexpression n'avait pas _GENDERNUMBER comme next step. submit directement apres
      case "affix":
      default:
        return _SUBMIT;
    }
  },

  _TRANSITIVITY: ({formikValues}) => {
    if (formikValues?.infinitif?.id === true) {
      return _SUBMIT;
    } else {
      return _TENSEMOODPERSON;
    }
  },
  _GENDERNUMBER: () => _SUBMIT,
  _TENSEMOODPERSON: () => _SUBMIT,
  _USAGEEXAMPLE: () => _SUBMIT
};
