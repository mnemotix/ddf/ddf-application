


import React from 'react';

import globalStyles from '../../../assets/stylesheets/globalStyles.js';
import {TextAreaField} from "../../widgets/FormV2";
import StepTemplate from './StepTemplate';


const help = "L'exemple est une présentation des emplois réels du mot dans un discours. Il sert à prouver que le mot est effectivement utilisé par des locuteurs francophones et à montrer dans quels contextes il est utilisé. Les exemples peuvent provenir de toutes sortes de sources (livres, chansons, émissions, etc.). ";

export default function StepUsageExample({title = 'Je rajoute un exemple et une source'}) {

  const gS = globalStyles();

  return (
    <StepTemplate title={title} help={help}>

      <TextAreaField
        name={'usageExample.value'}
        required={true}
        autoFocus
        placeholder="Entrer l'exemple"
        aria-label="Saisir ici l'exemple"
      />
      <div className={gS.stepTemplateSpacer} />

      <TextAreaField
        name={'usageExample.bibliographicalCitation'}
        required={true}
        autoFocus
        placeholder="Indiquer la source de l'exemple"
        aria-label="Saisir ici la source"
      />

    </StepTemplate>
  );
}


StepUsageExample.propTypes = {

};