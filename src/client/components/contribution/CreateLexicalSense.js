
import React, {useContext} from 'react';
import {useTranslation} from 'react-i18next';
import {useMutation, gql} from "@apollo/client";
import {formatRoute} from 'react-router-named-routes';
import {useHistory, useLocation} from 'react-router-dom';
import queryString from 'query-string';
import {MetaSEO} from "../../components/widgets/MetaSEO";
import {ROUTES} from '../../routes';
import {SimpleModalPage} from '../../layouts/mobile/SimpleModalPage';
import {DesktopMainLayout} from '../../layouts/desktop/DesktopMainLayout';
import {DesktopSubHeader} from '../../layouts/desktop/DesktopSubHeader';
import {useMediaQueries} from '../../layouts/MediaQueries';
import {notificationService as appNotificationService} from '../../services/NotificationService';
import {useBrowsingHistoryHelperService} from '../../services/BrowsingHistoryHelperService';
import {GraphQLErrorHandler} from '../../services/GraphQLErrorHandler';
import {UserContext} from "../../hooks/UserContext";
import {LexicalSenseEditor} from './form/LexicalSenseEditor';
import globalStyles from '../../assets/stylesheets/globalStyles';


export const gqlCreateLexicalSense = gql`
  mutation CreateLexicalSense(
    $definition: String!,
    $formWrittenRep: String!,
    $lexicalEntryTypeName: String!,
    $grammaticalCategoryId: String,
    $transitivityId: String,
    $moodId: String,
    $tenseId: String,
    $personId: String,
    $genderId: String,
    $numberId: String,
    $placeId: String!
  ) {
    createLexicalSense(
      input: {
        definition: $definition
        formWrittenRep: $formWrittenRep
        lexicalEntryTypeName: $lexicalEntryTypeName
        grammaticalCategoryId: $grammaticalCategoryId
        transitivityId: $transitivityId
        moodId: $moodId
        tenseId: $tenseId
        personId: $personId
        genderId: $genderId
        numberId: $numberId 
        placeId: $placeId
      }
    ) {
      createdObject {
          id
      }
    }
  }
`;


/**
 * This component requires a user to be logged. Otherwise it will redirect to the login page.
 *
 * The CreateLexicalSense screen uses the LexicalSenseEditor in the following way : 
 *
 * - By default, the editor is empty
 * - If the URL has the querystring parameter "form", the editor is initialized with this value as formWrittenRep
 * - On submit, the mutation to create a lexical sense is run
 */
export function CreateLexicalSense(props) {
  const gS = globalStyles();
  /* Services DI */
  const notificationService = props.notificationService || appNotificationService;

  /* Hooks */
  const {user} = useContext(UserContext);

  const {t} = useTranslation();
  const history = useHistory();
  const location = useLocation();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();
  const [createLexicalSenseMutation, {loading: isSubmitting}] = useMutation(gqlCreateLexicalSense);
  const {isMobile} = useMediaQueries();


  /** Initializing state from query string params */
  const parsedQueryString = queryString.parse(location.search);
  let initialFormWrittenRep = parsedQueryString.form;

  if (!user?.isLogged) {
    return null;
  } else {
    return isMobile ? renderMobile() : renderDesktop();
  }

  function renderMobile() {
    return (
      <SimpleModalPage
        title={t('CONTRIBUTION.CREATE_LEXICAL_SENSE.TITLE')}
        content={renderContent()}
        titleBgColor="purple"
      />
    );
  }

  function renderDesktop() {
    return (
      <DesktopMainLayout useDefaultGrid>
        <DesktopSubHeader
          primaryTitle={t('CONTRIBUTION.DESKTOP_TITLE')}
          secondaryTitle={t('CONTRIBUTION.CREATE_LEXICAL_SENSE.TITLE')}
          desktopHeaderPrimaryTitleColor="purple"
        />
        <div className={gS.SMPcontent}>
          {renderContent()}
        </div>
      </DesktopMainLayout>
    );
  }

  function renderContent() {
    return (
      <React.Fragment>
        <MetaSEO
          title={t('DOCUMENT_TITLES.CREATE_LEXICAL_SENSE')}
        />
        <LexicalSenseEditor
          canEditFormWrittenRep={true}
          formWrittenRep={initialFormWrittenRep}
          onSubmit={handleSubmit}
          onCancel={navigateBack}
          isSubmitting={isSubmitting}
          showStepPlace={true}
          canUpdate={true}
        />
      </React.Fragment>
    );
  }

  async function handleSubmit(formikValues, formikOptions) {

    const {formWrittenRep, definition, lexicalEntryTypeName} = formikValues;

    try {
      let variables = {
        formWrittenRep,
        definition,
        lexicalEntryTypeName,
        ...mergeVariablesWithFormikValues({formikValues})
      }

      let result = await createLexicalSenseMutation({variables});
      let newSense = result.data?.createLexicalSense?.createdObject;
      /* Build route to the newly created sense from the returned data */
      let senseUrl = formatRoute(ROUTES.FORM_LEXICAL_SENSE, {
        formQuery: formWrittenRep,
        lexicalSenseId: newSense.id
      });
      history.push(senseUrl, {showGreeting: true});
    } catch (error) {
      let {globalErrorMessage} = GraphQLErrorHandler(error, {formikOptions, t});
      notificationService.error(globalErrorMessage);
    } finally {
      formikOptions.setSubmitting(false);
    }
  }

  function navigateBack() {
    let locObject = browsingHistoryHelperService.getBackFromSecondaryScreenLocation();
    history.push(locObject?.location || '/');
  }
}


/**
 * Create a new object 'variables' who is returned,
 * Take non null values of 'possibleFields' from 'formikValues' and add them to 'variables'
 * if 'initialVariables' is set, it add them only if values of 'possibleFields' are different 
 * exemple : it add 'gender' in 'variables' only if initialVariables.gender !== formikValues.gender
 * it avoid to send already saved data to mutation
 * @param {object} formikValues values returned by formik after user choices
 * @param {object} initialVariables values saved from previous contribution
 * @returns {object} variables
 */
export function mergeVariablesWithFormikValues({formikValues, initialVariables = {}}) {
  const gramPossibleFields = ["grammaticalCategory", "transitivity", "gender", "number", "tense", "mood", "person"];
  function addGramFields({variables, formikValues}) {
    for (let value of gramPossibleFields) {
      if (formikValues?.[value]?.id) {
        variables[value + "Id"] = formikValues?.[value]?.id
      }
    }
  }
  let variables = {};
  // possible fields for input in src/server/datamodel/ontologies/ontolex/graphql/LexicalSenseGraphQLDefinition.js resolver
  const possibleFields = ["register", "place", "usageExample"];
  for (let value of possibleFields) {
    if (formikValues?.[value]?.id && initialVariables?.[value]?.id != formikValues?.[value]?.id) {
      variables[value + "Id"] = formikValues?.[value]?.id
    }
  }

  const differentGramCat = formikValues?.grammaticalCategory?.id != initialVariables?.grammaticalCategory?.id;
  let gramCatChanged = false;
  // then add the grammatical Fields
  for (let value of gramPossibleFields) {
    if (formikValues?.[value]?.id !== initialVariables?.[value]?.id) {
      gramCatChanged = true;
    }
  }
  if (gramCatChanged || differentGramCat) {
    addGramFields({variables, formikValues});
  }

  return variables;
}
