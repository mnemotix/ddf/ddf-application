import React, {useEffect, useContext} from "react";
import {useTranslation} from "react-i18next";
import {useHistory} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import {MetaSEO} from "../../widgets/MetaSEO";
import {SimpleModalPage} from "../../../layouts/mobile/SimpleModalPage";
import {DesktopMainLayout} from "../../../layouts/desktop/DesktopMainLayout";
import {DesktopSubHeader} from "../../../layouts/desktop/DesktopSubHeader";
import {useMediaQueries} from "../../../layouts/MediaQueries";
import {notificationService as appNotificationService} from "../../../services/NotificationService";
import {GraphQLErrorHandler} from "../../../services/GraphQLErrorHandler";
import {UserContext} from "../../../hooks/UserContext";
import globalStyles from "../../../assets/stylesheets/globalStyles.js";
import {ROUTES} from '../../../routes';
import {BackArrowLink} from "../../widgets/BackArrowLink";
import {BackArrowButton} from "../../widgets/BackArrowButton";
/**
 * show mobile/desktop html body around content
 * @param {bool}  showFormWrittenRep
 * @param {function} backArrowOnClick if not null, backArrowLink is replaced by BackArrowButton 
 * @returns 
 */
export function Template({error, children, title = 'CONTRIBUTION.EDIT_LEXICAL_SENSE_TITLE', showDesktopSubHeader = true,
  lexicalSenseId, formWrittenRep, showFormWrittenRep = false, backArrowOnClick = false, ...props}) {

  const gS = globalStyles();
  const {t} = useTranslation();
  const {user} = useContext(UserContext);
  const {isMobile} = useMediaQueries();
  let history = useHistory();

  const notificationService = props.notificationService || appNotificationService;

  useEffect(() => {
    if (error) {
      console.log("Template error", error)
      let {globalErrorMessage} = GraphQLErrorHandler(error, {t});
      notificationService.error(globalErrorMessage);
    }
  }, [error]);

  if (!user?.isLogged) {
    return null;
  } else {
    return isMobile ? renderMobile() : renderDesktop();
  }

  function renderMobile() {
    return (
      <SimpleModalPage
        title={t(title)}
        content={renderContent()}
        titleBgColor="yellow"
        onBack={history.goBack}
        onClose={() => history.push("/")}
        controls={['back', 'close']}
      />
    );
  }

  function renderDesktop() {
    return (
      <DesktopMainLayout useDefaultGrid>

        {showDesktopSubHeader &&
          <DesktopSubHeader
            primaryTitle={t('CONTRIBUTION.DESKTOP_TITLE')}
            secondaryTitle={t(title)}
            desktopHeaderPrimaryTitleColor="yellow"
          />
        }

        <div className={gS.SMPcontent}>
          {renderContent()}
        </div>
      </DesktopMainLayout>
    );
  }

  function renderContent() {
    return (
      <React.Fragment>
        <MetaSEO title={t('DOCUMENT_TITLES.EDIT_LEXICAL_SENSE', {formWrittenRep})} />
        {showFormWrittenRep && lexicalSenseId &&
          <div className={gS.formWrittenRep}>
            {formWrittenRep}
            {backArrowOnClick ?
              <BackArrowButton onClick={backArrowOnClick} />
              :
              <BackArrowLink to={formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT, {formQuery: formWrittenRep, lexicalSenseId}) + "?allsteps=true"} />
            }
          </div>
        }
        {children}
      </React.Fragment>
    );
  }



}


