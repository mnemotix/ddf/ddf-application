import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {gql} from "@apollo/client";
import Config from '../../../Config';
import {PurifyHtml} from '../../../utilities/PurifyHtml';
import {validationSchemas, _USAGEEXAMPLE} from "../form/LexicalSenseEditorConst";
import StepUsageExample from '../form/StepUsageExample';
import {LinkEdit} from "./LinkEdit";


const useStyles = makeStyles((theme) => ({
  source: {
    color: Config.colors.darkgray,
    fontSize: Config.fontSizes.sm,
    paddingTop: Config.spacings.tiny,
    fontStyle: 'italic'
  }
}));

export function EditLexicalSenseUsageExemples(props) {

  const classes = useStyles();
  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier un exemple"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="usageExamples.edges"
      gqlUpdate={gqlUpdateUsageExemple}
      fieldnameForDelete="usageExample"
      fieldnameForEdit="usageExample"
      addLabel="un exemple"
      noDataText="Cette définition n'a pas encore d'exemple"
      customDataText="Vous pouvez modifier ou ajouter un exemple."
      itemToString={(selectedItem) => selectedItem.value}
      renderItemLabel={renderItemLabel}
      toStep={_USAGEEXAMPLE}
      enableEdit={true}
      enableLocalAdd={true}
      formikValidationSchema={validationSchemas._USAGEEXAMPLE}
      formikInitializeValue={(selectedItem) => {
        return {
          usageExample: {
            "id": selectedItem.id,
            "value": selectedItem?.value || "",
            "bibliographicalCitation": selectedItem?.bibliographicalCitation || ""
          }
        };
      }}
      formikRenderStep={(persistedData) => <StepUsageExample title="Modifier l'exemple" persistedData={persistedData} />}
    />
  )
  function renderItemLabel(usageExample) {
    return <div>
      <i><PurifyHtml as="span" html={usageExample.value} /></i>
      <If condition={usageExample.bibliographicalCitation}>
        <div className={classes.source}>
          <PurifyHtml as="span" html={usageExample.bibliographicalCitation} />
        </div>
      </If>
    </div>
  }
}



const gqlLexicalSenseQuery = gql`
  query LexicalSenseUsages_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      usageExamples {
        edges {
          node {
            id
            bibliographicalCitation
            value
            canUpdate
          }          
        }        
      }       
    }
  }
`;


const gqlUpdateUsageExemple = gql`
 mutation UpdateLexicalSense_usages(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $usageExample: UsageExampleInput, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      usageExample: $usageExample 
    }) {
      updatedObject {
        id
        usageExamples {
          edges {
            node {
              id
              bibliographicalCitation
              value
            }
          }
        }
      }
    }
  }
`;


