import React from 'react';
import {gql} from "@apollo/client";
import {validationSchemas, _GRAMMATICAL_CONSTRAINT} from "../form/LexicalSenseEditorConst";
import StepGrammaticalConstraint from '../form/StepGrammaticalConstraint';
import {LinkEdit} from "./LinkEdit";

export function EditLexicalSenseGrammaticalconstraints(props) {
  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier une contrainte grammmaticale"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="grammaticalConstraints.edges"
      gqlDelete={gqlDeleteLexicalSense}
      gqlUpdate={gqlUpdateGrammaticalConstraint}
      fieldnameForDelete="grammaticalConstraintId"
      enableDelete={true}
      allowAnyOneToDeleteLink_until_reification={true}
      noDataText="Cette définition n'a pas encore de contrainte grammmaticale"
      itemToString={(selectedItem) => selectedItem.prefLabel}
      addLabel="une contrainte grammmaticale"
      toStep={_GRAMMATICAL_CONSTRAINT}
      enableLocalAdd={true}
      enableEdit={false}
      fieldnameForEdit="grammaticalConstraint"
      formikValidationSchema={validationSchemas._GRAMMATICAL_CONSTRAINT}
      formikRenderStep={(persistedData) => <StepGrammaticalConstraint title="Modifier un argot" persistedData={persistedData} />}
    />
  )

}



const gqlLexicalSenseQuery = gql`
  query LexicalSenseGrammConstraints_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      grammaticalConstraints {
        edges {
          node {
            id
            prefLabel
            definition        
          }          
        }        
      }       
    }
  }
`;


const gqlUpdateGrammaticalConstraint = gql`
 mutation UpdateLexicalSense_grammaticalConstraint(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $grammaticalConstraintId: String!, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      grammaticalConstraintId: $grammaticalConstraintId
    }) {
      updatedObject {
        id         
      }
    }
  }
`;


const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_grammaticalConstraint( $lexicalSenseId: ID!,  $writtenRep: String!, $grammaticalConstraintId: String! ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        grammaticalConstraintIdToDelete : $grammaticalConstraintId
      }
    ) {
      updatedObject {
        id      
       }
    }
  }
 `;