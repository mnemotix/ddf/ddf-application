import React from 'react';
import {gql} from "@apollo/client";
import {ROUTES} from '../../../routes';
import {validationSchemas, _GLOSSARY} from "../form/LexicalSenseEditorConst";
import StepGlossary from '../form/StepGlossary.js';
import {LinkEdit} from "./LinkEdit";



export function EditLexicalSenseGlossaries(props) {

  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier un lexique"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="glossaries.edges"
      gqlDelete={gqlDeleteLexicalSense}
      gqlUpdate={gqlUpdateGlossary}
      fieldnameForDelete="glossaryId"
      enableDelete={true}
      allowAnyOneToDeleteLink_until_reification={true}
      noDataText="Cette définition n'a pas encore de lexique"
      itemToString={(selectedItem) => selectedItem.prefLabel}
      addLabel="un lexique"
      toStep={_GLOSSARY}
      enableLocalAdd={true}
      enableEdit={false}
      fieldnameForEdit="glossary"
      formikValidationSchema={validationSchemas._GLOSSARY}
      formikRenderStep={(persistedData) => <StepGlossary title="Modifier le lexique" persistedData={persistedData} />}
    />
  )
}



const gqlLexicalSenseQuery = gql`
  query LexicalSenseGlossaries_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      glossaries {
        edges {
          node {
            id
            prefLabel
            definition
          }          
        }        
      }       
    }
  }
`;


const gqlUpdateGlossary = gql`
 mutation UpdateLexicalSense_glossary(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $glossaryId: String!, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      glossaryId: $glossaryId
    }) {
      updatedObject {
        id         
      }
    }
  }
`;


const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_glossary( $lexicalSenseId: ID!,  $writtenRep: String!, $glossaryId: String! ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        glossaryIdToDelete : $glossaryId
      }
    ) {
      updatedObject {
        id      
       }
    }
  }
 `;