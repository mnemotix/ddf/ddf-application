import React from 'react';
import {gql} from "@apollo/client";
import {LinkEdit} from "./LinkEdit";
import {ROUTES} from '../../../routes';
import {ShowWhereIAM} from '../../widgets/ShowWhereIAM';
import {LexicalSenseDistinctSemanticRelationList} from "../../LexicalSense/LexicalSenseDistinctSemanticRelationList";
import {gqlLexicalSenseSemanticRelationTypeFragment} from '../../../utilities/helpers/lexicalSenseSemanticRelationList';
import {gqlLexicalSenseQuery} from "../EditLexicalSense";

export function EditLexicalSenseSemanticRelations(props) {

  return (
    <ShowWhereIAM path="/contribution/LinkEdit/EditLexicalSenseSemanticRelations.js">
      <LinkEdit
        {...props}
        templateTitle="Modifier une relation sémantique"
        gqlQuery={gqlLexicalSenseQuery}
        customGqlVariables={{excludeDistinctWrittenRep: false, filterOnDistinctWrittenRep: false}}
        gqlConnectionPath="semanticRelations.edges"
        gqlDelete={gqlDeleteSemanticRelation}
        fieldnameForDelete="semanticRelationId"
        addLabel="une relation sémantique"
        addLinkRoute={ROUTES.FORM_LEXICAL_SENSE_ADD_SEMANTIC_RELATION}
        noDataText="Cette définition n'a pas encore de relation sémantique"
        itemToString={(selectedItem) => selectedItem?.prefLabel || selectedItem?.writtenRep || selectedItem?.label}        
        customRenderDatas={customRenderDatas}
        enableDelete={true}
        showDeleteIconInLexicalSenseDefinition={true}
      />
    </ShowWhereIAM>
  )

  function customRenderDatas({formWrittenRep, lexicalSenseId, setConfirmToDelete}) {
    return <LexicalSenseDistinctSemanticRelationList
      lexicalSenseId={lexicalSenseId}
      formQuery={formWrittenRep}
      setConfirmToDelete={setConfirmToDelete}
      showGlossaries={false}
    />
  }
}


const gqlDeleteSemanticRelation = gql`
  mutation DeleteLexicalSense_semanticRelation( $semanticRelationId: ID!) {
    removeEntity( input: { objectId: $semanticRelationId } ) {
      deletedId
    }
  }
`;


/*
const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_semanticRelation( $lexicalSenseId: ID!,  $writtenRep: String!, $semanticRelationId: String! ,$includeCreator: Boolean = true) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        semanticRelationIdToDelete :$semanticRelationId
      }
    ) {
      updatedObject {
        id       
        semanticProperties {
          edges {
            node {
              id
              prefLabel           
            }
          }
        }
        ...LexicalSenseSemanticRelationTypeFragment 

      }
    }
  }
  ${gqlLexicalSenseSemanticRelationTypeFragment}
`;
*/