import React from 'react';
import {gql} from '@apollo/client';

import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import {formatRoute} from 'react-router-named-routes';
import {Link} from 'react-router-dom';

import {useTranslation} from 'react-i18next';
import {makeStyles} from '@material-ui/core/styles';
import Config from '../../../Config';
import {ROUTES} from '../../../routes';
import {TableView} from '../../widgets/Table/';
import {apolloRemoveNodeCacheUpdate} from '../../../utilities/apolloRemoveNodeCacheUpdate';
import {Actions} from './Actions';
import LexicalSenseStatus from "../../LexicalSense/LexicalSensStatus";
import validIcon from '../../../assets/images/signs/checkmark_boxed_green.svg';
import reportingsIcon from '../../../assets/images/signs/exclamation_orange.svg';

export const useStyles = makeStyles((theme) => ({
  icon: {
    height: "20px"
  },
  iconSmall: {
    height: "18px"
  },
  orangeText: {
    fontWeight: Config.fontWeights.bold,
    color: Config.colors.orange
  },
  actionsColumn: {
    margin: 'auto'
  }
}));



export const gqlContributionsQuery = gql`
  query ContributionsList(
    $first: Int
    $after: String
    $filterOnLoggedUser: Boolean!
    $filterOnUserAccountId: ID
    $sortings: [SortingInput]
    $filterOnReviewed: Boolean
    $filterOnExistingSuppressionRating: Boolean
    $filterOnProcessed: Boolean
  ) {
    contributedLexicalSensesCount  (      
      filterOnLoggedUser: $filterOnLoggedUser
      filterOnUserAccountId: $filterOnUserAccountId      
      filterOnReviewed: $filterOnReviewed
      filterOnExistingSuppressionRating: $filterOnExistingSuppressionRating
      filterOnProcessed: $filterOnProcessed
    )
    contributedLexicalSenses(
      first: $first
      after: $after
      filterOnLoggedUser: $filterOnLoggedUser
      filterOnUserAccountId: $filterOnUserAccountId
      sortings: $sortings
      filterOnReviewed: $filterOnReviewed
      filterOnExistingSuppressionRating: $filterOnExistingSuppressionRating
      filterOnProcessed: $filterOnProcessed
    ) {
      edges {
        node {
          id
          definition
          createdAt
          createdBy
          canonicalFormWrittenRep
          reviewerGroupsCount
          processed
          validationRatingsCount
          suppressionRatingsCount
          reportingRatingsCount
          reportingRating {
            id
          }
          validationRating {
            id
          }
          suppressionRating {
            id
          }
        }
      }    
    }
  } 
`;

export function ContributionsListTable({hideColumns, reloadRef, ...props}) {
  const {t} = useTranslation();
  const classes = useStyles();

  return (
    <TableView
      showCheckbox={true}
      columns={createColumns()}
      gqlQuery={gqlContributionsQuery}
      gqlCountPath="contributedLexicalSensesCount"
      gqlConnectionPath="contributedLexicalSenses"
      ariaDescribedby={'Tableau regroupant toutes les contributions, les colonnes sont : date, forme, définition, état et signalement'}
      {...props}
      ref={reloadRef}
      colIndexForLoader={2}
    />
  )


  function createColumns() {
    let columns = [{
      ariaLabel: 'Trier le tableau par date',
      label: t('CONTRIBUTIONS_LIST.TABLE.HEADERS.DATE'),
      sortFor: 'createdAt',
      width: 3,
      customRender: (lexicalSense) => {
        return dayjs(lexicalSense.createdAt).format('L')
      }
    }, {
      label: t('CONTRIBUTIONS_LIST.TABLE.HEADERS.FORM'),
      width: 4,
      customRender: (lexicalSense) => {
        return <Link role="link" aria-label={"Lien vers la definition de " + lexicalSense.canonicalFormWrittenRep}
          to={formatRoute(ROUTES.FORM_LEXICAL_SENSE, {
            formQuery: lexicalSense.canonicalFormWrittenRep,
            lexicalSenseId: lexicalSense.id,
          })}
        >
          {lexicalSense.canonicalFormWrittenRep}
        </Link>
      }
    }, {
      label: t('CONTRIBUTIONS_LIST.TABLE.HEADERS.EDITED'),
      width: 12,
      customRender: (lexicalSense) => {
        return lexicalSense.definition
      }
    },
    {
      width: 2,
      ariaLabel: "Trier le tableau par status validées",
      title: t('CONTRIBUTIONS_LIST.TABLE.HEADERS.VALID_STATUS_TOOLTIP'),
      icon: <img className={classes.icon} src={validIcon} alt="Validé" />,
      sortFor: 'validationRatingCount',
      customRender: (lexicalSense) => {
        return lexicalSense?.validationRatingsCount > 0 ? lexicalSense.validationRatingsCount : null
      }
    },
    {
      ariaLabel: "Trier le tableau par nombre de signalement",
      title: t('CONTRIBUTIONS_LIST.TABLE.HEADERS.SIGNALED_TOOLTIP'),
      icon: <img className={classes.iconSmall} src={reportingsIcon} alt="signalé" />,
      sortFor: 'reportingRatingCount',
      colContentClassName: classes.orangeText,
      customRender: (lexicalSense) => {
        return lexicalSense?.reportingRatingsCount > 0 ? lexicalSense.reportingRatingsCount : null
      }
    },
    {
      width: 2,
      ariaLabel: "Trier le tableau par status relues ou non",
      label: t('CONTRIBUTIONS_LIST.TABLE.HEADERS.REREADED'),
      sortFor: 'validationRatingCount',
      customRender: (lexicalSense) => {
        return <LexicalSenseStatus lexicalSense={lexicalSense} />
      }
    },
    ];

    if (!hideColumns?.includes('person')) {
      columns.splice(3, 0, {
        width: 2,
        label: t('CONTRIBUTIONS_LIST.TABLE.HEADERS.AUTHOR'),
        customRender: (lexicalSense) => {
          return <>
            {lexicalSense.createdBy}<br />
            {/* <PersonAccountLabel userAccount={lexicalSense.creatorPerson?.userAccount} /> */}
          </>
        }
      });
    }
    if (!hideColumns?.includes('actions')) {
      columns.push({
        width: 3,
        label: t('CONTRIBUTIONS_LIST.TABLE.HEADERS.ACTIONS'),
        colContentClassName: classes.actionsColumn,
        customRender: (lexicalSense) => {
          return renderLexicalSenseActions(lexicalSense, reloadRef)
        }
      });
    }

    return columns
  }

  function renderLexicalSenseActions(lexicalSense, reloadRef) {
    return (
      <Actions
        lexicalSense={lexicalSense}
        reloadRef={reloadRef}
      />
    );
  }
}







ContributionsListTable.propTypes = {
  /**
   * Page size of results, for pagination. Defaults to 20
   */
  pageSize: PropTypes.number,
  /**
   * update selected ids in parent for action
   */
  onSelectedIDsUpdate: PropTypes.func,
  /**
   * If you want to mask some columns, put the column names in this array (default, all columns are displayed).
   * Possible values : 'person', 'actions'
   */
  hideColumns: PropTypes.arrayOf(PropTypes.oneOf(['person', 'actions'])),
  /**
   *  filters with  filterOnLoggedUser, filterOnUserAccountId, filterOnReviewed, filterOnExistingSuppressionRating, filterOnProcessed
   */
  filters: PropTypes.object
};

