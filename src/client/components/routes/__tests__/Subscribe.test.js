import React from 'react';
import {cleanup, getNodeText, fireEvent} from '@testing-library/react';
import {act} from 'react-dom/test-utils';
import waait from 'waait';
import {GraphQLErrorResponses} from '@mnemotix/synaptix.js/testing';

import {Subscribe} from '../Subscribe';
import {gqlRegisterUserAccountMutation} from '../../../services/UserAuthenticationService';
import {renderWithMocks} from '../../../../../jest/utilities/renderWithMocks';
import {NotificationServiceMock} from '../../../../../jest/utilities/servicesMocks/NotificationServiceMock';
import {NotistackServiceMock} from '../../../../../jest/utilities/servicesMocks/NotistackServiceMock';
import {UserContextMock} from '../../../../../jest/utilities/servicesMocks/UserAuthenticationServiceMock';
import {ROUTES} from '../../../routes';
import {SearchPlacesQuery} from '../../../services/GeolocService';


// mock used in test where component contains PlaceAutocomplete or ConceptAutomplete
const searchPlacesQueryMock = {
  request: {
    query: SearchPlacesQuery,
    variables: {qs: '', first: 4},
  }, result: () => {
    return {
      data: {
        "places": {
          "edges": [
            {
              "node": {
                "id": "geonames:2988506",
                "__typename": "City",
                "name": "Paris",
                "countryCode": "FR",
                "countryId": "geonames:3017382",
                "countryName": "France",
                "stateName": "Île-de-France"
              },
              "__typename": "PlaceInterfaceEdge"
            },
            {
              "node": {
                "id": "geonames:6455259",
                "__typename": "City",
                "name": "Paris",
                "countryCode": "FR",
                "countryId": "geonames:3017382",
                "countryName": "France",
                "stateName": "Île-de-France"
              },
              "__typename": "PlaceInterfaceEdge"
            },
            {
              "node": {
                "id": "geonames:3719265",
                "__typename": "City",
                "name": "Paris",
                "countryCode": "HT",
                "countryId": "geonames:3723988",
                "countryName": "Haïti",
                "stateName": "Nord"
              },
              "__typename": "PlaceInterfaceEdge"
            },
            {
              "node": {
                "id": "geonames:3719267",
                "__typename": "City",
                "name": "Paris",
                "countryCode": "HT",
                "countryId": "geonames:3723988",
                "countryName": "Haïti",
                "stateName": "Sud"
              },
              "__typename": "PlaceInterfaceEdge"
            },
            {
              "node": {
                "id": "geonames:6613989",
                "__typename": "City",
                "name": "Parisot",
                "countryCode": "FR",
                "countryId": "geonames:3017382",
                "countryName": "France",
                "stateName": "Occitanie"
              },
              "__typename": "PlaceInterfaceEdge"
            }
          ],
          "__typename": "PlaceInterfaceConnection"
        }
      }
    };
  },
}

let notificationServiceMock;
let notistackServiceMock;

/* Disable console.warn for the tests */
let consolewarn;
beforeAll(() => {
  consolewarn = console.warn;
  console.warn = jest.fn();
});
afterAll(() => {
  console.warn = consolewarn;
});

/* Setup notificationServiceMock */
beforeEach(() => {
  notificationServiceMock = new NotificationServiceMock();
  notistackServiceMock = NotistackServiceMock();
});

/* Cleanup DOM */
afterEach(() => {
  cleanup();
});

/**
 *
 * GraphQL mutations scenario to test :
 *
 * - response with formValidationError
 * - valid response with error property
 * - error when username already exists
 * - unexpected server error
 *
 *
 * - Errors :
 *   formValidationError :
 *     one graphQLError
 *     graphQLErrors of length 2
 *   Unexpected error (networkError)
 *   Offline error
 */

test('form input error', async () => {
  const mocks = [
    searchPlacesQueryMock,
    {
      request: {
        query: gqlRegisterUserAccountMutation,
        variables: {
          email: 'user@test.com',
          nickName: 'user',
          password: 'pass',
          gcuAccepted: true,
        },
      },
      result: () => {
        let errorMock = GraphQLErrorResponses.FormValidationErrorResponse;
        // We tweak a bit the error provided by synaptix.js, because the message IS_REQUIRED isn't handled in our case
        errorMock.errors[0].formInputErrors[0].field = 'input.email';
        errorMock.errors[0].formInputErrors[0].errors[0] = 'EMAIL_ALREADY_REGISTERED';
        errorMock.errors[0].formInputErrors[1].errors[0] = 'PASSWORD_TOO_SHORT';
        return errorMock;
      },
    },
  ];

  const {container, findByTestId, findByText, findByPlaceholderText} = renderWithMocks({
    locationPath: '/subscribe',
    gqlMocks: mocks,
    element: (
      <UserContextMock loggedIn={true}>
        <Subscribe notistackService={notistackServiceMock} notificationService={notificationServiceMock} removePlaceAutocomplete={true} />
      </UserContextMock>
    ),
    routePath: ROUTES.SUBSCRIBE,
  });

  let submitButton = await findByText('Valider');
  fireEvent.change(await findByPlaceholderText('Pseudonyme (ou nom)'), {target: {value: 'user'}});
  fireEvent.change(await findByPlaceholderText('Mot de passe'), {target: {value: 'pass'}});
  fireEvent.change(await findByPlaceholderText('Adresse de courriel'), {target: {value: 'user@test.com'}});
  fireEvent.click(await findByText(/J'accepte les/));
  fireEvent.click(submitButton);
  let emailErrorLabel = await findByTestId('email-error-msg');
  let passwordErrorLabel = await findByTestId('password-error-msg');
  expect(getNodeText(passwordErrorLabel)).toEqual('Mot de passe trop court');
  expect(getNodeText(emailErrorLabel)).toEqual('Cette adresse de courriel est déjà utilisée');
  expect(notificationServiceMock.error.mock.calls[0][0]).toEqual('Certains champs sont invalides');
});


