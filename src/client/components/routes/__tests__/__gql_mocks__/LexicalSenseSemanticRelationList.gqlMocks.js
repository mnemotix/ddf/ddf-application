import {LexicalSenseSemanticRelationListQuery} from "../../../LexicalSense/LexicalSenseDistinctSemanticRelationList";

export const lexicalSenseSemanticRelationListQuery_empty = {
  request: {
    query: LexicalSenseSemanticRelationListQuery,
    variables: {
      lexicalSenseId: "inv/lexical-sense/1234"
    }
  },
  result: {
    "data": {
      "lexicalSense": {
        "id": "inv/lexical-sense/1234",
        "semanticRelations": {
          "edges": [],
          "__typename": "SemanticRelationConnection"
        },
      }
    }
  }
};