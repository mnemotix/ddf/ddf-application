import React from 'react';
import { useTranslation } from 'react-i18next';

import { ProcessedMarkdownWrapper } from '../../../utilities/ProcessedMarkdownWrapper';
import { HelpPage } from '../../../layouts/responsive/HelpPage';
import content from '../../../assets/markdown/help_ddf_irl.md';

export function HelpDdfIrl() {
  const { t } = useTranslation();

  return (
    <HelpPage
      title={t('HELP.HEADER')}
      subtitle="Un titre"
      helpSectionTitle={t('HELP.SECTION_LINKS.OFFLINE')}
      content={<ProcessedMarkdownWrapper content={content} />}
      color="pink"
    />
  );
}

