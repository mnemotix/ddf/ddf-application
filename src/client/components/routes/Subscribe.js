import React, {useState, useRef} from 'react';
import clsx from "clsx";
import {Formik} from 'formik';
import {useTranslation, Trans} from 'react-i18next';
import {Link, useHistory} from 'react-router-dom';
import {ParseNewlines} from "../../utilities/ParseNewlines";
import {useSnackbar} from 'notistack';
import {makeStyles} from '@material-ui/core/styles';
import {ROUTES} from '../../routes';
import {SimpleModalPage} from '../../layouts/responsive/SimpleModalPage';
import {BracketButton} from "../widgets/BracketButton";
import FormikInput from '../widgets/forms/formik/FormikInput';
import FormikCheckbox from '../widgets/forms/formik/FormikCheckbox';
import {Form} from '../widgets/forms/formik/Form';
import Config from '../../Config';
import {MetaSEO} from "../widgets/MetaSEO";
import {FormBottom} from '../widgets/forms/FormBottom';
import {PlaceAutocompleteMUIA} from '../widgets/PlaceAutocompleteMUIA';
import {notificationService as appNotificationService} from '../../services/NotificationService';
import {getUserAuthenticationService} from '../../services/UserAuthenticationService';
import {useBrowsingHistoryHelperService} from '../../services/BrowsingHistoryHelperService';

const useStyles = makeStyles((theme) => ({
  secondLevelTitle: {
    color: Config.colors.pink,
    textAlign: "center",
    marginTop: -(Config.spacings.large),
    marginLeft: -(Config.spacings.medium),
    marginRight: -(Config.spacings.medium),
    marginBottom: Config.spacings.medium,
    padding: Config.spacings.medium,
    "& h2": {
      fontSize: Config.fontSizes.medium,
      marginBottom: Config.spacings.medium
    }
  },
  topInfoContent: {
    textAlign: "center",
    fontSize: Config.fontSizes.small,
    marginBottom: Config.spacings.small,
    color: Config.colors.purple
  },
  help: {
    textAlign: "center",
    marginTop: Config.spacings.small,

    "& em": {
      color: Config.colors.purple,
      fontStyle: 'normal'
    },
    "& p": {
      marginBottom: Config.spacings.tiny,
      /*
        fontSize: "10px",
        color: Config.colors.darkgray
        */
    }
  },
  marginTopMedium: {
    marginTop: Config.spacings.medium,
  },
  marginBottomMedium: {
    marginBottom: Config.spacings.medium,
  }
}));
 
import formStyles from '../widgets/forms/FormStyles.js';
import globalStyles from "../../assets/stylesheets/globalStyles";

/***
 *
 * List of error messages :
 *
 * - Frontend defined :
 *   - REQUIRED
 *   - INVALID_EMAIL
 * - Backend defined :
 *   - PASSWORD_TOO_SHORT
 *   - EMAIL_ALREADY_REGISTERED
 *
 */
export function Subscribe(props) {
  const {t} = useTranslation();
  const gS = globalStyles();
  const formStyle = formStyles();
  const classes = useStyles(); 
  // for jest test, have to use mocked up functions because we get "      Error: Uncaught [TypeError: Cannot read property 'enqueueSnackbar' of undefined]" error
  const notificationService = props.notificationService || appNotificationService;
  const removePlaceAutocomplete = props.removePlaceAutocomplete || false; // yarn test falls with PlaceAutocomplete. cannot fix it 
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {subscribe} = userAuthenticationService.useSubscribe();
  const {enqueueSnackbar} = props.notistackService || useSnackbar();
  const containerRef = useRef(null);
  const history = useHistory();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();
  const [inputValue, setInputValue] = useState('');

  return (
    <React.Fragment>
      <MetaSEO title={t('DOCUMENT_TITLES.SUBSCRIBE')} />
      <SimpleModalPage title={t('SUBSCRIBE_PAGE.TITLE')} content={renderContent()} titleBgColor="pink" />
    </React.Fragment>
  );

  function renderContent() {
    return (
      <React.Fragment>
        <div className={classes.secondLevelTitle}>
          <h2>{t('SUBSCRIBE_PAGE.SECOND_LEVEL_TITLE')}</h2>
        </div>
        <div className={classes.topInfoContent}>
          {t('SUBSCRIBE_PAGE.TOP_INFOBOX_CONTENT')}
        </div>
        <div ref={containerRef} >
          <Formik
            initialValues={userAuthenticationService.getSubscribeFormInitialValues()}
            validationSchema={userAuthenticationService.getSubscribeValidationSchema()}
            validateOnChange={false}
            validateOnBlur={true}
            onSubmit={handleSubscribe}>
            {({isSubmitting, setFieldValue}) => (
              <Form noValidate aria-label="formulaire pour créer un compte" >
                <FormikInput type="text" name="nickName" placeholder="Pseudonyme (ou nom)" aria-label="Saisir le nom d'utilisateur" />
                <FormikInput type="email" name="email" placeholder="Adresse de courriel" aria-label="Saisir votre email" data-/>
                <FormikInput type="password" name="password" placeholder="Mot de passe" aria-label="Saisir votre mot de passe" />
                {!removePlaceAutocomplete &&
                  <PlaceAutocompleteMUIA
                    placeholder="Lieu de vie"
                    value={inputValue}
                    inputRef={null}
                    containerRef={containerRef}
                    inputClassName={formStyle.input}
                    handleChange={(newObj) => {
                      setInputValue(newObj?.label)
                      setFieldValue("placeId", newObj?.id);
                    }}
                    handleSubmit={() => null}
                  />
                }
                {/* le label ne doit pas etre formaté ni contenir de saut de ligne sinon trans ne fonctionne plus
              label={<Trans i18nKey="SUBSCRIBE_PAGE.GCU_CHECKBOX_LABEL">I accept the <Link role="link" aria-label="CGU" to={ROUTES.GCU} target="_blank">terms and conditions</Link>.</Trans>}
               */}
                <div className={classes.marginTopMedium} />
                <FormikCheckbox
                  name="gcuAccepted"
                  label={
                    <Trans i18nKey="SUBSCRIBE_PAGE.GCU_CHECKBOX_LABEL">
                      J'accepte les
                      <Link role="link" aria-label="" to={ROUTES.GCU} target="_blank">
                        conditions générales d'utilisation
                      </Link>
                      .
                    </Trans>
                  }
                />
                <div className={clsx(formStyle.info, "secondary", classes.help, classes.marginBottomMedium)}>
                  <p>
                    <em>{t('SUBSCRIBE_PAGE.MANDATORY_FIELDS')}</em>
                  </p>
                  <p>
                    <ParseNewlines text={t('SUBSCRIBE_PAGE.BOTTOM_INFOBOX_CONTENT')} />
                  </p>
                </div>

                <FormBottom >
                  <div className={gS.buttonRow}>
                    <BracketButton
                      greyBg={true}
                      aria-label="Soumettre le formulaire de création de compte"
                      text={t('SUBSCRIBE_PAGE.SUBMIT')}
                      type="submit" disabled={isSubmitting} loading={isSubmitting}
                    />
                  </div>
                </FormBottom>
              </Form>
            )
            }
          </Formik>
        </div>
      </React.Fragment>
    );
  }

  function navigateBack() {
    let route = browsingHistoryHelperService.getBackFromSecondaryScreenLocation();
    history.push(route ? route.location : '/');
  }

  async function handleSubscribe(values, formikOptions) {
    let res = await subscribe(values, formikOptions);

    if (res.success) {
      enqueueSnackbar(t('LOGIN_PAGE.ACCOUNT_CREATED'), {variant: 'success'});
      let route = browsingHistoryHelperService.getAfterLoginLocation();
      history.push(route ? route.location : '/');
    }
    if (res.globalErrorMessage) {
      notificationService.error(res.globalErrorMessage);
    }
  }
}
