import React from 'react';
import PropTypes from 'prop-types';
import ddfLogo from '../../assets/images/ddf_logo.svg';
import {makeStyles} from '@material-ui/core/styles';
import Config from '../../Config';
import clsx from 'clsx';

import {StoresSectionMobile, StoresSectionDesktop} from "../widgets/StoresSection";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    height: '80vh',
    textAlign: 'center',
    fontWeight: Config.fontWeights.medium,
    fontSize: Config.fontSizes.large
  },
  subContainer: {
    "display": "flex",
    "alignItems": "flex-start",
    "justifyContent": "flex-start",
    "flexDirection": "column",
    maxWidth: "750px",
  },
  ddfLogo: {"height": "70px"},

  hugeMargin: {
    marginTop: Config.spacings.huge,
    marginBottom: Config.spacings.huge,
  },
  smallBTMargin: {
    marginTop: Config.spacings.huge,
    marginBottom: Config.spacings.medium
  },
  noBackgroundColor: {
    backgroundColor: "inherit ! important"
  },

  exempleRoot: {
    width: "100%",
    "WebkitTapHighlightColor": "transparent",
    "paddingLeft": "20px",
    "paddingRight": "20px",
    "paddingBottom": "10px",
    "marginBottom": "20px",
    "backgroundColor": "#fff",
    "paddingTop": "20px",
    fontWeight: Config.fontWeights.normal,
    fontSize: Config.fontSizes.medium,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    position: "relative",
    '&::before': {
      "content": "''",
      "position": "absolute",
      "bottom": "100%",
      "left": "19px",
      "borderBottomColor": "#dddddd"
    },
    '&::after': {
      "content": "''",
      "position": "absolute",
      "bottom": "100%",
      "left": "20px",
      "border": "10px solid transparent",
      "borderBottomColor": "#ffffff"
    }
  },
  exempleCountry: {
    "WebkitTapHighlightColor": "transparent",
    "padding": "0",
    "display": "flex"
  },
  exempleCountrySpan: {
    "WebkitTapHighlightColor": "transparent",

    "textDecoration": "inherit",
    "color": "#fff",
    "whiteSpace": "nowrap",
    "overflow": "hidden",
    "textOverflow": "ellipsis",
    "fontWeight": "700",
    "padding": "2px 10px",
    "backgroundColor": "#828282"
  },
  exempleType: {
    "WebkitTapHighlightColor": "transparent",
    "padding": "0",
    "marginTop": "10px",
    "fontStyle": "italic"
  },
  exempleTitle: {
    "WebkitTapHighlightColor": "transparent",
    "marginTop": "10px"
  },
  exempleDef: {
    "WebkitTapHighlightColor": "transparent",
    "fontSize": "12px",
    "color": "#828282",
    "marginTop": "10px",
    textAlign: "left",
    maxWidth: "720px"
  },
  exempleDefSpan1: {
    "WebkitTapHighlightColor": "transparent",
    "fontSize": "12px",
    "color": "#828282",
    "fontWeight": "700"
  },
  exempleDefSpan2: {
    "fontFamily": "Lato",
    "WebkitTapHighlightColor": "transparent",
    "fontSize": "12px",
    "color": "#828282",
    "margin": "0",
    "padding": "0",
    "fontStyle": "italic"
  },
  pinkColor: {
    color: "#8200a0"
  },
  italic: {

    "fontStyle": "italic"
  }

}));


export function UnderMaintenance() {
  const classes = useStyles();

  return (
    <div className={classes.container}>

      <img className={classes.ddfLogo} src={ddfLogo} alt="DDF" />
      <div className={classes.subContainer}>
        <div className={classes.smallBTMargin}>
          <span className={clsx(classes.pinkColor, classes.italic)}>Mazette</span>, le Dictionnaire des francophones est momentanément en maintenance.
        </div>
        {renderDefinition()}
        <div className={classes.hugeMargin}>
          Le site internet se refait une beauté pour vous proposer de nouvelles fonctionnalités. <br />
          Revenez très rapidement sur le Dictionnaire des francophones.
        </div>
        {
          /*
           <StoresSectionDesktop classesName={classes.noBackgroundColor} />
          */
        }
      </div>
    </div>
  );

  function renderDefinition() {
    return <div className={classes.exempleRoot}>
      <div className={classes.exempleCountry}>
        <div className={classes.exempleCountrySpan}>
          Monde francophone
        </div>

      </div>
      <div className={classes.exempleType}>Interjection</div>
      <div className={classes.exempleTitle}>
        Exprime l'<span className={classes.pinkColor}>étonnement</span> ou l'<span className={classes.pinkColor}>admiration</span>.
      </div>
      <div className={classes.exempleDef}>
        <span className={classes.exempleType}>Exemple&nbsp;:&nbsp;</span>
        <span className={classes.exempleType}>
          <span>
            <i><b>Mazette</b> ! Ma sœur est une mangeuse d'hommes ! Et de femmes ! Jamais j'aurais cru ça ! Et dire que c'est moi qu'on prend pour le queutard de la famille ! Tu cachais bien ton jeu !</i>
          </span>—<span>Stéphanie Maieron, <i>Élisa… Ce qui n'est pas dit n'existe pas</i>, Édilivre, 2014
          </span>
        </span>
      </div>
    </div>
  }
};


