import React, {useState, useEffect} from 'react';
import {useHistory, Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {useTranslation} from 'react-i18next';
import {ROUTES} from '../../routes';
import {ResponsiveMainLayout} from '../../layouts/responsive/ResponsiveMainLayout';
import {useMediaQueries} from '../../layouts/MediaQueries';
import {GeolocPicker} from '../widgets/GeolocPicker';
import FormSearchInput from '../widgets/FormSearchInput';
import {BracketButton} from '../widgets/BracketButton';
import {useBrowsingHistoryHelperService} from '../../services/BrowsingHistoryHelperService';

import MyImg from '../../components/widgets/MyImg';
import {FooterExplanationText} from '../../components/widgets/FooterExplanationText';
import {ImproveDdfButton} from '../../components/widgets/ImproveDdfButton';
import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import Config from '../../Config';

const useStyles = makeStyles((theme) => ({
  center: {
    marginLeft: "auto",
    marginRight: "auto",
  },
  marginBottom: {
    marginBottom: Config.spacings.medium
  },
  maxWidthCentered: {
    marginRight: 'auto',
    marginLeft: 'auto',
    maxWidth: 'min(80vw,500px)'
  },
  logo: {
    marginRight: 'auto',
    marginLeft: 'auto',
    marginTop: Config.spacings.small,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      marginTop: Config.spacings.huge,
      width: "180px"
    },
    width: "140px"
  },

  title: {
    textAlign: 'center',
    marginBottom: Config.spacings.huge,
    fontFamily: "Raleway",
    fontWeight: Config.fontWeights.medium,
    fontSize: Config.fontSizes.xlarge,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      fontSize: Config.fontSizes.xxlarge,
    }
  }
}));



export function Index(props) {
  const {t} = useTranslation();
  const history = useHistory();
  const {isMobile} = useMediaQueries();
  const classes = useStyles();
  let [searchQuery, setSearchQuery] = useState({label: "", id: ""});
  const [showIsRequired, setShowIsRequired] = useState(false);

  const mobileFooter = isMobile &&
    <div>
      <FooterExplanationText />
      <ImproveDdfButton currentForm={null} />
    </div>


  return (
    <ResponsiveMainLayout desktopHeaderHideSearch={true} mobileFooter={mobileFooter}>
      <div className={classes.maxWidthCentered}>
        <div className={clsx(classes.marginBottom, classes.logo)}>
          <Link role="link" aria-label="Retour à la page d'Accueil" to="/">
            <MyImg iconName={"ddfLogo"} color={"blank"} alt="Accueil" className={classes.logo} />
          </Link>
        </div>

        <div className={classes.marginBottom}>
          <h1 className={classes.title}>
            Dictionnaire
            <br />
            des francophones
          </h1>
        </div>

        <div className={classes.marginBottom}>
          <FormSearchInput
            showIsRequired={showIsRequired}
            placeholder={t(isMobile ? 'INDEX.SEARCH_PLACEHOLDER_MOBILE' : 'INDEX.SEARCH_PLACEHOLDER')}
            value={searchQuery?.label}
            onChange={(value) => {
              setShowIsRequired(false);
              setSearchQuery(value);
            }}
            onSubmit={performSearch}
            theme={stringNotEmptyOrNull(searchQuery?.label) ? 'purple' : 'light'}

          />
        </div>
        <div className={classes.marginBottom}>
          <GeolocPicker />
        </div>
        <BracketButton
          classNameButton={classes.center}
          greyBg={true}
          text={t('INDEX.SEARCH_BUTTON')} onClick={performSearch} />
      </div>
    </ResponsiveMainLayout>
  );

  function stringNotEmptyOrNull(string) {
    return !!string && string.length > 0
  }

  function performSearch() {
    if (!searchQuery?.label) {
      setShowIsRequired(true);
      return;
    }
    history.push(formatRoute(ROUTES.FORM_SEARCH, {formQuery: searchQuery.label}));
  }
}
