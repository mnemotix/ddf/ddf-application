import React, {useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Paper, IconButton, Accordion, AccordionSummary, AccordionDetails, Tooltip} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import FilterListIcon from "@material-ui/icons/FilterList";

import {prettifyNumber} from "../../../../utilities/helpers/tools";
import {upperCaseFirstLetter} from '../../../../utilities/helpers/tools';
import Config from '../../../../Config';
import {useMediaQueries} from '../../../../layouts/MediaQueries';
import globalStyles from '../../../../assets/stylesheets/globalStyles';
import clsx from "clsx";



const useStyles = makeStyles((theme) => ({
  container: {
    width: "100%",
    padding: '10px',
    marginBottom: Config.spacings.small
  },
  title: {
    fontSize: Config.fontSizes.large,
    marginBottom: Config.spacings.tiny,
  },
  flexCenter: {
    display: "flex",
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  bucketsContainer: {
    maxHeight: "175px",
    overflowY: "auto",
    scrollbarColor: `${Config.colors.purple} white`,
    padding: Config.spacings.small
  },
  bucket: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: Config.spacings.tiny,
    fontSize: Config.fontSizes.ml,
  },
  width100: {
    width: "100%"
  },
  alignRight: {
    display: "flex",
    justifyContent: 'center',
  },
  rm5: {
    marginRight: Config.spacings.tiny,
    "&:hover": {
      color: Config.colors.darkpurple
    }
  },
  selectedKey: {
    color: Config.colors.purple
  }
}));

/**
 * display aggregations of same type 
 * @param {Array} buckets to display
 * @param {{name,field}} meta with "name" to display as label and "field" for ES should query 
 * @param {function} onSelectCallback  : callback function for each clic on bucket ( select / deselect one )
 * @param {function} onDeselectAllCallback : callback to deselect all the bucket of the aggregation
 * @param {object} filterOne : to filter list of element, should contain filter function to return element to display
 *     filterOne = { label ,toFilter}

 * @returns 
 */
export function Aggregation({buckets, meta, selectedBuckets = [], onSelectCallback = false, onDeselectAllCallback = false,
  filterOne = false
}) {
  const {isMobile} = useMediaQueries();
  const classes = useStyles();
  const gS = globalStyles();
  const [expanded, setExpanded] = useState(false);
  const [filterActivate, setFilterActivate] = useState(true);

  if (isMobile) {
    return (
      <Accordion expanded={expanded} onChange={() => setExpanded(!expanded)}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          {renderTitle()}
        </AccordionSummary>
        <AccordionDetails>
          <div className={classes.width100}>
            {renderAllEntry()}
            {renderBuckets()}
            <div className={classes.alignRight}>
              <ExpandLessIcon onClick={() => setExpanded(false)} />
            </div>
          </div>
        </AccordionDetails>
      </Accordion>
    )
  } else {
    return (
      <Paper elevation={3} className={classes.container}>
        <div className={classes.title}>
          {renderTitle()}
        </div>
        <div className={classes.bucketsContainer}>
          {renderAllEntry()}
          {renderBuckets()}
        </div>
      </Paper>
    )
  }

  function renderTitle() {
    return <div className={classes.flexCenter}>
      {upperCaseFirstLetter(meta?.name) + " :"}
      {filterOne?.toFilter && <Tooltip title={filterOne?.label}>
        <IconButton aria-label="close" onClick={(evt) => {evt?.stopPropagation && evt.stopPropagation(); setFilterActivate(!filterActivate)}}>
          <FilterListIcon style={{color: filterActivate ? Config.colors.purple : Config.colors.darkgray}} />
        </IconButton>
      </Tooltip>}
    </div>
  }

  function renderAllEntry() {
    return (
      <Bucket
        key={'all'}
        label={'Tout'}
        isSelected={false}
        onClick={() => onDeselectAllCallback && onDeselectAllCallback(meta?.field)}
        doc_count={''}
      />
    )
  }

  function renderBuckets() {
    let source = buckets;
    if (filterOne?.toFilter && filterActivate) {
      source = filterOne?.toFilter(buckets)
    }
    return source.map(({key, doc_count}) => {

      const isSelected = selectedBuckets.filter(({label}) => label === key)?.length > 0;

      return (
        <Bucket
          key={key}
          label={key}
          isSelected={isSelected}
          onClick={() => onSelectCallback && onSelectCallback(meta?.field, key, isSelected)}
          doc_count={doc_count}
        />
      )
    })
  }

  function Bucket({label, doc_count, isSelected, onClick}) {
    return <div className={classes.bucket}>
      <div className={clsx(classes.rm5, gS.clickable, isSelected && classes.selectedKey)} onClick={onClick}>
        {label}
      </div>
      <div className={classes.rm5} dangerouslySetInnerHTML={{__html: prettifyNumber(doc_count)}} />

    </div>
  }
}
