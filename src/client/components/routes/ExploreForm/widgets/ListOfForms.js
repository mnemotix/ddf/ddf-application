
import React, {useState, useEffect, useRef} from 'react';

import {useLazyQuery} from '@apollo/client';
import {useTranslation} from "react-i18next";
import {gql} from "@apollo/client";
import {makeStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {ROUTES} from '../../../../routes';
import {useMediaQueries} from "../../../../layouts/MediaQueries";
import LoadingGif from "../../../widgets/LoadingGif";
import {Paginator} from '../../../widgets/Paginator';
import Config from '../../../../Config';
import {Alphabet} from './Alphabet';
import {unique, cutLongWord} from '../../../../utilities/helpers/tools';
import globalStyles from '../../../../assets/stylesheets/globalStyles';

const _MINCOUNT = 200;
const gqlListOfFormsQuery = gql`
 query Form_Query_Start_With(
    $first: Int, $after: String, 
    $startWithLetter: String, $startWithNumber:Boolean , $startWithNonAlphaNum:Boolean,
    $mustFilters: String, $sortings: [SortingInput]
  ) {
    count:lexicalSensesForWrittenRepStartWithCount(
      startWithLetter: $startWithLetter, startWithNumber: $startWithNumber , startWithNonAlphaNum: $startWithNonAlphaNum,
      mustFilters: $mustFilters
    )
    lexicalSenses:lexicalSensesForWrittenRepStartWith(
      startWithLetter: $startWithLetter, startWithNumber: $startWithNumber ,startWithNonAlphaNum: $startWithNonAlphaNum,  
      first: $first, after: $after, 
      mustFilters: $mustFilters, sortings: $sortings 
    )     
 }
`;

const FORM_HEIGHT = 26;
const FORM_WIDTH = 240;

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column"
  },
  resultContainer: {
    padding: Config.spacings.large,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      padding: Config.spacings.medium
    },
    backgroundColor: Config.colors.white,
    display: 'flex',
    flexDirection: "column",
    justifyContent: 'space-between',
  },
  formsContainerOld: {
    height: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  formsContainer: {
    display: "flex",
    flexDirection: "column",
    flexWrap: "wrap",
    minHeight: "375px",
  },
  formsCol: {
    marginRight: "25px"
  },
  form: {
    // minHeight: `${FORM_HEIGHT}px`,
    color: Config.colors.purple,
    wordBreak: "break-word",
    overflowWrap: "break-word",
    width: `calc(${FORM_WIDTH}px - 10px)`,
    display: "block",
    paddingLeft: "1em",
    textIndent: "-1em",
    paddingBottom: "5px"
  },
  m15: {
    marginTop: '15px'
  },

}));
/* formsContainerGrid: {
    minHeight: "210px",
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      minHeight: "410px",
    },
    display: "grid",
    gridAutoFlow: "column",
  },*/

/**
 * Display list of forms/writtenRep in grid
 * 
 * @param {string} mustFilters
 * @param {boolean} loading : is parent loading data ? display a loaderGif her to preserve display template
 * @param {int} countAll : count for all forms with this mustFilters, without filtering on a specif letter. if count < 200 doesn't show alphabet's button
 * @returns 
 */
export function ListOfForms({mustFilters, loading: parentLoading, countAll = false}) {
  const gS = globalStyles();
  const classes = useStyles();
  const {isSmallMobile, isMobile} = useMediaQueries();
  const {t} = useTranslation();
  const myRef = useRef();

  // enable filtering on a specif letter only if countAll > 200 lexicalSenses
  const enableAlphabet = Number.isInteger(countAll) && countAll >= _MINCOUNT;

  //const [numberOfColumns, setNumberOfColumns] = useState(false);
  const [numberOfRows, setNumberOfRows] = useState(false);
  const pageSize = isSmallMobile ? 20 : isMobile ? 40 : 54
  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedLetter, setSelectedLetter] = useState("a");

  const [lexicalSenses, setLexicalSenses] = useState({});
  /*data: {lexicalSenses} = {}, */
  const [getData, {loading, error}] = useLazyQuery(gqlListOfFormsQuery, {
    variables: getGqlVariables(),
    fetchPolicy: 'cache-and-network',
    onCompleted: (data) => {
      setLexicalSenses(JSON.parse(data?.lexicalSenses));
      setPageCount(Math.ceil(data?.count / pageSize));
    },
  });

  useEffect(() => {
    getData();
  }, [currentPage]);

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedLetter]);

  useEffect(() => {
    if (mustFilters) {
      setCurrentPage(1);
      setSelectedLetter("a");
    }
  }, [mustFilters]);

  // use a ref to get component width to compute how much columns we want to display
  const getWidth = () => {
    if (myRef.current) {
      const colTmp = Math.floor(myRef.current.offsetWidth / FORM_WIDTH);
      // setNumberOfColumns(colTmp);
      setNumberOfRows(Math.ceil(pageSize / colTmp))
    }
  };

  useEffect(() => {
    // get initial width
    getWidth();
    // and on each update it on each resize
    window.addEventListener("resize", getWidth);
    return () => {
      window.removeEventListener("resize", getWidth);
    };
  }, []);

  function getGqlVariables() {
    const after = currentPage > 1 ? `offset:${((pageSize) * (currentPage - 1)) - 1}` : null;

    const startWithNumber = selectedLetter === "0-9";
    const startWithNonAlphaNum = selectedLetter === "*";

    return {
      first: pageSize,
      after,
      startWithLetter: (enableAlphabet && !startWithNumber && !startWithNonAlphaNum) ? selectedLetter : "",
      startWithNumber,
      startWithNonAlphaNum,
      mustFilters,
      sortings: [{"sortBy": "canonicalFormWrittenRep", "isSortDescending": false}]
    };
  }

  const style = isSmallMobile ? {} : {minHeight: `${15 + FORM_HEIGHT * numberOfRows}px`};
  return (
    <div className={classes.container}>
      <div className={gS.exploreMinHeight}>
        {enableAlphabet && <Alphabet selectedLetter={selectedLetter} setSelectedLetter={setSelectedLetter} />}
      </div>
      <div className={classes.resultContainer} ref={myRef}>
        <div style={style}>
          {loading || parentLoading ?
            <LoadingGif addContainer={true} />
            :
            renderForms()
          }
        </div>
        <div className={classes.m15} />
        <Paginator currentPage={currentPage} pageCount={pageCount} setCurrentPage={setCurrentPage} />
      </div>
    </div>
  )


  function renderForms() {

    const style = isSmallMobile ? {} : {height: `${15 + FORM_HEIGHT * numberOfRows}px`};

    if (lexicalSenses?.edges?.length > 0) {

      const uniqForms = unique(lexicalSenses?.edges.map(({node}) => node), "canonicalFormWrittenRep");
      return <div className={classes.formsContainer} style={style}>
        {uniqForms?.map(({canonicalFormWrittenRep}, index) => {
          return <span key={canonicalFormWrittenRep + "-" + index}>
            <Link key={canonicalFormWrittenRep + "-" + index} role="link" className={classes.form}
              aria-label={"Voir les définitions de " + canonicalFormWrittenRep}
              rel="noopener noreferrer" target="_blank"
              to={formatRoute(ROUTES.FORM_SEARCH, {formQuery: encodeURIComponent(canonicalFormWrittenRep)})}
            > {cutLongWord(canonicalFormWrittenRep)}
            </Link>
          </span>
        })}
      </div>

    } else {

      return <div className={classes.formsContainer} style={style}>
        Pas de résultat
      </div>

    }


  }
}

