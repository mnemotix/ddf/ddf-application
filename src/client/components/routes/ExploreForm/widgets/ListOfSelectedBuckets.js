

import React from 'react';


import {makeStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {ROUTES} from '../../../../routes';
import PinkChip from '../../../widgets/PinkChip';


const useStyles = makeStyles((theme) => ({
  selectedBucketsContainer: {
    display: "flex",
    flexWrap: "wrap",
    marginTop: "15px"
  },
  chips: {
    cursor: "pointer !important",
    fontStyle: "italic"
  }
}));

/**
 * Display list of chips for each selectedBuckets
 * 
 * @param {object} selectedBuckets
 * @param {function} onSelectCallback : callback to deselect all the bucket of the aggregation
 * @returns 
 */
export function ListOfSelectedBuckets({selectedBuckets = {}, onSelectCallback = false}) {

  const classes = useStyles();


  return <div className={classes.selectedBucketsContainer}>
    {renderChips()}
  </div>

  function renderChips() {
    let chips = [];

    for (const field in selectedBuckets) {

      for (const item of selectedBuckets?.[field]) {
        const {label, id, type} = item;

        let to = null;
        if (id) {
          if (field === "placesForAggs") {
            to = formatRoute(ROUTES.EXPLORE_LOCALISATION_FORM, {id});
          } else if (type) {
            to = formatRoute(ROUTES.EXPLORE_CONCEPT_FORM, {type, id})
          }
        }

        chips.push(renderChip({field, label, to}));
      }
    }
    return chips;
  }

  function renderChip({field, label, to}) {
    // disabled for now
    // if (false && to) {
    if (to) {
      return <PinkChip
        key={field + label}
        isMine={true} useWhiteBackground={true}
        onDelete={() => onSelectCallback && onSelectCallback(field, label, true)}
        label={<Link role="link" className={classes.chips} aria-label={"Lancer une recherche sur " + label} to={to} target="_blank" >{label} </Link>}
      />
    } else {
      return <PinkChip key={field + label} label={label} isMine={true} useWhiteBackground={true}
        onDelete={() => onSelectCallback && onSelectCallback(field, label, true)}
      />
    }
  }

}

