import React from 'react';;

import {useMediaQueries} from "../../../../layouts/MediaQueries";
import {prettifyNumber} from "../../../../utilities/helpers/tools";
import {makeStyles} from '@material-ui/core/styles';
import Config from '../../../../Config';

const useStyles = makeStyles((theme) => ({
  likeCount: {
    position: "relative",
    textAlign: "center",
    padding: Config.spacings.large,
    backgroundColor: Config.colors.white,
    border: `1px solid ${Config.colors.mediumgray}`,
    marginBottom: `${Config.spacings.medium}px`,
  },
  likeCountAfter: {
    content: "''",
    display: 'block',
    position: 'absolute',
    top: "25px",
    right: "-15px",
    width: 0,
    height: 0,
    borderLeft: `15px solid ${Config.colors.mediumgray}`,
    borderTop: "15px solid transparent",
    borderBottom: "15px solid transparent",
  },
  likeCountAfterWhite: {
    content: "''",
    display: 'block',
    position: 'absolute',
    top: "25px",
    right: "-14px",
    width: 0,
    height: 0,
    borderLeft: `15px solid white`,
    borderTop: "15px solid transparent",
    borderBottom: "15px solid transparent",
  },
}));


export function FrameWithArrow({count}) {

  const classes = useStyles();
  const {isSmallMobile} = useMediaQueries();
  return (
    <div className={classes.likeCount}>
      <div dangerouslySetInnerHTML={{__html: prettifyNumber(count) + " mots dans la liste"}} />
      {!isSmallMobile && <>
        <div className={classes.likeCountAfter} />
        <div className={classes.likeCountAfterWhite} />
      </>}
    </div>
  );
};





