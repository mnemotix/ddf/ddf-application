import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useQuery } from "@apollo/client";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { gql } from "@apollo/client";
import { formatRoute } from "react-router-named-routes";
import { ROUTES } from "../../../routes";
import { Template } from "./widgets/Template";
import { ListOfForms } from "./widgets/ListOfForms";
import { FrameWithArrow } from "./widgets/FrameWithArrow";
import { useStyles } from "./styles";
import { countryContinent } from "../../../utilities/helpers/countryContinentMapping";

const PlaceQuery = gql`
  query Place($placeId: ID!, $mustFilters: String) {
    countAll: lexicalSensesForWrittenRepStartWithCount(
      mustFilters: $mustFilters
    )
    place(id: $placeId) {
      name
      countryCode
      ... on City {
        stateName
        countryId
        countryName
      }
      ... on State {
        countryId
        countryName
        stateCode
      }
      __typename
    }
  }
`;

/**
 * explore forms by geographic localisation
 *
 */
export function ExploreLocalisationForms({}) {
  let params = useParams();
  const classes = useStyles();
  const { t } = useTranslation();
  let id = decodeURIComponent(params.id);

  //  const filters = [`places:${id}`];
  const mustFilters = JSON.stringify([
    {
      bool: {
        minimum_should_match: 1,
        should: [
          {
            term: {
              places: id
                .replace("geonames:", "https://www.geonames.org/")
                .replace(
                  "ddfa:",
                  "http://data.dictionnairedesfrancophones.org/authority/"
                ),
            },
          },
        ],
      },
    },
  ]);

  const {
    data: { countAll, place } = {},
    loading,
    error,
  } = useQuery(PlaceQuery, {
    variables: {
      placeId: id,
      mustFilters,
    },
  });

  return (
    <Template
      leftContent={renderLeftContent()}
      rightContent={renderRightContent()}
    />
  );

  function renderLeftContent(params) {
    const continent =
      place?.countryCode && countryContinent(place?.countryCode);

    return (
      <div>
        <div className={classes.littleText}>{t("EXPLORE.LOCALISATION")}</div>
        {place?.name && <div className={classes.bigText}>{place.name}</div>}

        {place?.countryId && (
          <div className={classes.mbMedium}>
            Pays : {createLink(place?.countryId, place?.countryName)}
          </div>
        )}
        {continent && (
          <div className={classes.mbMedium}>
            Continent : {continent?.name}{" "}
            {/*createLink(continent?.id, continent?.name)*/}
          </div>
        )}
        <If condition={Number.isInteger(countAll)}>
          <div className={classes.MT24}>
            <FrameWithArrow count={countAll} />
          </div>
        </If>
      </div>
    );
  }

  function renderRightContent() {
    return (
      <div className={classes.MT24}>
        <ListOfForms
          mustFilters={mustFilters}
          loading={loading}
          countAll={countAll}
        />
      </div>
    );
  }

  function createLink(id, name) {
    return (
      <Link
        role="link"
        className={classes.link}
        aria-label={name}
        to={formatRoute(ROUTES.EXPLORE_LOCALISATION_FORM, { id })}
      >
        {name}
      </Link>
    );
  }
}
