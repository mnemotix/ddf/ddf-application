import React, {useState, useEffect} from 'react';
import {useLazyQuery, gql} from "@apollo/client";
import {Paper, Modal, IconButton} from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import clsx from "clsx";
import {Template} from './widgets/Template';
import {Aggregation} from './widgets/Aggregation';
import {ListOfForms} from './widgets/ListOfForms';
import {ListOfSelectedBuckets} from './widgets/ListOfSelectedBuckets.js';
import useDebounce from '../../../hooks/useDebounce';
import useAsyncApolloQuery from '../../../hooks/useAsyncApolloQuery';
import {useStyles} from "./styles";
import {FrameWithArrow} from './widgets/FrameWithArrow';
import globalStyles from '../../../assets/stylesheets/globalStyles';
import {getTypeFromSchemes} from "../../../utilities/helpers/tools";
import DivButton from '../../widgets/DivButton';

/**
 * show facettes for exploration 
 */

export function ExploreAggs({props}) {
  const gS = globalStyles();
  const classes = useStyles();

  // now aggs use filter stringified and send as it
  const [mustFilters, setMustForAggs] = useState("");
  // don't use selectedBuckets directly, use debounced instead
  const [selectedBuckets, setSelectedBuckets] = useState({});
  const [debouncedSelectedBuckets, setDebouncedSelectedBuckets] = useState({});

  // keep previous data to avoid blink while loading
  const [aggregations, setAggregations] = useState(false);
  const [countAll, setCountAll] = useState(false);
  // type of search, AND or OR ? 
  const [useAndQuery, setUseAndQuery] = useState(true);
  // help modal for type of search button 
  const [helpModalOpen, setHelpModalOpen] = useState(false);

  const [getData, {loading, error}] = useLazyQuery(gqlAggs, {
    variables: {mustFilters},
    onCompleted: (data) => {
      const parsedData = JSON.parse(data?.lexicalSensesAggs);
      setAggregations(parsedData?.aggregations);
      setCountAll(parsedData?.total);
    }
  });


  // load first data
  useEffect(() => {
    if (!loading) {
      getData();
    }
  }, []);

  // load new data 
  useEffect(() => {
    if (!loading) {
      getData();
    }
  }, [mustFilters]);


  useEffect(() => {
    /*  aggs must need  :  
         [{"bool": {
           "minimum_should_match": 1,
           "should": [
             { "term": { "placesForAggs": "algerie" } }
           ]
         }} ,
         {"bool": {
           // to do a AND query, minimum_should_match should be equal to the number or term , so 4 her. 
           // to do a OR query, minimum_should_match should be equal to 1
           "minimum_should_match": 1, 
           "should": [
             {"term": { "domainForAggs": "football" } }, { "term": { "domainForAggs": "commerce" } }, 
             {"term": { "domainForAggs": "droit" } }, { "term": { "domainForAggs": "mythologie" } }           ]
         }}]
   */
    let newMustForAggs = [];

    // logique OU 
    for (const propertyName in debouncedSelectedBuckets) {
      let newShouldForMust = [];
      for (const value of debouncedSelectedBuckets[propertyName]) {
        newShouldForMust.push({"term": {[`${propertyName}.keyword_not_normalized`]: value?.label}})
        // newFilters.push(`${propertyName.replace(".keyword", "")}:${value?.label}`);
      }
      if (newShouldForMust?.length > 0) {
        newMustForAggs.push({
          "bool": {
            "minimum_should_match": useAndQuery ? newShouldForMust?.length : 1,
            "should": newShouldForMust
          }
        });
      }
    }

    setMustForAggs(JSON.stringify(newMustForAggs));
  }, [debouncedSelectedBuckets, useAndQuery]);

  // set filters on each aggs change
  const debouncedSelectedBucketsTmp = useDebounce(selectedBuckets, 1000);
  useEffect(() => {
    setDebouncedSelectedBuckets(debouncedSelectedBucketsTmp);
  }, [debouncedSelectedBucketsTmp]);

  return (
    <Template
      leftContent={renderLeftContent()}
      rightContent={renderRightContent()}
      useModalForLeftContent={true}
    />
  );

  function renderLeftContent(params) {

    if (loading) {
      return <div></div>
    }

    if (!loading && !aggregations) {
      return <div>Oups, une erreur est survenue pour les facettes.</div>
    }

    let aggs = [];
    for (const index in aggregations) {
      const agg = aggregations[index];
      let filterOne = false;

      if (agg?.meta?.field === 'glossaryForAggs') {
        filterOne = {
          label: "masquer les noms propres",
          toFilter: buckets => filterBuckets(buckets)
        }
      }
      aggs.push(
        <Aggregation buckets={agg?.buckets} meta={agg?.meta} key={index}
          selectedBuckets={selectedBuckets?.[agg?.meta?.field] || []}
          onSelectCallback={onSelectCallback}
          onDeselectAllCallback={onDeselectAllCallback}
          filterOne={filterOne}
        />
      )
    }

    return <div>
      <div className={clsx(classes.bigText, classes.worldText, gS.exploreMinHeight)}>
        Explorer
      </div>
      {Number.isInteger(countAll) && <FrameWithArrow count={countAll} />}
      {renderSearchType()}
      <div className={classes.MT24}>
        {aggs}
      </div>
    </div>
  }

  function renderRightContent() {
    // <ListOfForms filters={filters} loading={loading} countAll={countAll} /> 
    return <div className={classes.sticky}>
      <ListOfForms mustFilters={mustFilters} loading={loading} countAll={countAll} />
      <ListOfSelectedBuckets selectedBuckets={selectedBuckets} onSelectCallback={onSelectCallback} />
    </div>
  }

  function renderSearchType() {

    return <Paper elevation={3} className={classes.searchTypeContainer}>
      <Modal
        open={helpModalOpen}
        onClose={() => setHelpModalOpen(false)}
        role="alertdialog"
        aria-modal="true"
        aria-labelledby="Rubrique d'aide pour ce champ"
      >

        <div className={classes.modalContent}>
          <div className={classes.modalCloseButton} >
            <IconButton aria-label="close" onClick={() => setHelpModalOpen(false)}>
              <CloseIcon />
            </IconButton>
          </div>
          Le filtrage des données peut se faire de 2 façons, par <i className={classes.purpleText}>Union</i> ou par <i className={classes.purpleText}>Intersection</i>.
          Il s'applique uniquement aux éléments choisis au sein d'une même catégorie.
          <br /><br />


          <h3>Exemple 1</h3><br />
          <div className={classes.mbMedium} >
            Dans la catégorie <i className={classes.purpleText}>Glossaires</i>, si l'on choisit les entrées <i className={classes.purpleText}>Plantes</i> et <i className={classes.purpleText}>Oiseaux </i>
            alors les résultats affichés seront :
          </div>

          - pour l'<i className={classes.purpleText}>Union</i>, ceux appartenant à l'une de ces 2 catégories, donc <i className={classes.purpleText}>Plantes <b>OU </b>Oiseaux</i> ;
          <br />
          - pour l'<i className={classes.purpleText}>Intersection</i>, ceux appartenant simultanément à ces 2 catégories, donc <i className={classes.purpleText}>Plantes <b>ET </b>Oiseaux</i> (ce qui, dans ce cas précis, risque de ne pas renvoyer de résultats).
          <br /><br /><br />

          <h3>Exemple 2</h3><br />

          <div className={classes.mbMedium} >
            Si l'on choisit, dans la la catégorie <i className={classes.purpleText}>Glossaires</i>, les entrées <i className={classes.purpleText}>Plantes</i> et <i className={classes.purpleText}>Oiseaux </i> et,
            dans la catégorie <i className={classes.purpleText}>Domaines</i> les entrées <i className={classes.purpleText}>Zoologie</i> et <i className={classes.purpleText}>Botanique </i>,
            les résultats affichés seront :
          </div>

          - pour l'<i className={classes.purpleText}>Union</i>, ceux appartenant à <i className={classes.purpleText}>(Plantes <b>OU </b>Oiseaux) <b>ET </b> (Zoologie <b>OU </b>Botanique)</i> ;
          <br />
          - pour l'<i className={classes.purpleText}>Intersection</i>, ceux appartenant à <i className={classes.purpleText}>(Plantes <b>ET </b>Oiseaux) <b>ET </b> (Zoologie <b>ET </b>Botanique)</i>.
          <br /><br />





        </div>
      </Modal>

      <div className={classes.flexspace}>
        <DivButton
          aria-label="Changer le type de recherche"
          onClick={() => {
            setUseAndQuery(!useAndQuery)
          }} >
          {useAndQuery ? "Recherche : Intersection (et)" : "Recherche : Union (ou)"}
        </DivButton>
        <IconButton onClick={() => setHelpModalOpen(true)} aria-label={"aide"}>
          <HelpOutlineIcon />
        </IconButton>

      </div>

    </Paper>
  }

  async function onSelectCallback(fieldName, key, isSelected) {
    let fieldBuckets = selectedBuckets?.[fieldName] || [];
    if (isSelected) {
      // remove the clicked bucket
      fieldBuckets = fieldBuckets.filter(({label}) => label !== key);
    } else {
      // add the clicked bucket

      const b = await getAsyncPlace(fieldName, key);
      fieldBuckets.push(b);
    }

    setSelectedBuckets({
      ...selectedBuckets,
      [fieldName]: fieldBuckets
    });

  }

  function onDeselectAllCallback(fieldName) {
    setSelectedBuckets({
      ...selectedBuckets,
      [fieldName]: []
    })
  }
}


const gqlAggs = gql`
 query Aggregations($mustFilters: String) {
    lexicalSensesAggs(mustFilters: $mustFilters)   
 }
`;

// query to get id of a given concept to create chip's link
const gqlGetConcept = gql`
  query AsyncGrammaticalProperties($qs: String!, $filters: [String!]) {
    grammaticalProperties(
      qs: $qs
      filters: $filters
      first: 1
      sortings: [{sortBy: "name"}]
    ) {
      edges {
        node {
          id
          schemes {
            edges {
              node {
                id
              }
            }
          }
        }
        __typename
      }
      __typename
    }
  }
`

/*
 query to get id of a given place to create chip's link
 for now we don't have controlled vocabulary  with all the places (like geonames)
 ( when a user add a new place to a definition, and this place doesn't exist in the geonames API are queried and we save only the ID, no name, country etc )
 
 so when looking for a given place name, we take results of both and search in result for a strict match ( placeName === prefLabel || placeName === name )
*/
async function getAsyncPlace(fieldName, key) {
  if (fieldName === "placesForAggs") {

    const data = await useAsyncApolloQuery(
      gqlGetPlace,
      {
        "first": 5,
        "qs": key,
        "filters": [
          `prefLabel:${key}`,
          "inScheme:http://data.dictionnairedesfrancophones.org/authority/place"
        ]
      }
    );
    let results = [];
    // look in voc first because this search is more strict ( not using fulltext)
    data?.voc?.edges?.map(({node}) => {
      if (node?.prefLabel.toLowerCase() === key.toLowerCase()) {
        results.push(node)
      }
    })
    // if no result try to fallback on geonames API results
    if (results?.length < 1) {
      data?.geo?.edges?.map(({node}) => {
        if (node?.name.toLowerCase() === key.toLowerCase()) {
          results.push(node)
        }
      })
    }

    return {label: key, id: results?.[0]?.id, type: "place"};

  } else {

    let type = false;
    if (fieldName === "domainForAggs") {
      type = "domain"
    } else if (fieldName === "glossaryForAggs") {
      type = "glossary"
    }
    let filters = [`prefLabel:${key}`];

    if (type) {
      filters.push(`inScheme:http://data.dictionnairedesfrancophones.org/authority/${type}`);
    }
    const data = await useAsyncApolloQuery(
      gqlGetConcept,
      {"qs": "", filters},
      (data) => data?.grammaticalProperties?.edges?.[0]?.node
    );
    const id = data?.id;
    const schemeType = getTypeFromSchemes(data);
    return {label: key, id, type: schemeType}
  }
}


/*
 concepts() is the controlled vocabulary 
 places() query Geonames API with fulltext search, 
 so when looking for a given place name, we take results of both and search in result for a strict match ( placeName === prefLabel || placeName === name )
*/
const gqlGetPlace = gql`
query AsyncPlaces($qs: String!, $first: Int, $filters: [String!]) {
  voc: concepts(    
    filters: $filters
    first: $first
    sortings: [{ sortBy: "prefLabel" }]
  ) {
    edges {
      node {
        id
        prefLabel
      }
      __typename
    }
    __typename
  }

  geo: places(qs: $qs, first: $first) {
    edges {
      node {
        id         
        name
      }
      __typename
    }
    __typename
  }
}
`



const listOfTermToIgnore = ["anciennes", "anciens", "arrondissements", "autorites", "cantons", "capitales",
  "chaines", "chefs lieux", "circonscriptions", "communautes d", "comtes d", "constellations", "continents",
  "cours d", "départements", "districts d", "établissements", "états", "ethnonymes", "étoiles",
  "europe", "îles", "lander-allemands", "lieux-mythologiques", "mers",
  "montagnes", "municipalité", "fromage", "localités", "noms de", "numéros de",
  "odonymes", "organisations internationales", "pays ", "péninsules", "préfectures", "provinces", "quartiers",
  "régions", "réserves indiennes", "sous ", "territoires", "toponymes ", "unions supranationales", "volcans", "villes du quebec", "voivodies de pologne"];


function filterBuckets(buckets) {
  return buckets.filter(({key}) => {
    const _key = key.toLowerCase();
    return !listOfTermToIgnore.some(item => _key.startsWith(item))
  })
}