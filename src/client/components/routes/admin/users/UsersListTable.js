import React from 'react';
import {gql} from '@apollo/client';

import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import {TableView} from '../../../widgets/Table/';
import {UserGroups} from './UserGroups';
import {UserActions} from './UserActions';



const gqlUsersQuery = gql`
  query UsersAccounts($first: Int, $after: String, $qs:String,  $sortings: [SortingInput], $filters: [String]) {
    userAccountsCount( qs: $qs,filters: $filters)
    userAccounts(first: $first, after: $after, sortings: $sortings, qs: $qs,filters: $filters) {
      edges {
        node {
          id
          createdAt
          username
          userId
          isDisabled
          isUnregistered
          person {
            nickName
          }
          userGroups {
            edges {
              node {
                id
                label
              }
            }
          }
        }
      }
      pageInfo {
        startCursor
        endCursor 
      }
    }    
  }  
`;


export function UsersListTable({userGroups, reloadRef, ...props}) {

  return (
    <TableView
      pageSize={10}
      showCheckbox={false}
      columns={createColumns()}
      gqlQuery={gqlUsersQuery}
      gqlCountPath="userAccountsCount"
      gqlConnectionPath="userAccounts"
      ariaDescribedby={'Tableau regroupant toutes les utilisateurs'}
      {...props}
      ref={reloadRef}
      colIndexForLoader={2}
    />
  )


  function createColumns() {
    return [
      {
        ariaLabel: 'Trier le tableau par date',
        width: 3,
        label: 'Date',
        sortFor: 'createdAt',
        customRender: (userAccount) => {
          return dayjs(userAccount.createdAt).format('L')
        }
      }, {
        label: "Email",
        width: 6,
        ariaLabel: 'Trier le tableau par email',
        sortFor: 'username',
        customRender: (userAccount) => userAccount.username
      }, {
        label: "Surnom",
        width: 3,
        customRender: (userAccount) => {
          return userAccount.person?.nickName
        }
      }, {
        label: "Groupes",
        width: 10,
        customRender: (userAccount) => {
          if (!userAccount.isUnregistered) {
            return <UserGroups userAccount={userAccount} userGroups={userGroups} />
          } else {
            return null
          }
        }
      }, {
        label: "Actions",
        width: 3,
        customRender: (userAccount) => <UserActions userAccount={userAccount} />
      }
    ];
  }
}




UsersListTable.propTypes = {
  /**
   * Page size of results, for pagination. Defaults to 10
   */
  pageSize: PropTypes.number,

  /**
   *  filters with  filterOnLoggedUser, filterOnUserAccountId, filterOnReviewed, filterOnExistingSuppressionRating, filterOnProcessed
   */
  filters: PropTypes.object
};

