
/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, {useState} from "react";
import {makeStyles} from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CircularProgress from "@material-ui/core/CircularProgress";
import {UsersGroupTag} from "./UserGroupTag";
import {useTranslation} from "react-i18next";
import {useMutation, gql} from "@apollo/client";
import {notificationService} from "../../../../services/NotificationService";
import {GraphQLErrorHandler} from "../../../../services/GraphQLErrorHandler";
import Config from '../../../../Config';
import LoadingGif from "../../../widgets/LoadingGif";

const gqlUpdateUserAccountMutation = gql`
  mutation UpdateUserAccount($userAccountId: ID! $userAccountInput: UserAccountInput!){
    updateUserAccount(input: {objectId: $userAccountId, objectInput: $userAccountInput}){
      updatedObject{
        id
        ... on UserAccount{
          userGroups{
            edges{
              node{
                id
                label
              }
            }
          }
        }
      }
    }
  }
`;

const gqlRemoveUserGroupMutation = gql`
  mutation RemoveUserGroup($userAccountId: ID! $userGroupId: ID!){
    removeEntityLink(input: {sourceEntityId: $userAccountId, targetEntityId: $userGroupId, linkName: "hasUserGroup"}){
      sourceEntity{
        id
        ... on UserAccount{
          userGroups{
            edges{
              node{
                id
                label
              }
            }
          }
        }
      }
    }
  }
`;


const useStyles = makeStyles((theme) => ({
  container: {
    fontSize: '0.8rem',
    display: "flex",
    flexDirection: "row",
    justifyContent: 'center',
    alignItems: 'center'
  },
  flex: {
    display: "flex",
    flexDirection: "row"
  },
  ml10: {marginLeft: "5px"}
}));



export function UserGroups({userAccount, userGroups}) {
  const {t} = useTranslation();
  const classes = useStyles();
  const [value, setValue] = useState("")

  const onMutationError = async (error) => {
    let {globalErrorMessage} = GraphQLErrorHandler(error, {t});
    await notificationService.error(globalErrorMessage);
  };

  const [addToUserGroup, {loading: savingAddToUserGroup}] = useMutation(gqlUpdateUserAccountMutation, {
    onCompleted: async (data) => {
      if (data?.updateUserAccount) {
        await notificationService.success('Groupe ajouté avec succès');
      }
    },
    onError: onMutationError
  });

  const [removeFromUserGroup] = useMutation(gqlRemoveUserGroupMutation, {
    onCompleted: async (data) => {
      if (data?.removeEntityLink) {
        await notificationService.success('Groupe retiré avec succès');
      }
    },
    onError: onMutationError
  });

  return (
    <div className={classes.flex} >
      <SelectUserGroup
        value={value}
        handleChangeValue={(value) => {
          setValue(value);
          if (value && value.length > 0) {
            addUserAccountToGroup({userAccount, userGroupId: value});
          }
        }}
        savingAddToUserGroup={savingAddToUserGroup}
        userGroups={userGroups}
        excludedGroupsId={userAccount?.userGroups?.edges.map(({node: {id}}) => id)}
      />

      {userAccount?.userGroups?.edges.map(({node: userGroup}, indexGroup) => (
        <UsersGroupTag key={indexGroup} name={userGroup.label || userGroup.id} onRemove={() => removeUserAccountFromGroup({
          userAccount,
          userGroup
        })} />
      ))}
    </div>

  );

  async function addUserAccountToGroup({userAccount, userGroupId}) {
    await addToUserGroup({
      variables: {
        userAccountId: userAccount.id,
        userAccountInput: {
          userGroupInputs: [{
            id: userGroupId
          }]
        }
      }
    })
  }

  async function removeUserAccountFromGroup({userAccount, userGroup}) {
    await removeFromUserGroup({
      variables: {
        userAccountId: userAccount.id,
        userGroupId: userGroup.id
      },
      update(cache, {data}) {
        const {userGroups} = data?.removeEntityLink?.sourceEntity;
        const id = `UserAccount:${userAccount.id}`;

        cache.writeFragment({
          id,
          fragment: gql`
            fragment userAccount on UserAccount {
               userGroups
            }
          `,
          data: {
            ...userAccount,
            userGroups,
          }
        });
      }
    })
  }
}

/**
 * 
 * @param {array} excludedGroupsId is an array with group id to exclude them from display, if a user is already admin don't display `admin` in list for exemple
 * @returns 
 */
export function SelectUserGroup({value, handleChangeValue, savingAddToUserGroup, userGroups, excludedGroupsId = []}) {
  const classes = useStyles();
  return <FormControl variant="outlined" className={classes.container}>
    <Select
      inputProps={{'aria-label': 'Rajouter un groupe à cet utilisateur'}}
      value={value}
      displayEmpty
      className={classes.fontSizeSmall}
      onChange={(event) => handleChangeValue(event.target.value)}
    >
      <MenuItem className={classes.fontSizeSmall} value={""}>
        <em>Sélectionner un groupe</em>
      </MenuItem>


      {userGroups.edges
        .filter(({node: userGroup}) => {
          return !excludedGroupsId.includes(userGroup.id)
        })
        .map(({node: userGroup}) => {
          let label = userGroup.label || userGroup.id;
          return <MenuItem className={classes.fontSizeSmall} key={userGroup.id} value={userGroup.id} label={label}>{label}</MenuItem>
        })}
    </Select>
    {savingAddToUserGroup &&
      <LoadingGif small={true} className={classes.ml5} />
    }
  </FormControl>

}