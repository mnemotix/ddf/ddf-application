import React, {useEffect, useRef, useState} from 'react';
import {gql, useQuery} from '@apollo/client';
import {UsersListTable} from './UsersListTable';
import {Filters} from './Filters';
import {useSnackbar} from 'notistack';
import {useTranslation} from 'react-i18next';

import {MetaSEO} from "../../../widgets/MetaSEO";
import {SimpleModalPage} from '../../../../layouts/responsive/SimpleModalPage';



const gqlUsersQuery = gql`
  query UserGroups {
    userGroups {
      edges {
        node {
          id
          label
        }
      }
    }
  }
`;



export function Users(props) {
  const {t} = useTranslation();

  /* load all groups only one times.
  i cannot get them back from table if i do a query to get users & groups in one shoot
  */
  const {data: {userGroups} = {}, loading, error} = useQuery(gqlUsersQuery, {
    fetchPolicy: 'cache-and-network'
  });

  const userListTableReloadRef = useRef();
  const {enqueueSnackbar} = useSnackbar();


  function newFilters() {
    return {

    }
  }
  const [filters, setFilters] = useState(newFilters());

  function handleFilters(newfilters) {
    setFilters(newfilters)
  }

  function handleOnSelectAction(action) {
    switch (action) {
      // case "processed": break;
      default: console.warn("handleOnSelectAction unknown action name", action)
    }
  }



  return (
    <React.Fragment>
      <MetaSEO title={t('DOCUMENT_TITLES.ADMIN_USERS')} />
      <SimpleModalPage wideWidth={true} title="Utilisateurs" content={renderContent()} titleBgColor="pink" />
    </React.Fragment>
  );



  function renderContent() {
    return (
      <React.Fragment>
        <Filters
          onFiltersUpdate={handleFilters}
          onResetFilters={() => setFilters(newFilters())}
          onSelectAction={handleOnSelectAction}
          userGroups={userGroups}
        />
        {!loading &&
          <UsersListTable
            userGroups={userGroups}
            onSelectedIDsUpdate={() => { }}
            filters={filters}
            reloadRef={userListTableReloadRef}
          />
        }
      </React.Fragment>
    );
  }

}
