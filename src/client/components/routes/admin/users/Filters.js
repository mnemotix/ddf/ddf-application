import React, {useEffect, useRef, useState} from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import Config from '../../../../Config';
import Grid from '@material-ui/core/Grid';

import FormControl from '@material-ui/core/FormControl';
import DivButton from '../../../widgets/DivButton';
import {PersonAutocompleteMUIA} from '../../../widgets/PersonAutocompleteMUIA';
import useDebounce from '../../../../hooks/useDebounce';
import {useStyles as useFilterStyles} from "../../../contribution/ContributionsList/Filters";
import {SelectUserGroup} from "./UserGroups";

Filters.propTypes = {
  onFiltersUpdate: PropTypes.func.isRequired,
};


export function Filters({onFiltersUpdate, userGroups, filtersInitialValues, onResetFilters}) {
  const classes = useFilterStyles();

  const containerRef = useRef(null);
  // input value for person name only
  const [personInputValue, setPersonInputValue] = useState('');

  // for filter on group
  const [groupValueFilter, setGroupValueFilter] = useState("")
  // for filter on nickname, i use debounce on personInputValue to set userValueFilter to avoid excessive reload
  const debouncedPersonInputValue = useDebounce(personInputValue, 1500);

  useEffect(() => {
    let filters = []
    if(debouncedPersonInputValue?.length > 0) {
      filters.push(`hasPerson.nickName = ${debouncedPersonInputValue}`);
    }
    if (groupValueFilter?.length > 0) {
      filters.push(`hasUserGroup.id = ${groupValueFilter}`);
    }
    onFiltersUpdate({filters });
  }, [groupValueFilter, debouncedPersonInputValue]);


  function resetFilters() {
    setGroupValueFilter("");
    setPersonInputValue("");
    onResetFilters();
  }

  return (
    <div className={clsx(classes.mediumPadding, classes.container)}>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid item zeroMinWidth className={classes.purpleText}>
          Filtres
        </Grid>
        <Grid item zeroMinWidth className={clsx(classes.purpleText, classes.underline)}>
          <DivButton
            aria-label="Supprimer tous les filtres"
            onClick={resetFilters} >
            Supprimer tous les filtres
          </DivButton>
        </Grid>
      </Grid>

      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="center"
      >
        <Grid item xs={12} md={4} lg={3} zeroMinWidth className={classes.flexCol}>
          {userGroups &&
            <SelectUserGroup
              value={groupValueFilter}
              handleChangeValue={(value) => {
                setGroupValueFilter(value);
              }}
              savingAddToUserGroup={false}
              userGroups={userGroups}
              excludedGroupsId={[]}
            />
          }
        </Grid>
        <Grid item xs={12} md={4} lg={3} zeroMinWidth className={classes.flexCol}>
          <FormControl variant="outlined" className={clsx(classes.formControl, classes.borderInput)} ref={containerRef} >
            <PersonAutocompleteMUIA
              showIsRequired={false}
              placeholder={"Surnom"}
              handleChange={handlePersonChange}
              handleSubmit={handlePersonSubmit}
              value={personInputValue || ""}
              inputRef={null}
              containerRef={containerRef}
              addForm={false}
              labelCreator={({node: {nickName, userAccount}}) => {
                return ({label: nickName, id: userAccount.id})
              }}
            />
          </FormControl>
        </Grid>
      </Grid>
    </div>)

  function handlePersonChange(newObj) {
    //  console.log("handlePersonChange", newObj);
    setPersonInputValue(newObj?.label || "")

  }

  function handlePersonSubmit() {return null;}
}


