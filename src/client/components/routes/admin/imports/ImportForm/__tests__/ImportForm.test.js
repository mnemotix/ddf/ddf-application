import '@testing-library/jest-dom';
import {screen, fireEvent} from "@testing-library/react";
import {act} from 'react-dom/test-utils';
import userEvent from "@testing-library/user-event";
import waait from "waait";

import {ImportForm} from "../ImportForm";
import {renderWithMocks} from "../../../../../../../../jest/utilities/renderWithMocks";
import {gqlLexicographicResources} from "../useLexicographicResourcesOptions";
import {gqlCreateImport} from "../useCreateImportMutation";



const mockEnqueueSnackbar = jest.fn();

jest.mock('notistack', () => ({
  ...jest.requireActual('notistack'),
  useSnackbar: () => {
    return {
      enqueueSnackbar: mockEnqueueSnackbar
    };
  }
}));

jest.mock('../uploadFileXHR', () => ({
  uploadFileXHR: jest.fn(() => Promise.resolve("1234567890"))
}));


const gqlMocks = [{
  request: {
    query: gqlLexicographicResources,
  },
  result: {
    "data": {
      "lexicographicResources": {
        "totalCount": 1,
        "edges": [
          {
            "node": {
              "id": "asom",
              "altLabel": "asom"
            }
          }
        ],
      }
    }
  }
},
{
  request: {
    query: gqlCreateImport,
    variables: {
      input: {
        objectInput: {
          dicoName: "asom",
          fileId: "1234567890"
        }
      }
    }
  },
  result: {
    "data": {
      "createImportJob": {
        "success": true
      }
    }
  }
}];

describe("Import Form component", () => {
  it("Renders a disabled submit button if form is not filled", async () => {
    renderWithMocks({
      gqlMocks,
      element: <ImportForm />
    });

    await act(waait);

    // Given a fileInput, a source selector and a button
    const inputFile = screen.getByTestId("input-file");
    const selectSource = screen.getByTestId("select-source");
    const button = screen.getByTestId("action-button");

    // Then the component should render correcty
    expect(selectSource).toBeInTheDocument();
    expect(inputFile).toBeInTheDocument();
    expect(button).toBeInTheDocument();

    // If user click on the button
    await userEvent.click(button);

    // And nothing should have happened
    expect(button).toHaveAttribute("aria-disabled", "true")
    expect(mockEnqueueSnackbar).not.toHaveBeenCalled();
  });

  it("Renders an enabled submit button if form is correctly filled", async () => {
    renderWithMocks({
      gqlMocks,
      element: <ImportForm />
    });

    await act(waait);

    // Given a fileInput, a source selector and a button
    const inputFile = screen.getByTestId("input-file");
    const selectSource = screen.getByTestId("select-source");
    const button = screen.getByTestId("action-button");

    // If user select a source
    const file = new File([new ArrayBuffer(1)], 'file.csv', {type: 'text/csv'});

    await userEvent.selectOptions(selectSource, ["asom"]);
    await userEvent.upload(inputFile, file);

    await act(waait);

    // If user click on the button
    fireEvent.click(button);

    // Then button should be enabled
    expect(button).toHaveAttribute("aria-disabled", "false")

    await act(waait);

    // And import must be requested with success.
    expect(mockEnqueueSnackbar).toHaveBeenCalledWith('Tâche créée avec succès !', { variant: 'success' } );
  });
});