export function uploadFileXHR({
  file,
  onProgress
}) {
  if (!file) {
    return;
  }

  return new Promise((done, fail) => {
    let data = new FormData();
    data.append('csvFile', file);

    let request = new XMLHttpRequest();
    request.withCredentials = true;
    request.open('POST', '/api/upload/csv');

    request.upload.addEventListener('progress', function (e) {
      const progress = Math.round(e.loaded / e.total * 100);
      onProgress(progress);
    });

    request.addEventListener('load', function (e) {
      if (request.status === 200) {
        done(request.response);
      } else {
        fail(request.response)
      }
    });

    request.send(data);
  })

}