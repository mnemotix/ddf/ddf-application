import {gql, useQuery} from "@apollo/client";

export const gqlLexicographicResources = gql`
  query LexicographicResources {
    lexicographicResources(filters:["storedInNamedGraph = *", "isCsvUpdatable = true"]){
      totalCount
      edges {
        node {
          id
          altLabel
        }
      }
    }
  }
`;

/**
 * @return {{id: *, label: *, value: *}[]}
 */
export function useLexicographicResourcesOptions(){
  const {data : {lexicographicResources} = {}} = useQuery(gqlLexicographicResources);

  const lexicographicResourceOptions = (lexicographicResources?.edges || []).map(({node: lexicographicResource}) => ({
    id: lexicographicResource.id,
    value: lexicographicResource.altLabel,
    label: lexicographicResource.altLabel
  }));

  return lexicographicResourceOptions;
}