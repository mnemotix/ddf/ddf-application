import {useEffect, useReducer} from "react";
import {uploadFileXHR} from "./uploadFileXHR";

export const Actions = {
  FILE_UPLOAD_PROGRESS: "file_upload_progress",
  FILE_UPLOAD_SUCCESS: "file_upload_success",
  FILE_UPLOAD_ERROR: "file_upload_error",
  RESET: "reset"
}

export function reducer(state, action) {
  switch (action.type) {
    case Actions.FILE_UPLOAD_PROGRESS: {
      return {
        ...state,
        uploadProgress: action.progress,
        error: null
      }
    }
    case Actions.FILE_UPLOAD_SUCCESS:
      return {
        ...state,
        uploadedFileId: action.fileHash,
        uploadProgress: null,
        error: null
      }
    case Actions.FILE_UPLOAD_ERROR:
      return {
        ...state,
        uploadedFileId: null,
        uploadProgress: null,
        error: action.error
      }
  }

  return {};
}

/**
 * @return {{uploadFile: function, uploadProgress: number, uploadedFileId: string, error: string}}
 */
export function useFileUpload({importFormObject, updateImportFormObject} = {} ){
  const [{uploadProgress, error}, updateState] = useReducer(reducer, {});

  useEffect(() => {
    if(!importFormObject.fileId){
      updateState({
        type: Actions.RESET
      })
    }
  }, [importFormObject]);

  return  {
    uploadProgress,
    error,
    uploadFile
  }

  async function uploadFile(file){
    try{
      const fileId = await uploadFileXHR({
        file,
        onProgress: (progress) => {
          updateState({
            type: Actions.FILE_UPLOAD_PROGRESS,
            progress
          })
        }
      });
      updateState({
        type: Actions.FILE_UPLOAD_SUCCESS,
        fileHash:  fileId
      });
      updateImportFormObject({
        fileId
      })
    } catch (e){
      updateState({
        type: Actions.FILE_UPLOAD_ERROR,
        error:  request.response
      });
    }
  }
}