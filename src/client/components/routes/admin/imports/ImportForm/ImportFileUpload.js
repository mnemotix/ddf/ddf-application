/*
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";

import Config from "../../../../../Config";
import {useFileUpload} from "./useFileUpload";

const useStyles = makeStyles((theme) => ({
  fileUploadError:{
    color:  theme.palette.error.light,
    fontSize: Config.fontSizes.sm,
    marginTop: theme.spacing(1)
  },
  fileUploadSuccess: {
    color: theme.palette.success.light,
    fontSize: Config.fontSizes.sm,
    marginTop: theme.spacing(1)
  },
  fileInput: {
    borderStyle: "dashed",
    borderColor: Config.colors.mediumgray,
    borderRadius: 4,
    padding: theme.spacing(1),
    width: "100%"
  }
}));

export function ImportFileUpload({ importFormObject, updateImportFormObject } = {}) {
  const { t } = useTranslation();
  const classes = useStyles();

  const {
    uploadProgress, error, uploadFile
  } = useFileUpload({importFormObject, updateImportFormObject});

  return (
    <>
      <input type="file" onChange={handleFileChange} accept="text/csv" data-testid="input-file"/>
      <If condition={uploadProgress}>
        <div className={classes.fileUploadSuccess}>
          {t("UPLOAD_CSV.FILE_UPLOAD.UPLOADING")} - ({uploadProgress} %)
        </div>

      </If>
      <If condition={error}>`
        <div className={classes.fileUploadError}>
          {t("UPLOAD_CSV.FILE_UPLOAD.ERROR")} - {error}
        </div>
      </If>
      <If condition={importFormObject.fileId}>
        <div className={classes.fileUploadSuccess}>
          {t("UPLOAD_CSV.FILE_UPLOAD.SUCCESS")}
        </div>
      </If>
    </>
  );

  function handleFileChange(e){
    uploadFile(e.target.files[0]);
  }
}
