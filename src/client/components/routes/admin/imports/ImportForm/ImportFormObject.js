export class ImportFormObject{
  _fileId;
  _lexicographicResource;

  /**
   * @param {string} [fileId]
   * @param {string} [lexicographicResource]
   */
  constructor({fileId, lexicographicResource} = {}) {
    this._fileId = fileId;
    this._lexicographicResource = lexicographicResource;
  }

  get fileId() {
    return this._fileId;
  }

  get lexicographicResource() {
    return this._lexicographicResource;
  }

  importEnabled(){
    return this._lexicographicResource && this._fileId;
  }
}