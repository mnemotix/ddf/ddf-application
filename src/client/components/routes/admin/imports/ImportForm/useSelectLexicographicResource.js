import {useState} from "react";
import {useLexicographicResourcesOptions} from "./useLexicographicResourcesOptions";

/**
 * @return {{lexicographicResourceOptions: {id: *, label: *, value: *}[]}}
 */
export function useSelectLexicographicResource(){
  const lexicographicResourceOptions = useLexicographicResourcesOptions();

  return {
    lexicographicResourceOptions
  }
}