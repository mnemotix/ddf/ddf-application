import {useMutation, gql} from "@apollo/client";

export const gqlCreateImport = gql`
  mutation CreateImport($input: CreateImportJobInput!) {
    createImportJob(input: $input) {
      success
    }
  }
`;

export function useCreateImportMutation() {
  const [doCreateImport, {loading}] = useMutation(gqlCreateImport);

  function createImport({
    importFormObject
  }) {
    return doCreateImport({
      variables: {
        input: {
          objectInput: {
            dicoName: importFormObject.lexicographicResource,
            fileId: importFormObject.fileId
          }
        }
      }
    });
  }

  return {
    loading,
    createImport
  }
}