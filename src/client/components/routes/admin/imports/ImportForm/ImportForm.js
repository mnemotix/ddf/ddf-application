import {useSnackbar} from "notistack";
import {useTranslation} from "react-i18next";
import Grid from "@material-ui/core/Grid";

import {makeStyles} from "@material-ui/core/styles";
import {ActionButton} from "../../../../widgets/ActionButton";
import {ImportFileUpload} from "./ImportFileUpload";
import {useImportForm} from "./useImportForm";
import {ImportSelectLexicographicResource} from "./ImportSelectLexicographicResource";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "flex",
    alignItems: "center",
    width: "100%"
  },
  fileInput: {
    border: "dashed 1px",
    borderColor: "lightgrey",
    borderRadius: 4
  }
}));

export function ImportForm({} = {}) {
  const { t } = useTranslation();
  const {enqueueSnackbar} = useSnackbar();
  const classes = useStyles();

  const {
    loading,
    importFormObject,
    updateImportFormObject,
    createImport
  } = useImportForm();

  return (
    <Grid container className={classes.form} spacing={2}>
      <Grid item xs={12} md className={classes.fileInput}>
        <ImportFileUpload importFormObject={importFormObject}  updateImportFormObject={updateImportFormObject}/>
      </Grid>

      <Grid item  xs={12} md>
        <ImportSelectLexicographicResource  importFormObject={importFormObject}  updateImportFormObject={updateImportFormObject} />
      </Grid>

      <Grid item  xs={12} md={3}>
        <ActionButton onClick={handleCreateImport} variant="contained" color="orange" autoFocus loading={loading} disabled={!importFormObject.importEnabled()} text={"Lancer l'import"} showText/>
      </Grid>
    </Grid>
  );

  async function handleCreateImport(){
    try{
      await createImport();
      enqueueSnackbar(t("JOB.ACTIONS.CREATE.SUCCESS"), {variant: "success"});
    }catch (e){
      enqueueSnackbar(t("JOB.ACTIONS.CREATE.ERROR", {message: e.message}), {variant: "error"});
    }
  }
}
