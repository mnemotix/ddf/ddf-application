/*
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Config from "../../../../../Config";
import {useSelectLexicographicResource} from "./useSelectLexicographicResource";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "flex",
    alignItems: "center",
    width: "100%"
  },
  fileUploadError:{
    color:  theme.palette.error.light,
    fontSize: Config.fontSizes.sm,
    marginTop: theme.spacing(1)
  },
  fileUploadSuccess: {
    color: theme.palette.success.light,
    fontSize: Config.fontSizes.sm,
    marginTop: theme.spacing(1)
  },
  fileInput: {
    borderStyle: "dashed",
    borderColor: Config.colors.mediumgray,
    borderRadius: 4,
    padding: theme.spacing(1),
    width: "100%"
  }
}));

export function ImportSelectLexicographicResource({ importFormObject, updateImportFormObject } = {}) {
  const { t } = useTranslation();
  const classes = useStyles();

  const {
    lexicographicResourceOptions
  } = useSelectLexicographicResource();

  return (
    <FormControl className={classes.formControl} variant="outlined" size={"small"} fullWidth>
      <InputLabel htmlFor="source">{t("UPLOAD_CSV.FORM.SOURCE")}</InputLabel>
      <Select
        id="source"
        native
        placeholder={"Sélectionner "}
        value={importFormObject.lexicographicResource || ""}
        onChange={handleSelectLexicographicResource}
        label={t("UPLOAD_CSV.FORM.SOURCE")}
        inputProps={{
          "data-testid": "select-source"
        }}
      >
        <option value="">
        </option>

        {lexicographicResourceOptions.map(({id, label, value}) => (
          <option key={id} value={value}>
            {label}
          </option>
        ))}
      </Select>
    </FormControl>
  );

  function handleSelectLexicographicResource(e){
    updateImportFormObject({lexicographicResource: e.target.value});
  }
}
