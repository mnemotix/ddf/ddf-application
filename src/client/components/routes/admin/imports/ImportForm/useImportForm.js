import {useState} from "react";
import {ImportFormObject} from "./ImportFormObject";
import {useCreateImportMutation} from "./useCreateImportMutation";

/**
 * @return {{setLexicographicResource: (value: (prevState: undefined) => undefined) => void, lexicographicResource: undefined, onCreateImport: ((function(): Promise<void>)|*), loading: boolean, importEnabled: (undefined|((value: (prevState: undefined) => undefined) => void)), createImport: function}}
 */
export function useImportForm(){
  const {createImport: mutateCreateImport, loading} = useCreateImportMutation();
  const [importFormObject, setImportFormObject] = useState(new ImportFormObject());

  return  {
    loading,
    importFormObject,
    updateImportFormObject,
    createImport,
    resetForm
  }

  function updateImportFormObject({fileId, lexicographicResource}){
    const updatedImportFormObject = new ImportFormObject({
      fileId: fileId || importFormObject.fileId,
      lexicographicResource: lexicographicResource || importFormObject.lexicographicResource
    });

    setImportFormObject(updatedImportFormObject);
  }

  function resetForm(){
    setImportFormObject(new ImportFormObject());
  }

  async function createImport(){
    if(importFormObject.importEnabled()) {
      await mutateCreateImport({importFormObject});
      resetForm();
    }
  }
}

