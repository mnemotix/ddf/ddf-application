import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import {TableView} from '../../../widgets/Table';
import {gqlImports} from "./gql/Imports.gql";
import {useTranslation} from "react-i18next";
import {useMemo} from "react";
import upperFirst from "lodash/upperFirst";

export function ImportsHistory({pageSize  = 10} = {}) {
  const {t} = useTranslation();

  const columns = useMemo(() => {
    return [
      {
        label: "Nom",
        width: 6,
        ariaLabel: 'Trier le tableau par email',
        customRender: ({name}) => t("UPLOAD_CSV.JOB_SOURCE_NAME", {source: upperFirst(name || "")})
      },
      {
        label: "Statut",
        ariaLabel: 'Trier le tableau par statut',
        width: 3,
        customRender: ({status}) => {
          return t(`JOB.STATUS.${status.toUpperCase()}`);
        }
      },
      {
        label: "Message",
        width: 6,
        customRender: ({message}) => message
      },
      {
        ariaLabel: 'Trier le tableau par date d\'import du fichier',
        width: 3,
        label: 'Début de l\'import',
        sortFor: 'startedAt',
        customRender: ({startedAt}) => {
          return dayjs(startedAt).format('LLL')
        }
      },
      {
        ariaLabel: 'Trier le tableau par date de fin de l\'import',
        width: 3,
        label: 'Fin de l\'import',
        sortFor: 'endedAt',
        customRender: ({endedAt}) => {
          if(endedAt){
            return dayjs(endedAt).format('LLL')
          }
        }
      },
    ];
  }, []);

  return (
    <TableView
      pageSize={20}
      showCheckbox={false}
      columns={columns}
      gqlQuery={gqlImports}
      filters={{filters: ["category != import_audio"]}}
      gqlConnectionPath="imports"
      ariaDescribedby={'Tableau regroupant tous imports CSV'}
      colIndexForLoader={2}
      pollInterval={2000}
      defaultSortBy={"startedAt"}
    />
  )
}




ImportsHistory.propTypes = {
  /**
   * Page size of results, for pagination. Defaults to 10
   */
  pageSize: PropTypes.number,
};

