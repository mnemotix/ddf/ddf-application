import {useTranslation} from 'react-i18next';
import {MetaSEO} from "../../../widgets/MetaSEO";
import {SimpleModalPage} from '../../../../layouts/responsive/SimpleModalPage';
import {ImportsHistory} from "./ImportsHistory";
import {ImportForm} from "./ImportForm/ImportForm";
import useGlobalStyles from "../../../../assets/stylesheets/globalStyles";

export function Imports({ }) {
  const {t} = useTranslation();
  const classes = useGlobalStyles();

  return (
    <>
      <MetaSEO title={t('DOCUMENT_TITLES.ADMIN_UPLOAD_CSV')} />
      <SimpleModalPage wideWidth={true} title={t('UPLOAD_CSV.TITLE')} content={renderContent()} titleBgColor="pink" />
    </>
  );

  function renderContent() {
    return (
      <>
        <div className={classes.whiteContainer}>
          <ImportForm/>
        </div>

        <ImportsHistory />
      </>
    )
  }
}
