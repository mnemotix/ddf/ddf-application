import React, {useContext, useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import {useHistory, Link} from 'react-router-dom';
import {ROUTES} from '../../../routes';
import {FormContainer} from '../../widgets/forms/FormContainer';
import {MetaSEO} from "../../widgets/MetaSEO";
import {Button} from '../../widgets/forms/Button';
import {SimpleModalPage} from '../../../layouts/responsive/SimpleModalPage';

import {notificationService as appNotificationService} from '../../../services/NotificationService';
import {UserContext} from '../../../hooks/UserContext';

export function AdminIndex(props) {
  const {t} = useTranslation();
  const history = useHistory();
  const notificationService = props.notificationService || appNotificationService;
  const {user} = useContext(UserContext);

  useEffect(() => {
    (async () => {
      if (user?.isLogged && !(user.userAccount.isOperator || user.userAccount.isAdmin)) {
        await notificationService.error("Seul les administrateurs peuvent accéder à ce contenu");
        history.push('/');
      }
    })();
  }, [user]);

  return (
    <React.Fragment>
      <MetaSEO title={t('DOCUMENT_TITLES.ADMIN_INDEX')} />
      <SimpleModalPage
        title={t('ADMIN.INDEX.TITLE')}
        content={renderContent()}
        titleBgColor="yellow"
      />
    </React.Fragment>
  );

  function renderContent() {
    return (
      <FormContainer>
        <If condition={user?.userAccount?.isOperator || user?.userAccount?.isAdmin}>
          <Button type="button" as={Link} role="link" aria-label={t('MOBILE_MENU.OPERATORS.CONTRIBUTIONS')} to={ROUTES.CONTRIBUTIONS_REVIEW}>{t('MOBILE_MENU.OPERATORS.CONTRIBUTIONS')}</Button>
        </If>
        <If condition={user?.userAccount?.isAdmin}>
          <Button type="button" as={Link} role="link" aria-label={t('MOBILE_MENU.ADMIN.USERS')} to={ROUTES.ADMIN_USERS}>{t('MOBILE_MENU.ADMIN.USERS')}</Button>
        </If>
        <If condition={user?.userAccount?.isAdmin}>
          <Button type="button" as={Link} role="link" aria-label={t('MOBILE_MENU.ADMIN.REPORTING')} to={ROUTES.ADMIN_REPORTING}>
            {t('MOBILE_MENU.ADMIN.REPORTING')}
          </Button>
        </If>
        <Button type="button" as={Link} role="link" aria-label="SPARQL" to={ROUTES.SPARQL}>SPARQL</Button>
        <Button type="button" as={Link} role="link" aria-label="Fichiers de DUMP" to={ROUTES.DUMP_FILES}>Fichiers de DUMP</Button>
        <Button type="button" as={Link} role="link" aria-label={t("UPLOAD_CSV.TITLE")} to={ROUTES.IMPORT_SOURCE}>{t("UPLOAD_CSV.TITLE")}</Button>
        <Button type="button" as={Link} role="link" aria-label={t("MONITOR_DATA_INGEST.TITLE")} to={ROUTES.MONITOR_DATA_INGEST}>{t("MONITOR_DATA_INGEST.TITLE")}</Button>
      </FormContainer>
    );
  }
}

