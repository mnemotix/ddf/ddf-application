import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import {TableView} from '../../../widgets/Table';
import {gqlDataIngestJobs} from "./DataIngestHistoryJobs.gql";
import {useTranslation} from "react-i18next";
import {useMemo} from "react";

export function DataIngestHistory({pageSize  = 20} = {}) {
  const {t} = useTranslation();

  const columns = useMemo(() => {
    return [
      {
        label: "Catégorie",
        width: 6,
        customRender: ({category}) => t(`MONITOR_DATA_INGEST.COLUMNS.CATEGORY.${(category || "import_source").toUpperCase()}`)
      },
      {
        label: "Nom",
        width: 6,
        customRender: ({name}) => name
      },
      {
        label: "Statut",
        width: 3,
        customRender: ({status}) => {
          return t(`JOB.STATUS.${status.toUpperCase()}`);
        }
      },
      {
        label: "Message",
        width: 6,
        customRender: ({message}) => message
      },
      {
        ariaLabel: 'Trier le tableau par date d\'import du fichier',
        width: 3,
        label: 'Début de l\'import',
        sortFor: 'startedAt',
        customRender: ({startedAt}) => {
          return dayjs(startedAt).format('LLL')
        }
      },
      {
        ariaLabel: 'Trier le tableau par date de fin de l\'import',
        width: 3,
        label: 'Fin de l\'import',
        sortFor: 'endedAt',
        customRender: ({endedAt}) => {
          if(endedAt){
            return dayjs(endedAt).format('LLL')
          }
        }
      },
    ];
  }, []);

  return (
    <TableView
      pageSize={pageSize}
      showCheckbox={false}
      columns={columns}
      gqlQuery={gqlDataIngestJobs}
      filters={{filters: ["category: import_audio"]}}
      gqlConnectionPath="dataIngestJobs"
      ariaDescribedby={"Tableau de monitoring d'ingestion de données"}
      colIndexForLoader={2}
      pollInterval={2000}
      defaultSortBy={"startedAt"}
    />
  )
}




DataIngestHistory.propTypes = {
  /**
   * Page size of results, for pagination. Defaults to 10
   */
  pageSize: PropTypes.number,
};

