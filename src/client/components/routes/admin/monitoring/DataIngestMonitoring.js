import {useTranslation} from 'react-i18next';
import {MetaSEO} from "../../../widgets/MetaSEO";
import {SimpleModalPage} from '../../../../layouts/responsive/SimpleModalPage';
import {DataIngestHistory} from "./DataIngestHistory";

export function DataIngestMonitoring({ }) {
  const {t} = useTranslation();

  return (
    <>
      <MetaSEO title={t('DOCUMENT_TITLES.ADMIN_MONITOR_DATA_INGEST')} />
      <SimpleModalPage wideWidth={true} title={t('MONITOR_DATA_INGEST.TITLE')} content={renderContent()} titleBgColor="pink" />
    </>
  );

  function renderContent() {
    return (
      <DataIngestHistory />
    )
  }
}
