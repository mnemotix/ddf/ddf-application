import React, {useState, useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import {makeStyles} from "@material-ui/core/styles";
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import Link from '@material-ui/core/Link';
import dayjs from 'dayjs';
import {MetaSEO} from "../../widgets/MetaSEO";
import {SimpleModalPage} from '../../../layouts/responsive/SimpleModalPage';
import {Col, ColHeader, Row, Table} from '../../widgets/Table/TableComponents.js';
import {Paginator} from '../../widgets/Paginator';
import Config from '../../../Config.js';
import globalStyles from '../../../assets/stylesheets/globalStyles.js';
import {useEnvironment} from '../../../hooks/useEnvironment';



const useStyles = makeStyles((theme) => ({
  minwidth90: {
    minWidth: "90px"
  },
  sortIcon: {
    cursor: "pointer",
    marginLeft: Config.spacings.tiny,
  },

  noResults: {
    textAlign: "center",
    color: Config.colors.darkgray
  },

  center: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  marginTop: {"marginTop": "20px"}
}));

export function DumpFiles(props) {
  const {t} = useTranslation();
  const classes = useStyles();
  const gS = globalStyles();

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [files, setFiles] = useState([]);
  const [isSortDescending, setIsSortDescending] = useState(true);

  const pageSize = useEnvironment('PAGINATION_SIZE', {isInt: true});
  const [currentPage, setCurrentPage] = useState(1);
  const [pageCount, setPageCount] = useState(0);

  // load data on first start
  useEffect(() => {
    getFilesList()
  }, [])

  // on each page change
  useEffect(() => {

    getFilesList()
  }, [currentPage]);


  function getFilesList() {


    const after = currentPage > 1 ? `${((pageSize) * (currentPage - 1))}` : 0;
    const url = window.location.origin + `/api/dumpfiles?first=${pageSize}&after=${after}`;

    setLoading(true);
    fetch(url)
      .then(res => res.json())
      .then((res) => {
        setLoading(false);
        const {count, files} = res;
        setPageCount(Math.ceil(count / pageSize));
        const sortedRes = sortResults(files)
        setFiles(sortedRes);
      },
        (error) => {
          setLoading(true);
          setError(error);
        }
      )
  }


  return (
    <React.Fragment>
      <MetaSEO title={t('DOCUMENT_TITLES.ADMIN_DUMP')} />
      <SimpleModalPage wideWidth={true} title="Fichiers de Dump" content={
        <>
          {renderTable()}
          <Paginator currentPage={currentPage} pageCount={pageCount} setCurrentPage={setCurrentPage} />
        </>
      }
        titleBgColor="pink" />
    </React.Fragment>
  );

  function sortResults(results) {
    return results.sort(function (a, b) {
      return new Date(a.date) - new Date(b.date);
    });
  }


  function renderTable() {
    if (error) {
      return <div>Erreur : {error.message}</div>;
    } else if (loading) {
      return <div>Chargement...</div>;
    } else {
      return (
        <Table aria-describedby={"fichiers de DUMP"}>
          <Row heading>
            <ColHeader idkey={'filename'} className={classes.minwidth90}>Nom</ColHeader>
            <ColHeader idkey={'taille'} className={classes.minwidth90}>Taille</ColHeader>
            <ColHeader idkey={'date'} className={classes.minwidth90}
              onClick={() => {
                setIsSortDescending(!isSortDescending);
                setFiles(files.reverse());
              }}
            >
              <div className={gS.flexCenter}>
                Date
                <span className={classes.sortIcon}>
                  {isSortDescending ? "▼" : "▲"}
                </span>
              </div>
            </ColHeader>
            <ColHeader idkey={'download'} className={classes.minwidth90}>Téléchargement</ColHeader>
          </Row>
          {renderContent()}

        </Table>
      );
    }
  }


  function renderContent() {
    if (!files || files.length < 1) {
      return <Row key={0} >
        <Col colSpan={4}>Aucun fichier</Col>
      </Row>
    }
    return files.map(({filename, path, size, creation}, index) =>
      <Row key={index} even={index % 2 === 1}>
        <Col idkey={'filename'} className={classes.minwidth90}>{filename}</Col>
        <Col idkey={'taille'} className={classes.minwidth90}>{size}</Col>
        <Col idkey={'date'} className={classes.minwidth90}>{dayjs(creation).format('L à LT')}</Col>
        <Col idkey={'download'} className={classes.minwidth90}>{renderLink(path)}</Col>
      </Row>
    )
  }
  function renderLink(path) {
    if (!!path && path.length > 0) {
      let href = window.location.origin + "/api/downloadfile/" + encodeURIComponent(path);
      return <Link href={href} className={classes.link} download>
        <CloudDownloadIcon />
      </Link>
    }
  }

}
