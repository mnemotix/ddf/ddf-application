import React from 'react';
import {gql} from '@apollo/client';
import {useTranslation} from 'react-i18next';
import dayjs from 'dayjs';
import {formatRoute} from 'react-router-named-routes';
import {Link} from 'react-router-dom';
import {TableView} from '../../../widgets/Table/';
import {ROUTES} from '../../../../routes';
import {convertIdToReason} from "../../../LexicalSense/LexicalSenseActions/ReportingRatingAction/ReportingRatingCause"

const gqlReportingsQuery = gql`
  query ReportingRatings ($first: Int, $after: String, $qs:String,  $sortings: [SortingInput], $filters: [String]){
    reportingRatingsCount( qs: $qs,filters: $filters)
    reportingRatings(first: $first, after: $after, sortings: $sortings, qs: $qs,filters: $filters) {
      edges {
        node {
          id
          reportingMessage
          types
          createdAt
          actionsCount
          creatorUserAccount {
            id
            username
          }         
          createdBy          
          reviewedLexicalSenseFormWrittenRep
          entity {
            id 
          }          
        }
      }
    }
  } `

/*
  reviewedLexicalSenseFormWrittenRepID
  entity {
    id 
    ... on LexicalSense {
      canonicalFormWrittenRep        
    }   
  }   
*/


export function ReportingsList({reloadRef, ...props}) {
  let {t} = useTranslation();
  return (
    <TableView
      pageSize={10}
      showCheckbox={false}
      columns={createColumns()}
      gqlQuery={gqlReportingsQuery}
      gqlCountPath="reportingRatingsCount"
      gqlConnectionPath="reportingRatings"
      ariaDescribedby={'Tableau regroupant toutes les signalements'}
      {...props}
      ref={reloadRef}
      colIndexForLoader={3}
    />
  )

  /*
    Définition signalée
    type de signalement
    compte ayant signalé
    date et heure de signalement
    Éventuel message complémentaire
  */

  function createColumns() {
    return [
      {
        ariaLabel: 'Trier par terme',
        width: 2,
        label: 'Terme',
        sortFor: 'reviewedLexicalSenseFormWrittenRep',
        customRender: ({reviewedLexicalSenseFormWrittenRep, entity}) => {

          if (reviewedLexicalSenseFormWrittenRep) {
            if (entity?.id) {
              return <Link role="link" aria-label="Localisations"
                target="_blank"
                to={formatRoute(ROUTES.FORM_LEXICAL_SENSE, {formQuery: reviewedLexicalSenseFormWrittenRep, lexicalSenseId: entity?.id})}
              >
                {reviewedLexicalSenseFormWrittenRep}
              </Link>
            } else {
              return reviewedLexicalSenseFormWrittenRep
            }
          } else {
            return null
          }
        }
      },
      {
        ariaLabel: 'Trier par date',
        label: 'Date',
        width: 2,
        sortFor: 'createdAt',
        customRender: (reporting) => dayjs(reporting.createdAt).format('L à LT')

      }, {
        label: "Auteur",
        width: 4,
        ariaLabel: 'Trier par auteur',
        sortFor: 'createdBy',
        customRender: (reporting) => `${reporting.createdBy?.[0]} (${reporting?.creatorUserAccount?.username})`
      }, {
        label: "Type",
        width: 8,
        ariaLabel: 'Trier par type',
        sortFor: 'types',
        tooltip: "Attention le tri se fait sur la valeur du type qui est en anglais, pas sur le label traduit en français",
        customRender: (reporting) => getTranslatedMeaning(reporting.id)

      }, {
        label: "Commentaire",
        width: 8,
        customRender: (reporting) => {
          return reporting?.reportingMessage
        }
      }
    ];
  }

  function getTranslatedMeaning(id) {

    let meaning = convertIdToReason(id);

    return meaning ? t(`LEXICAL_SENSE_DETAIL.MODAL.REASONS.${meaning}`) : 'error'
  }
}
