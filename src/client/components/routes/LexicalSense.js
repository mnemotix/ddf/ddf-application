/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React, {useState, useContext} from 'react';


import {ShowWhereIAM} from '../../components/widgets/ShowWhereIAM';
import clsx from 'clsx';
import {useTranslation} from 'react-i18next';

import {useQuery, gql} from '@apollo/client';
import {Link, Route, Switch, useParams, useHistory} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {ROUTES} from '../../routes';
import {MetaSEO} from '../widgets/MetaSEO';
import {makeStyles} from '@material-ui/core/styles';
import contributionsIcon from '../../assets/images/contributions_purple.svg';
import globalStyles from '../../assets/stylesheets/globalStyles';
import EditIcon from '@material-ui/icons/Edit';
import {LexicalSenseDistinctSemanticRelationList} from '../LexicalSense/LexicalSenseDistinctSemanticRelationList';
import {LexicalSenseDesktopSideColumn, LexicalSenseSource} from '../LexicalSense/LexicalSenseDesktopSideColumn';
import {LexicalSenseMobileActions} from '../LexicalSense/LexicalSenseMobileActions';
import {LexicalSenseTopPostAboutSense} from '../LexicalSense/LexicalSenseTopPostAboutSense';

import {LexicalSenseLoader} from '../LexicalSense/LexicalSenseLoader';
import {LexicalSenseSources, LexicalSenseSourcesMobilePage} from '../LexicalSense/LexicalSenseSources';
import {LexicalSenseCountries, LexicalSenseCountriesMobilePage} from '../LexicalSense/LexicalSenseCountries';
import {Desktop, Mobile, useMediaQueries} from '../../layouts/MediaQueries';
import {FullScreenOverlay} from '../../layouts/mobile/FullScreenOverlay';
import {DesktopSubHeader} from '../../layouts/desktop/DesktopSubHeader';
import {ResponsiveGrid} from '../../layouts/responsive/ResponsiveGrid';

import {LexicalSenseMobileEthymology} from '../LexicalSense/LexicalSenseMobileEthymology';
import {LexicalSenseHistoryTable} from '../LexicalSense/LexicalSenseHistoryTable';
import {LexicalSenseDefinition} from '../LexicalSense/LexicalSenseDefinition';
import {IsReadOnlyWarning} from '../../hooks/useIsReadOnly';
import {UserContext} from '../../hooks/UserContext';
import {FloatingButton} from '../widgets/FloatingButton';
import {LexicalSenseCopyright} from '../widgets/LexicalSenseCopyright';
import Greeting from '../widgets/Greeting';
import {GoTo} from "../contribution/form/GoTo";
import Config from "../../Config";
import {gqlLexicalSenseCountriesFragment} from '../../utilities/helpers/countries';
import {decodeURIComponentIfNeeded} from "../../utilities/helpers/tools";
import {FormSeeAlso} from "../Form/FormSeeAlso";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 500,
    minHeight: 200,
  },
  fabPurple: {
    color: theme.palette.common.white,
    backgroundColor: Config.colors.purple,
    '&:hover': {
      backgroundColor: Config.colors.darkpurple,
    },
  },
  minWidth: {
    minWidth: "75vw"
  },
  section: {
    marginLeft: Config.spacings.medium,
    marginRight: Config.spacings.medium
  },
  desktopSubHeaderFormQuery: {
    color: Config.colors.black
  },

  editLink: {
    color: Config.colors.purple,
    display: "flex",
    padding: "5px",
    margin: "2px",
    boxSizing: "border-box",
    borderRadius: "50px",
    height: "35px",
    alignItems: "center",
    justifyContent: "center",
    border: `2px solid ${Config.colors.purple}`,
    backgroundColor: Config.colors.white
  },
  editLinkIcon: {
    width: `${Config.fontSizes.small * 1.5}px`,
    height: `${Config.fontSizes.small * 1.5}px`,
    marginLeft: Config.spacings.tiny,
    marginRight: "8px"
  },
  editLinkImg: {
    width: "100%",
    height: "100%"
  }

}));

export const LexicalSenseQuery = gql`
  query LexicalSense_Query($lexicalSenseId: ID!, $formQs: String) {
    lexicalSense(id: $lexicalSenseId) {
      id
      definition
      lexicographicResourceName
      validationRatingsCount
      canUpdate
      # needed to count them for meta informations
      lexicalEntryGrammaticalPropertyLabels
      ...LexicalSenseCountriesFragment
    }
    lexicalSensesCountForAccurateWrittenForm(formQs: $formQs)
  }
  ${gqlLexicalSenseCountriesFragment}
 
`;

export function LexicalSense(props = {}) {
  const classes = useStyles();
  let params = useParams();
  let history = useHistory();
  const gS = globalStyles();

  const {user} = useContext(UserContext);
  const {isDesktop} = useMediaQueries();

  // show or not the greeting message when user add new Sense
  const [showGreeting] = useState(history?.location?.state?.showGreeting);

  let formQuery = decodeURIComponentIfNeeded(params.formQuery);
  let lexicalSenseId = decodeURIComponentIfNeeded(params.lexicalSenseId);

  let {t} = useTranslation();
  let {loading, error, data} = useQuery(LexicalSenseQuery, {
    variables: {
      lexicalSenseId,
      formQs: formQuery,
    },
  });
  const showBackButton = !!(data?.lexicalSensesCountForAccurateWrittenForm && data?.lexicalSensesCountForAccurateWrittenForm > 1);
  let lexicalSense = data?.lexicalSense;


  let metaDefinition = formQuery;

  if (!loading) {
    if (lexicalSense?.lexicalEntryGrammaticalPropertyLabels?.length > 0) {
      metaDefinition += ` - ${lexicalSense?.lexicalEntryGrammaticalPropertyLabels.join(', ')}`
    }
    metaDefinition += ` - ${lexicalSense?.definition}`
  }

  return (
    <ShowWhereIAM path="components/routes/LexicalSense.js">
      <React.Fragment>
        <MetaSEO
          title={t('DOCUMENT_TITLES.LEXICAL_SENSE', {formQuery})}
          description={metaDefinition} /*        name={formQuery} addScript={true}*/
        />

        <ResponsiveGrid desktopFirstSection={renderDesktopTitle} desktopSideColumn={showGreeting ? false : renderDesktopSideColumn} fabButton={createFabButton()}>
          <If condition={loading && !lexicalSense}>
            <LexicalSenseLoader />
          </If>
          <If condition={!loading && !lexicalSense}>
            <div className={clsx(classes.definitionSection, "definitionSection")}>{t('LEXICAL_SENSE_DETAIL.NO_RESULTS')}</div>
          </If>

          <If condition={!loading && lexicalSense}>
            <Mobile>
              <Switch>
                <Route exact path={ROUTES.FORM_LEXICAL_SENSE} render={() => renderLexicalSenseDetails(props)} />
                <Route exact path={ROUTES.FORM_LEXICAL_SENSE_SOURCES} render={renderMobileSourcesOverlayPage} />
                <Route exact path={ROUTES.FORM_LEXICAL_SENSE_COUNTRIES} render={renderMobileCountriesOverlayPage} />
              </Switch>
            </Mobile>
            <Desktop>
              <Switch>
                <Route exact path={ROUTES.FORM_LEXICAL_SENSE} render={renderLexicalSenseDetails} />
                <Route exact path={ROUTES.FORM_LEXICAL_SENSE_SOURCES} render={renderDesktopSourcesPage} />
                <Route exact path={ROUTES.FORM_LEXICAL_SENSE_COUNTRIES} render={renderDesktopCountriesPage} />
              </Switch>
            </Desktop>
          </If>
          {showGreeting && <>
            <GoTo formQuery={formQuery} lexicalSenseId={lexicalSenseId} editMode={true} />
            <LexicalSenseCopyright addMarginTop={true} />
          </>}

        </ResponsiveGrid>
      </React.Fragment>
    </ShowWhereIAM >
  );

  // props?.notistackService is used only in tests
  function renderLexicalSenseDetails(props) {
    return (
      <ShowWhereIAM path="components/routes/LexicalSense.js renderLexicalSenseDetails">
        <>
          <Mobile>
            <LexicalSenseMobileEthymology formQuery={formQuery} />
          </Mobile>

          <LexicalSenseDefinition
            formQuery={formQuery}
            lexicalSenseId={lexicalSenseId}
          />

          <LexicalSenseDistinctSemanticRelationList
            lexicalSenseId={lexicalSenseId}
            formQuery={formQuery}
            theme={classes}
          />

          {!isDesktop &&
            <div className={classes.section} id="LexicalSenseSourceLexicalSenseMobileActions">
              <br /><br />
              <LexicalSenseSource
                formQuery={formQuery}
                source={lexicalSense?.lexicographicResourceName}
                id={lexicalSense.id}
              />
              <div className={gS.marginBottom} />
              <LexicalSenseMobileActions
                lexicalSenseId={lexicalSenseId}
                formQuery={formQuery}
                onRemoveSuccess={navigateBack}
                notistackService={props?.notistackService}
              />
            </div>
          }

          <LexicalSenseTopPostAboutSense
            lexicalSenseId={lexicalSenseId}
            formQuery={formQuery}
          />

        </>
      </ShowWhereIAM >
    );
  }

  function renderDesktopTitle() {
    let backLinkToSense = {
      backLinkTo: formatRoute(ROUTES.FORM_LEXICAL_SENSE, {
        lexicalSenseId,
        formQuery,
      }),
      backLinkText: t('LEXICAL_SENSE_DETAIL.BACK_TO_SENSE_LINK'),
    };

    let backLinkToForm = showBackButton
      ? {
        backLinkTo: formatRoute(ROUTES.FORM_SEARCH, {formQuery}),
        backLinkText: t('LEXICAL_SENSE_DETAIL.BACK_TO_FORM_LINK'),
      }
      : {};


    let primaryTitle = showGreeting ? <Greeting /> : (
      <span>{t('FORM.DESKTOP.RESEARCHED_FORM')}
        <span className={classes.desktopSubHeaderFormQuery}>{formQuery}</span>
      </span>
    );

    return (
      <ShowWhereIAM path="renderDesktopTitle ">
        <Switch>
          <Route
            path={ROUTES.FORM_LEXICAL_SENSE_SOURCES}
            render={() => (
              <DesktopSubHeader
                primaryTitle={primaryTitle}
                secondaryTitle={t('LEXICAL_SENSE_DETAIL.DESKTOP.SOURCES')}
                {...backLinkToSense}
              />
            )}
          />
          <Route
            path={ROUTES.FORM_LEXICAL_SENSE_COUNTRIES}
            render={() => (
              <DesktopSubHeader
                primaryTitle={primaryTitle}
                secondaryTitle={t('LEXICAL_SENSE_DETAIL.DESKTOP.GEOGRAPHICAL_DETAILS')}
                {...backLinkToSense}
              />
            )}
          />
          <Route
            path={ROUTES.FORM_LEXICAL_SENSE}
            render={() => (
              <DesktopSubHeader
                primaryTitle={primaryTitle}
                secondaryTitle={showGreeting ? formQuery : t('LEXICAL_SENSE_DETAIL.DESKTOP.DEFINITION')}
                {...backLinkToForm}
              />
            )}
          />
        </Switch>
      </ShowWhereIAM>
    );
  }

  function renderDesktopSideColumn() {

    if (loading || !lexicalSense) {
      return <div />;
    } else {
      const {canUpdate} = lexicalSense;
      const label = canUpdate ? t('LEXICAL_SENSE_DETAIL.EDIT_THIS_DEFINITION') : t('LEXICAL_SENSE_DETAIL.ENRICH_THIS_DEFINITION')

      return <LexicalSenseDesktopSideColumn lexicalSenseId={lexicalSenseId} formQuery={formQuery} onRemoveSuccess={navigateBack}
        editButton={
          <div>
            <Link role="link" aria-label={label} className={classes.editLink} to={createEditLink()}>
              <span className={classes.editLinkIcon}>
                <img alt="Editer" src={contributionsIcon} className={classes.editLinkImg} />
              </span>
              {label}
            </Link>
          </div>
        }
      />;
    }
  }


  function createEditLink() {
    return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT, {formQuery, lexicalSenseId}) + "?allsteps=true";
  }

  function createFabButton() {
    /* if (loading || !lexicalSense || !lexicalSense?.canUpdate) {
       return null;
     }
 
     if (lexicalSense?.canUpdate) {
       return <FloatingButton locked={false} to={createEditLink()} classname={classes.fabPurple} icon={EditIcon} />
     }*/
    if (loading || !lexicalSense) {
      return null;
    }

    return <FloatingButton locked={false} to={createEditLink()} classname={classes.fabPurple} icon={EditIcon} />

  }

  function renderMobileSourcesOverlayPage() {
    return (
      <ShowWhereIAM path="lexicalSense renderMobileSourcesOverlayPage">
        <FullScreenOverlay>
          <LexicalSenseSourcesMobilePage lexicalSenseId={lexicalSenseId} formQuery={formQuery} />
        </FullScreenOverlay>
      </ShowWhereIAM>
    );
  }

  function renderMobileCountriesOverlayPage() {
    let places = (lexicalSense?.places?.edges || []).map(({node}) => node);
    return (
      <ShowWhereIAM path="lexicalSense renderMobileCountriesOverlayPage">
        <FullScreenOverlay>
          <LexicalSenseCountriesMobilePage lexicalSenseId={lexicalSenseId} formQuery={formQuery} places={places} />
        </FullScreenOverlay>
      </ShowWhereIAM>
    );
  }

  function renderDesktopCountriesPage() {
    console.log(lexicalSense?.places)
    let places = (lexicalSense?.places?.edges || []).map(({node}) => node);
    return (
      <ShowWhereIAM path="lexicalSense renderDesktopCountriesPage">

        <LexicalSenseCountries lexicalSenseId={lexicalSenseId} formQuery={formQuery} places={places} />

      </ShowWhereIAM>
    );
  }

  function renderDesktopSourcesPage() {
    return (
      <ShowWhereIAM path="lexicalSense renderDesktopSourcesPage">
        <>

          <LexicalSenseSources lexicalSenseId={lexicalSenseId} formQuery={formQuery} />

          <If condition={user?.userAccount?.isAdmin || user?.userAccount?.isOperator}>
            <LexicalSenseHistoryTable lexicalSenseId={lexicalSenseId} />
          </If>
        </>
      </ShowWhereIAM>
    );
  }

  function navigateBack() {
    history.push(formatRoute(ROUTES.FORM_SEARCH, {formQuery}));
  }
}

