
import React from 'react';
import {useTranslation} from 'react-i18next';
import {useMutation} from "@apollo/client";
import {Formik} from 'formik';

import {FoldableContainer} from '../../../../widgets/FoldableContainer';
import {Button} from '../../../../widgets/forms/Button';
import {ButtonRow} from '../../../../widgets/forms/ButtonRow';
import FormikInput from '../../../../widgets/forms/formik/FormikInput';
import {Form} from '../../../../widgets/forms/formik/Form';
import {getUserAuthenticationService} from '../../../../../services/UserAuthenticationService';

import {notificationService as appNotificationService} from "../../../../../services/NotificationService";

import Config from '../../../../../Config';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  container: {
    fontSize: Config.fontSizes.xsmall,
    color: Config.colors.darkgray,
    "& p": {
      marginBottom: Config.spacings.small
    },
    "& label": {
      marginBottom: Config.spacings.tiny
    }
  }
}));




/***
 *
 * List of error messages :
 *
 * - Frontend defined :
 *   - REQUIRED
 *   - PASSWORDS_DO_NOT_MATCH
 * - Backend defined :
 *   - IS_REQUIRED
 *   - PASSWORD_TO_SHORT
 *   - PASSWORDS_DO_NOT_MATCH
 *   - WRONG_OLD_PASSWORD
 *
 */
export function EditPassword(props) {
  const classes = useStyles();
  const {t} = useTranslation();
  const notificationService = props.notificationService || appNotificationService;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {resetPassword} = userAuthenticationService.useResetPassword();

  return (
    <FoldableContainer title={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.TITLE')}>
      {({close}) => (
        <div className={classes.container}>
          <p>{t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.TOP_CAPTION')}</p>
          <Formik
            initialValues={userAuthenticationService.getResetPasswordFormInitialValues()}
            validationSchema={userAuthenticationService.getResetPasswordValidationSchema()}
            onSubmit={(values, formikOptions) => handleSubmit(values, formikOptions, close)}
            validateOnChange={false}
            validateOnBlur={false}>
            {({isSubmitting}) => (
              <Form noValidate aria-label="Formulaire pour modifier votre mot de passe">
                <label htmlFor="oldPassword">{t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.OLD_PASSWORD_CAPTION')}</label>
                <FormikInput type="password" name="oldPassword" id="oldPassword"
                  placeholder={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.OLD_PASSWORD_PLACEHOLDER')}
                  aria-label={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.OLD_PASSWORD_PLACEHOLDER')}
                  required={true}
                />

                <label htmlFor="newPassword">{t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.NEW_PASSWORD_CAPTION')}</label>
                <FormikInput type="password" name="newPassword" id="newPassword" placeholder={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.NEW_PASSWORD_PLACEHOLDER')}
                  aria-label={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.NEW_PASSWORD_PLACEHOLDER')}
                  required={true}
                />

                <label htmlFor="newPasswordConfirm">{t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.PASSWORD_CONFIRMATION_CAPTION')}</label>
                <FormikInput type="password" name="newPasswordConfirm" id="newPasswordConfirm" placeholder={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.PASSWORD_CONFIRMATION_PLACEHOLDER')}
                  aria-label={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.PASSWORD_CONFIRMATION_PLACEHOLDER')}
                  required={true}
                />

                <ButtonRow>
                  <Button type="button" secondary onClick={close}
                    aria-label={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.CANCEL')} >
                    {t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.CANCEL')}
                  </Button>
                  <Button type="submit" disabled={isSubmitting} loading={isSubmitting}
                    aria-label={t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.SUBMIT')} >
                    {t('MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.SUBMIT')}
                  </Button>
                </ButtonRow>
              </Form>
            )}
          </Formik>
        </div>
      )}
    </FoldableContainer>
  );


  async function handleSubmit(values, formikOptions, close) {
    let result = await resetPassword(values, formikOptions);

    if (result?.success) {
      await notificationService.success(t("MY_ACCOUNT.PROFILE.SETTINGS.PASSWORD_EDIT.SUCCESS"));
      close();
    }

    if (result?.globalErrorMessage) {
      notificationService.error(result.globalErrorMessage);
    }
  }
}
