import React, {useMemo} from 'react';
import {useTranslation} from 'react-i18next';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {useHistory} from 'react-router-dom';
import {ParseNewlines} from "../../../../utilities/ParseNewlines";
import {gql, useMutation, useQuery} from "@apollo/client";
import {Form} from '../../../widgets/forms/formik/Form';
import FormikInput from '../../../widgets/forms/formik/FormikInput';

import {Button} from '../../../widgets/forms/Button';
import {ButtonRow} from '../../../widgets/forms/ButtonRow';
import {FormBottom} from '../../../widgets/forms/FormBottom'; 
import formStyles from '../../../widgets/forms/FormStyles.js';
import {notificationService as appNotificationService} from '../../../../services/NotificationService';
import {GraphQLErrorHandler} from '../../../../services/GraphQLErrorHandler';
import {ROUTES} from '../../../../routes';
import {Autocomplete} from '../../../widgets/forms/formik/Autocomplete';
import {AutocompleteController} from '../../../widgets/forms/AutocompleteController';
import {removeDiacritics} from '../../../../utilities/removeDiacritics';

const validationSchema = Yup.object().shape({
  yearOfBirth: Yup.number()
    .min(1905)
    .max((new Date()).getFullYear())
});

const gqlProfileEditMeQuery = gql`
  query ProfileEdit_Me {
    me {
      id
      gender
      yearOfBirth
      linguisticProfile
      otherInformation
      otherSpokenLanguages
    }
  }
`;

const gqlProfileEditMeMutation = gql`
  mutation ProfileEdit(
    $personId: ID!, 
    $gender: String, 
    $yearOfBirth: Int, 
    $linguisticProfile: String, 
    $otherInformation: String, 
    $otherSpokenLanguages: String
  ){
    updatePerson(input: {
      objectId: $personId, 
      objectInput: {
        gender: $gender,
        yearOfBirth: $yearOfBirth,
        linguisticProfile: $linguisticProfile,
        otherInformation: $otherInformation,
        otherSpokenLanguages: $otherSpokenLanguages
      }
    }){
      updatedObject{
        id
        gender
        yearOfBirth
        linguisticProfile
        otherInformation
        otherSpokenLanguages
      }
    }
  }
`;

export class GenderAutocompleteController extends AutocompleteController {
  possibleValues = [{
    caption: 'Féminin',
    value: 'feminin',
  }, {
    caption: 'Masculin',
    value: 'masculin',
  }, {
    caption: 'Autre',
    value: 'other'
  }];

  onSuggestionsFetchRequested(suggestionInput) {
    let qs = removeDiacritics(suggestionInput.value).toLowerCase().trim();
    let suggestions = [...this.possibleValues];
    suggestions.push({
      caption: 'Ne souhaite pas répondre',
      noReply: true
    });
    this.updateSuggestions(suggestions);
  }

  getSuggestionValue(suggested) {
    if (suggested?.noReply) {
      return '';
    } else {
      return suggested?.caption;
    }
  }

  getFormikSuggestionValue(suggested) {
    if (suggested?.noReply) {
      return '';
    } else {
      return suggested?.caption;
    }
  }

  renderSuggestionText(suggested) {
    return suggested?.caption;
  }

  onSuggestionsClearRequested() {
    this.updateSuggestions([]);
  }

  shouldRenderSuggestions() {
    return true;
  }
}

export class LinguisticProfileAutocompleteController extends AutocompleteController {
  possibleValues = [
    'Français langue maternelle',
    'Français langue seconde ou étrangère',
    'Apprentissage adulte',
    'Apprentissage scolaire'
  ];

  onSuggestionsFetchRequested(suggestionInput) {
    let qs = removeDiacritics(suggestionInput.value).toLowerCase().trim();
    let suggestions = this.possibleValues.filter(value => removeDiacritics(value).toLowerCase().match(qs));
    this.updateSuggestions(suggestions);
  }

  getSuggestionValue(suggested) {
    return suggested;
  }

  getFormikSuggestionValue(suggested) {
    return suggested;
  }

  onSuggestionsClearRequested() {
    this.updateSuggestions([]);
  }

  shouldRenderSuggestions() {
    return true;
  }
}

export function ProfileEdit(props) {
  const formStyle = formStyles();
  const {t} = useTranslation();
  const history = useHistory();
  const notificationService = props.notificationService || appNotificationService;
  const [updatePersonMutation] = useMutation(gqlProfileEditMeMutation);
  const {data} = useQuery(gqlProfileEditMeQuery, {fetchPolicy: 'cache-and-network'});
  const genderAutocompleteController = useMemo(() => new GenderAutocompleteController());
  const linguisticProfileAutocompleteController = useMemo(() => new LinguisticProfileAutocompleteController());


  return (
    <React.Fragment>
      <p className={formStyle.info}><ParseNewlines text={t('MY_ACCOUNT.PROFILE.EDIT.YOU_CAN_EDIT_THESE_FIELDS')} /></p>
      <If condition={!!data}>
        <Formik
          initialValues={{
            gender: data.me.gender || '',
            yearOfBirth: data.me.yearOfBirth || '',
            linguisticProfile: data.me.linguisticProfile || '',
            otherInformation: data.me.otherInformation || '',
            otherSpokenLanguages: data.me.otherSpokenLanguages || ''
          }}
          initialStatus={{
            autocompleteInputValues: {
              gender: data.me.gender,
              linguisticProfile: data.me.linguisticProfile
            }
          }}
          validationSchema={validationSchema}
          validateOnChange={false}
          validateOnBlur={false}
          onSubmit={handleSubmit}>
          {({isSubmitting}) => {
            return (
              <Form aria-label="Modifier mon profil">
                <Autocomplete
                  name="gender"
                  placeholder={t('MY_ACCOUNT.PROFILE.EDIT.PLACEHOLDERS.GENDER')}
                  controller={genderAutocompleteController}
                  selectNullOnEmpty
                />
                <FormikInput
                  type="number"
                  name="yearOfBirth"
                  aria-label={t('MY_ACCOUNT.PROFILE.EDIT.PLACEHOLDERS.BIRTH_YEAR')}
                  placeholder={t('MY_ACCOUNT.PROFILE.EDIT.PLACEHOLDERS.BIRTH_YEAR')}
                />
                <Autocomplete
                  name="linguisticProfile"
                  placeholder={t('MY_ACCOUNT.PROFILE.EDIT.PLACEHOLDERS.LINGUISTIC_PROFILE')}
                  controller={linguisticProfileAutocompleteController}
                  selectNullOnEmpty
                />
                <FormikInput
                  type="text"
                  name="otherSpokenLanguages"
                  placeholder={t('MY_ACCOUNT.PROFILE.EDIT.PLACEHOLDERS.SPOKEN_LANGUAGES')}
                />
                <FormikInput
                  type="text"
                  name="otherInformation"
                  aria-label={t('MY_ACCOUNT.PROFILE.EDIT.PLACEHOLDERS.OTHER_INFORMATION')}
                  placeholder={t('MY_ACCOUNT.PROFILE.EDIT.PLACEHOLDERS.OTHER_INFORMATION')}
                />
                <FormBottom>
                  <ButtonRow>
                    <Button type="submit" aria-label={t('MY_ACCOUNT.PROFILE.EDIT.SUBMIT')} disabled={isSubmitting} loading={isSubmitting}>{t('MY_ACCOUNT.PROFILE.EDIT.SUBMIT')}</Button>
                  </ButtonRow>
                </FormBottom>
              </Form>
            )
          }}
        </Formik>
      </If>
    </React.Fragment>
  );

  async function handleSubmit(values, formikOptions) {
    try {
      await updatePersonMutation({
        variables: {
          ...values,
          personId: data?.me.id,
          yearOfBirth: values.yearOfBirth || null /* If the field is empty, yearOfBirth will be "", but we want an integer or null */
        }
      });
      history.push(ROUTES.MY_ACCOUNT_PROFILE);
    } catch (error) {
      let {globalErrorMessage} = GraphQLErrorHandler(error, {formikOptions, t});
      notificationService.error(globalErrorMessage);
    } finally {
      formikOptions.setSubmitting(false);
    }
  }
}
