import React from 'react';
import {render, cleanup} from '@testing-library/react';
// import { MockedProvider } from '@apollo/client/testing';
import {act} from 'react-dom/test-utils';
import waait from 'waait';

import {Contributions} from '../Contributions';
import {renderWithMocks} from '../../../../../../../jest/utilities/renderWithMocks';
import {NotistackServiceMock} from '../../../../../../../jest/utilities/servicesMocks/NotistackServiceMock';
import {gqlContributionsQuery} from '../../../../contribution/ContributionsList/ContributionsListTable';


afterEach(cleanup);


describe("MyAccount Contributions", () => {
  it("lists the user contributions", async () => {
    let gqlMocks = [{
      request: {
        query: gqlContributionsQuery,
        variables: {
          "first": 10,
          "after": null,
          "filterOnLoggedUser": true,
          "filterOnReviewed": null,
          "filterOnUserAccountId": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": null,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": false
            }
          ]
        }
      },
      result: {
        "data": {
          "contributedLexicalSensesCount": 880,
          "contributedLexicalSenses": {
            "edges": [
              {
                "node": {
                  "id": "lexical-sense/6p7hah8cblhimj",
                  "definition": "new 1",
                  "createdAt": "2020-01-27T16:13:49+01:00",
                  "createdBy": "Derek Admin",
                  "canonicalFormWrittenRep": "affairé",

                  "processed": false,
                  "reviewerGroupsCount": 1,
                  "validationRatingsCount": 1,
                  "suppressionRatingsCount": 1,
                  "reportingRatingsCount": 1,
                  "validationRating": {
                    "id": "validation-rating/u95j8glpius7gf",
                    "__typename": "ValidationRating"
                  },
                  "suppressionRating": {
                    "id": "suppression-rating/v33xfa8bhsxynh",
                    "__typename": "ValidationRating"
                  },
                  "reportingRating": {
                    "id": "reporting-rating/a44hxfa8bhxauie",
                    "__typename": "ReportingRating"
                  },
                  "__typename": "LexicalSense"
                },
                "__typename": "LexicalSenseEdge"
              },
              {
                "node": {
                  "id": "lexical-sense/loip527riw7a18",
                  "definition": "Test de definition",
                  "createdAt": "2020-01-15T15:44:49+01:00",
                  "createdBy": "Derek Admin",
                  "canonicalFormWrittenRep": "affairé",
                  "processed": false,
                  "reviewerGroupsCount": 0,
                  "validationRatingsCount": 0,
                  "suppressionRatingsCount": 0,
                  "reportingRatingsCount": 0,
                  "validationRating": null,
                  "suppressionRating": null,
                  "reportingRating": null,
                  "__typename": "LexicalSense"
                },
                "__typename": "LexicalSenseEdge"
              }
            ],
            "pageInfo": {
              "__typename": "PageInfo",
              "endCursor": "offset:2",
              "hasNextPage": false
            },
            "__typename": "LexicalSenseConnection"
          }
        }
      }
    },
    {
      request: {
        query: gqlContributionsQuery,
        variables: {
          "first": 10,
          "after": null,
          "filterOnLoggedUser": true,
          "filterOnReviewed": null,
          "filterOnUserAccountId": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": null,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ]
        }
      },
      result: {
        "data": {
          "contributedLexicalSensesCount": 880,
          "contributedLexicalSenses": {
            "edges": [
              {
                "node": {
                  "id": "lexical-sense/6p7hah8cblhimj",
                  "definition": "new 1",
                  "createdAt": "2020-01-27T16:13:49+01:00",
                  "createdBy": "Derek Admin",
                  "canonicalFormWrittenRep": "affairé",

                  "processed": false,
                  "reviewerGroupsCount": 1,
                  "validationRatingsCount": 1,
                  "suppressionRatingsCount": 1,
                  "reportingRatingsCount": 1,
                  "validationRating": {
                    "id": "validation-rating/u95j8glpius7gf",
                    "__typename": "ValidationRating"
                  },
                  "suppressionRating": {
                    "id": "suppression-rating/v33xfa8bhsxynh",
                    "__typename": "ValidationRating"
                  },
                  "reportingRating": {
                    "id": "reporting-rating/a44hxfa8bhxauie",
                    "__typename": "ReportingRating"
                  },
                  "__typename": "LexicalSense"
                },
                "__typename": "LexicalSenseEdge"
              },
              {
                "node": {
                  "id": "lexical-sense/loip527riw7a18",
                  "definition": "Test de definition",
                  "createdAt": "2020-01-15T15:44:49+01:00",
                  "createdBy": "Derek Admin",
                  "canonicalFormWrittenRep": "affairé",
                  "processed": false,
                  "reviewerGroupsCount": 0,
                  "validationRatingsCount": 0,
                  "suppressionRatingsCount": 0,
                  "reportingRatingsCount": 0,
                  "validationRating": null,
                  "suppressionRating": null,
                  "reportingRating": null,
                  "__typename": "LexicalSense"
                },
                "__typename": "LexicalSenseEdge"
              }
            ],
            "pageInfo": {
              "__typename": "PageInfo",
              "endCursor": "offset:2",
              "hasNextPage": false
            },
            "__typename": "LexicalSenseConnection"
          }
        }
      }
    },
    {
      request: {
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "after": null,
          "filterOnUserAccountId": null,
          "filterOnLoggedUser": true,
          "filterOnReviewed": null,
          "filterOnExistingSuppressionRating": null,
          "filterOnProcessed": null,
          "sortings": [
            {
              "sortBy": "createdAt",
              "isSortDescending": true
            }
          ]
        }
      },
      result: {
        "data": {
          "contributedLexicalSensesCount": 880,
          "contributedLexicalSenses": {
            "edges": [
              {
                "node": {
                  "id": "lexical-sense/6p7hah8cblhimj",
                  "definition": "new 1",
                  "createdAt": "2020-01-27T16:13:49+01:00",
                  "createdBy": "Derek Admin",
                  "canonicalFormWrittenRep": "affairé",
                  "processed": false,
                  "reviewerGroupsCount": 1,
                  "validationRatingsCount": 1,
                  "suppressionRatingsCount": 1,
                  "reportingRatingsCount": 1,
                  "validationRating": {
                    "id": "validation-rating/u95j8glpius7gf",
                    "__typename": "ValidationRating"
                  },
                  "suppressionRating": {
                    "id": "suppression-rating/v33xfa8bhsxynh",
                    "__typename": "ValidationRating"
                  },
                  "reportingRating": {
                    "id": "reporting-rating/a44hxfa8bhxauie",
                    "__typename": "ReportingRating"
                  },
                  "__typename": "LexicalSense"
                },
                "__typename": "LexicalSenseEdge"
              },
              {
                "node": {
                  "id": "lexical-sense/loip527riw7a18",
                  "definition": "Test de definition",
                  "createdAt": "2020-01-15T15:44:49+01:00",
                  "createdBy": "Derek Admin",
                  "canonicalFormWrittenRep": "affairé",
                  "processed": false,
                  "reviewerGroupsCount": 0,
                  "validationRatingsCount": 0,
                  "suppressionRatingsCount": 0,
                  "reportingRatingsCount": 0,
                  "validationRating": null,
                  "suppressionRating": null,
                  "reportingRating": null,
                  "__typename": "LexicalSense"
                },
                "__typename": "LexicalSenseEdge"
              }
            ],
            "pageInfo": {
              "__typename": "PageInfo",
              "endCursor": "offset:2",
              "hasNextPage": false
            },
            "__typename": "LexicalSenseConnection"
          }
        }
      }
    }];


    const {container} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <Contributions notistackService={NotistackServiceMock()} />,
    });

    await act(waait);
    await act(waait);

    expect(container).toMatchSnapshot();
  });
});
