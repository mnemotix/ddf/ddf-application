import React from 'react';
import {Switch, Route, useParams} from 'react-router-dom';
import {useTranslation} from 'react-i18next';
import {ROUTES} from '../../routes';
import {FormLayout} from '../Form/FormLayout';
import {FormLexicalSenseList} from '../Form/FormLexicalSenseList';
import {FormTopPostsDesktop} from '../Form/FormTopPostsDesktop';

import {ErrorBoundary} from '../general/ErrorBoundary';
import {LexicalSense} from './LexicalSense';
import {ResponsiveGrid} from '../../layouts/responsive/ResponsiveGrid';
import {DesktopSubHeader} from '../../layouts/desktop/DesktopSubHeader';
import {usePlaceContext} from '../../hooks/PlaceContext';

import {Mobile} from '../../layouts/MediaQueries';
import {LexicalSenseMobileEthymology} from '../LexicalSense/LexicalSenseMobileEthymology';
import {decodeURIComponentIfNeeded} from "../../utilities/helpers/tools";
import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";


const useStyles = makeStyles((theme) => ({
  desktopSideColumn: {
    marginTop: "45px"
  },
  desktopSubHeaderFormQuery: {
    color: Config.colors.black
  },
  bottomSpacing: {
    paddingBottom: Config.spacings.small,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      paddingBottom: Config.spacings.medium
    }
  }
}));

export function Form(props) {
  const {t} = useTranslation();
  const classes = useStyles();
  let params = useParams();
  let currentForm = decodeURIComponentIfNeeded(params.formQuery);

  const {currentPlace} = usePlaceContext();

  return renderContent();

  function renderContent() {
    return (
      <ErrorBoundary>
        <Switch>
          <Route path={ROUTES.FORM_LEXICAL_SENSE} render={() =>
            <FormLayout currentForm={currentForm} currentPlace={currentPlace} showBackIcon={true}>
              <LexicalSense />
            </FormLayout>
          } />
          <Route render={() =>
            <FormLayout currentForm={currentForm} currentPlace={currentPlace}>
              {renderFormSearch()}
            </FormLayout>
          } />
        </Switch>
      </ErrorBoundary>
    );
  }

  function renderFormSearch() {
    return (
      <ResponsiveGrid desktopSideColumn={renderDesktopSideColumn} desktopFirstSection={renderDesktopFirstSection}>
        <Mobile>
          <div className={classes.bottomSpacing}>
            <LexicalSenseMobileEthymology formQuery={currentForm} />
          </div>
        </Mobile>
        <FormLexicalSenseList formQuery={currentForm} currentPlace={currentPlace} />
      </ResponsiveGrid>
    );
  }

  function renderDesktopSideColumn() {
    return (
      <div className={classes.desktopSideColumn}>
        <FormTopPostsDesktop formQuery={currentForm} />
      </div>
    );
  }

  function renderDesktopFirstSection() {
    return (
      <DesktopSubHeader
        desktopHeaderPrimaryTitleColor={'purple'}
        primaryTitle={
          <>
            <span >{t('FORM.DESKTOP.RESEARCHED_FORM')}</span>
            <span className={classes.desktopSubHeaderFormQuery}>{currentForm}</span>
          </>
        }
      />
    );
  }


}
