import React from 'react';
import {useTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../../layouts/responsive/InformationPage';
import content from '../../../assets/markdown/politique_confidentialite.md';

export function PolitiqueConfidentialite() {
  const {t} = useTranslation();
  return <InformationPage
    metaTitle={t('POLITIQUE_CONFIDENTIALITE_PAGE_TITLE')}
    content={<ProcessedMarkdownWrapper content={content} />}
  />
}

