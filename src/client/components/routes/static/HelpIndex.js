import React from 'react';
import {Link} from 'react-router-dom';
import {ParseNewlines} from "../../../utilities/ParseNewlines";
import {useTranslation} from 'react-i18next';
import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import {MetaSEO} from "../../../components/widgets/MetaSEO";
import {ResponsiveMainLayout} from '../../../layouts/responsive/ResponsiveMainLayout';
import {HelpMobileHeader} from '../../../layouts/mobile/MobileMainLayout/HelpMobileHeader';
import {ROUTES} from '../../../routes';


import globalStyles from '../../../assets/stylesheets/globalStyles.js';
import Config from '../../../Config';

const buttonBorderWidth = '3px'


export const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    [theme.breakpoints.down(Config.mediaQueries.mobileMaxWidth)]: {
      flexDirection: 'column',
      height: `calc(100vh - ${Config.heights.mobileHeader + 2 * Config.spacings.small}px)`
    },
    [theme.breakpoints.up(Config.mediaQueries.mobileMaxWidth)]: {
      flexWrap: 'wrap',
      justifyContent: 'space-between'
    },
    '& a': {
      textDecoration: 'inherit'
    }
  },
  button: {
    border: `${buttonBorderWidth} solid white`,
    paddingLeft: Config.spacings.tiny,
    paddingRight: Config.spacings.tiny
  },
  textContainer: {
    height: '100%',
    marginTop: `-${buttonBorderWidth}`,
    marginBottom: `-${buttonBorderWidth}`,
    paddingTop: Config.spacings.tiny,
    paddingBottom: Config.spacings.tiny,
    paddingLeft: Config.spacings.small,
    paddingRight: Config.spacings.small,
    textAlign: 'center',
    fontWeight: Config.fontWeights.semibold,
    [theme.breakpoints.down(Config.mediaQueries.mobileMaxWidth)]: {
      fontSize: Config.fontSizes.small
    },
    [theme.breakpoints.up(Config.mediaQueries.mobileMaxWidth)]: {
      fontSize: Config.fontSizes.medium
    }
  },
  section: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: Config.colors.white,
    [theme.breakpoints.down(Config.mediaQueries.mobileMaxWidth)]: {
      flexGrow: 1
    },
    [theme.breakpoints.up(Config.mediaQueries.mobileMaxWidth)]: {
      width: `calc(50% - ${Config.spacings.medium / 2}px )`,
      marginBottom: Config.spacings.medium,
      paddingTop: Config.spacings.huge,
      paddingBottom: Config.spacings.huge
    }
  },
  bgGreen: {
    backgroundColor: Config.colors.green,
    "& div": {
      backgroundColor: Config.colors.green
    }
  },
  bgOrange: {
    backgroundColor: Config.colors.orange,
    "& div": {
      backgroundColor: Config.colors.orange
    }
  },
  bgYellow: {
    backgroundColor: Config.colors.yellow,
    "& div": {
      backgroundColor: Config.colors.yellow
    }
  },
  bgPink: {
    backgroundColor: Config.colors.pink,
    "& div": {
      backgroundColor: Config.colors.pink
    }
  }

}));


export function HelpIndex() {
  const classes = useStyles();
  const gS = globalStyles();
  const {t} = useTranslation();
  return (
    <React.Fragment>
      <MetaSEO title={t('DOCUMENT_TITLES.HELP')} />
      <ResponsiveMainLayout mobileHeader={renderHeader()} desktopUseDefaultGrid>
        <div className={classes.container}>
          {renderSection({text: t('HELP.SECTION_LINKS.SEARCH_AND_USER_ACCOUNT'), path: ROUTES.HELP_HOW_TO_SEARCH, color: 'bgGreen'})}
          {renderSection({text: t('HELP.SECTION_LINKS.UNDERSTAND_ARTICLE'), path: ROUTES.HELP_HOW_TO_READ, color: 'bgOrange'})}
          {renderSection({text: t('HELP.SECTION_LINKS.HOW_TO_CONTRIBUTE'), path: ROUTES.HELP_HOW_TO_CONTRIBUTE, color: 'bgYellow'})}
          {renderSection({text: t('HELP.SECTION_LINKS.OFFLINE'), path: ROUTES.HELP_DDF_IRL, color: 'bgPink'})}
        </div>
      </ResponsiveMainLayout>
    </React.Fragment>
  );

  function renderSection({text, path, color}) {
    return (
      <Link role="link" aria-label={"Page d'aide pour " + text} to={path}
        className={clsx(classes.section, gS.clickable, classes[color])}>
        <div className={classes.button}>
          <div className={classes.textContainer}>
            <ParseNewlines text={text} />
          </div>
        </div>
      </Link>
    );
  }

  function renderHeader() {
    return <HelpMobileHeader title={t('HELP.HEADER')} search={true} ddfLogo={true} />;
  }
}
