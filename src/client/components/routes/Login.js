/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import React from 'react';
import {Formik} from 'formik';
import {useTranslation, Trans} from 'react-i18next';
import {useHistory, Link} from 'react-router-dom';
import {makeStyles} from '@material-ui/core/styles';
import {ROUTES} from '../../routes';
import {SimpleModalPage} from '../../layouts/responsive/SimpleModalPage';
import FormikInput from '../widgets/forms/formik/FormikInput';
import {Form} from '../widgets/forms/formik/Form';
import {FormBottom} from '../widgets/forms/FormBottom';
import {MetaSEO} from "../widgets/MetaSEO";
import {BracketButton} from "../widgets/BracketButton";

import {notificationService as appNotificationService} from '../../services/NotificationService';
import {
  getUserAuthenticationService
} from '../../services/UserAuthenticationService';
import {useBrowsingHistoryHelperService} from '../../services/BrowsingHistoryHelperService';
 
import formStyles from '../widgets/forms/FormStyles.js';
import {useIsReadOnly} from "../../hooks/useIsReadOnly";
import globalStyles from "../../assets/stylesheets/globalStyles";

export function Login(props) {
  const gS = globalStyles();
  const formStyle = formStyles();
  const {t} = useTranslation();
  const notificationService = props.notificationService || appNotificationService;

  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {login} = userAuthenticationService.useLogin();

  const history = useHistory();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();
  const isReadOnly = useIsReadOnly();

  return (
    <React.Fragment>
      <MetaSEO title={t('DOCUMENT_TITLES.LOGIN')} />
      <SimpleModalPage
        title={t('LOGIN_PAGE.TITLE')}
        content={renderContent()}
        titleBgColor="pink"
      />
    </React.Fragment>
  );

  function renderContent() {
    return (
      <React.Fragment>
        <Formik
          initialValues={userAuthenticationService.getLoginFormInitialValues()}
          validationSchema={userAuthenticationService.getLoginValidationSchema()}
          validateOnChange={false}
          validateOnBlur={false}
          onSubmit={handleLogin}>
          {({isSubmitting}) => (
            <Form noValidate aria-label="Formulaire pour se connecter avec votre compte">
              <p className={formStyle.info}>{t('LOGIN_PAGE.IF_YOU_ALREADY_HAVE_AN_ACCOUNT')}</p>

              <FormikInput aria-required={true} type="email" name="email" autocomplete="email" placeholder={t('LOGIN_PAGE.EMAIL')} aria-label="Saisir l'email" />

              <FormikInput aria-required={true} type="password" name="password" autocomplete="current-password" placeholder={t('LOGIN_PAGE.PASSWORD')} aria-label="Saisir le mot de passe" />

              {!isReadOnly &&
                <p className={formStyle.info}>
                  <Trans i18nKey="LOGIN_PAGE.CREATE_ACCOUNT_INFOTEXT">You can <Link role="link" aria-label="Créer un compte" to={ROUTES.SUBSCRIBE}>Créer un compte</Link>.</Trans>
                </p>
              }
              <p className={formStyle.info}>
                <span>{t('LOGIN_PAGE.LOST_CREDENTIALS_INFOTEXT')}</span>
                <span> </span>
                <Link role="link" aria-label="Mot de passe oublié" to={ROUTES.PASSWORD_FORGOTTEN}>{t('LOGIN_PAGE.LOST_CREDENTIALS_LINK_TEXT')}</Link>
              </p>
              <div className={formStyle.info} />
              <FormBottom >
                <div className={gS.buttonRow}>
                  <BracketButton
                    greyBg={true}
                    aria-label="Soumettre le formulaire de connexion"
                    text={t('LOGIN_PAGE.SUBMIT')}
                    type="submit" disabled={isSubmitting} loading={isSubmitting}
                  />
                </div>
              </FormBottom>
            </Form>
          )}
        </Formik>
      </React.Fragment>
    );
  }

  async function handleLogin(values, formikOptions) {
    let res = await login(values, formikOptions);

    if (res.success) {
      let route = browsingHistoryHelperService.getAfterLoginLocation();
      history.push(route ? route.location : '/');
    }
    if (res.globalErrorMessage) {
      notificationService.error(res.globalErrorMessage);
    }
  }

}


