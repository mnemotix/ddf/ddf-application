Politique de confidentialité
============================

1- Informations relatives aux données personnelles collectées sur le site
--------------------------------------------------------------------------

### 1.1. Origine et finalité de la collecte des données personnelles

Le *Dictionnaire des francophones* met en œuvre plusieurs traitements de
données personnelles ayant pour finalité d’assurer :

-   la collecte de la géolocalisation de l’Utilisateur rend possible l’affichage de résultats de recherche personnalisés dans le
    *Dictionnaire des francophones*, en fonction de sa position
    géographique ; la géolocalisation est soit automatique, réalisée à
    partir des capteurs ou des caractéristiques de connexion à internet
    de l’Utilisateur, soit saisie manuellement par l’Utilisateur ; pour
    des raisons de confidentialité, la géolocalisation est imprécise, le
    maillage le plus fin retenu s’arrêtant au niveau de la ville ou
    commune, ou de l’arrondissement (uniquement pour les plus grandes
    villes) ; cette fonctionnalité ne permet en aucun cas la collecte
    d’adresses personnelles ;

-   la collecte d’une adresse électronique, d’un pseudonyme et d’un mot
    de passe est obligatoire pour rendre possible la contribution des
    utilisateurs au *Dictionnaire des francophones*, à travers les
    fonctionnalités permettant d’une part l’ajout d’une nouvelle entrée,
    et d’autre part celles permettant de compléter une entrée existante
    avec des informations complémentaires ;

-   la géolocalisation (optionnelle) de l’Utilisateur lors son
    inscription permet de classer et de présenter ses contributions aux
    autres Utilisateurs selon plusieurs zones géographiques non
    identifiantes telles que « Monde francophone », « Europe », ou « Afrique » ;
    cette fonctionnalité ne permet en aucun cas la collecte d’adresses
    personnelles ;

-   les informations complémentaires optionnelles éventuellement
    collectées sur la page « Mon profil », lorsque l’utilisateur a
    exprimé son consentement à ce que ces données puissent être
    utilisées à des fins scientifiques et/ou de recherche, à savoir :

    -   son genre (champ fermé),

    -   son année de naissance (champ fermé),

    -   son rapport avec la langue française (champ fermé),

    -   ses autres langues parlées (champ ouvert),

    -   son domaine d’expertise (champ ouvert),

    sont mises à disposition du conseil scientifique du *Dictionnaire des
    francophones* dans l’objectif de mener des études sociolinguistiques,
    analyses ou tout autre projet de recherche à vocation purement
    scientifique à partir des contributions des Utilisateurs ; nous invitons
    les Utilisateurs à ne pas utiliser les champs ouverts proposés sur la
    page « Mon profil » pour renseigner des données à caractère personnel
    autres que celles demandées ;

-   les adresses IP des Utilisateurs et leurs différentes connexions
    sur le *Dictionnaire des francophones* sont collectées pour
    permettre le suivi statistique de l’audience du *Dictionnaire des
    francophones* au moyen de l’outil *Matomo* (version à code ouvert
    disponible sur [*https://fr.matomo.org/*](https://fr.matomo.org/)), sous réserve de l’acceptation du bandeau apparaîssant à l’ouverture du site.

Les données recueillies sur le *Dictionnaire des francophones* ne
sauraient provenir que de l’enregistrement volontaire par les
Utilisateurs d’une adresse de courrier électronique et/ou de données
nominatives permettant de bénéficier de certaines fonctionnalités
additionnelles telles que la possibilité de contribuer et d’enrichir le
*Dictionnaire des francophones*, exception faite des adresses IP des
visiteurs, qui sont enregistrées automatiquement par les systèmes de
notre hébergeur Huma-Num et de l’outil d’analyse de trafic *Matomo*.

### 1.2. Destination des données personnelles collectées sur le site

Les données personnelles collectées lors de l’inscription sur le
*Dictionnaire des francophones* ne sont accessibles que par l’équipe du
*Dictionnaire des francophones* en charge de son animation et de son
exploitation, parmi lesquelles figurent des agents de l’Institut international pour la Francophonie, de son
prestataire Mnémotix, d’éventuels membres du conseil scientifique, d’agent du ministère de la
Culture à l’exception des éventuelles données personnelles qui seraient indiquées
par l’Utilisateur dans des champs des entrées du *Dictionnaire des
francophones* qui sont affichés au public, tels que les champs « Mot ou
expression » ou le champ « Définition ».

Les mots de passe des Utilisateurs sont chiffrés par nos systèmes, à
l’aide de l’algorithme PBKDF2 (algorithme décrit par le standard
Internet RFC 2898), lors
de l’inscription sur le *Dictionnaire des francophones* et ne sont
accessibles ni par l’Éditeur, ni par ses Partenaires, ni par aucune
autre personne ou tiers ; l’Hébergeur s’engage à garantir la stricte
confidentialité de ces données en mettant en œuvre tous les moyens
techniques nécessaires pour y parvenir.


Les données personnelles collectées sur le *Dictionnaire des
francophones* sont à usage purement interne, et ne font l’objet d’aucune
communication, cession ou divulgation à des tiers, à l’exception des
données collectées sur la page « Mon profil » pour lesquelles
l’utilisateur a donné son consentement pour une exploitation
scientifique, qui peuvent être transmises à des Partenaires
scientifiques de l’Université Jean Moulin Lyon 3 pour être exploitées à des fins
uniquement scientifiques (analyse des contributions, travaux de recherche linguistiques…). Il est précisé ici que :

- les Partenaires scientifiques devront au préalable avoir été
accrédités par le conseil scientifique du *Dictionnaire des
francophones* ;

- les Partenaires scientifiques dûment habilités par le conseil
scientifique ne pourront accéder qu’aux contributions publiques des
Usagers et aux informations qu’ils ont renseignées sur la page « Mon
profil » ; plus particulièrement, les données des comptes utilisateurs
associées à ces contributions (adresse électronique et pseudonyme) ne
seront pas divulguées, ce qui permet de garantir l’anonymat des données
ainsi transmises.

### 1.3. Responsable des traitements de données personnelles collectées sur le site

Le responsable de l’ensemble des traitements de données à caractère
personnel du *Dictionnaire des francophones* est le Président de l’Université Jean Moulin Lyon 3.

### 1.4. Conservation des données personnelles collectées sur le site

L’Université Jean Moulin Lyon 3 conserve les données personnelles permettant
la contribution pendant une période de trois (3) ans qui court à compter
de la dernière connexion réussie de l’Utilisateur avec ses identifiants
personnels, période à compter de laquelle les données relatives à son
profil seront automatiquement effacées.

L’Utilisateur peut par ailleurs à tout moment demander la suppression
complète de ses données en supprimant son compte personnel depuis la
page « modifier mes préférences » de son profil utilisateur
([*http://www.dictionnairedesfrancophones.org/my\_account/profile/settings*](http://www.dictionnairedesfrancophones.org/my_account/profile/settings))
sur le *Dictionnaire des francophones* ; ce faisant, l’intégralité de
ses données sera supprimée, à l’exception d’un identifiant unique
conservé pour des raisons logicielles et qui ne comporte aucune donnée à
caractère personnel, et de ses contributions au *Dictionnaire des
francophones*.

L’ensemble des données du *Dictionnaire des francophones* est hébergé
par la très grande infrastructure de recherche Huma-Num sur des serveurs
situés exclusivement en France, y compris les données d’analyse
d’audience réalisées au moyen de l’outil Matomo, installé sur la même
infrastructure.

L’Université Jean Moulin Lyon 3 garantit la confidentialité des informations
enregistrées sur le *Dictionnaire des francophones*. Les adresses
électroniques et les données nominatives des Utilisateurs inscrits n’y
apparaissent à aucun moment, sauf si l’utilisateur a souhaité signer sa
contribution directement dans l’un des champs publics du *Dictionnaire
des francophones*, ce qui est fortement déconseillé.

2- Politique de protection des données personnelles 
-----------------------------------------------------

L’adoption par l’Union européenne du règlement 2016/79 relatif à la
protection des personnes physiques à l’égard du traitement des données
personnelles et à la libre circulation de ces données (RGPD) fixe un
cadre pour la protection des données personnelles, en renforçant
celui issu de la loi n°78-17 relative à l’Informatique, aux fichiers et
aux libertés du 6 janvier 1978 modifiée.

En particulier, le texte renforce les droits pour les personnes
concernées par des traitements de données à caractère personnel : droits
à l’information, d’accès, d’obtenir copie des données et d’opposition,
dès lors qu’une personne a retiré son consentement au traitement de ces
données.

Le RGPD consacre également de nouveaux droits : droit à l’oubli
(effacement général des données collectées), droit à la portabilité des
données.

Enfin, le RGPD impose un principe de transparence, en obligeant tout
responsable de traitement à fournir une information concise, claire,
compréhensible et aisément accessible sur les principales
caractéristiques du traitement des données.

3- Exercer vos droits
----------------------

Pour toute information ou tout exercice de vos droits (droit d’accès, de
rectification ou d’effacement, droit à la limitation du traitement,
droit à la portabilité des données et droit d’opposition) sur les
traitements de données personnelles opérés par le *Dictionnaire des
francophones* et ses différents services, vous pouvez contacter notre
délégué à la protection des données (DPD) à
l’adresse suivante :
[*dpd@univ-lyon3.fr*](mailto:dpd@univ-lyon3.fr).

Vous pouvez également contacter le délégué à la protection des données
(DPD) par courrier en écrivant à l’adresse suivante :

Université Jean Moulin Lyon 3\
À l’attention du délégué à la protection des données (DPD)\
1C, avenue des Frères Lumière\
CS 78242\
69372 Lyon Cedex 08

Vous disposez également du droit d’introduire une réclamation auprès de
l’autorité de contrôle compétente ([*CNIL*](https://www.cnil.fr/)) si
vous considérez que le *Dictionnaire des francophones* n’a pas respecté
vos droits.

En savoir plus sur vos droits : [*CNIL - Droits de la
personne*](https://www.cnil.fr/fr/reglement-europeen-protection-donnees/chapitre3)

4- Témoins de connexion (COOKIES)
----------------------------------

### 4.1. Utilisation des témoins de connexion (cookies) sur le Dictionnaire des francophones

Les options d’enregistrement de vos identifiants, mots de passe et de la
géolocalisation de l’appareil sur le site internet du *Dictionnaire des
francophones* peuvent occasionner l’emploi d’un témoin de connexion ou
cookie persistant (stocké sur le disque dur de votre ordinateur).

La génération de ces témoins de connexion n’est en aucun cas
systématique ; ces fichiers ont pour seule fonction de simplifier votre
accès en supprimant la phase de saisie de votre identifiant et de votre
mot de passe. Nous vous recommandons de ne pas avoir recours à ces
témoins de connexion si vous accédez à notre site internet depuis un
poste en accès public.

Ces témoins de connexion ne sont en aucune façon conçus à des fins de
collecte de données personnelles.

### 4.2. Suppression des témoins de connexion présents sur votre ordinateur

L’utilisateur pourra désactiver ces témoins de connexion par
l’intermédiaire des paramètres figurant au sein de son logiciel de
navigation et éventuellement les refuser de la manière décrite à
l’adresse suivante :
[*https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser*](https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser).

Attention : cette manipulation pourra entraîner la suppression de tous
les témoins de connexion utilisés par le navigateur, y compris ceux
employés par d’autres sites internet, ce qui peut conduire à la perte de
certaines informations ou certains réglages.
