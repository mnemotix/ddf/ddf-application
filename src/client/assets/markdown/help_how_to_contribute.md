La contribution est l’un des aspects les plus importants du *Dictionnaire des francophones* !

Au delà des informations déjà consultables, ce sont les utilisateurs et utilisatrices qui vont faire la richesse du Dictionnaire des francophones. Vous pouvez contribuer de bien des façons différentes, c’est facile et nous vous y aiderons !

# Pourquoi contribuer ? 

Vos connaissances sur la langue sont précieuses, car chaque groupe de francophones a des usages spécifiques et que la langue évolue en permanence. En tant que francophone, vous avez des connaissances sur votre propre langue, et vous êtes invités à les partager avec la large communauté francophone. Les compétences à la rédaction et à la documentation des usages du français sont celles des linguistes et des lexicographes, mais elles peuvent s’acquérir par la pratique, par l’exemple des définitions déjà rédigées et grâce aux accompagnements pédagogiques fournis au sein de l’interface du *Dictionnaire des francophones*. Les contributions volontaires sont relues, affinées et enrichies par le comité de relecture composé d’experts et d’expertes du monde entier.

# Ajouter un mot nouveau, une nouvelle expression

Si le mot que vous recherchez n’existe pas dans le Dictionnaire des francophones, vous pouvez le rajouter. Il vous suffit de cliquer sur *Enrichir le DDF* pour accéder à un formulaire de contribution.

Vous pourrez alors mettre la catégorie grammaticale, le lieu d’usage, la définition et un exemple d’usage. Dans une version à venir bientôt, vous pourrez en plus partager d’autres informations, telles que les marques d’usage, les relations sémantiques et la prononciation. 

# Ajouter des informations dans une entrée existante

Il vous suffit de cliquer sur *Modifier la définition* depuis la fiche d’une définition !

Vous pouvez rajouter un exemple d’usage, mais aussi bien d’autres informations comme le lieu d’usage, des marques d’usage, des relations sémantiques ou inclure un mot dans un lexique.

Les exemples sont des phrases qui présentent un usage du mot et facilitent sa compréhension, des conseils de rédactions sont proposés plus bas sur cette page. Les autres informations sont présentées dans les pages d’explication sur la lecture du site. Le formulaire de contribution propose des voies de navigation revenant vers la définition modifiée et permettant d’explorer les autres voies, pour ajouter d’autres informations pour supprimer ses ajouts précédents. Seuls les opérateurs et opératrices peuvent supprimer les ajouts faits par d’autres personnes.

# Valider le contenu du dictionnaire par les votes et signaler un abus

Les contributions ont un indicateur de validation, disponible pour toute personne inscrite. L’évaluation par le plus grand nombre est un aspect très important, c’est une contribution directe à la qualité du DDF !

Trois types de votes sont proposés :

- *[/] Je valide* confirme la fiabilité de la contribution et la fait remonter dans la liste des résultats de recherche. Par ce vote, vous signalez qu’un sens, une indication géographique ou une marque est conforme à ce que vous savez de l’usage d’un mot particulier. 
- *[!] Je signale* attire l’attention sur un problème (contenu inapproprié, abusif, à contrôler). Ce vote signale aux opérateurs et opératrices que l’information devrait être relu.
- *[-] À supprimer* initie le processus d’élimination de la contribution. Cette action est réservée aux opérateurs et opératrices.   

<br>

# Comment rédiger une définition

De manière générale, une définition se présente comme une périphrase du mot défini, qui présente les éléments constituant le sens de ce mot. Le but d’une définition est donc de faire comprendre le concept auquel le mot défini renvoie (le mot « éléphant » renvoie au concept d’éléphant). Pour se faire, il y a des règles à respecter  :

## Une définition ne décrit qu’un seul concept
C’est-à-dire qu’on ne définit pas un autre mot au sein de la définition du mot en entrée. Par exemple, la définition de corbeau « grand oiseau à plumage noir ou gris » ne décrit que « corbeau ». Si « plumage » était défini, la définition deviendrait incorrecte.

## La définition d’un concept ne devrait contenir que des éléments essentiels à sa compréhension
Donc idéalement, une définition ne contient que les caractères distinctifs de l’objet, c’est-à-dire des caractères qui permettent de comprendre ce qu’est l’objet, mais également de distinguer le concept des autres membres de sa catégorie. Par exemple, la définition de « requin » doit permettre de le différencier des autres poissons.

## Le concept n’est décrit qu’une seule fois
C’est-à-dire qu’on ne reformule pas une définition en son sein. Par exemple, une définition du type « chlore : corps simple, ou atome… » serait jugée incorrecte puisqu’on reformule le concept. 

## La définition doit être affirmative
C’est-à-dire qu’on dit ce qu’est le concept plutôt que ce qu’il n’est pas, on n’utilise donc pas de négation. On utilise la négation que si le mot défini a une valeur négative comme « invisible » ou « apolitique ». 

## Si possible, la définition doit tenir en une seule phrase
Cette phrase doit être la plus concise possible, commencer par une majuscule et se terminer par un point. La présence d’un verbe n’est pas obligatoire. Pour autant, elle doit être complète et présenter tous les caractères essentiels à la compréhension du mot. Les définitions en un ou deux mots sont donc à proscrire.

## La définition doit être neutre
On ne donne pas son avis dans une définition. On ne dira donc pas que telle viande est la meilleure ou que telle odeur est agréable s’il n’y a pas lieu de le dire (on dira que parfum est « agréable » uniquement parce que la définition l’exige).

## La définition en elle-même ne comporte pas d’exemples
Les exemples sont renvoyés dans une autre partie juste après la définition. Il est possible d’en insérer dans la définition uniquement s’ils sont indispensables à la compréhension.

## Éviter la circularité des définitions dans le Dictionnaire des francophones
Deux définitions ne doivent pas se renvoyer l’une à l’autre sous peine de ne pas pouvoir obtenir les informations recherchées. Par exemple, « mammifère » ne pourrait pas être défini par « objet d’étude de la mammalogie » si « mammalogie » est définie par « étude des mammifères ».

## Le premier mot d’une définition a une nature bien définie
Par exemple, une définition de verbe commencera presque toujours par un verbe, celle d’un nom par un nom. Plus précisément, le nom doit être un hyperonyme du nom défini, c’est-à-dire la catégorie à laquelle appartient ce mot (« félin » est l’hyperonyme de « chat »). 

Cette règle ne s’applique pas aux autres parties du discours. Par exemple, la définition d’un adjectif commence presque toujours par un pronom relatif (que, qui) ou par une formule du type « se dit de » ou « relatif à ». La définition d’un adverbe commence la plupart du temps par « de manière à/d’une manière ». 

Notons également que ce premier mot ne peut pas être précédé d’un article, d’un adjectif démonstratif ou d’un pronom démonstratif. Il ne devrait pas non plus être précédé d’un adjectif indéfini (on ne dira pas « Toute maladie », mais simplement « maladie » pour parler de maladie vénérienne) ou d’un pronom indéfini (on ne dirait pas « Chaque maladie », mais simplement « maladie »).

## Le terme défini ne doit pas apparaître dans sa définition
Par exemple, « pêche » ne doit pas apparaître dans la définition de « pêche ». De même, ses dérivés ne devraient pas non plus y figurer. Définir « pêche » par « action effectuée par le pêcheur » n’aurait pas vraiment d’intérêt. Par contre, si le terme présente des homonymes (mots qui n’ont pas le même sens, mais qui ont la même forme graphique), ils pourraient y figurer.

## Puisqu’une définition doit être complète, elle ne doit pas se terminer par « etc. » ou « … »
Une définition doit être close et ne pas donner l’impression qu’il manque des éléments importants.

## Les parenthèses doivent être utilisées avec modération
En général, on préfère même éviter d’en mettre. S’il y en a dans la définition, elles ne peuvent pas comprendre d’éléments indispensables à la compréhension.

## La description d’un objet concret doit faire figurer ses caractéristiques physiques
Dans l’idéal, on voudrait pouvoir se faire une image mentale de l’objet en lisant sa définition. Donc si l’on définit un objet visible et qui présente des caractéristiques physiques évidentes et communes à tous les membres de cette classe d’objets, on les présente.
Par exemple, on ne dirait pas juste qu’un vélo roule, mais également qu’il comporte un guidon et des roues.
À noter que ces caractéristiques physiques sont préférablement présentées avant les caractéristiques plus abstraites comme le but de l’objet, les gens qui peuvent l’utiliser…

Pour aller plus loin  : Vézina, Robert. La rédaction de définitions terminologiques. 2009. Office québécois de la langue française [En ligne](https://www.oqlf.gouv.qc.ca/ressources/bibliotheque/terminologie/redaction_def_terminologiques_2009.pdf)

<br>

# Comment rédiger un exemple

L’exemple est une présentation des emplois réels du mot dans un discours. Il pourrait donc figurer dans une conversation ou dans un texte et doit donc être représentatif de la langue. Dans un dictionnaire, cet exemple sert à illustrer la définition qui le précède et contient le mot défini afin de le montrer en contexte. Il peut être un extrait de texte ou de discours enregistré si la source est sérieuse ou du moins avérée correcte. 

Lorsque c’est possible, la source est indiquée à la suite de l’exemple afin de pouvoir le retrouver dans un contexte plus large qui peut aider à mieux le comprendre. Dans l’ordre, l’indication de source comprend le nom de l’auteur ou autrice, le nom de l’ouvrage/du discours dont elle est tirée, l’édition, la date de publication et le numéro de page. 

L’exemple peut aussi être inventé de toute pièce, mais il doit toujours illustrer la définition et faire figurer le mot défini. Prenons le nom « cuisse », on peut le définir par « partie de la jambe comprise entre la hanche et le genou ». L’exemple doit donc forcément présenter le mot « cuisse ». Un bon exemple serait quelque chose comme « Antoine mange une cuisse de poulet. ».

Idéalement, l’exemple doit être une seule phrase complète, simple et pas trop longue pour ne pas noyer le mot défini dans trop d’informations. Si l’exemple est une citation, il peut être pris au milieu d’une longue phrase, dans ce cas il n’est pas nécessaire de présenter la phrase complète, et un fragment suffit. Une unique définition pourra présenter un bon nombre d’exemples à condition qu’ils soient tous utiles à la compréhension.

Enfin, il est important de préciser qu’un exemple ne doit pas être le reflet d’une opinion quelconque. S’il est construit, il doit l’être de manière neutre. S’il est cité, il doit non seulement ne pas présenter d’opinion en son sein, mais il ne doit pas non plus être tiré d’un texte controversé, trop connoté ou trop politiquement marqué. En effet, l’exemple ne doit pas offenser le lectorat ni véhiculer d’éléments néfastes ou dégradants.
