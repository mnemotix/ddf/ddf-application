Mentions légales - Dictionnaire des francophones 
================================================
1- Éditeur
-----------

Le site internet
[*www.dictionnairedesfrancophones.org*](http://www.dictionnairedesfrancophones.org)
ainsi que les applications mobiles « *Dictionnaire des francophones* »
sont édités par l’Université Jean Moulin Lyon 3, 1C avenue des Frères Lumière, CS 78242, 69372 Lyon CEDEX 08 (France).

2- Directeur de la publication
------------------------------

Le directeur de la publication est M. Éric Carpano, président de l’Université Jean Moulin Lyon 3.

Les contenus proposés sur le *Dictionnaire des francophones* proviennent
des contributions des usagers et de différentes sources externes,
sélectionnées par le conseil scientifique du *Dictionnaire des
francophones*. Nous vous invitons à consulter la page [*Présentation du
projet*](http://www.dictionnairedesfrancophones.org/presentation) pour
davantage d’informations à ce sujet.

3- Hébergement
--------------

Le site internet
[*www.dictionnairedesfrancophones.org*](http://www.dictionnairedesfrancophones.org)
est hébergé par la très grande infrastructure de recherche
[*Huma-Num*](https://www.huma-num.fr/) - UMS 3598 CNRS, 54 boulevard
Raspail, 75006 Paris.

4- Réalisation du *Dictionnaire des francophones*
------------------------------------------------

La plate-forme technique du *Dictionnaire des francophones* est
développée par la société [*Mnémotix*](https://www.mnemotix.com/), sous
le pilotage de l’Institut international pour la Francophonie (Université
Jean Moulin Lyon 3).

Pour plus d’informations sur les partenaires et prestataires impliqués
dans la conception et la réalisation du *Dictionnaire des francophones*,
nous vous invitons à consulter la page
[*Crédits*](http://www.dictionnairedesfrancophones.org/credits).
