Nous suivre
======================

Des contenus liés au *Dictionnaire des francophones* sont diffusés sur les réseau numériques, afin d’informer sur l’existence du site internet et des applications mobiles, de mettre en avant son contenu, ses fonctionnalités ou les personnes qui y participent.

* [Blogue du Dictionnaire des francophones](https://blogue.dictionnairedesfrancophones.org/) : articles sur les mises à jour, le travail de l’ombre pour la fabrication de l’outil, la présentation des ressources qu’il contient
* [Instagram](https://www.instagram.com/dfrancophones/) : mots du jour, discussions autour des usages du français et des contributions au DDF
* [Twitter](https://twitter.com/DFrancophones) : mots du jour, discussions autour des usages du français et des contributions au DDF
* [Facebook](https://www.facebook.com/dfrancophones) : mots du jour, discussions autour des usages du français et des contributions au DDF
* [Mastodon](https://mastodon.social/@Dfrancophones) : mots du jour, discussions autour des usages
* [LinkedIn](https://www.linkedin.com/showcase/dfrancophones/) : circulation de ressources pour l’usage et le réusage du DDF
* [Youtube](https://www.youtube.com/channel/UCxUp8n1jLiFOu5V00XZdodQ) : vidéos de présentation du DDF et capsules vidéos variées autour du DDF
