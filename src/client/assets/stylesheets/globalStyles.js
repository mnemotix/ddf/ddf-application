import Config from '../../Config';
import {makeStyles} from '@material-ui/core/styles';

const globalStyles = makeStyles((theme) => ({

  '@keyframes fadingMostlyVisible': {
    "0%": {"opacity": 0.3},
    "20%": {"opacity": 0.3},
    "50%": {"opacity": 0.8},
    "80%": {"opacity": 0.3},
    "100%": {"opacity": 0.3}
  },
  loadingOverlay: {
    position: 'relative',
    overflow: 'hidden',
    '&::before': {
      content: "' '",
      position: "absolute",
      top: "0",
      right: "0",
      bottom: "0",
      left: "0",
      zIndex: "1",
      background: Config.colors.white,
      // background: "white",
      animationName: `$fadingMostlyVisible`,
      animationDuration: "1.5s",
      animationTimingFunction: "linear",
      animationIterationCount: "infinite",
    },
    "&:active": {
      filter: "initial"
    }
  },
  whiteContainer: {
    margin: `${Config.spacings.small}px 0px`,
    padding: `${Config.spacings.small}px`,
    backgroundColor: Config.colors.white,
    fontSize: Config.fontSizes.medium
  },
  outlineFocus: {
    "& *:focus:not(.focus-visible)": {
      outline: "none"
    },
    "&:focus-visible": {
      //outline: `${Config.colors.yellow} solid 4px`,
      outlineOffset: "0.3rem",
      borderRadius: "0.3rem"
    }
  },
  clickable: {
    userSelect: "none",
    cursor: "pointer",
    "&:active": {
      filter: "brightness(80%)"
    }
  },
  selectedDiv: {
    border: `1px solid ${Config.colors.purple} !important`,
    color: `${Config.colors.purple} !important`,
  },
  // single modal page content
  SMPcontent: {
    paddingTop: Config.spacings.large,
    paddingBottom: Config.spacings.large,
    paddingRight: Config.spacings.medium,
    paddingLeft: Config.spacings.medium,
    [theme.breakpoints.down(Config.mediaQueries.mobileMaxWidth)]: {
      paddingTop: Config.spacings.medium,
      paddingBottom: Config.spacings.medium,
      paddingRight: Config.spacings.small,
      paddingLeft: Config.spacings.small,
    }
  },
  // from DesktopSubHeader but used in many places
  secondaryTitle: {
    fontSize: Config.fontSizes.large,
    marginBottom: Config.spacings.medium,
    [theme.breakpoints.down(Config.mediaQueries.mobileMaxWidth)]: {
      display: 'none'
    }
  },

  stepTemplateSpacer: {
    marginBottom: Config.spacings.large,
  },
  stepTemplateTitle: {
    fontSize: Config.fontSizes.large,
    color: Config.colors.verydarkgray
  },


  buttonRow: {
    display: "flex",
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  cancelButton: {
    fontSize: Config.fontSizes.large,
    color: Config.colors.verydarkgray
  },

  formWrittenRep: {
    display: "flex",
    justifyContent: "space-between",
    fontSize: Config.fontSizes.large,
    fontWeight: Config.fontWeights.bold,
    marginLeft: Config.spacings.tiny,
    marginBottom: Config.spacings.small
  },

  marginBottomSmall: {
    marginBottom: Config.spacings.small
  },

  topPostAboutSense: {
    backgroundColor: Config.colors.lightgray,
    color: Config.colors.darkgray,
    fontSize: Config.fontSizes.small,
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small
  },
  topPostTitle: {
    fontSize: Config.fontSizes.small,
    fontWeight: Config.fontWeights.bold,
    color: Config.colors.orange,
    marginBottom: Config.spacings.tiny
  },

  exploreMinHeight: {
    minHeight: 85,
    [theme.breakpoints.down("sm")]: {
      minHeight: Config.spacings.large,
    }
  },

  flexCenter: {display: 'flex', alignItems: 'center', justifyContent: 'center'},
  flexStart: {display: 'flex', alignItems: 'center', justifyContent: 'flex-start'},
  green: {color: `${Config.colors.green} !important `},
  orange: {color: `${Config.colors.orange} !important `},
  yellow: {color: `${Config.colors.yellow} !important `},
  pink: {color: `${Config.colors.pink} !important `},
  black: {color: `${Config.colors.black} !important `},
  purple: {color: `${Config.colors.purple} !important `},
  gray: {color: `${Config.colors.darkgray} !important `},
  white: {color: `${Config.colors.white} !important `}

}));



export default globalStyles;