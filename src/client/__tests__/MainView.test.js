import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import {render} from '@testing-library/react';
import {act} from 'react-dom/test-utils';
import waait from 'waait';
import {MainView} from '../MainView';

let envDataMock = {environment: {__typename: 'Environment', READ_ONLY_MODE_ENABLED: '0', UNDER_MAINTENANCE: '0'}};
let userMock = {
  isLogged: true,
  id: 123,
  avatar: null,
  nickName: 'John Doe',
  userAccount: {
    username: 'johndoe@ddf.fr',
    isAdmin: false,
    isOperator: false,
    isContributor: true,
  },
};

jest.mock('../components/contribution/EditLexicalSense', () => ({
  EditLexicalSense: () => <div>EditLexicalSense component rendered</div>,
}));
jest.mock('../components/routes/Form', () => ({Form: () => <div>Form component rendered</div>}));

describe('routing', () => {
  describe('interaction between FORM_LEXICAL_SENSE_EDIT and FORM_SEARCH', () => {
    test('route FORM_LEXICAL_SENSE_EDIT takes precedence over FORM_SEARCH', async () => {
      let locationPath = '/form/pomme/sense/lexicalSense123/edit';

      const {findByText} = render(
        <MemoryRouter initialEntries={[locationPath]}>
          <MainView user={userMock} envData={envDataMock} />
        </MemoryRouter>
      );

      await act(waait);
      let component = await findByText('EditLexicalSense component rendered');
      expect(component).toBeDefined();

      expect(1).toEqual(1);
    });

    test('route FORM_SEARCH displays correctly', async () => {
      let locationPath = '/form/pomme/sense/lexicalSense123';
      const {findByText} = render(
        <MemoryRouter initialEntries={[locationPath]}>
          <MainView user={userMock} envData={envDataMock} />
        </MemoryRouter>
      );

      await act(waait);
      let component = await findByText('Form component rendered');
      expect(component).toBeDefined();
    });
  });
});
