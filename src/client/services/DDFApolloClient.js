import {ApolloClient, ApolloLink, HttpLink, InMemoryCache, concat} from '@apollo/client';
import {onError} from '@apollo/client/link/error';
import possibleTypes from '../gql/possibleTypes.json';

export const inMemoryCache = new InMemoryCache({
  possibleTypes
});

// create the client and pass the middleware et cache config
let DDFApolloClient;


export function getDDFApolloClient() {
  if (!DDFApolloClient) {
    const httpLink = new HttpLink({uri: '/graphql', headers: {"Lang": "fr"}});
    // middleware for gql error
    const errorLink = onError(({graphQLErrors, networkError}) => {
      if (graphQLErrors) {
        graphQLErrors.map(({message, extraInfos, stack, path}) => {
          const ignore = ['NOT AUTHENTICATED', 'USER_MUST_BE_AUTHENTICATED', 'INVALID_CREDENTIALS'];
          if (ignore.includes(message.toUpperCase())) {
            // what to do if user not authenticated?
            // route to login is already done
          } else {
            if (typeof extraInfos === 'object' && extraInfos !== null) {
              extraInfos = JSON.stringify(extraInfos);
            }

            console.error(
              `[GraphQL error]: Message: ${message}, extraInfos: ${extraInfos}, 
            stack:${JSON.stringify(stack, null, 2)}, Path: ${path}`
            );
          }
        });
      }

      if (networkError && process.env.NODE_ENV !== "test") {
        console.log(`[Network error]: ${networkError}`);
      }
    });

    DDFApolloClient = new ApolloClient({
      link: concat(errorLink,httpLink),
      cache: inMemoryCache,
      defaultOptions: {
        watchQuery: {
          query: 'cache-and-network',
        },
      },
    })
  }

  if (process.env.NODE_ENV === 'test') {
    let miniStackTrace = new Error().stack
      .split('\n')
      .splice(1, 5)
      .join('\n');
    console.warn(
      "You are using the default apollo client DDFApolloClient in a test environment. This probably won't work " +
      "because the default apollo client hasn't access to the graphQL mocks (as provided by apollo MockedProvider). You should either mock " +
      'the service that use the apollo client in your test suite, or inject this service with a mocked apollo client such as the one ' +
      'defined at /jest/utilities/MockedApolloClient.js\n\n' +
      miniStackTrace
    );
  }

  return DDFApolloClient;
}

/**
 * @param {string} uri
 * @param {function} fetch
 * @param {object} headers
 * @return {*}
 */
export function getApolloClientForSSR({uri, fetch, headers}) {
  if (!DDFApolloClient) {
    DDFApolloClient = new ApolloClient({
      ssrMode: true,
      link: new HttpLink({
        uri,
        fetch,
        credentials: 'same-origin',
        headers
      }),
      cache: inMemoryCache
    });
  }

  return DDFApolloClient;
}



