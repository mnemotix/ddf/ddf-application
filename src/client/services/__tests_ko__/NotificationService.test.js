import {notificationService} from '../NotificationService';


test("default yes/no option text for the application instance", async () => {
  let assertionPromise = new Promise((resolve) => {
    notificationService.messages$.subscribe((messageObject) => {
      expect(messageObject).toMatchObject({
        message: 'my message',
        type: 'error',
        yesText: 'Oui',
        noText: 'Non'
      });
      resolve();
    });
  });
  notificationService.alert('my message', {type: 'error', confirm: true});
  await assertionPromise;
});
