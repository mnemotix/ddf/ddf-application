import {MenuService} from '../MenuService';
import {take} from 'rxjs/operators';

let menuService;

beforeEach(() => {
  menuService = new MenuService();
});

describe("displayStatus", () => {
  it("emits 'closed' on first subscribe", (done) => {
    let count = 0;
    menuService.displayStatus.pipe(take(1)).subscribe({
      next: (status) => {
        count++;
        expect(status).toEqual('closed');
      },
      complete: () => {
        expect(count).toEqual(1);
        done();
      }
    });
  });

  it("toggles the status", (done) => {
    let count = 0;
    menuService.displayStatus.pipe(take(3)).subscribe({
      next: (status) => {
        count++;
        if (count == 1 || count == 3) {
          expect(status).toEqual('closed');
        } else if (count == 2) {
          expect(status).toEqual('open');
        }
      },
      complete: () => {
        expect(count).toEqual(3);
        done();
      }
    });

    menuService.toggleMenu();
    menuService.toggleMenu();
  });
});
