import {BehaviorSubject}  from 'rxjs';
import {take}             from 'rxjs/operators';

export class MenuService {

  /** Public interface **/

  /* 
   * Get the observable the emit events to reflect the menu open status. On subscribe you get the current status.
   *
   * @return {Observable<('open'|'closed')>} 
   */
  get displayStatus() {
    return this._displayStatusSubject;
  }

  /*
   * Toggle the open status of the menu
   */
  toggleMenu() {
    this.displayStatus
    .pipe(take(1))
    .subscribe((curStatus) => {
      let newStatus = curStatus === 'open' ? 'closed' : 'open';
      this._displayStatusSubject.next(newStatus);
    });
  }

  /*
   * Sets the menu display status to closed
   */
  closeMenu() {
    this._displayStatusSubject.next('closed');
  }


  /** Private **/
  constructor() {
    this._displayStatusSubject = new BehaviorSubject('closed');
  }
}

export const menuService = new MenuService();
