import {Subject} from 'rxjs';
import invariant from 'invariant';

/**
 *
 * The service provides a way to send application wide notification messages (popup, or alerts, messages).
 * One one hand, the code that need to emits messages has access to methods such as `alert()`, `success()`,
 * `info()` to send messages to display to the user. On the other hand, a consumer code responsible for message
 * display has access to an Observable that emits the message that need to be displayed. These two sides are independant
 * and one does not need to worry about the implementation of the other in order to use the service.
 *
 * # Description of the service from the sender side
 *
 * There are several level of messages : "error", "info" and "success". They work exactly the same, it's up to
 * the displayer to color code these messages accordingly.
 *
 * To send a message you can use the following methods :
 *
 *  - error(msg, opts)
 *  - info(msg, opts)
 *  - success(msg, opts)
 *  - alert(msg, opts)
 *
 *  `alert()` is the generic method, if you use this, you need to specify the type (error, info or success) in the provided options.
 *  `error()`, `info()` and `success()` are just shortcuts that will call `alert()` with the right type.
 *
 *  Description of the options you can provide :
 *
 *  - `confirm`: boolean. If true, the message will be displayed with confirmations buttons "yes" and "no". Otherwise (by default),
 *     just a message is displayed (the only control given to the user is to close the message).
 *  - `yesText`: string (default to `i18n.t('CONFIRMATION_MODAL.YES')`). The text to use as "yes" button in case of a confirmation message.
 *  - `noText`: string (default to `i18n.t('CONFIRMATION_MODAL.NO')`). The text to use as "yes" button in case of a confirmation message.
 *
 *  The alert() (and derived) methods return a Promise with an object of the following format :
 *
 *  {
 *    confirmationResult: "yes" | "no"
 *  }
 *
 *  The promise resolves when the user closes the messages (be it a confirmation message or not). The `confirmationResult` property
 *  reflect the choice made by the user in case of a confirmation message. In case of a simple message (no confirmation),
 *  the `confirmationResult` will always be true (although you don't really need the value, just the fact that the promise resolved
 *  is sufficient to know the message was closed)
 *
 *  # Description of the service from the consumer side
 *
 *  All messages sent by the sender are stored and emitted in a RXJS observable, as an object containing all data necessary to
 *  display the message. You need to listen to NotificationService.messages$. Each message is an object of the following form :
 *
 *  {
 *    "message": <string>,
 *    "type": <string>
 *    "confirm": <boolean>,
 *    "yesText": <string>,
 *    "noText": <string>,
 *    "confirmWithYes": <function>,
 *    "confirmWithNo": <function>
 *  }
 *
 *  - message : the message to display
 *  - type : the type of message ("error", "info", "success")
 *  - confirm : if true, this is a confirmation message and you need to display "yes"/"no" controls
 *  - yesText, noText : the text to display for the yes and no buttons respectively. You don't have to provide default values,
 *                      there will always be text provided to these properties.
 *  - confirmWithYes, confirmWithNo : handler to call (without arguments) when the message is closed. In case of a simpe message,
 *                      call confirmWithYes(). In case of a confirmation message, call the method corresponding to the user choice.
 *
 *
 *
 *
 * Parameters
 * ==========
 *
 * @param {Object} options
 * @param {string | Promise<string>} options.yesDefaultText (Optional) The default text to use for the "yes" option of confirmation messages. Defaults to "yes".
 * @param {string | Promise<string>} options.noDefaultText (Optional) The default text to use for the "no" option of confirmation messages. Defaults to "no".
 *
 */
export class NotificationService {

  constructor({yesDefaultText = "yes", noDefaultText = "no"} = {}) {
    this.setupYesNoDefaultText(yesDefaultText, noDefaultText);
    this._messages$ = new Subject();
  }

  async setupYesNoDefaultText(yesDefaultText, noDefaultText) {
    let self = this;
    if (typeof yesDefaultText === 'string') {
      self.yesDefaultText = yesDefaultText;
    } else {
      self.yesDefaultText = 'yes';
      yesDefaultText.then(text => {
        self.yesDefaultText = text;
      });
    }
    if (typeof noDefaultText === 'string') {
      self.noDefaultText = noDefaultText;
    } else {
      self.noDefaultText= 'no';
      noDefaultText.then(text => self.noDefaultText = text);
    }
  }

  /**
   *
   * @typedef {Object} MessageObject
   * @property {String} message The content of the message
   * @property {String} type    The type of the message. On of "error", "info", "success"
   * @property {boolean} confirm If true, the message is a confirmation message. The use must be provided with "yes" and "no" controls
   * @property {string} yesText The text for the 'yes' control in case of confirmation message
   * @property {string} noText The text for the 'no' control in case of confirmation message
   * @property {function} confirmWithYes A callback to call without argument when the user choose "yes" on a confirmation message. Or
   *                                     call this callback by default when the user closes a message that is not a confirmation message,
   *                                     in order to notify the sender that the message has been displayed and closed.
   * @property {function} confirmWithNo A callback to call without argument when the user choose "no" on a confirmation message.
   *
   * @returns {Observable<MessageObject>}
   */
  get messages$() {
    return this._messages$;
  }

  /**
   * @typedef {Object} MessageOptions
   * @property {string} type "error", "info", or "success". Optionnal when using the shortcut methods errro(), info(), success().
   * @property {boolean} confirm If yes, make this message a confirmation message
   * @property {string} yesText Optionnal. If not provided, the default value defined in the service's constructor is used.
   * @property {string} noText Optionnal. If not provided, the default value defined in the service's constructor is used.
   *
   * @typedef {Object} MessageResult
   * @property {String} confirmationResult "yes" or "no". Reflects the user choice on the confirmation message. In case of normal message,
   *                                       the value is always "yes", and reflects the fact that the user closed the message.
   *
   * @param {string} message
   * @param {MessageOptions} options
   * @returns {Promise<MessageResult>}
   */
  alert(message, {type, confirm, ...otherOptions} = {}) {
    invariant(['error', 'info', 'success'].includes(type), "MessageOptions.type must be one of 'error', 'info' or 'success'");
    let yesText = otherOptions.yesText || this.yesDefaultText;
    let noText = otherOptions.noText || this.noDefaultText;

    let resultPromise = new Promise((resolve, reject) => {
      this._messages$.next({
        message,
        type,
        confirm,
        yesText,
        noText,
        confirmWithYes: () => resolve({confirmationResult: "yes"}),
        confirmWithNo: () => resolve({confirmationResult: "no"}),
      });
    });
    return resultPromise;
  }

  success(message, options = {}) {
    options.type = 'success';
    return this.alert(message, options);
  }

  info(message, options = {}) {
    options.type = 'info';
    return this.alert(message, options);
  }

  error(message, options = {}) {
    options.type = 'error';
    return this.alert(message, options);
  }
}


export const notificationService = new NotificationService({
  yesDefaultText: "Oui",
  noDefaultText: "Non"
});