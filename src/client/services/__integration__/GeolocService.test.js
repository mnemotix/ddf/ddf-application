import {SearchPlacesQuery, PlaceFromCoordinatesQuery} from '../GeolocService';
import {apolloClient} from '../../../../jest/utilities/integrationApolloClient';

describe("graphQL queries", () => {
  test('SearchPlacesQuery is valid', async () => {
    let expectedResult = {
      "places": {
        "__typename": "PlaceInterfaceConnection",
        "edges": [
          {
            "__typename": "PlaceInterfaceEdge",
            "node": {
              "id": "geonames:3017382",
              "__typename": "Country",
              "name": "France",
              "countryCode": "FR"
            }
          },
          {
            "__typename": "PlaceInterfaceEdge",
            "node": {
              "id": "geonames:11832782",
              "__typename": "City",
              "name": "Frances",
              "countryCode": "PH",
              "countryId": "geonames:geonames:1694008",
              "countryName": "Philippines",
              "stateName": "Luçon centrale"
            }
          }
        ]
      }
    };

    let {data} = await apolloClient.query({
        query: SearchPlacesQuery,
        variables: {
          qs: 'france',
          first: 2
        }
    });

    expect(data).toEqual(expectedResult); 
  });

  test('PlaceFromCoordinatesQuery is valid', async() => {
    let expectedResult = {
      "placeByGeoCoords": {
        "id": "geonames:6454071",
        "__typename": "City",
        "name": "Grenoble",
        "countryCode": "FR",
        "countryId": "geonames:geonames:3017382",
        "countryName": "France",
        "stateName": "Auvergne-Rhône-Alpes"
      }
    };

    let {data} = await apolloClient.query({
        query: PlaceFromCoordinatesQuery,
        variables: {
          lat:  45.186692, 
          lng:  5.726016 
        }
    });

    expect(data).toEqual(expectedResult); 
  });
});

