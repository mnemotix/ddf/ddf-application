import React, {useContext, useState, useEffect} from 'react';

import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {formatRoute} from 'react-router-named-routes';
import {useHistory, Link} from 'react-router-dom';
import {ParseNewlines} from "../../../utilities/ParseNewlines";
import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

import clsx from 'clsx';
import {ROUTES} from '../../../routes';
import FormSearchInput from '../../../components/widgets/FormSearchInput';
import {BracketButton} from '../../../components/widgets/BracketButton';
import {GeolocPicker} from '../../../components/widgets/GeolocPicker';
import DivButton from '../../../components/widgets/DivButton';
import {useIsReadOnly} from '../../../hooks/useIsReadOnly';
import {UserContext} from '../../../hooks/UserContext';
import {usePlaceContext} from '../../../hooks/PlaceContext';
import ddfLogo from '../../../assets/images/ddf_logo.svg';

import geolocMarkerPurpleIcon from '../../../assets/images/geoloc_marker_purple.svg';
import geolocMarkerGreyIcon from '../../../assets/images/geoloc_marker_grey.svg';
import adminIcon from '../../../assets/images/desktop_header/admin.svg';
import adminIconOrange from '../../../assets/images/desktop_header/admin_orange.svg';
import exploreIcon from '../../../assets/images/desktop_header/explore.svg';
import helpIcon from '../../../assets/images/desktop_header/help.svg';
import accountIcon from '../../../assets/images/desktop_header/account.svg';
import Config from '../../../Config';

const useStyles = makeStyles((theme) => ({
  searchButton: {
    fontSize: `${Config.fontSizes.medium}px`,
    marginLeft: `${Config.spacings.small}px`
  },
  searchText: {
    "marginLeft": `4px`,
    "marginRight": `4px`,
  },
  title: {
    "display": "flex",
    "alignItems": "center",
    "width": "300px"
  },
  header: {
    "display": "flex",
    "justifyContent": "space-between",
    "backgroundColor": "white",
    "paddingLeft": Config.spacings.huge,
    "paddingRight": Config.spacings.huge,
    "height": Config.heights.desktopHeader
  },
  headerSection: {
    display: 'flex',
    alignItems: 'center'
  },
  a: {
    color: 'inherit',
    textDecoration: 'inherit'
  },
  ddfLogo: {"height": "30px"},
  ddfTitle: {"fontWeight": Config.fontWeights.bold, "marginLeft": Config.spacings.medium, color: Config.colors.black},
  searchBarContainer: {"flexGrow": "1", "maxWidth": "400px"},
  searchBarSection: {"flexGrow": "1", "display": "flex", "justifyContent": "center"},
  geolocMarkerIcon: {"height": "20px", "marginLeft": Config.spacings.small},
  textualLink: {"textAlign": "center"},
  buttons: {"justifyContent": "flex-end", "width": "300px"},
  button: {"marginLeft": Config.spacings.small},
  img: {"height": "40px"},

  modalTitle: {
    marginBottom: "30px"
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    width: 'min(35vw,420px)'
  },
  centerM15: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: theme.spacing(4)
  }
}));

DesktopHeader.propTypes = {
  /**
   * Boolean flag to hide the search bar and search button from the header. By default, the search is visible.
   */
  hideSearch: PropTypes.bool,
};
export function DesktopHeader({hideSearch}) {

  const classes = useStyles();
  const {t} = useTranslation();
  const history = useHistory();
  const {user} = useContext(UserContext);
  const {currentPlace} = usePlaceContext();
  const [searchQuery, setSearchQuery] = useState('');
  //let searchQuery = '';// fix dic-684 , effeet de bord avec useState
  const [showIsRequired, setShowIsRequired] = useState(false);
  const [geolocOpened, setGeolocOpened] = useState(false);
  const isReadOnly = useIsReadOnly();

  const geolocated = !!currentPlace?.id;

  function renderGeoloc() {
    return (
      <Modal
        open={geolocOpened}
        onClose={() => setGeolocOpened(false)}
        role="alertdialog"
        aria-modal="true"
        aria-labelledby="Choisir sa géolocalisation"
        aria-describedby="Soit via recherche soit via localisation automatique"
        className={classes.modal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={geolocOpened}>
          <div className={classes.paper}>
            <div className={classes.modalTitle}>
              Changer votre géolocalisation
            </div>
            <GeolocPicker closeGeolocParentModal={() => setGeolocOpened(false)} displayInColumn={true} />
            <div className={classes.centerM15}>
              <BracketButton
                greyBg={false}
                text={"Valider"}
                aria-label="Fermer la fenêtre"
                onClick={() => setGeolocOpened(false)} >
              </BracketButton>
            </div>
          </div>
        </Fade>
      </Modal>
    );

  }

  return (
    <header className={classes.header}>
      {renderGeoloc()}
      <div className={classes.headerSection}>
        <Link accessKey="1" role="link" aria-label="Retourner à la page d'accueil" className={classes.title} to={'/'}>
          <img className={classes.ddfLogo} src={ddfLogo} alt="DDF" />
          <div className={classes.ddfTitle}>
            <ParseNewlines text={t('DESKTOP_HEADER.TITLE')} />
          </div>
        </Link>
      </div>

      <div className={clsx(classes.headerSection, classes.searchBarSection)}>
        <If condition={!hideSearch}>
          <div className={classes.searchBarContainer}>
            <FormSearchInput
              size="medium"
              theme="dark"
              showIsRequired={showIsRequired}
              placeholder={t('INDEX.SEARCH_PLACEHOLDER')}
              onChange={(value) => {
                setShowIsRequired(false);
                setSearchQuery(value);
                //  searchQuery = value; // fix dic-684
              }}
              onSubmit={performSearch}
              autofocus={false}
            />
          </div>

          <DivButton onClick={() => setGeolocOpened(true)} aria-label="Changer votre géolocalisation" >
            <img className={classes.geolocMarkerIcon} src={geolocated ? geolocMarkerPurpleIcon : geolocMarkerGreyIcon} alt="gps" />
          </DivButton>
          <BracketButton greyBg={false} classNameButton={classes.searchButton} classNameText={classes.searchText} text={t('DESKTOP_HEADER.SEARCH_BUTTON')} onClick={performSearch} />

        </If>
      </div>

      <div className={clsx(classes.headerSection, classes.buttons)} id="desktopnav" role="navigation">
        <If condition={user?.userAccount?.isAdmin || user?.userAccount?.isOperator}>
          <div>
            <Link role="link"
              className={classes.textualLink}
              to={ROUTES.ADMIN}
              aria-label={t('DESKTOP_HEADER.TOOLTIPS.ADMIN')}
              title={isReadOnly ? t('DESKTOP_HEADER.TOOLTIPS.ADMINREADONLY') : t('DESKTOP_HEADER.TOOLTIPS.ADMIN')}>
              <img src={isReadOnly ? adminIconOrange : adminIcon} alt="admin" className={classes.img} />
            </Link>
          </div>
        </If>
        <Link role="link" aria-label={t('DESKTOP_HEADER.TOOLTIPS.EXPLORE')}
          className={classes.button}
          to={ROUTES.EXPLORE} title={t('DESKTOP_HEADER.TOOLTIPS.EXPLORE')}
          accessKey="6">
          <img src={exploreIcon} alt="Aide" className={classes.img} />
        </Link>
        <Link role="link" aria-label={t('DESKTOP_HEADER.TOOLTIPS.HELP')}
          className={classes.button}
          to={ROUTES.HELP_INDEX} title={t('DESKTOP_HEADER.TOOLTIPS.HELP')}
          accessKey="6">
          <img src={helpIcon} alt="Aide" className={classes.img} />
        </Link>
        <Link role="link" aria-label={t(`DESKTOP_HEADER.TOOLTIPS.${user?.isLogged ? 'MY_ACCOUNT' : 'LOGIN'}`)}
          className={classes.button}
          to={user?.isLogged ? ROUTES.MY_ACCOUNT_PROFILE : ROUTES.LOGIN}
          title={t(`DESKTOP_HEADER.TOOLTIPS.${user?.isLogged ? 'MY_ACCOUNT' : 'LOGIN'}`)}
        >
          <img src={accountIcon} alt="Compte" className={classes.img} />
        </Link>
      </div>
    </header >
  );

  function performSearch() {
    if (!searchQuery?.label) {
      setShowIsRequired(true);
      return;
    }
    history.push(formatRoute(ROUTES.FORM_SEARCH, {formQuery: searchQuery.label}));
  }
}
