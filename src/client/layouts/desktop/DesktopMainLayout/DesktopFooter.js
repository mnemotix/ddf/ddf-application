import React from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Link} from 'react-router-dom';

import {ROUTES} from '../../../routes';
import {ImproveDdfButton} from '../../../components/widgets/ImproveDdfButton';
import {FooterExplanationText} from '../../../components/widgets/FooterExplanationText';
import {StoresSectionDesktop} from "../../../components/widgets/StoresSection";
import {useDdfButton} from "../../../hooks/useDdfButton";
import ddfLogo from '../../../assets/images/ddf_logo.svg';
import creativeCommonsLogo from '../../../assets/images/creative_commons.svg';
import mcLogo from '../../../assets/images/partners/ministre-culture.jpg';
import iifLogo from '../../../assets/images/partners/iif.jpg';
import oifLogo from '../../../assets/images/partners/oif.jpg';
import aufLogo from '../../../assets/images/partners/auf.jpg';
import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import Config from '../../../Config';
import globalStyles from '../../../assets/stylesheets/globalStyles.js';

import packageVersion from '../../../../../package.json';
// don't destructure in import directly for package.json => warning 
const {version} = packageVersion;

export const useStyles = makeStyles((theme) => ({
  improveSection: {
    width: "500px",
    marginTop: `${Config.heights.footerImproveButton - Config.heights.improveDdfButton}px`,
    marginRight: 'auto',
    marginLeft: 'auto',
    pointerEvents: 'auto'
  },
  improveSectionFiller: {
    height: `${Config.heights.footerImproveButton}px`
  },

  mainSection: {
    display: 'flex',
    backgroundColor: Config.colors.white,
    paddingLeft: Config.spacings.huge,
    paddingRight: Config.spacings.huge,
    paddingTop: Config.spacings.tiny,
    paddingBottom: Config.spacings.tiny,
    height: `${Config.heights.footerLinksBanner}px`,
    alignItems: 'center'
  },
  ddfLogo: {
    height: '20px',
  },
  creativeCommonsLogo: {
    height: '20px'
  },
  width100: {
    width: "100px"
  },

  links: {
    display: "flex",
    justifyContent: "center",
    flexGrow: 1,
    alignItems: "flex-start",
    '& a': {
      textDecoration: "none",
      fontFamily: "Raleway",
      fontWeight: Config.fontWeights.semibold,
      color: Config.colors.purple,
      whiteSpace: "nowrap",
      paddingTop: Config.spacings.tiny
    },
  },
  widthContainer: {
    maxWidth: "600px",
    display: "flex",
    flexGrow: "1",
    justifyContent: "space-between",
    alignItems: "center",
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  c2: {
    fontSize: '13px'
  },
  partnersSection: {
    height: `${Config.heights.footerPartnersLinks}px`,
    paddingTop: Config.spacings.tiny,
    // backgroundColor: Config.colors.white,
    marginTop: "10px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column"
  },
  logos: {
    justifyContent: 'space-between',
    display: 'flex',
    alignItems: 'center',
    width: '600px',
  },
  logo: {
    height: "60px"
  },
  mclogo: {
    height: "80px",
    marginBottom: "20px"
  },
  version: {
    display: 'flex',
    justifyContent: 'center',
    color: Config.colors.darkgray,
    paddingRight: '25px'
  },
  mnemotix: {
    '& a': {
      color: Config.colors.white,
      fontSize: '1px'
    }
  }
}));



DesktopFooter.propTypes = {
  /** The current form written representation, used for the "improve DDF" button */
  currentForm: PropTypes.string,
};
export function DesktopFooter(props) {
  const {t} = useTranslation();
  const classes = useStyles();
  const gS = globalStyles();
  const {currentForm} = props;
  const ddfButtonDisplayed = useDdfButton();

  return (
    <footer>
      <If condition={ddfButtonDisplayed}>
        <div className={classes.improveSection}>
          <FooterExplanationText />
          <ImproveDdfButton currentForm={currentForm} />
        </div>
      </If>
      <If condition={!ddfButtonDisplayed}>
        <div className={classes.improveSectionFiller}></div>
      </If>

      <div className={classes.mainSection}>
        <div className={classes.width100}>
          <img className={classes.ddfLogo} src={ddfLogo} alt="DDF" />
        </div>

        <div className={classes.links} id="footernav" role="navigation">
          <div className={classes.widthContainer}>
            <div className={classes.column}>
              <Link role="link" aria-label={t('PROJECT_PRESENTATION_PAGE_TITLE')} to={ROUTES.PRESENTATION}>{t('PROJECT_PRESENTATION_PAGE_TITLE')}</Link>
              <Link role="link" aria-label={t('PARTNERS_PAGE_TITLE')} to={ROUTES.PARTNERS}>{t('PARTNERS_PAGE_TITLE')}</Link>
            </div>
            <div className={clsx(classes.column, classes.c2)}>
              <Link role="link" aria-label={t('CREDITS_PAGE_TITLE')} to={ROUTES.CREDITS}>{t('CREDITS_PAGE_TITLE')}</Link>
              <Link role="link" accessKey="3" aria-label={t('PLAN_DU_SITE_PAGE_TITLE')} to={ROUTES.PLAN_DU_SITE}>{t('PLAN_DU_SITE_PAGE_TITLE')}</Link>
              <Link role="link" accessKey="0" aria-label={t('ACCESSIBILITE_PAGE_TITLE')} to={ROUTES.ACCESSIBILITE}>{t('ACCESSIBILITE_PAGE_TITLE')}</Link>
            </div>
            <div className={clsx(classes.column, classes.c2)}>
              <Link role="link" aria-label={t('MENTIONS_LEGALES_PAGE_TITLE')} to={ROUTES.MENTIONS_LEGALES}>{t('MENTIONS_LEGALES_PAGE_TITLE')}</Link>
              <Link role="link" aria-label={t('GCU_PAGE_TITLE')} to={ROUTES.GCU}>{t('GCU_PAGE_TITLE')}</Link>
              <Link role="link" aria-label={t('POLITIQUE_CONFIDENTIALITE_PAGE_TITLE')} to={ROUTES.POLITIQUE_CONFIDENTIALITE}>{t('POLITIQUE_CONFIDENTIALITE_PAGE_TITLE')}</Link>
            </div>
          </div>
        </div>

        <div className={classes.width100}>
          <img className={classes.creativeCommonsLogo} src={creativeCommonsLogo} alt="Creative Commons" />
        </div>
      </div>
      <StoresSectionDesktop />

      <div className={classes.partnersSection} >
        {/*<div className={classes.logos} role="banner">
          <img className={classes.mclogo} src={mcLogo} alt="Ministre de la culture" />
          <img className={classes.logo} src={iifLogo} alt="Institut international pour la francophonie" />
          <img className={classes.logo} src={aufLogo} alt="Agence universitaire de la francophonie" />
          <img className={classes.logo} src={oifLogo} alt="Organisation internationale de la francophonie" />
        </div>*/ }
        <div className={classes.version}>{"Version " + version}</div>
        <div className={classes.mnemotix}><a tabIndex="-1" href="https://www.mnemotix.com">mnemotix</a> </div>
      </div>

    </footer>
  );

}
