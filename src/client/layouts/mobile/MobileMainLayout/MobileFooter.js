import React from 'react';
import PropTypes from 'prop-types';
import {ImproveDdfButton} from '../../../components/widgets/ImproveDdfButton';
import {useDdfButton} from "../../../hooks/useDdfButton";

import {makeStyles} from '@material-ui/core/styles';
import Config from '../../../Config';


export const useStyles = makeStyles((theme) => ({
  placeholder: {
    height: `${Config.heights.improveDdfButton}px`
  },
  fixedContainer : {
    position: 'fixed',
    zIndex: 3,
    bottom: 0,
    left: 0,
    right: 0,
  } 
}));



export function MobileFooter({currentForm}) {
  const classes = useStyles();
  const ddfButtonDisplayed = useDdfButton();

  if (!ddfButtonDisplayed) {
    return null
  }
  return (
    <div className={classes.placeholder}>
      <div className={classes.fixedContainer}>
        <ImproveDdfButton currentForm={currentForm} />
      </div>
    </div>
  );

}

MobileFooter.propTypes = {
  currentForm: PropTypes.string,
};