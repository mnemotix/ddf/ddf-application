import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import clsx from "clsx";
import Config from '../../../Config';
import {makeStyles} from '@material-ui/core/styles';
import {useTranslation} from 'react-i18next';
import {useHistory, Link} from 'react-router-dom';

import {ROUTES} from '../../../routes';
import {StoresSectionMobile} from "../../../components/widgets/StoresSection";
import {menuService} from '../../../services/MenuService';
import {getUserAuthenticationService} from '../../../services/UserAuthenticationService';
import {notificationService as appNotificationService} from '../../../services/NotificationService';
import {useBrowsingHistoryHelperService} from '../../../services/BrowsingHistoryHelperService';
import {useIsReadOnly} from '../../../hooks/useIsReadOnly';
import {UserContext} from '../../../hooks/UserContext';


import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import exploreIcon from '../../../assets/images/desktop_header/explore.svg';
import helpIcon from '../../../assets/images/help_icon.svg';
import ddfLogo from '../../../assets/images/ddf_logo.svg';
import adminIcon from '../../../assets/images/admin.svg';
import adminIconOrange from '../../../assets/images/admin_orange.svg';

import contributionsIcon from '../../../assets/images/contributions_purple.svg';
import settingsIcon from '../../../assets/images/settings.svg';
import creditsIcon from '../../../assets/images/credits_icon.svg';
import partnersIcon from '../../../assets/images/partners_icon.svg';
import logoTablette from '../../../assets/images/tablette.svg';



const useStyles = makeStyles((theme) => ({
  spaceBetween: {
    "width": "100%",
    "display": "flex",
    "justifyContent": "space-between",
    "alignItems": "center",
    "paddingTop": "6px",
    "paddingRight": "6px"
  },
  hover: {
    "&:hover": {
      backgroundColor: Config.colors.pink,
      color: Config.colors.white
    }
  },
  menuEntry: {
    display: "flex",
    alignItems: "center",
    paddingLeft: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
    color: "inherit",
    textDecoration: "inherit",
  },
  bigDdfIconImg: {
    alignItems: "center",
    justifyContent: "center",
    width: "75px"
  },
  icon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "18px",
    marginRight: Config.spacings.medium,
  },
  iconImg: {
    width: "100%",
    height: "100%"
  },
  hr: {
    borderColor: Config.colors.mediumgray,
    marginLeft: Config.spacings.large,
    marginRight: Config.spacings.large
  },
  userSection: {
    padding: Config.spacings.medium,
    marginBottom: Config.spacings.medium,
    backgroundColor: Config.colors.pink,
    color: Config.colors.white
  },
  username: {
    fontWeight: Config.fontWeights.bold,
    marginBottom: Config.spacings.tiny
  }

}));

MenuLink.propTypes = {
  to: PropTypes.string.isRequired,
};

function MenuLink({idkey, to, ariaLabel, children, ...props}) {

  const classes = useStyles();

  return (
    <Link key={idkey} role="link" aria-label={ariaLabel} to={to} onClick={navigate} className={clsx(classes.menuEntry, classes.hover)} {...props}>
      {children}
    </Link>
  );

  function navigate() {
    menuService.closeMenu();
  }
}

// affiche une entrée de menu mais sans lien, comme pour les titres de catégories ou les composants
// hover permet de rajouter le CSS hover ou non 
function MenuEntry({ariaLabel, hover, children, idkey}) {
  const classes = useStyles();

  return <div key={idkey} aria-label={ariaLabel} className={clsx(classes.menuEntry, hover && classes.hover)}>
    {children}
  </div>
}

const menuStatic = [
  {text: 'PROJECT_PRESENTATION_PAGE_TITLE', to: ROUTES.PRESENTATION, src: ddfLogo},
  {text: 'PARTNERS_PAGE_TITLE', to: ROUTES.PARTNERS, src: partnersIcon},
  {text: 'CREDITS_PAGE_TITLE', to: ROUTES.CREDITS, src: creditsIcon, insertHr: true},
  {text: 'MOBILE_MENU.EXPLORE', to: ROUTES.EXPLORE, src: exploreIcon, insertHr: true},
  {text: 'MOBILE_MENU.MENTIONS_LEGALES', to: ROUTES.MENTIONS_LEGALES, src: helpIcon},
  {text: 'PLAN_DU_SITE_PAGE_TITLE', to: ROUTES.PLAN_DU_SITE, src: null},
  {text: 'ACCESSIBILITE_PAGE_TITLE', to: ROUTES.ACCESSIBILITE, src: null},
  {text: 'GCU_PAGE_TITLE', to: ROUTES.GCU, src: null},
  {text: 'POLITIQUE_CONFIDENTIALITE_PAGE_TITLE', to: ROUTES.POLITIQUE_CONFIDENTIALITE, src: null, insertHr: true},
  {text: null, idkey: 'STORESECTIONMOBILE', hover: true, Component: <StoresSectionMobile />, to: null, src: logoTablette, insertHr: true},
  {text: 'MOBILE_MENU.HELP', to: ROUTES.HELP_INDEX, src: helpIcon}
]

export function MobileMenu(props) {
  const classes = useStyles();
  const notificationService = props.notificationService || appNotificationService;
  const history = useHistory();
  const {t} = useTranslation();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();
  const isReadOnly = useIsReadOnly();

  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {logout} = userAuthenticationService.useLogout();
  const {user} = useContext(UserContext);
  
  return (
    <nav className={clsx(!user?.isLogged && classes.noLoggedUser)} id="mobilenav" role="navigation">
      <div className={classes.spaceBetween}>
        <MenuLink ariaLabel={"Accueil"} to={ROUTES.INDEX}>
          <div >
            <img alt="Accueil" src={ddfLogo} className={classes.bigDdfIconImg} />
          </div>
        </MenuLink>
        <IconButton aria-label="fermer menu" onClick={() => menuService.closeMenu()}>
          <CloseIcon />
        </IconButton>
      </div>

      {renderCurrentUserSection()}
      {menuStatic.map(({idkey, text, to, src, insertHr, hover, Component}, index) => (
        <div key={text + idkey}>
          {!to ?
            <MenuEntry idkey={text + idkey} ariaLabel={t(text)} hover={hover} >
              <div className={classes.icon}>
                {src && <img className={classes.iconImg} alt={t(text)} src={src} />}
              </div>
              {text && <div className={classes.label}>{t(text)}</div>}
              {Component}
            </MenuEntry>
            :
            <MenuLink idkey={text + idkey} ariaLabel={t(text)} to={to}>
              <div className={classes.icon}>
                {src && <img className={classes.iconImg} alt={t(text)} src={src} />}
              </div>
              <div className={classes.label}>{t(text)}</div>
            </MenuLink>
          }
          {insertHr && <hr className={classes.hr} key={index} />}
        </div>
      ))}

    </nav >
  );

  function renderCurrentUserSection() {
    if (user?.isLogged) {
      return (
        <React.Fragment>
          <div className={classes.userSection}>
            <div className={classes.username}>{`[ ${user.nickName} ]`}</div>
            <div className={classes.email}>{user.userAccount.username}</div>
          </div>
          <MenuLink ariaLabel={t('MOBILE_MENU.MY_ACCOUNT')} to={ROUTES.MY_ACCOUNT_PROFILE}>
            <div className={classes.icon}>
              <img className={classes.iconImg} alt="Mon compte" src={settingsIcon} />
            </div>
            <div className={classes.label}>{t('MOBILE_MENU.MY_ACCOUNT')}</div>
          </MenuLink>
          <MenuLink ariaLabel={t('MOBILE_MENU.MY_ACCOUNT_CONTRIBUTIONS')} to={ROUTES.MY_ACCOUNT_CONTRIBUTIONS}>
            <div className={classes.icon}>
              <img className={classes.iconImg} alt="Mes contributions" src={contributionsIcon} />
            </div>
            <div className={classes.label}>{t('MOBILE_MENU.MY_ACCOUNT_CONTRIBUTIONS')}</div>
          </MenuLink>
          <If condition={user.userAccount.isOperator || user.userAccount.isAdmin}>
            <hr className={classes.hr} />
            <If condition={user.userAccount.isAdmin}>
              <MenuLink ariaLabel={t('MOBILE_MENU.ADMIN.USERS')} to={ROUTES.ADMIN}>
                <div className={classes.icon}>
                  <img className={classes.iconImg} alt="admin" src={isReadOnly ? adminIconOrange : adminIcon} />
                </div>
                <div className={classes.label}>{isReadOnly ? t('DESKTOP_HEADER.TOOLTIPS.ADMINREADONLY') : t('DESKTOP_HEADER.TOOLTIPS.ADMIN')}</div>
              </MenuLink>
            </If>

            <MenuLink ariaLabel={t('MOBILE_MENU.OPERATORS.CONTRIBUTIONS')} to={ROUTES.CONTRIBUTIONS_REVIEW}>
              <div className={classes.icon}></div>
              <div className={classes.label}>{t('MOBILE_MENU.OPERATORS.CONTRIBUTIONS')}</div>
            </MenuLink>
            <If condition={user.userAccount.isAdmin}>
              <MenuLink ariaLabel={t('MOBILE_MENU.ADMIN.USERS')} to={ROUTES.ADMIN_USERS}>
                <div className={classes.icon}></div>
                <div className={classes.label}>{t('MOBILE_MENU.ADMIN.USERS')}</div>
              </MenuLink>
            </If>
            <hr className={classes.hr} />
          </If>

          <MenuLink ariaLabel={t('LOGOUT')} to={ROUTES.INDEX} onClick={handleLogout}>
            <div className={classes.icon}></div>
            <div className={classes.label}>{t('LOGOUT')}</div>
          </MenuLink>
          <hr className={classes.hr} />
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <MenuLink ariaLabel={t('LOGIN')} to={ROUTES.LOGIN}>
            <div className={classes.icon}>
              <img className={classes.iconImg} alt="Connexion" src={settingsIcon} />
            </div>
            <div className={classes.label}>{t('LOGIN')}</div>
          </MenuLink>
          {!isReadOnly && (
            <MenuLink ariaLabel={t('SUBSCRIBE')} to={ROUTES.SUBSCRIBE}>
              <div className={classes.icon}>
                <img className={classes.iconImg} alt="S'abonner" src={contributionsIcon} />
              </div>
              <div className={classes.label}>{t('SUBSCRIBE')}</div>
            </MenuLink>
          )}
          <hr className={classes.hr} />
        </React.Fragment>
      );
    }
  }

  async function handleLogout(evt) {
    evt.preventDefault();
    menuService.closeMenu();
    let res = await logout();
    if (res.success) {
      /*
       * Redirect strategy : if the current page is not suited for logged out user, redirect to index, otherwise
       * don't redirect
       */
      let loc = browsingHistoryHelperService.getAfterLogoutLocation();
      if (!loc) {
        history.push('/');
      }
    }
    if (res.globalErrorMessage) {
      notificationService.error(res.globalErrorMessage);
    }
  }
}
