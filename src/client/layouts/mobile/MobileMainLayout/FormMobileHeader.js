import React from 'react';
import PropTypes from 'prop-types';
import {MobileHeader} from './MobileHeader';

import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import Config from '../../../Config';


const useStyles = makeStyles((theme) => ({
  title: {
    fontFamily: "Raleway",
    fontSize: Config.fontSizes.xxlarge,
    fontWeight: Config.fontWeights.semibold,
    overflow: "hidden",
    overflowWrap: "break-word",
    maxWidth: "55vw",
    textOverflow: "clip"
  },
  smallFontSize: {
    fontSize: Config.fontSizes.large,
  }
}));

FormMobileHeader.propTypes = {
  geolocMarker: PropTypes.bool,
  search: PropTypes.bool,
  backFct: PropTypes.bool,
  currentForm: PropTypes.string,
  currentPlace: PropTypes.object,
};

export function FormMobileHeader({search, geolocMarker, currentForm, currentPlace, showBackIcon}) {

  const classes = useStyles();

  return <MobileHeader showBackIcon={showBackIcon} search={search} geolocMarker={geolocMarker} content={renderTitle()}
    currentForm={currentForm} currentPlace={currentPlace}
  />

  function renderTitle() {
    if (currentForm) {
      let smallFontSize = currentForm?.length > 7;
      return <div className={clsx(classes.title, smallFontSize && classes.smallFontSize)}>
        {currentForm}
      </div>;
    } else {
      return null;
    }
  }
}
