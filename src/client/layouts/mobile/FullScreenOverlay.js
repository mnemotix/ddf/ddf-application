import React from 'react';
import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  container: {
    position: 'fixed',
    height: '100vh',
    width: '100vw',
    top: 0,
    left: 0,
    overflow: 'scroll'
  }
}));


export function FullScreenOverlay({children}) {
  const classes = useStyles();
  return <div className={classes.container}>{children}</div>;
}
