import React from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import {useBrowsingHistoryHelperService} from '../../../services/BrowsingHistoryHelperService';
import DdfLogo from '../../../assets/images/ddf_logo.svg';
import backArrow from '../../../assets/images/back_arrow_purple.svg';
import cross from '../../../assets/images/cross_purple.svg';
import Config from '../../../Config';


export const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    height: `${Config.heights.mobileHeader}px`,
    backgroundColor: Config.colors.white
  },
  ddfLogo: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '80px',
    "& img": {
      width: '100%'
    }
  },
  subTitle: {
    height: `${Config.heights.mobileHeader * 0.75}px`,
    backgroundColor: Config.colors.purple,
    color: Config.colors.white,
    display: "flex",
    alignItems: "center",
    textAlign: "center",
    justifyContent: "center",
    fontSize: Config.fontSizes.large
  },
  itemContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "40px",
    fontSize: Config.fontSizes.large,
    fontWeight: Config.fontWeights.bold,
    "& img": {
      height: '100%',
      width: '100%'
    }
  },
  backButton: {
    cursor: 'pointer',
    height: Config.fontSizes.medium
  },
  closeButton: {
    cursor: 'pointer',
    height: '12px'
  }
}));

/**
 * Usage :
 *
 * <SimpleModalPageHeader title="Mon titre" />
 *
 */
export function SimpleModalPageHeader({onBack, onClose, controls, title, titleBgColor}) {
  const classes = useStyles();
  let {browsingHistoryHelperService} = useBrowsingHistoryHelperService();
  const history = useHistory();

  let bgColorStyle = {};
  if (titleBgColor && Config?.colors?.[titleBgColor]) {
    bgColorStyle = {backgroundColor: Config.colors[titleBgColor]}
  }

  return (
    <>
      <div className={classes.container}>
        {renderBackButton()}
        <div className={classes.ddfLogo}>
          <img src={DdfLogo} alt="DDF logo" />
        </div>
        {renderCloseButton()}
      </div>
      {title && <div className={classes.subTitle} style={bgColorStyle}>{title}</div>}
    </>
  );

  function renderBackButton() {
    if (controls.includes('back')) {
      return (
        <div role="button"
          aria-label={"Retour"}
          tabIndex={0}
          className={clsx(classes.itemContainer, classes.backButton)} onClick={backButtonHandler}>
          <img src={backArrow} alt="Retour" />
        </div>
      );
    } else {
      return <div className={classes.itemContainer} />;
    }
  }

  function renderCloseButton() {
    if (controls.includes('close')) {
      return (
        <div role="button"
          aria-label={"Fermer"}
          className={clsx(classes.itemContainer, classes.closeButton)} onClick={closeButtonHandler}>
          <img src={cross} alt="Fermer" />
        </div>
      );
    } else {
      return <div className={classes.itemContainer} />;
    }

  }

  function backButtonHandler() {
    if (onBack) {
      onBack();
    } else {
      let locObject = browsingHistoryHelperService.getBackFromSecondaryScreenLocation();
      history.push(locObject ? locObject.location : '/');
    }
  }

  function closeButtonHandler() {
    if (onClose) {
      onClose();
    }
  }

}

SimpleModalPageHeader.propTypes = {
  /** string or react element that will be inserted in the header */
  title: PropTypes.node,
  /** string or react element that will be inserted into the body of the page */
  content: PropTypes.node.isRequired,
  /** 
   * Describes the controls present in the mobile header. 
   * Array of strings which can include `'back'` and `'close'`. By default it is `['back']` 
   */
  controls: PropTypes.arrayOf(PropTypes.oneOf(['back', 'close'])),
  /** handler on click on back button. By default, the back button navigates to the previous location of the main navigation space */
  onBack: PropTypes.func,
  /** handler on click on close button */
  onClose: PropTypes.func,

  /** A color string among the application color palette as defined in Config.js. Default color is purple */
  titleBgColor: PropTypes.string
};

SimpleModalPageHeader.defaultProps = {
  theme: {}
};
