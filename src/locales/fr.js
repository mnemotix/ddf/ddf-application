const fr = {
  LOGIN: 'Se connecter',
  LOGOUT: 'Se déconnecter',
  SUBSCRIBE: "S'inscrire",
  EXPLORE: {
    TITLE: 'Exploration des données',
    LOCALISATION: "Aire d'usage",
    LOCALISATION_TYPE: {
      AIRES: "Aires",
      CONTINENT: "Continent",
      COUNTRY: "Pays",
      STATE: "Etat",
      CITY: "Ville"
    }
  },
  FORM: {
    TOP_POSTS: {
      FORM_HEADER: 'Forme :',
      ETYMOLOGY_HEADER: 'Étymologie :',
      DESKTOP: {
        FORM_HEADER: 'Discussion sur la forme',
        ETYMOLOGY_HEADER: "Discussion sur l'étymologie",
      },
    },
    SEE_ALSO: {
      TITLE: 'Voir aussi',
      RELATED: 'Vocabulaire apparenté',
      NO_RELATED: 'Aucun résultat',
      FORM_VARIATION: 'Variante graphique',
      DERIVED: 'Mots dérivés',
    },
    NO_RESULTS: 'Pas de résultat pour cette entrée',
    NO_DEFINITION: "Pas de définition pour cette forme.",
    DEFINITIONS_COUNT: "{{count}} définitions pour ce mot dont: {{definition}}",
    LEXICOGRAPHIC_RESOURCE: "Source",
    PLACES: "Région d'usage",
    PLACES_plural: "Régions d'usage",
    CLOSE_FORMS_SUGGESTIONS: 'SUGGESTIONS DE RECHERCHE :',
    DEFINITION: 'Définition',
    DEFINITIONS: 'Définitions',
    DESKTOP: {
      RESEARCHED_FORM: 'Terme recherché : ',
    },
  },
  LEXICAL_SENSE_DETAIL: {
    EXAMPLE_LABEL: 'Exemple',
    ABOUT_SENSE_TITLE: 'Sens et usages',
    BACK_TO_FORM_LINK: 'Retour à la liste des définitions',
    BACK_TO_SENSE_LINK: 'Retour à la définition',
    EDIT_THIS_DEFINITION: 'Modifier la définition',
    ENRICH_THIS_DEFINITION: "Enrichir la définition",
    NO_RESULTS: "Cette définition n'existe pas",
    SOURCE: 'Source : {{source}}',
    ACTIONS: {
      LIKE: 'Je valide',
      LIKE_DONE: "Validé",
      UNLIKE: "J'annule ma validation...",
      ASK_REMOVAL: "J'invalide",
      UNASK_REMOVAL: "J'annule mon invalidation...",
      REPORT: 'Je signale',
      REPORT_DONE: 'Signalé',

      UNREPORT: 'Je retire mon signalement',
      PROCESS: 'Marquer comme traité',
      UNPROCESS: 'Marquer comme "non traité"',
      REMOVE: 'Supprimer',
      LEXICAL_SENSE_REMOVE_SUCCESS: 'La définition a été supprimée.',
      LEXICAL_SENSE_PROCESSED: "La définition a été marquée comme traitée",
      REPORT_SUCCESS: 'Signalé avec succés',
      REPORT_ERROR: "Erreur lors de l'envoi du signalement",
    },
    MODAL: {

      WHATISPRB: "De quel type de problème s'agit'il ?",
      SEND: "Envoyer",
      OTHERANONYMDETAILS:
        'Pour que votre problème soit résolu, soyez précis dans la description. Ce message sera diffusé à plusieurs personnes et ne sera pas anonyme. Si votre problème concerne un cas précis de harcèlement qui nécessite la protection de votre idendité, écrivez plutôt à',
      REASONS: {
        MeaninglessContentReporting: "Test de débutant, essai, contenu dénué de sens en l'état",
        CopyrightInfringementReporting: "Problème de respect du droit d'auteur ou de plagiat",
        DiscriminationInducementReporting:
          'Incitation à la haine ou à la discrimination de personne en raison de leurs origines, de leur sexe, de leur genre, de leur orientation sexuelle ou de leur handicap',
        ViolenceThreatReporting:
          "Menaces, apologie d'actes criminels ou incistations à la violence, incitation à commettre des actes criminels, incluant par exemple pédophilie, vol, meurtre, escroquerie, trafic illicite.",
        ContributorBullyingReporting: 'Harcèlement contre une personne participant au Dictionnaire des francophones',
        PrivacyInfringementReporting:
          "Divulgation d'informations personnelles ou propriétés d'un autre utilisateur, personne, ou organisation",
        SlanderReporting: 'Injure, dénigrement, diffamation, moquerie',
        AdvertisementReporting: 'Publipostage, message inutile ou publicitaire',
        LayoutReporting: "Problème de mise en forme ou de présentation de l'information pouvant être dû à l'importation des données",

        OtherContentReporting: 'Problème lié au conteu',
        OtherPersonReporting: 'Problème lié aux personnes',
        OtherWebsiteReporting: 'Problème lié au fonctionnement du site',
        UncategorizedReporting: "Autre raison"
      },
    },
    DESKTOP: {
      LIKE_COUNT: '{{count}} personne a validé cette définition',
      LIKE_COUNT_plural: '{{count}} personnes ont validé cette définition',
      LIKE_COUNT_zero: "Personne n'a validé cette définition",
      TOP_POSTS: {
        SENSE_HEADER: "Discussion sur l'usage",
      },
      DEFINITION: 'Définition',
      SOURCES: 'Sources',
      GEOGRAPHICAL_DETAILS: 'Précisions géographiques',
    },
  },
  INDEX: {
    SEARCH_PLACEHOLDER: 'Écrivez le terme recherché ici',
    SEARCH_PLACEHOLDER_MOBILE: 'Terme recherché',
    GEOLOC_PLACEHOLDER: 'Choisir une localisation',
    AUTO_GEOLOC: 'Utiliser ma position actuelle',
    FOOTER:
      "Comprendre et partager \n les mots avec plusieurs dictionnaires \n du français parlé partout dans le monde. \n Participer à l'enrichissement collectif.",
    SEARCH_BUTTON: 'Rechercher',
  },
  GEOLOC: {
    ERRORS: {
      PERMISSION_DENIED: "Nous n'avons pas la permission d'accéder à votre localisation",
      UNKNOWN: 'Un problème nous empêche de vous géolocaliser',
    },
  },
  FOOTER: {
    IMPROVE_THE_DDF: 'Enrichir le',
    IMPROVE_THE_DDF_TOOLTIP: "Accéder au formulaire de contribution pour ajouter des informations."
  },
  MOBILE_MENU: {
    MENTIONS_LEGALES: "Mentions légales",
    LIEN_STORE: 'Application pour mobile et tablette',
    HELP: 'Aide et documentation',
    EXPLORE: 'Explorer le dictionnaire',
    MY_ACCOUNT: 'Mon compte',
    MY_ACCOUNT_CONTRIBUTIONS: 'Mes dernières contributions',
    ADMIN: {
      USERS: 'Gestion des utilisateurs',
      REPORTING: 'Gestion des signalements'
    },
    OPERATORS: {
      CONTRIBUTIONS: 'Gestion des contributions',
    },
  },
  GEOGRAPHICAL_DETAILS: {
    NO_COUNTRY_LABEL: 'Monde francophone',
    TITLE: 'Précisions géographiques',
    PAGE_CAPTION: "D'après les données collectées, ce sens est en usage notamment dans les régions suivantes : ",
    NO_RESULTS: "Pas d'information géographique pour cette définition",
  },
  SOURCES_PAGE: {
    TITLE: 'Sources',
    WIKTIONARY_LINK: 'Historique des versions de «\u202F{{formQuery}}\u202F»',
  },
  HELP: {
    HEADER: 'Aide et documentation',
    HOW_TO_SEARCH: 'Effectuer une recherche simple',
    SECTION_LINKS: {
      SEARCH_AND_USER_ACCOUNT: 'Recherche\net compte utilisateur',
      UNDERSTAND_ARTICLE: 'Comprendre un article\n de dictionnaire',
      HOW_TO_CONTRIBUTE: 'Contribution \n et enrichissement',
      OFFLINE: 'Faire vivre le DDF\nhors des écrans',
    },
    DESKTOP: {
      BACK_LINK: 'Retour au menu',
    },
  },
  DESKTOP_HEADER: {
    TITLE: 'Dictionnaire\ndes francophones',
    SEARCH_BUTTON: 'Rechercher',
    TOOLTIPS: {
      EXPLORE: "Explorer",
      HELP: 'Aide',
      LOGIN: 'Se connecter',
      MY_ACCOUNT: 'Mon compte',
      ADMIN: "Espace d'administration",
      ADMINREADONLY: "Espace d'administration (En lecture seule)",
    },
  },
  DESKTOP_FOOTER: {
    ABOUT: 'À propos',
    VERSION_HISTORY: 'Historique des versions',
    OUR_PARTNERS: 'En partenariat avec : ',
  },
  GCU_PAGE_TITLE: "Conditions générales d'utilisation",
  PARTNERS_PAGE_TITLE: 'Partenaires',
  CREDITS_PAGE_TITLE: 'Crédits',
  PROJECT_PRESENTATION_PAGE_TITLE: 'Présentation du projet',

  PLAN_DU_SITE_PAGE_TITLE: 'Plan du site',
  ACCESSIBILITE_PAGE_TITLE: 'Accessibilité',
  MENTIONS_LEGALES_PAGE_TITLE: 'Mentions légales',
  POLITIQUE_CONFIDENTIALITE_PAGE_TITLE: 'Politique de confidentialité',

  LOGIN_PAGE: {
    TITLE: "S'identifier",
    IF_YOU_ALREADY_HAVE_AN_ACCOUNT: 'Si vous avez déjà un compte, identifiez vous :',
    EMAIL: 'Adresse de courriel',
    ACCOUNT_CREATED: 'Compte crée avec succès',
    PASSWORD: 'Mot de passe',
    CREATE_ACCOUNT_INFOTEXT: 'Sinon, vous pouvez <1>créer un compte</1>.',
    LOST_CREDENTIALS_INFOTEXT: 'Vous ne retrouvez plus votre mot de passe ?',
    LOST_CREDENTIALS_LINK_TEXT: "Procédure de récupération de compte à partir d'une adresse courriel",
    SUBMIT: 'Valider',
  },
  PASSWORD_FORGOTTEN_PAGE: {
    TITLE: 'Mot de passe oublié',
    PROCEDURE: "Pour réinitialiser le mot de passe, merci de renseigner l'adresse mail utilisée pour vous connecter.",
    EMAIL: 'Adresse de courriel',
    NEXT_STEP: "Une fois ce formulaire validé, vous recevrez un mail à l'adresse indiquée contenant un lien de réinitialisation de votre mot de passe.",
    SUBMIT: 'Valider',
    MAIL_SENT_SUCCESS: 'Votre demande a bien été prise en compte, vérifiez votre messagerie pour réinitialiser votre mot de passe.',
  },
  SUBSCRIBE_PAGE: {
    TITLE: "S'inscrire",
    SECOND_LEVEL_TITLE: "Créez un compte et contribuez à l'enrichissement du Dictionnaire des francophones.",
    TOP_INFOBOX_CONTENT: "Si vous souhaitez conserver votre anonymat, préférez l'usage d'un pseudonyme.",
    BOTTOM_INFOBOX_CONTENT:
      `Remplir le champ « lieu de vie » vous permettra de pré-remplir cette information lors de vos prochaines visites. 
      La géolocalisation proposée sur ce site est imprécise, pour des raisons de confidentialité. Vous ne pouvez sélectionner dans la liste des choix proposés que des communes, villes, arrondissements, régions ou pays. Il est notamment impossible d'indiquer dans ce champ une adresse complète. 
      Vous pourrez modifier ces informations ultérieurement à partir de l'onglet « mon profil ». Cette information ne sera pas accessible au public.`,
    GCU_CHECKBOX_LABEL: "J'accepte les <1>conditions générales d'utilisation</1>.",
    MANDATORY_FIELDS: '* Champs obligatoires',
    SUBMIT: 'Valider',
    CANCEL: 'Annuler',
  },
  MY_ACCOUNT: {
    TITLE: 'Mon compte',
    MY_PROFILE_LINK: 'Mon profil',
    MY_CONTRIBUTIONS_LINK: 'Mes contributions',
    DESKTOP_MENU_TITLE: 'CATÉGORIES',
    PROFILE: {
      INDEX: {
        THIS_IS_YOUR_PROFILE: 'Voici les informations de votre profil :',
        EDIT_PROFILE: 'Compléter mon profil',
        EDIT_SETTINGS: 'Modifier mes préférences',
      },
      EDIT: {
        TITLE: 'Modifier mon profil',
        YOU_CAN_EDIT_THESE_FIELDS:
          'Si vous le souhaitez, vous pouvez ajouter les informations suivantes sur votre profil. Ces informations ne sont pas ' +
          "accessibles par le public mais permettront d'améliorer les travaux scientifiques ou de recherche. Elles permettront par exemple de mener des analyses " +
          'ou des études sociolinguistiques sur les contributions du Dictionnaire des francophones. Ces données pourront être communiquées à des partenaires tiers ' +
          'à des fins exclusivement scientifiques et/ou de recherche ce qui nécessite le recueil de votre consentement au préalable.\nAvertissement : nous invitons ' +
          "les utilisateurs à ne pas communiquer de données personnelles ni d'opinions politiques ou religieuses dans les champs proposés ci-dessous.",
        SUBMIT: 'Valider',
        PLACEHOLDERS: {
          PLACE: 'Lieu de vie',
          GENDER: 'Genre (masculin, féminin, autre)',
          BIRTH_YEAR: 'Année de naissance',
          LINGUISTIC_PROFILE: 'Profil linguistique',
          SPOKEN_LANGUAGES: 'Autres langues parlées',
          OTHER_INFORMATION: "Champ d'expertise",
        },
        SUCCESS: 'Profil mis à jour avec succès',
      },
      SETTINGS: {
        TITLE: 'Modifier mes préférences',
        CURRENT: 'Actuellement : ',
        MODIFY: 'Modifier',
        SUBMIT: 'Valider',
        CANCEL: 'Annuler',
        PLACE: {
          TITLE: 'Lieu',
          HINT: "Vous pouvez modifier ici la zone d'usage à privilégier dans les recherches.",
          EMPTY: '(aucun lieu)',
        },
        PREFERRED_DOMAIN: {
          TITLE: 'Domaine du vocabulaire',
          HINT: "Vous pouvez spécifier un domaine de préférences pour vos recherches, tels que biologie, linguistique, jeux vidéo, etc. Cette précision modifiera l'ordre d'affichage des résultats de recherche afin d'afficher en premier le domaine spécifié.",
          EMPTY: '(aucune préférence)',
        },
        PASSWORD_EDIT: {
          TITLE: 'Modifier mon mot de passe',
          TOP_CAPTION: 'Vous souhaitez modifier le mot de passe de votre compte :',
          OLD_PASSWORD_CAPTION: "Saisissez l'ancien mot de passe",
          OLD_PASSWORD_PLACEHOLDER: 'Mot de passe',
          NEW_PASSWORD_CAPTION: 'Nouveau mot de passe',
          NEW_PASSWORD_PLACEHOLDER: 'Mot de passe',
          PASSWORD_CONFIRMATION_CAPTION: 'Confirmation',
          PASSWORD_CONFIRMATION_PLACEHOLDER: 'Mot de passe',
          CANCEL: 'Annuler',
          SUBMIT: 'Valider',
          SUCCESS: 'Mot de passe mis à jour',
        },
        ACCOUNT_DELETION: {
          TITLE: 'Supprimer mon compte',
          TOP_CAPTION: "Attention cette action est définitive. La suppression de votre compte n'effacera pas les apports et discussions passées.",
          CANCEL: 'Annuler',
          SUBMIT: 'Valider',
          SUCCESS: 'Le compte a été supprimé',
          ARE_YOU_SURE: 'Vous êtes sur le point de supprimer votre compte, êtes vous sûr ?',
        },
      },
    },
    CONTRIBUTIONS: {
      TITLE: 'Mes contributions',
    },
  },
  FORM_ERRORS: {
    FIELD_ERRORS: {
      REQUIRED: 'Champ obligatoire',
      INVALID_EMAIL: "Format de l'adresse de courriel invalide",
      PASSWORD_TOO_SHORT: 'Mot de passe trop court',
      EMAIL_ALREADY_REGISTERED: 'Cette adresse de courriel est déjà utilisée',
      PASSWORDS_DO_NOT_MATCH: 'La confirmation ne correspond pas au nouveau mot de passe',
      WRONG_OLD_PASSWORD: "L'ancien mot de passe est incorrect",
      GCU_MUST_BE_ACCEPTED: 'Vous devez accepter les conditions générales',
    },
    GENERAL_ERRORS: {
      FORM_VALIDATION_ERROR: 'Certains champs sont invalides',
      UNEXPECTED_ERROR: "Un problème non identifié nous empêche d'effectuer cette action. Veuillez réessayer plus tard.",
      INVALID_CREDENTIALS: 'Ces identifiants ne sont pas valides',
      USER_MUST_BE_AUTHENTICATED: 'Vous devez être connecté avec un compte utilisateur pour effectuer cette action',
      USER_NOT_ALLOWED: "Vous n'avez pas la permission d'effectuer cette action",
      READ_ONLY_MODE_ENABLED: "L'écriture des données a été désactivée pour nous permettre des travaux de maintenance de l'application. Veuillez réessayer plus tard."
    },
  },
  CONFIRMATION_MODAL: {
    YES: 'Oui',
    NO: 'Non',
  },
  ADMIN: {
    USERS: {
      ACTIONS: {
        UNREGISTER_ACCOUNT: 'Supprimer',
        DISABLE_ACCOUNT: 'Bloquer',
        ENABLE_ACCOUNT: 'Débloquer',
      },
      VALIDATION: {
        UNREGISTER_ACCOUNT: 'Vous êtes sur le point de supprimer un compte, êtes vous sûr ?',
      },
      IS_UNREGISTERED: 'Ce compte a été supprimé',
    },
    INDEX: {
      TITLE: 'Administration',
    },
  },
  CONTRIBUTION: {
    CREATE_LEXICAL_SENSE: {
      FORM_PLACEHOLDER: 'Mot ou expression',
      TITLE: "J'enrichis le DDF",
      POS_PLACEHOLDER: 'Catégorie grammaticale',
      DEFINITION_PLACEHOLDER: 'Définition',
      CANCEL: 'Annuler',
      NEXT: 'Continuer',
      SUBMIT: 'Publier',
    },
    EDIT_LEXICAL_SENSE_TITLE: 'Modifier une définition',
    ENRICH_LEXICAL_SENSE_TITLE: 'Enrichir une définition',
    ADD_SEMANTIC_RELATION: "J'ajoute un lien avec un autre mot",
    DESKTOP_TITLE: 'Enrichir le Dictionnaire des francophones',
  },

  LEXICAL_SENSE_HISTORY: {
    TABLE: {
      NO_RESULTS: 'Aucun historique',
      ERROR_RESULTS: "Un problème inattendu nous empêche d'afficher les résultats"
    },
    TYPENAME: {
      CREATION: "Création",
      UPDATE: "Mise à jour"
    }
  },
  CONTRIBUTIONS_LIST: {
    FILTERS: {
      ADD_FILTERS_BUTTON: 'Ajouter des filtres',
      EDIT_FILTERS_BUTTON: 'Modifier les filtres',
      SAVE: 'Enregistrer',
      CANCEL: 'Annuler',
      RADIO_DISABLED_OPTION_LABEL: 'Ne pas filtrer',
    },
    TABLE: {
      HEADERS: {
        DATE: 'Date',
        FORM: 'Forme',
        EDITED: 'Contenu modifié',
        AUTHOR: 'Auteur',
        VALID_STATUS_TOOLTIP: 'Définition validée ou non',
        SIGNALED_TOOLTIP: 'Définition signalée ou non',
        REREADED: "Relecture",
        ACTIONS: 'Actions',
      },
      NO_RESULTS: 'Aucun résultat',
      ERROR_RESULTS: "Un problème inattendu nous empêche d'afficher les résultats",
      SEE_NEXT: 'Voir les contributions suivantes',
    },
  },
  DOCUMENT_TITLES: {
    DEFAULT: 'Dictionnaire des francophones',
    GENERIC: '{{title}} - Dictionnaire des francophones',
    FORM_SEARCH: 'Recherche «\u202F{{formQuery}}\u202F» - Dictionnaire des francophones',
    LEXICAL_SENSE: 'Définition «\u202F{{formQuery}}\u202F» - Dictionnaire des francophones',
    CREATE_LEXICAL_SENSE: 'Ajouter une définition - Dictionnaire des francophones',
    EDIT_LEXICAL_SENSE: 'Modifier la définition «\u202F{{formWrittenRep}}\u202F» - Dictionnaire des francophones',
    HELP: 'Aide - Dictionnaire des francophones',
    SUBSCRIBE: 'Inscription - Dictionnaire des francophones',
    LOGIN: 'Connexion - Dictionnaire des francophones',
    MY_ACCOUNT: 'Mon compte - Dictionnaire des francophones',
    ADMIN_INDEX: 'Admin - Dictionnaire des francophones',
    ADMIN_CONTRIBUTIONS: 'Contributions - Admin - Dictionnaire des francophones',
    ADMIN_USERS: 'Utilisateurs - Admin - Dictionnaire des francophones',
    ADMIN_REPORTING: 'Signalements - Admin - Dictionnaire des francophones',
    ADMIN_DUMP: "Admin - dump des données",
    ADMIN_UPLOAD_CSV: "Admin - Mise à jour des dictionnaires",
    ADMIN_MONITOR_DATA_INGEST: "Admin - Monitoring de l'ingestion des données"
  },
  AUTOCOMPLETE: {
    NO_RESULTS: 'Pas de résultat',
  },
  JOB: {
    NAME: "Tâche",
    STATUS: {
      TITLE: "Statut",
      DONE: "Terminée",
      SHUTDOWN: "Terminée",
      UPDATED: "En cours...",
      STARTED: "En cours...",
      ERROR: "Erreur"
    },
    MESSAGE: "Message",
    STARTED_AT: "Date de début",
    UPDATED_AT: "Date de mise à jour",
    ENDED_AT: "Date de fin",
    ACTIONS: {
      CREATE: {
        TITLE:  "Créer une tâche",
        SUCCESS: "Tâche créée avec succès !",
        ERROR: "Tâche non crée, une erreur a eu lieu. {{message}}"
      },
      TERMINATE: {
        TITLE:  "Annuler une tâche",
        SUCCESS: "Demande d'annulation lancée avec succès !"
      },
      SELECT: "Sélectionner dans la liste ci-dessous la tâche à démarrer",
    }
  },
  UPLOAD_CSV: {
    TITLE: "Mise à jour des dictionnaires",
    FORM: {
      SOURCE: "Choix du dictionnaire"
    },
    FILE_UPLOAD: {
      SUCCESS: "Fichier prêt à être importé",
      ERROR: "Erreur au téléversement.",
      UPLOADING: "Téléversement du fichier"
    },
    JOB_SOURCE_NAME: "Mise à jour du dictionnaire \"{{source}}\""
  },
  MONITOR_DATA_INGEST: {
    TITLE: "Monitoring de l'ingestion de données",
    COLUMNS: {
      CATEGORY: {
        TITLE: "Catégorie",
        IMPORT_AUDIO: "Mise à jour des fichiers audios",
        IMPORT_SOURCE: "Mise à jour de dictionnaire",
        IMPORT_WKT: "Mise à jour du Wiktionnaire"
      }
    }
  }
};

export default fr;