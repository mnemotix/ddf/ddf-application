/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
export default {
  UUID: {
    description: 'This is the application UUID.',
    defaultValue: 'ddf_application',
    defaultValueInProduction: true
  },
  APP_PORT: {
    description: 'This is listening port of the application.',
    defaultValue: 3034
  },
  APP_URL: {
    description: 'This is the base url of the application.',
    defaultValue: () => `http://localhost:${process.env.APP_PORT}`,
    exposeInGraphQL: true,
  },
  SCHEMA_NAMESPACE_MAPPING: {
    description: 'The DDF ontology schema namespace mapping',
    defaultValue: JSON.stringify({
      "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
      "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
      "foaf": "http://xmlns.com/foaf/0.1/",
      "prov": "http://www.w3.org/ns/prov#",
      "sioc": "http://rdfs.org/sioc/ns#",
      "dc": "http://purl.org/dc/terms/",
      "dct": "http://purl.org/dc/terms/",
      "mnx": "http://ns.mnemotix.com/ontologies/2019/8/generic-model/",
      "skos": "http://www.w3.org/2004/02/skos/core#",
      "schema": "https://schema.org/",
      "ddf": "http://data.dictionnairedesfrancophones.org/ontology/ddf#",
      "lexicog": "http://www.w3.org/ns/lemon/lexicog#",
      "ontolex": "http://www.w3.org/ns/lemon/ontolex#",
      "lexinfo": "http://www.lexinfo.net/ontology/2.0/lexinfo#",
      "geonames": "https://www.geonames.org/",
      "inv": "http://data.dictionnairedesfrancophones.org/dict/inv/",
      "wkt": "http://data.dictionnairedesfrancophones.org/dict/wikt/",
      "belgicismes": "http://data.dictionnairedesfrancophones.org/dict/belgicismes/",
      "dict": "http://data.dictionnairedesfrancophones.org/dict/",
      "ddfa": "http://data.dictionnairedesfrancophones.org/authority/",
    }),
    defaultValueInProduction: true
  },
  NAMED_GRAPHS_MAPPING: {
    description: 'The mapping of named graphs',
    defaultValue: JSON.stringify({
      "ddf:Person": "http://data.dictionnairedesfrancophones.org/users/graph",
      "mnx:UserAccount": "http://data.dictionnairedesfrancophones.org/users/graph",
      "mnx:EmailAccount": "http://data.dictionnairedesfrancophones.org/users/graph",
    }),
    defaultValueInProduction: true
  },
  AUTHORITY_NAMESPACE_URI: {
    description: 'The DDF authorities (vocabularies) namespace URI',
    defaultValue: 'http://data.dictionnairedesfrancophones.org/authority/',
    defaultValueInProduction: true
  },
  NODES_NAMESPACE_URI: {
    description: 'The DDF nodes (or individuals) (or class instances) namespace URI',
    defaultValue: 'http://data.dictionnairedesfrancophones.org/dict/contrib/',
    defaultValueInProduction: true
  },
  NODES_PREFIX: {
    description: 'The DDF nodes or individuals) (or class instances) namespace URI',
    defaultValue: 'contrib',
    defaultValueInProduction: true
  },
  NODES_NAMED_GRAPH: {
    description: 'The DDF default named graph to insert triples',
    defaultValue: 'http://data.dictionnairedesfrancophones.org/dict/contrib/graph',
    defaultValueInProduction: true
  },
  PUBLIC_NAMED_GRAPHS: {
    description: 'The list of public Named Graphs used to protect public SPARQL queries ',
    defaultValue: JSON.stringify([
      "<http://data.dictionnairedesfrancophones.org/dict/ontologies/graph>",
      "<http://data.dictionnairedesfrancophones.org/dict/voc/graph>",
      "<http://data.dictionnairedesfrancophones.org/dict/contrib/graph>",
      "<http://data.dictionnairedesfrancophones.org/dict/wikt/graph/1583708400000>",
      "<http://data.dictionnairedesfrancophones.org/dict/wikt/graph/1608678000000>",
      "<http://data.dictionnairedesfrancophones.org/dict/wikt/graph/1609887600000>",
      "<http://data.dictionnairedesfrancophones.org/dict/wikt/graph/1609974000000>",
      "<http://data.dictionnairedesfrancophones.org/dict/wikt/graph/1634767200000>",
      "<http://data.dictionnairedesfrancophones.org/dict/asom/graph>",
      "<http://data.dictionnairedesfrancophones.org/dict/inv/graph>",
      "<http://data.dictionnairedesfrancophones.org/ontology/ddf#GroupsDataNG>"      
    ]),
    defaultValueInProduction: true
  },
  ADMIN_USER_GROUP_ID: {
    description: 'The DDF UserGroup Administrator Id (or URI)',
    defaultValue: () => `${process.env.NODES_NAMESPACE_URI}user-group/AdministratorGroup`,
    defaultValueInProduction: true
  },
  CONTRIBUTOR_USER_GROUP_ID: {
    description: 'The DDF UserGroup contributor Id (or URI)',
    defaultValue: () => `${process.env.NODES_NAMESPACE_URI}user-group/ContributorGroup`,
    defaultValueInProduction: true
  },
  OPERATOR_USER_GROUP_ID: {
    description: 'The DDF UserGroup operator Id (or URI)',
    defaultValue: () => `${process.env.NODES_NAMESPACE_URI}user-group/OperatorGroup`,
    defaultValueInProduction: true
  },
  DDF_LEXICOGRAPHIC_RESOURCE_ID: {
    description: 'The DDF lexicog:LexicographicResource Id (or URI) contributions are attached to.',
    defaultValue: () => `http://data.dictionnairedesfrancophones.org/resource/ddf`,
    defaultValueInProduction: true
  },
  OAUTH_DISABLED: {
    description: 'This is the OAUTH authentication URL',
    defaultValue: '0',
    defaultValueInProduction: true
  },
  OAUTH_BASE_URL: {
    description: 'This is the OAUTH server host',
    defaultValue: 'http://localhost:8181'
  },
  OAUTH_REALM: {
    description: "This is the OAUTH realm used for this app",
    defaultValue: 'ddf',
  },
  OAUTH_AUTH_URL: {
    description: 'This is the OAUTH authentication URL',
    defaultValue: () => `${process.env.OAUTH_BASE_URL}/auth/realms/${process.env.OAUTH_REALM}/protocol/openid-connect/auth`,
    defaultValueInProduction: true
  },
  OAUTH_TOKEN_URL: {
    description: 'This is the OAUTH token validation URL',
    defaultValue: () => `${process.env.OAUTH_BASE_URL}/auth/realms/${process.env.OAUTH_REALM}/protocol/openid-connect/token`,
    defaultValueInProduction: true
  },
  OAUTH_LOGOUT_URL: {
    description: 'This is the OAUTH logout URL',
    defaultValue: () => `${process.env.OAUTH_BASE_URL}/auth/realms/${process.env.OAUTH_REALM}/protocol/openid-connect/logout`,
    defaultValueInProduction: true,
  },
  OAUTH_REALM_CLIENT_ID: {
    description: 'This is the OAUTH Realm client ',
    defaultValue: 'api'
  },
  OAUTH_REALM_CLIENT_SECRET: {
    description: 'This is the OAUTH shared secret between this client app and OAuth2 server.',
    defaultValue: 'e9a7be6b-5850-4d22-8740-a8d535e2880b',
    obfuscate: true
  },
  OAUTH_ADMIN_USERNAME: {
    description: 'This is the OAUTH admin username',
    defaultValue: 'admin'
  },
  OAUTH_ADMIN_PASSWORD: {
    description: 'This is the OAUTH admin password',
    defaultValue: 'ddfpwd!',
    obfuscate: true
  },
  OAUTH_ADMIN_TOKEN_URL: {
    description: 'This is the OAUTH token validation URL for admin session',
    defaultValue: () => `${process.env.OAUTH_BASE_URL}/auth/realms/master/protocol/openid-connect/token`,
    defaultValueInProduction: true
  },
  OAUTH_ADMIN_API_URL: {
    description: 'This is the OAUTH API admin session',
    defaultValue: () => `${process.env.OAUTH_BASE_URL}/auth/admin/realms/${process.env.OAUTH_REALM}`,
    defaultValueInProduction: true
  },
  OAUTH_CERTS_URL: {
    description: "This is the OAUTH token certs URL",
    defaultValue: () =>
      `${process.env.OAUTH_BASE_URL}/auth/realms/${process.env.OAUTH_REALM}/protocol/openid-connect/certs`,
    defaultValueInProduction: true,
  },
  RABBITMQ_DISABLED: {
    description: "Is RabbitMQ disabled ?",
    defaultValue: "0",
    defaultValueInProduction: true,
  },
  RABBITMQ_HOST: {
    description: 'This is RabbitMQ host.',
    defaultValue: 'localhost'
  },
  RABBITMQ_PORT: {
    description: 'This is RabbitMQ port.',
    defaultValue: 5672
  },
  RABBITMQ_LOGIN: {
    description: 'This is RabbitMQ login.',
    defaultValue: 'guest'
  },
  RABBITMQ_PASSWORD: {
    description: 'This is RabbitMQ password.',
    defaultValue: 'ddfpwd!',
    obfuscate: true
  },
  RABBITMQ_EXCHANGE_NAME: {
    description: 'This is RabbitMQ exchange name.',
    defaultValue: 'local-ddf'
  },
  RABBITMQ_EXCHANGE_DURABLE: {
    description: 'Is RabbitMQ exchange durable.',
    defaultValue: "0",
    defaultValueInProduction: true
  },
  RABBITMQ_RPC_TIMEOUT: {
    description: 'RPC timeout',
    defaultValue: 10e3,
    defaultValueInProduction: true
  },
  INDEX_DISABLED: {
    description: "Is index disabled",
    defaultValue: "0",
    defaultValueInProduction: true,
  },
  INDEX_MASTER_URI: {
    description: "Index master URI (ES endpoint)",
  },
  INDEX_CLUSTER_USER: {
    description: " Index cluster user",
    defaultValue: () => "",
    defaultValueInProduction: true,
  },
  INDEX_CLUSTER_PWD: {
    description: "Index cluster password",
    obfuscate: true,
    defaultValue: () => "",
    defaultValueInProduction: true,
  },
  INDEX_PREFIX_TYPES_WITH: {
    description: "Prefix all index types with a prefix",
    defaultValue: 'local-ddf-',
  },
  RDFSTORE_ROOT_URI: {
    description: "RDF Graph root URI",
  },
  RDFSTORE_USER: {
    description: "RDF Graph Store user",
  },
  RDFSTORE_PWD: {
    description: "RDF Graph Store password",
    obfuscate: true,
  },
  RDFSTORE_REPOSITORY_NAME: {
    description: "RDF Graph Store default repository name",
  },
  RDFSTORE_VERSION: {
    description: 'GraphDB <10 use entityFilter as filter function name, GraphDB >10 use documentFilter',
    defaultValue: 8,
    exposeInGraphQL: true ,
    defaultValueInProduction: true
  },
  LOG_LEVEL: {
    description: 'RabbitMQ log level (ERROR|DEBUG|TRACE)',
    defaultValue: "NONE",
    defaultValueInProduction: true
  },
  GEONAMES_USERNAME: {
    description: 'Geonames usename to call geonames API',
    defaultValue: 'dev.mnemotix',
  },
  READ_ONLY_MODE_ENABLED: {
    description: 'Is the Stack read only ?',
    defaultValue: '0',
    exposeInGraphQL: true,
    defaultValueInProduction: true
  },
  DUMP_FILES_PATH: {
    description: 'Location of dump files',
    defaultValue: '~/data/ddf/local/data/exports/',
    exposeInGraphQL: true    
  },
  UPLOAD_CSV_FILES_PATH: {
    description: 'Location of uploaded CSV files',
    defaultValue: '/tmp'
  },
  PAGINATION_SIZE: {
    description: 'Size of the pagination for DumpFiles',
    defaultValue: 10,
    exposeInGraphQL: true  ,
    defaultValueInProduction: true
  },
  UNDER_MAINTENANCE: {
    description: 'Display under maintenance page',
    defaultValue: '0',
    exposeInGraphQL: true ,
    defaultValueInProduction: false
  },

}
