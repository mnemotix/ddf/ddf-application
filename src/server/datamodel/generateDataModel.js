/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {DataModel, mergeRules, MnxDatamodels} from "@mnemotix/synaptix.js";
import {generateAclMiddlewares, generateAclRules} from "./acl/aclMiddlewares";
import {ddfDataModel} from "./ontologies/ddf";
import {ddfContributionDataModel} from "./ontologies/ddf-contribution";
import {lexicogDataModel} from "./ontologies/lexicog";
import {ontolexDataModel} from "./ontologies/ontolex";
import {geonamesDataModel} from "./ontologies/geonames";
import {PersonDefinition} from "./ontologies/ddf-contribution/PersonDefinition";
import {backofficeDataModel} from "./ontologies/backoffice";

/**
 * This is an instance of the datamodel.
 * @see https://gitlab.com/mnemotix/synaptix.js#datamodel
 */
export function generateDataModel({ environmentDefinition = {} } = {}) {
  const dataModel = new DataModel()
    .mergeWithDataModels([
      MnxDatamodels.mnxCoreDataModel,
      MnxDatamodels.mnxAgentDataModel,
      ddfDataModel,
      ddfContributionDataModel,
      lexicogDataModel,
      ontolexDataModel,
      geonamesDataModel,
      backofficeDataModel
    ]);

  const aclMiddlewares = generateAclMiddlewares(dataModel);
  const aclRules       = generateAclRules();

  return dataModel
    .addDefaultSchemaTypeDefs()
    .addSSOMutations()
    .addMiddlewares([aclMiddlewares])
    .addShieldRules(mergeRules(aclRules))
    .setModelDefinitionForLoggedUserPerson(PersonDefinition)
    .exposeEnvironmentDefinition({ environmentDefinition });
}
