/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {
  ModelDefinitionAbstract,
  LiteralDefinition,
  GraphQLTypeDefinition
} from "@mnemotix/synaptix.js";

import {ImportJobGraphQLDefinition} from "./ImportJobGraphQLDefinition";

export default class JobDefinition extends  ModelDefinitionAbstract{
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Job";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "synaptixjob";
  }

  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "name",
        rdfDataProperty: "rdfs:label",
        isSearchable: true
      }),
      new LiteralDefinition({
        literalName: "category",
        rdfDataProperty: "rdfs:category",
      }),
      new LiteralDefinition({
        literalName: "startedAt",
        pathInIndex: "started_at",
        rdfDataProperty: "mnx:startedAt",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#date",
      }),
      new LiteralDefinition({
        literalName: "updatedAt",
        pathInIndex: "updated_at",
        rdfDataProperty: "mnx:updatedAt",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#date",
      }),
      new LiteralDefinition({
        literalName: "endedAt",
        pathInIndex: "ended_at",
        rdfDataProperty: "mnx:endedAt",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#date",
      }),
      new LiteralDefinition({
        literalName: "status",
        pathInIndex: "status",
        rdfDataProperty: "mnx:status",
      }),
      new LiteralDefinition({
        literalName: "message",
        pathInIndex: "message",
        rdfDataProperty: "mnx:message",
      }),
      new LiteralDefinition({
        literalName: "processedIterations",
        pathInIndex: "processed",
        rdfDataProperty: "mnx:processedIterations",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#integer",
      }),
      new LiteralDefinition({
        literalName: "totalIterations",
        pathInIndex: "total",
        rdfDataProperty: "mnx:totalIterations",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#integer",
      }),
    ]
  }
}
