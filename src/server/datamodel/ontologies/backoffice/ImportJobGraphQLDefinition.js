import { GraphQLTypeDefinition } from "@mnemotix/synaptix.js";
import {ImportJobGraphQLCreateMutation} from "./ImportJobGraphQLCreateMutation";
import {JobGraphQLUpdateMutation} from "./JobGraphQLUpdateMutation";

export class ImportJobGraphQLDefinition extends GraphQLTypeDefinition {
  static getCreateMutation() {
    return new ImportJobGraphQLCreateMutation();
  }

  /**
   * @inheritdoc
   */
  static getUpdateMutation() {
    return new JobGraphQLUpdateMutation();
  }
}
