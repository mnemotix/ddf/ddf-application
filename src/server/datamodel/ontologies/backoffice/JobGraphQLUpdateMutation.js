import env from "env-var";
import { GraphQLUpdateMutation, generateUpdateMutationResolver } from "@mnemotix/synaptix.js";
import JobDefinition from "./JobDefinition";

export class JobGraphQLUpdateMutation extends GraphQLUpdateMutation {
  /**
   * @inheritDoc
   */
  generateResolver(modelDefinition) {
    return generateUpdateMutationResolver({
      modelDefinition,
      resolver: this.updateJob.bind(this)
    });
  }

  /**
   * @param _
   * @param {object} input
   * @param {SynaptixDatastoreSession} synaptixSession
   */
  async updateJob(_, { input : {objectId, objectInput: {name} = {}} = {}}, synaptixSession){
    try {
      await synaptixSession
        .getIndexClient()
        .getClient()
        .update({
          id: objectId,
          index: `${env.get("INDEX_PREFIX_TYPES_WITH").asString()}${JobDefinition.getIndexType()}`,
          body: {
            doc: {
              ended_at: new Date(),
              message: "Manually terminated",
              status: "error"
            }
          }
        });

      return {
        success: true
      };
    } catch (e) {
      console.log(e);
      return {
        success: false
      };
    }
  }
}