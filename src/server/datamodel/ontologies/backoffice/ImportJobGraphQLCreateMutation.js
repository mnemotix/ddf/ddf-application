import { GraphQLCreateMutation, generateCreateMutationResolver} from "@mnemotix/synaptix.js";

export class ImportJobGraphQLCreateMutation extends GraphQLCreateMutation {
  /**
   * @inheritDoc
   */
  generateResolver(modelDefinition) {
    return generateCreateMutationResolver({
      modelDefinition,
      resolver: this.createJob.bind(this)
    });
  }

  /**
   * @param _
   * @param {object} input
   * @param {SynaptixDatastoreSession} synaptixSession
   */
  async createJob(_, { input : {objectInput: {dicoName, fileId} = {}} = {}}, synaptixSession){
    try {
      const resp = await synaptixSession
        .getNetworkLayer()
        .request("job.confstart", {
          headers: {
            command: "job.confstart",
            sender: process.env.UUID,
            timestamp: Date.now(),
          },
          body: {
            id : dicoName,
            qname: "org.ddf.annotator.jobs.DDFMapperJob",
            category: "ImportJob",
            args:{
              dicoName: {
                label: "dicoName",
                value: dicoName.toLowerCase()
              },
              filePath : {
                label: "filePath",
                value: `/data/upload/${fileId}`
              }
            }
          }
        });

      return {
        success: true
      };
    } catch (e) {
      console.log(e);
      return {
        success: false
      };
    }
  }
}