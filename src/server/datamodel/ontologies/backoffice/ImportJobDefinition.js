/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {
  ModelDefinitionAbstract,
  LiteralDefinition
} from "@mnemotix/synaptix.js";

import {ImportJobGraphQLDefinition} from "./ImportJobGraphQLDefinition";
import JobDefinition from "./JobDefinition";

export default class ImportJobDefinition extends  ModelDefinitionAbstract{

  static getParentDefinitions(){
    return [JobDefinition];
  }

  static getGraphQLDefinition() {
    return ImportJobGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:ImportJob";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return super.getIndexType();
  }

  static getIndexFilters(){
    // return [{
    //   "term":  {
    //     [this.getLink("category").getPathInIndex()]: "ImportJob"
    //   }
    // }]
  }

  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "fileId",
        pathInIndex: "fileId",
        rdfDataProperty: "mnx:fileId",
      }),
      new LiteralDefinition({
        literalName: "dicoName",
        rdfDataProperty: "mnx:dicoName",
      }),
    ]
  }
}
