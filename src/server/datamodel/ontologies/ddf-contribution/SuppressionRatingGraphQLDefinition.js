import {
  GraphQLTypeDefinition
} from "@mnemotix/synaptix.js";
import {SuppressionRatingDefinition} from "./SuppressionRatingDefinition";
import {LexicalSenseDefinition} from "../ontolex/LexicalSenseDefinition";
import {generateAggregationRatingCreateMutation} from "./helpers/generateAggregationRatingCreateMutation";

export class SuppressionRatingGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * @inheritDoc
   */
  static getCreateMutation() {
    return generateAggregationRatingCreateMutation({
      AggregationRatingDefinition: SuppressionRatingDefinition,
      lexicalSenseLinkDefinition: LexicalSenseDefinition.getLink("hasSuppressionRating")
    })
  }
}

