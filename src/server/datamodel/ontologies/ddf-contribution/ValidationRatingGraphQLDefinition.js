import {
  GraphQLTypeDefinition
} from "@mnemotix/synaptix.js";
import {ValidationRatingDefinition} from "./ValidationRatingDefinition";
import {LexicalSenseDefinition} from "../ontolex/LexicalSenseDefinition";
import {generateAggregationRatingCreateMutation} from "./helpers/generateAggregationRatingCreateMutation";

export class ValidationRatingGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * @inheritDoc
   */
  static getCreateMutation() {
    return generateAggregationRatingCreateMutation({
      AggregationRatingDefinition: ValidationRatingDefinition,
      lexicalSenseLinkDefinition: LexicalSenseDefinition.getLink("hasValidationRating")
    })
  }
}

