/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  ModelDefinitionAbstract,
  MnxOntologies,
  LinkDefinition,
  GraphQLInterfaceDefinition, EntityDefinition,
  LabelDefinition, LinkPath
} from "@mnemotix/synaptix.js";


import {LexicalSenseDefinition} from "../ontolex/LexicalSenseDefinition";


export class AggregateRatingDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [MnxOntologies.mnxCommon.ModelDefinitions.EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLInterfaceDefinition;
  }

  /**
   * @inheritDoc
   */
  static isInstantiable() {
    return false;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "schema:AggregateRating";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "aggregate-rating";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasRater',
        pathInIndex: "rater",
        rdfObjectProperty: 'ddf:rater',
        relatedModelDefinition: MnxOntologies.mnxAgent.ModelDefinitions.UserAccountDefinition,
        graphQLPropertyName: "rater",
        relatedInputName: "userAccountRaterInput"
      }),
      new LinkDefinition({
        linkName: 'hasEntity',
        pathInIndex: "entity",
        rdfObjectProperty: 'schema:itemReviewed',
        relatedModelDefinition: EntityDefinition,
        graphQLPropertyName: "entity",
        relatedInputName: "entityInput"
      })
    ];
  }

  

  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "reviewedLexicalSenseFormWrittenRepID",
        inIndexOnly: true,
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("hasEntity")})
          .property({propertyDefinition: LexicalSenseDefinition.getLabel("canonicalFormWrittenRep")})
      }),
      new LabelDefinition({
        labelName: "reviewedLexicalSenseFormWrittenRep",
        inIndexOnly: true,
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("hasEntity")})
          .append({
            linkPath: LexicalSenseDefinition.getLabel("canonicalFormWrittenRep").getLinkPath()
          })
      })
    ]
  }
}



