import {
  generateConnectionArgs, getObjectResolver,
  GraphQLInterfaceDefinition, PropertyFilter
} from "@mnemotix/synaptix.js";
import {PostDefinition} from "./PostDefinition";
import {LexicalSenseDefinition} from "../ontolex/LexicalSenseDefinition";

export class PostGraphQLDefinition extends GraphQLInterfaceDefinition {
  static getExtraGraphQLCode(){
    return `
extend type Query{
  """ Get posts (aka discussion) for a written form etymology """
  postsAboutEtymologyForAccurateWrittenForm(formQs: String, ${generateConnectionArgs()}): PostConnection
  
  """ Get top-ranked post for a written form style """
  topPostAboutEtymologyForAccurateWrittenForm(formQs: String): Post
  
  """ Get posts (aka discussion) for a written form """
  postsAboutFormForAccurateWrittenForm(formQs: String, ${generateConnectionArgs()}): PostConnection
  
  """ Get top-ranked post for a written form """
  topPostAboutFormForAccurateWrittenForm(formQs: String): Post
}
`
  }

  static getExtraResolvers(){
    return {
      Query: {
        /**
         * @param _
         * @param {string} formQs
         * @param {object} args
         * @param {SynaptixDatastoreSession} synaptixSession
         */
        postsAboutEtymologyForAccurateWrittenForm: async (_, {formQs, ...args}, synaptixSession) => {
          let posts = await getPostsAboutEtymologyForAccurateWrittenForm({synaptixSession, formQs});
          return synaptixSession.wrapObjectsIntoGraphQLConnection(posts, args)
        },
        /**
         * @param _
         * @param {string} formQs
         * @param {object} args
         * @param {SynaptixDatastoreSession} synaptixSession
         */
        topPostAboutEtymologyForAccurateWrittenForm: async (_, {formQs, ...args}, synaptixSession, gqlInfo) => {
          return getPostsAboutEtymologyForAccurateWrittenForm({synaptixSession, formQs, firstOne: true, gqlInfo});
        },
        /**
         * @param _
         * @param {string} formQs
         * @param {object} args
         * @param {SynaptixDatastoreSession} synaptixSession
         * @param {GraphQLResolveInfo} gqlInfo Gql information
         */
        postsAboutFormForAccurateWrittenForm: async (_, {formQs, ...args}, synaptixSession, gqlInfo) => {
          let posts = await getPostsAboutFormForAccurateWrittenForm({synaptixSession, formQs, gqlInfo});
          return synaptixSession.wrapObjectsIntoGraphQLConnection(posts, args)
        },
        /**
         * @param _
         * @param {string} formQs
         * @param {object} args
         * @param {SynaptixDatastoreSession} synaptixSession
         * @param {GraphQLResolveInfo} gqlInfo Gql information
         */
        topPostAboutFormForAccurateWrittenForm: async (_, {formQs, ...args}, synaptixSession, gqlInfo) => {
          return getPostsAboutFormForAccurateWrittenForm({synaptixSession, formQs, firstOne: true, gqlInfo});
        }
      }
    }
  }
}

/**
 * @param {string} formQs
 * @param {boolean} [firstOne]
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {GraphQLResolveInfo} gqlInfo Gql information
 * @return {Model[]}
 */
async function getPostsAboutEtymologyForAccurateWrittenForm({formQs, synaptixSession, firstOne, gqlInfo}) {
  return getObjectResolver(PostDefinition, {}, {
    propertyFilters: [
      new PropertyFilter({
        propertyDefinition: PostDefinition.getLabel("aboutEtymology"),
        value: formQs,
        isStrict: true
      })
    ]
  }, synaptixSession, gqlInfo);
}

/**
 * @param {string} formQs
 * @param {boolean} [firstOne]
 * @param {SynaptixDatastoreSession} synaptixSession
 * @param {GraphQLResolveInfo} gqlInfo Gql information
 * @return {Model[]}
 */
async function getPostsAboutFormForAccurateWrittenForm({formQs, synaptixSession, firstOne, gqlInfo}) {
  return getObjectResolver(PostDefinition, {}, {
    propertyFilters: [
      new PropertyFilter({
        propertyDefinition: PostDefinition.getLabel("aboutForm"),
        value: formQs,
        isStrict: true
      })
    ]
  }, synaptixSession, gqlInfo);
}
