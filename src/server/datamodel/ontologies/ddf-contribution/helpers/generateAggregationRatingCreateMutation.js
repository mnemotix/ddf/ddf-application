/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  createObjectInConnectionResolver,
  GraphQLCreateMutation,
  I18nError,
  LinkPath,
  Model,
  SynaptixDatastoreSession,
  ModelDefinitionAbstract,
  LinkDefinition
} from "@mnemotix/synaptix.js";
import {LexicalSenseDefinition} from "../../ontolex/LexicalSenseDefinition";
import merge from "lodash/merge";
import {AggregateRatingDefinition} from "../AggregateRatingDefinition";

/**
 * @param {typeof ModelDefinitionAbstract}  AggregationRatingDefinition
 * @param {LinkDefinition} lexicalSenseLinkDefinition
 * @return {GraphQLCreateMutation}
 */
export function generateAggregationRatingCreateMutation({
  AggregationRatingDefinition,
  lexicalSenseLinkDefinition
}) {
  return new GraphQLCreateMutation({
    extraInputArgs: `
        """ Lexical sense id """
        lexicalSenseId: ID!
      `,
    description: `
       Create a new rating associated to logged user.
  
       This mutation input is protected and following i18nkey errors can be sent :
    
       - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous
       - **ALREADY_RATED** : If the lexicalSense validation rating is already created for the logged user.
      `,
    /**
     * @param lexicalSenseId
     * @param objectInput
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    resolver: async ({lexicalSenseId, objectInput} = {}, synaptixSession) => {
      return createAggregationRating({
        AggregationRatingDefinition,
        lexicalSenseLinkDefinition,
        lexicalSenseId,
        aggregationRatingInput: objectInput,
        synaptixSession
      })
    }
  })
}


/**
 * @param {typeof ModelDefinitionAbstract}  AggregationRatingDefinition
 * @param {LinkDefinition} lexicalSenseLinkDefinition
 * @param {string} lexicalSenseId
 * @param {object} aggregationRatingInput
 * @param {SynaptixDatastoreSession} synaptixSession
 */
export async function createAggregationRating({
  AggregationRatingDefinition,
  lexicalSenseLinkDefinition,
  lexicalSenseId,
  aggregationRatingInput,
  synaptixSession
}) {
  let userAccount = await synaptixSession.getLoggedUserAccount();

  if (userAccount) {
    if (await synaptixSession.isObjectExistsForLinkPath({
      object: new Model(synaptixSession.extractIdFromGlobalId(lexicalSenseId)),
      modelDefinition: LexicalSenseDefinition,
      linkPath: new LinkPath()
        .step({
          linkDefinition: lexicalSenseLinkDefinition,
        })
        .step({
          linkDefinition: AggregationRatingDefinition.getLink("hasRater"),
          targetId: userAccount.id
        })
    })) {
      throw new I18nError("This lexical sense has already been rated by logged user", "ALREADY_RATED");
    }

    aggregationRatingInput = merge(aggregationRatingInput, {
      [AggregateRatingDefinition.getLink("hasEntity").getGraphQLInputName()]: {
        id: synaptixSession.extractIdFromGlobalId(lexicalSenseId)
      },
      [AggregateRatingDefinition.getLink("hasRater").getGraphQLInputName()]: {
        id: userAccount.id
      }
    });

    return synaptixSession.createObject(
      {
        modelDefinition: AggregationRatingDefinition,
        objectInput: aggregationRatingInput
      }
    )
  }
}