import {
  GraphQLTypeDefinition,
} from "@mnemotix/synaptix.js";
import {PersonDefinition} from "./PersonDefinition";
import {getLinkedGeonamesPlaceResolver} from "../geonames/services/graphql/resolvers/getLinkedGeonamesPlaceResolver";

export class PersonGraphQLDefinition extends GraphQLTypeDefinition {
  static getExtraResolvers(){
    return {
      Person: {
        /**
         * @param _
         * @param {string} formQs
         * @param {object} args
         * @param {SynaptixDatastoreSession} synaptixSession
         */
        place: getLinkedGeonamesPlaceResolver.bind(this, PersonDefinition.getLink("place"))
      }
    }
  }
}