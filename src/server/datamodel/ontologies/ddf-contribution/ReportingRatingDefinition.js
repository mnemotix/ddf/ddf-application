/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  LinkDefinition, MnxOntologies,
  ModelDefinitionAbstract,
  LabelDefinition
} from "@mnemotix/synaptix.js";
import { AggregateRatingDefinition } from "./AggregateRatingDefinition";
import {ReportingRatingGraphQLDefinition} from "./ReportingRatingGraphQLDefinition";

export class ReportingRatingDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AggregateRatingDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return ReportingRatingGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ddf:ReportingRating";
  }

  /**
   * @inheritDoc
   */
  static getIndexType(){
    return AggregateRatingDefinition.getIndexType();
  }

  /**
   * @inheritDoc
   */
  static getIndexFilters(){
    return [{
      "term":  {
        "types": "http://data.dictionnairedesfrancophones.org/ontology/ddf#ReportingRating"
      }
    }]
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    let parentLinkDefinitions = super.getLinks();
    const indexOfHasEntityLink = parentLinkDefinitions.findIndex((link) => link.getLinkName() === "hasEntity");
    parentLinkDefinitions.splice(indexOfHasEntityLink, 1);

    return [
      ...parentLinkDefinitions,
      new LinkDefinition({
        linkName: 'hasEntity',
        pathInIndex: "entity",
        rdfReversedObjectProperty: 'ddf:hasReportingRating',
        relatedModelDefinition: MnxOntologies.mnxCommon.ModelDefinitions.EntityDefinition,
        graphQLPropertyName: "entity",
        graphQLInputName: "entityInput"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'reportingMessage',
        rdfDataProperty: 'ddf:reportingMessage'
      })
    ];
  }
}

