import {
  GraphQLInterfaceDefinition
} from "@mnemotix/synaptix.js";

import {getLexicalEntryContribInputsFromFormWrittenRep} from "./helpers/getLexicalEntryContribInputsFromFormWrittenRep";

export class LexicalEntryGraphQLDefinition extends GraphQLInterfaceDefinition {
  static getExtraGraphQLCode(){
    return `
type FormWrittenRepContribInputDefinitions {
  lexicalEntryTypeName: String
  grammaticalCategorySchemeId: ID
}
  
extend type Query{
  """ 
  Get a (LexicalEntry inherited type name | Grammatical category scheme ID) tuple given a form written representation. The rules are :
  
  If "formWrittenRep" :
   - Contains spaces => it's a MultiWordExpression type
   - Begins/ends with a "-" => it's an Affix type
   - Otherwise => it's a Word
  """
  lexicalEntryContribInputsFromFormWrittenRep(formWrittenRep: String): FormWrittenRepContribInputDefinitions
}
`
  }

  static getExtraResolvers(){
    return {
      Query: {
        /**
         *
         * @param _
         * @param {string} formWrittenRep
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         */
        lexicalEntryContribInputsFromFormWrittenRep: (_, {formWrittenRep}) => getLexicalEntryContribInputsFromFormWrittenRep(formWrittenRep)
      }
    }
  }
}
