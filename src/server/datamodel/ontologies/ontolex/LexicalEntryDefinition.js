/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  EntityDefinition, LabelDefinition,
  LinkDefinition, LinkPath,
  ModelDefinitionAbstract
} from "@mnemotix/synaptix.js";

import { FormDefinition } from "./FormDefinition";
import { LexicalSenseDefinition } from "./LexicalSenseDefinition"
import { PostDefinition } from "../ddf-contribution/PostDefinition";
import { SemanticRelationDefinition } from "../ddf/SemanticRelationDefinition";
import { EntryDefinition } from "../lexicog/EntryDefinition";
import { GrammaticalPropertyDefinition } from "./GrammaticalPropertyDefinition";
import {LexicalEntryGraphQLDefinition} from "./LexicalEntryGraphQLDefinition";
import {SemanticPropertyDefinition} from "./SemanticPropertyDefinition";

export class LexicalEntryDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return LexicalEntryGraphQLDefinition;
  }

  /**
   * @return {boolean}
   */
  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ontolex:LexicalEntry";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
     return 'lexical-entry';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'entry',
        symmetricLinkName: 'lexicalEntry',
        rdfObjectProperty: "ddf:isDescribedBy",
        relatedModelDefinition: EntryDefinition,
        isPlural: false,
        relatedInputName: "entryInput"
      }),
      new LinkDefinition({
        linkName: 'contributions',
        symmetricLinkName: 'lexicalEntry',
        rdfObjectProperty: 'ddf:hasItemAboutEtymology',
        relatedModelDefinition: PostDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'canonicalForm',
        symmetricLinkName: "lexicalEntry",
        pathInIndex: 'canonicalForm',
        rdfObjectProperty: 'ontolex:canonicalForm',
        relatedModelDefinition: FormDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        relatedInputName: "canonicalFormInput"
      }),
      new LinkDefinition({
        linkName: 'lexicalForms',
        pathInIndex: 'lexicalForms',
        rdfObjectProperty: 'ontolex:lexicalForm',
        relatedModelDefinition: FormDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true,
        relatedInputName: "canonicalFormInputs"
      }),
      new LinkDefinition({
        linkName: 'otherForms',
        pathInIndex: 'otherForms',
        rdfObjectProperty: 'ontolex:otherForm',
        relatedModelDefinition: FormDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true,
        relatedInputName: "otherFormInputs"
      }),
      new LinkDefinition({
        linkName: 'senses',
        symmetricLinkName: "lexicalEntry",
        pathInIndex: 'senses',
        rdfObjectProperty: 'ontolex:sense',
        relatedModelDefinition: LexicalSenseDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true,
        relatedInputName: "lexicalSenseInputs"
      }),
      new LinkDefinition({
        linkName: 'semanticRelations',
        symmetricLinkName: 'lexicalEntries',
        rdfObjectProperty: 'ddf:lexicalEntryHasSemanticRelationWith',
        relatedModelDefinition: SemanticRelationDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'grammaticalProperties',
        symmetricLinkName: "lexicalEntries",
        rdfObjectProperty: 'ddf:hasGrammaticalProperty',
        relatedModelDefinition: GrammaticalPropertyDefinition,
        isPlural: true,
      }),
      new LinkDefinition({
        linkName: 'hasGlossary',
        rdfObjectProperty: 'ddf:entryHasGlossary',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        excludeFromIndex: true,
        excludeFromInput: true
      }),
    ]
  }

  static getLabels(){
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "canonicalFormWrittenRep",
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("canonicalForm")})
          .property({
            propertyDefinition: FormDefinition.getLabel("writtenRep")
          })
      })
    ]
  }
}

