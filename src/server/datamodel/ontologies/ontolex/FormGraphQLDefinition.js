
import {
  generateConnectionArgs,
  GraphQLTypeDefinition
} from "@mnemotix/synaptix.js";
import {FormDefinition} from "./FormDefinition";

export class FormGraphQLDefinition extends GraphQLTypeDefinition {
  static getExtraGraphQLCode() {
    return `
 extend type Query{
  """ Search for forms where written form start with a specific word """ 
  formsStartWith(
      after: String
      first: Int
      startWith: String
      ${generateConnectionArgs()}
  ): String
}
`
  }

  static getExtraResolvers() {
    return {
      Query: {
        /**
         *return lexicalSense where writtenRep start with a given letter 
         * @param parent
         * @param {string} startWith        
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         */

        formsStartWith: async (parent, {startWith, ...args}, synaptixSession, info) => {

          const result = await synaptixSession.getIndexService().getNodes({
            modelDefinition: FormDefinition,
            limit: synaptixSession.getLimitFromArgs(args),
            offset: synaptixSession.getOffsetFromArgs(args),
            getExtraQueryParams: () => {

              return {
                "query": {
                  "bool": {
                    "must": [
                      {"bool": {"should": [{"match_phrase_prefix": {"writtenRep": {"query": startWith}}}]}},
                      {"wildcard": {"writtenRep.keyword": {"value": startWith + "*"}}}
                    ]
                  }
                },
                "_source": {"includes": ["id", "writtenRep"]},
                "size": 15,
                "from": 0,
                "sort": [
                  {
                    "writtenRep.keyword": "asc"
                  },
                  "_score"
                ]
              }
            },
            rawResult: true,
          });


          let forms = {
            edges: result?.hits?.map((hit) => {           
              return {
                "node": {
                  id: hit?._id || hit?.id,
                  ...hit?.["_source"]
                }
              }
            })
          }
          return JSON.stringify(forms);
        },
      }
    }
  }
}
