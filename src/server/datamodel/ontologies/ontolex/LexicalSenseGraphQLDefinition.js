import env from "env-var";
import {
  generateConnectionArgs,
  getObjectsResolver,
  GraphQLTypeDefinition,
  I18nError,
  LinkFilter,
  LinkPath,
  MnxOntologies,
  PropertyFilter,
  QueryFilter,
  Sorting,
  GraphQLProperty,
  getObjectsCountResolver,
  GraphQLCreateMutation,
  Model,
  GraphQLUpdateMutation,
  FormValidationError, getLinkedObjectsResolver
} from "@mnemotix/synaptix.js";
import {SemanticRelationDefinition} from "../ddf/SemanticRelationDefinition";
import {LexicalEntryDefinition} from "./LexicalEntryDefinition";
import {LexicalSenseDefinition} from "./LexicalSenseDefinition";

import {AggregateRatingDefinition} from "../ddf-contribution/AggregateRatingDefinition";
import {ReportingRatingDefinition} from "../ddf-contribution/ReportingRatingDefinition";
import {ValidationRatingDefinition} from "../ddf-contribution/ValidationRatingDefinition";
import {SuppressionRatingDefinition} from "../ddf-contribution/SuppressionRatingDefinition";
import {getEntityAggregatingRatingForLoggedUserResolver} from "../ddf-contribution/helpers/getEntityAggregatingRatingForLoggedUserResolver";
import {assertGrammaticalCategoryIdMatchFormWrittenRep} from "./helpers/assertGrammaticalCategoryIdMatchFormWrittenRep";
import {FormDefinition} from "./FormDefinition";
import {getLexicalEntryContribInputsFromFormWrittenRep} from "./helpers/getLexicalEntryContribInputsFromFormWrittenRep";
import {geonamesClient} from "../geonames/services/api/geonamesClient";
import {getLinkedGeonamesPlacesResolver} from "../geonames/services/graphql/resolvers/getLinkedGeonamesPlacesResolver";

/**
 * @TODO 
 * _IDONTKNOW : patch temporaire 
 * 
 * sur certaines interfaces il faut afficher un bouton 'je ne sais pas' 
 * ( qui n'est pas dans tout les vocabulaires controlés) pour que l'user le choissise si besoin. 
 * cet CONST est utilisé dans le front également pour marquer les données à ne pas persister 
 * quand id === _IDONTKNOW
 * 
 */
const _IDONTKNOW = 'idontknow';
const UserAccountDefinition = MnxOntologies.mnxAgent.ModelDefinitions.UserAccountDefinition;

// all this field need to be added to getUpdateMutation.GraphQLUpdateMutation.extraInputArgs
export const allowedIdFields = {
  registerId: "registerInput",
  placeId: "placeInputs",
  domainId: "domainInput",
  temporalityId: "temporalityInput",
  frequencyId: "frequencyInput",
  connotationId: "connotationInput",
  sociolectId: "sociolectInput",
  grammaticalConstraintId: "grammaticalConstraintInput",
  textualGenreId: "textualGenreInput",
  glossaryId: "glossaryInputs",
};
// all this field need to be added to getUpdateMutation.GraphQLUpdateMutation.extraInputArgs
export const allowedToDeleteFields = {
  registerIdToDelete: "registerInputToDelete",
  placeIdsToDelete: "placeInputsToDelete",
  domainIdToDelete: "domainInputToDelete",
  temporalityIdToDelete: "temporalityInputToDelete",
  frequencyIdToDelete: "frequencyInputToDelete",
  connotationIdToDelete: "connotationInputToDelete",
  sociolectIdToDelete: "sociolectInputToDelete",
  grammaticalConstraintIdToDelete: "grammaticalConstraintInputToDelete",
  textualGenreIdToDelete: "textualGenreInputToDelete",
  glossaryIdToDelete: "glossaryInputsToDelete"
};

export class LexicalSenseGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * @return {GraphQLProperty[]}
   */
  static getExtraProperties() {
    return [
      new GraphQLProperty({
        name: "reviewed",
        type: "Boolean",
        description: "Is this lexicalSense reviewed",
        /**
         * @param {Model} lexicalSense
         * @param _
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         */
        typeResolver: async (lexicalSense, _, synaptixSession) => {
          // We assume that a processed entity is reviewed as well.
          if (lexicalSense.processed === true) {
            return true;
          }

          const adminGroupId = env.get("ADMIN_USER_GROUP_ID").required().asString();
          const operatorGroupId = env.get("OPERATOR_USER_GROUP_ID").required().asString();

          let hasAdminRating = await synaptixSession.isObjectExistsForLinkPath({
            object: lexicalSense,
            modelDefinition: LexicalSenseDefinition,
            linkPath: new LinkPath()
              .step({linkDefinition: LexicalSenseDefinition.getLink('hasAggregationRating')})
              .step({linkDefinition: AggregateRatingDefinition.getLink('hasRater')})
              .step({linkDefinition: UserAccountDefinition.getLink('hasUserGroup'), targetId: adminGroupId})
          });

          let hasOperatorRating = await synaptixSession.isObjectExistsForLinkPath({
            object: lexicalSense,
            modelDefinition: LexicalSenseDefinition,
            linkPath: new LinkPath()
              .step({linkDefinition: LexicalSenseDefinition.getLink('hasAggregationRating')})
              .step({linkDefinition: AggregateRatingDefinition.getLink('hasRater')})
              .step({linkDefinition: UserAccountDefinition.getLink('hasUserGroup'), targetId: operatorGroupId})
          });

          return hasAdminRating || hasOperatorRating;
        }
      }),
      new GraphQLProperty({
        name: "validationRating",
        type: "ValidationRating",
        description: "Validation rating instance contextualized by logged user.",
        typeResolver: getEntityAggregatingRatingForLoggedUserResolver.bind(this, ValidationRatingDefinition)
      }),
      new GraphQLProperty({
        name: "suppressionRating",
        type: "SuppressionRating",
        description: "Suppression rating instance contextualized by logged user.",
        typeResolver: getEntityAggregatingRatingForLoggedUserResolver.bind(this, SuppressionRatingDefinition)
      }),
      new GraphQLProperty({
        name: "reportingRating",
        type: "ReportingRating",
        description: "Reporting rating instance contextualized by logged user.",
        typeResolver: getEntityAggregatingRatingForLoggedUserResolver.bind(this, ReportingRatingDefinition)
      })
    ];
  }

  /**
   * @return {GraphQLProperty[]}
   */
  static getOverridenProperties() {
    const semanticRelationLinkDefinition = LexicalSenseDefinition.getLink("semanticRelations");

    return [
      new GraphQLProperty({
        name: semanticRelationLinkDefinition.getGraphQLPropertyName(),
        type: "SemanticRelationConnection",
        description: "Get semantic relations",
        args: `filterOnDistinctWrittenRep: Boolean, excludeDistinctWrittenRep: Boolean ${generateConnectionArgs()}`,
        /**
         *
         * @param {Model} lexicalSense
         * @param {string[]} filterOnDistinctWrittenRep
         * @param {string[]} excludeDistinctWrittenRep
         * @param {object} args
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         * @param {GraphQLResolveInfo} gqlInfo Gql information
         */
        typeResolver: async (lexicalSense, {filterOnDistinctWrittenRep, excludeDistinctWrittenRep, ...args} = {}, synaptixSession, gqlInfo) => {
          if (!args.filters) {
            args.filters = [];
          }

          if (filterOnDistinctWrittenRep || excludeDistinctWrittenRep) {
            args.filters.push(`${SemanticRelationDefinition.getProperty("semanticRelationTypeScopeNote").getPropertyName()} = ${filterOnDistinctWrittenRep ? "distinctWrittenRep" : "sameWrittenRep"}`)
          }

          return getLinkedObjectsResolver(LexicalSenseDefinition.getLink("semanticRelations"), lexicalSense, args, synaptixSession, gqlInfo);
        }
      })
    ];
  }

  static getExtraGraphQLCode() {
    return `
extend type Query{
  """ Search for lexical senses matching an accurate written form """ 
  lexicalSensesForAccurateWrittenForm(
    formQs: String
    filterByPlaceId: ID
    ${generateConnectionArgs()}
  ): LexicalSenseConnection
  
  """ Count for lexical senses matching an accurate written form """ 
  lexicalSensesCountForAccurateWrittenForm(
    formQs: String
    filters: [String]
  ): Int
  
  """  lexical sense aggragation """ 
  lexicalSensesAggs(
      mustFilters: String      
      ${generateConnectionArgs()}
  ): String

  """ Search for lexical senses where written form start with a specific letter """ 
  lexicalSensesForWrittenRepStartWith(
      after: String
      first: Int
      startWithLetter: String
      startWithNumber: Boolean
      startWithNonAlphaNum: Boolean
      mustFilters: String
      ${generateConnectionArgs()}
  ): String

  """ count for lexical senses where written form start with a specific letter """ 
  lexicalSensesForWrittenRepStartWithCount(
      startWithLetter: String
      startWithNumber: Boolean
      startWithNonAlphaNum: Boolean
      mustFilters: String
      ${generateConnectionArgs()}
  ): Int
  
   """ 
   Search for contributed lexical senses
   
   Possible sorting properties to use in "sortings: [{sortBy: "...."}]" parameter: 
   
    - createdAt
    - reportingRatingCount
    - suppressionRatingCount
    - validationRatingCount
    
   Possible filtering parameters: 
   
    - filterOnLoggedUser: Only returns contributions for logged querying user.
    - filterOnUserAccountId: Only returns contributions for a specific userAccountId (secured and only available for Admin and Operator).
    - filterOnProcessed: Only returns processed/non processed contributions
    - filterOnReviewed: Only returns reviewed/non reviewed contributions
    - filterOnExistingSuppressionRating: Only returns contributions that have (or not) suppression rating(s).
    
   """
  contributedLexicalSenses(
    ${generateConnectionArgs()} 
    filterOnLoggedUser: Boolean 
    filterOnUserAccountId: ID
    filterOnProcessed: Boolean
    filterOnReviewed: Boolean
    filterOnExistingSuppressionRating: Boolean
  ): LexicalSenseConnection

  """ Count for contributedLexicalSenses """
  contributedLexicalSensesCount(
    ${generateConnectionArgs()}
    filterOnLoggedUser: Boolean 
    filterOnUserAccountId: ID
    filterOnProcessed: Boolean
    filterOnReviewed: Boolean
    filterOnExistingSuppressionRating: Boolean
  ): Int
}
`
  }

  static getExtraResolvers() {
    async function createResolver({
      getResolver,
      parent,
      filterOnLoggedUser,
      filterOnUserAccountId,
      filterOnProcessed,
      filterOnReviewed,
      filterOnExistingSuppressionRating,
      args,
      synaptixSession,
      info,
    }) {
      let linkPaths = [];
      let propertyFilters = [];
      let queryFilters = [];
      let linkFilters = [];

      if (filterOnLoggedUser || filterOnUserAccountId) {
        let userAccountId;

        if (filterOnUserAccountId) {
          userAccountId = synaptixSession.extractIdFromGlobalId(filterOnUserAccountId);
        } else {
          if (!(await synaptixSession.isLoggedUser())) {
            throw new I18nError(`User must be logged to request it's contribution`, 'USER_MUST_BE_AUTHENTICATED', 401);
          }

          userAccountId = (await synaptixSession.getLoggedUserAccount()).id;
        }

        linkFilters.push(
          new LinkFilter({
            linkDefinition: LexicalSenseDefinition.getLink('hasCreatorUserAccount'),
            id: userAccountId,
          })
        );
      } else {
        linkFilters.push(
          new LinkFilter({
            linkDefinition: LexicalSenseDefinition.getLink('hasCreationAction'),
            any: true,
          })
        );
      }

      if (filterOnProcessed != null) {
        propertyFilters.push(
          new PropertyFilter({
            propertyDefinition: LexicalSenseDefinition.getProperty('processed'),
            value: filterOnProcessed,
          })
        );
      }

      if (filterOnReviewed != null) {
        const adminGroupId = env.get('ADMIN_USER_GROUP_ID').required().asString();
        const operatorGroupId = env.get('OPERATOR_USER_GROUP_ID').required().asString();

        queryFilters.push(
          new QueryFilter({
            filterDefinition: LexicalSenseDefinition.getFilter('isReviewed'),
            filterGenerateParams: {
              userGroupIds: [adminGroupId, operatorGroupId],
            },
            isStrict: true,
            isNeq: filterOnReviewed === false,
          })
        );
      }

      if (filterOnExistingSuppressionRating != null) {
        queryFilters.push(
          new QueryFilter({
            filterDefinition: LexicalSenseDefinition.getFilter('hasSuppressionRatings'),
            isStrict: true,
            isNeq: filterOnExistingSuppressionRating === false,
          })
        );
      }

      return getResolver(
        LexicalSenseDefinition,
        parent,
        {
          ...args,
          linkPaths,
          propertyFilters,
          queryFilters,
          linkFilters,
        },
        synaptixSession,
        info
      );
    }

    /**
     * depending on param use different query, filter on number only or in non-alpha-numeric or on writtenRep start with a specif letter
     */
    function createWildcardStartWith({startWithNonAlphaNum, startWithNumber, startWithLetter}) {
      let queryFilters = [];

      if (startWithNonAlphaNum) {
        queryFilters.push({
          "regexp": {
            "canonicalFormWrittenRep.keyword": {value: "[^a-z0-9].*"}
          }
        });
      } else if (startWithNumber) {
        queryFilters.push({
          "regexp": {
            "canonicalFormWrittenRep.keyword": {value: "[0-9].*"}
          }
        })
      } else if (startWithLetter && startWithLetter?.length > 0) {
        queryFilters.push({
          "wildcard": {
            "canonicalFormWrittenRep.keyword": {value: startWithLetter + "*"}
          }
        });
      }

      return queryFilters;
    }

    function parseMustFilters(mustFilters) {
      let must = [];
      try {
        // if (mustFilters && Array.isArray(mustFilters)) {          must = mustFilters;        } else
        if (mustFilters && mustFilters?.length > 1) {
          must = JSON.parse(mustFilters);
        }
      } catch (error) {
        console.log("******************")
        console.log(mustFilters)
        console.log("----------")
        console.log(error);
        console.log("******************")
      }
      return must;
    }

    return {
      Query: {
        /**
         *
         * @param parent
         * @param {string} formQs
         * @param {string} [filterByPlaceId = "geonames:3017382"] Filters lexicalSenses according a place. "France" by default.
         * @param {object} args
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         */
        lexicalSensesForAccurateWrittenForm: async (parent, {formQs, filterByPlaceId = "geonames:3017382", ...args}, synaptixSession, info) => {
          args.sortings = [];
          if (filterByPlaceId) {

            args.sortings.push(
              new Sorting({
                sortingDefinition: LexicalSenseDefinition.getSorting('placesCountSort'),
                descending: true,
              })
            );

            let place = await geonamesClient.getPlaceById({id: filterByPlaceId});

            if (place.lat && place.lng) {
              args.sortings.push(new Sorting({
                sortingDefinition: LexicalSenseDefinition.getSorting('nearby'),
                params: {
                  lat: parseFloat(place.lat),
                  lon: parseFloat(place.lng),
                },
              })
              );
            }
          }

          args.sortings.push(
            new Sorting({
              sortingDefinition: LexicalSenseDefinition.getSorting('ratingScore'),
              descending: true,
              params: {
                factor: 0.7
              },
            })
          );

          args.queryFilters = [
            new QueryFilter({
              filterDefinition: LexicalSenseDefinition.getFilter('accurateFormWrittenRep'),
              filterGenerateParams: {formQs},
            }),
          ];

          return getObjectsResolver(LexicalSenseDefinition, parent, args, synaptixSession, info);
        },
        /**
         *
         * @param parent
         * @param {string} formQs
         * @param {array} filters
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         */
        lexicalSensesCountForAccurateWrittenForm: async (parent, {formQs, filters}, synaptixSession) => {
          return getObjectsCountResolver(
            LexicalSenseDefinition,
            parent,
            {
              filters,
              queryFilters: [
                new QueryFilter({
                  filterDefinition: LexicalSenseDefinition.getFilter('accurateFormWrittenRep'),
                  filterGenerateParams: {formQs},
                }),
              ],
            },
            synaptixSession
          );
        },

        /**
         *return lexicalSense where writtenRep start with a given letter 
         * @param parent
         * @param {string} startWithLetter 
         * @param {boolean} startWithNumber
         * @param {boolean} startWithNonAlphaNum 
         * @param {string} mustFilters : must filter
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         */

        lexicalSensesForWrittenRepStartWith: async (parent, {startWithNonAlphaNum, startWithLetter, startWithNumber, mustFilters, ...args}, synaptixSession, info) => {

          const result = await synaptixSession.getIndexService().getNodes({
            modelDefinition: LexicalSenseDefinition,
            limit: synaptixSession.getLimitFromArgs(args),
            offset: synaptixSession.getOffsetFromArgs(args),
            getExtraQueryParams: () => {
              let must = parseMustFilters(mustFilters);
              must = [...must, ...createWildcardStartWith({startWithNonAlphaNum, startWithNumber, startWithLetter})];
              let allQuery = {
                "query": {
                  "bool": {
                    must
                  }
                },
                "_source": {"includes": ["canonicalFormWrittenRep"]},
                "sort": [{"canonicalFormWrittenRep.keyword": "asc"}],
                "track_total_hits": true, // return the real number not limited to 10000
              };
              return allQuery;
            },
            rawResult: true,
          });


          let lexicalSenses = {
            edges: result?.hits?.map((hit) => {
              return {
                "node": {
                  id: hit?._id || hit?.id,
                  ...hit?.["_source"]
                }
              }
            })
          }

          return JSON.stringify(lexicalSenses);
        },

        /**
        * count lexicalSense where writtenRep start with a given letter, number or other
        * @param parent
        * @param {string} startWithLetter 
        * @param {boolean} startWithNumber
        * @param {boolean} startWithNonAlphaNum 
        * @param {string} mustFilters : must filter
        * @param {SynaptixDatastoreRdfSession} synaptixSession
        */
        lexicalSensesForWrittenRepStartWithCount: async (parent, {startWithNonAlphaNum, startWithLetter, startWithNumber, mustFilters, ...args}, synaptixSession, info) => {

          const result = await synaptixSession.getIndexService().getNodes({
            modelDefinition: LexicalSenseDefinition,
            limit: 0,
            getExtraQueryParams: () => {
              let must = parseMustFilters(mustFilters);
              must = [...must, ...createWildcardStartWith({startWithNonAlphaNum, startWithNumber, startWithLetter})];
              let allQuery = {
                "query": {
                  "bool": {
                    must
                  }
                },
                "track_total_hits": true, // return the real number not limited to 10000
              };
              return allQuery;
            },
            rawResult: true,
          });

          return JSON.stringify(result?.total || 0);

        },

        /**
         * return lexicalSense aggs  
         * @param parent
         * @param {string} startWithLetter
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         */
        lexicalSensesAggs: async (parent, {mustFilters}, synaptixSession, info) => {

          const result = await synaptixSession.getIndexService().getNodes({
            modelDefinition: LexicalSenseDefinition,
            limit: 0,
            getExtraQueryParams: () => {
              let must = parseMustFilters(mustFilters);

              /**
               * for each field you want to create an aggregation, you need to index it as litterals or label. It don't work for link/edge
               * meta is used in frontEnd to know each aggs name and field for filterquery ( aggs keys are lost for some reason, 
               * it doesn't return and object but an array of {meta,buckets})
               */
              let allQuery = {
                "query": {
                  "bool": {
                    must
                  }
                },
                "track_total_hits": true, // return the real number not limited to 10000
                aggs: {
                  "places": {
                    "meta": {"name": "Aires", "field": "placesForAggs"},
                    "terms": {"size": 1000, "field": "placesForAggs.keyword_not_normalized"}
                  },
                  /* "grammaticals": {
                     "meta": {"name": "Catégories grammaticales", "field": "lexicalEntryGrammaticalPropertyLabels.keyword"},
                     "terms": {"size": 25, "field": "lexicalEntryGrammaticalPropertyLabels.keyword"}
                   },*/
                  "domains": {
                    "meta": {"name": "Domaines", "field": "domainForAggs"},
                    "terms": {"size": 1000, "field": "domainForAggs.keyword_not_normalized"}
                  },
                  "registers": {
                    "meta": {"name": "Marques lexicales", "field": "lexicalMarksForAggs"},
                    "terms": {"size": 1000, "field": "lexicalMarksForAggs.keyword_not_normalized"}
                  },
                  "glossaries": {
                    "meta": {"name": "Glossaires", "field": "glossaryForAggs"},
                    "terms": {"size": 1000, "field": "glossaryForAggs.keyword_not_normalized"}
                  }
                }
              };
              return allQuery;
            },
            rawResult: true,
          });

          return JSON.stringify(result);

        },


        /**
         * @param parent
         * @param filterOnUserAccountId
         * @param {string} [filterOnLoggedUser]
         * @param {boolean} [filterOnProcessed]
         * @param {boolean} [filterOnReviewed]
         * @param {boolean} [filterOnExistingSuppressionRating]
         * @param {ResolverArgs} args
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         * @param {GraphQLResolveInfo} info
         * @return {Model[]}
         */
        contributedLexicalSenses: (
          parent,
          {filterOnLoggedUser, filterOnUserAccountId, filterOnProcessed, filterOnReviewed, filterOnExistingSuppressionRating, ...args},
          synaptixSession,
          info
        ) => {
          return createResolver({
            getResolver: getObjectsResolver,
            parent,
            filterOnLoggedUser,
            filterOnUserAccountId,
            filterOnProcessed,
            filterOnReviewed,
            filterOnExistingSuppressionRating,
            args,
            synaptixSession,
            info,
          });
        },

        contributedLexicalSensesCount: (
          parent,
          {filterOnLoggedUser, filterOnUserAccountId, filterOnProcessed, filterOnReviewed, filterOnExistingSuppressionRating, ...args},
          synaptixSession,
          info
        ) => {
          return createResolver({
            getResolver: getObjectsCountResolver,
            parent,
            filterOnLoggedUser,
            filterOnUserAccountId,
            filterOnProcessed,
            filterOnReviewed,
            filterOnExistingSuppressionRating,
            args,
            synaptixSession,
            info,
          });
        },
      },
      LexicalSense: {
        places: getLinkedGeonamesPlacesResolver.bind(this, LexicalSenseDefinition.getLink('hasPlace')),
      },
    };
  }

  /**
   * @inheritDoc
   */
  static getCreateMutation() {
    return new GraphQLCreateMutation({
      extraInputArgs: `
        """ Lexical sense definition """
        definition: String
        """ Related form written representation """
        formWrittenRep: String
        """ Related lexicalEntry inherited type name """
        lexicalEntryTypeName: String
        """ Related grammatical category concept ID """
        grammaticalCategoryId: String
        """ Related transitivity concept ID """
        transitivityId: String
        """ Related mood concept ID """
        moodId: String
        """ Related tense concept ID """
        tenseId: String
        """ Related person concept ID """
        personId: String
        """ Related gender concept ID """
        genderId: String
        """ Related number concept ID """
        numberId: String
        """ Related geonames place ID """
        placeId: String
    `,
      description: `
      Create a new lexical sense, precising it's related form written representaiton, lexicalEntry typename and grammatical category Id.
      the transitivity/mood/tense/person/gender and number Id are optionnal depending on the grammatical category.

      This mutation input is protected by the Synaptix.js [formValidationMiddleware](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#form-input-validation). Following i18nkey errors can be sent :

      - **USER_MUST_BE_AUTHENTICATED** : If inited session is anomymous.
      - **IS_REQUIRED** : If an input field is missing.
      - **LEXICAL_ENTRY_TYPENAME_NOT_AUTHORIZED** : If "lexicalEntryTypeName" input field is set with not authorized value.
      - **LEXICAL_ENTRY_TYPENAME_NOT_MATCH_FORM** : If "lexicalEntryTypeName" input field not match form written representation
      - **GRAMMATICAL_CATEGORY_NOT_MATCH_FORM** : If "lexicalEntryTypeName" input field not match form written representation    
      - **VERB_MOODID_PERSONID_TENSEID_NOT_MATCHING** : In a verb all the following fields should be all field or all false : moodId,personId,tenseId 
      - **GENDERID_AND_NUMBERID_NOT_FIELD** : Gender and number should be field
    `,
      /**
       * @param definition
       * @param formWrittenRep
       * @param lexicalEntryTypeName
       * @param grammaticalCategoryId
       * @param placeId    // required placeId
       * @param transitivityId  // only if grammaticalCategory is a verb (no matter if conjugate)
       * @param moodId      // only if a verbalInflection ( conjugated verb )
       * @param tenseId     // only if a verbalInflection ( conjugated verb )
       * @param personId    // only if a verbalInflection ( conjugated verb )
       * @param genderId    // only if a InflectablePOS ( not a conjugated verb )
       * @param numberId    // only if a InflectablePOS ( not a conjugated verb )
       * @param {SynaptixDatastoreSession} synaptixSession
       */
      resolver: async ({definition, formWrittenRep, lexicalEntryTypeName, placeId, grammaticalCategoryId, transitivityId, moodId, tenseId, personId, genderId, numberId, ...otherProps} = {}, synaptixSession) => {
        console.log("---------    getCreateMutation props -------------");
        console.log({definition, formWrittenRep, lexicalEntryTypeName, placeId, grammaticalCategoryId, transitivityId, moodId, tenseId, personId, genderId, numberId, otherProps});
        console.log("|-------------------------------------------------|");

        if (!definition || definition.trim() === "") {
          throw new FormValidationError(`definition is required`, [{
            field: "definition",
            errors: ["REQUIRED"]
          }]);
        }

        if (!formWrittenRep || formWrittenRep.trim() === "") {
          throw new FormValidationError(`formWrittenRep is required`, [{
            field: "formWrittenRep",
            errors: ["REQUIRED"]
          }]);
        }

        if (!grammaticalCategoryId || grammaticalCategoryId.trim() === "") {
          throw new FormValidationError(`grammaticalCategoryId is required`, [{
            field: "grammaticalCategory",
            errors: ["REQUIRED"]
          }]);
        }

        if (!placeId) {
          throw new FormValidationError(`placeId is required`, [{
            field: "input.placeId",
            errors: ["REQUIRED"]
          }]);
        }


        /**
         * First validate that submitted grammaticalCategoryId is in right scheme
         */
        await assertGrammaticalCategoryIdMatchFormWrittenRep({grammaticalCategoryId, formWrittenRep, synaptixSession});

        let {
          grammaticalCategoryInputName,
          extraLexicalEntryInput,
          newLexicalEntryTypeName
        } = setupInputsCatergories({
          lexicalEntryTypeName, grammaticalCategoryId, synaptixSession,
          transitivityId, moodId, personId, tenseId, genderId, numberId
        });
        lexicalEntryTypeName = newLexicalEntryTypeName;

        let objectInput = {
          // fields of LexicalSense
          definition,
          processed: false,
          placeInputs: [{id: synaptixSession.extractIdFromGlobalId(placeId)}],
          lexicalEntryInput: {
            // fields of lexicalEntry
            inheritedTypename: lexicalEntryTypeName,
            canonicalFormInput: {
              writtenRep: formWrittenRep
            },
            entryInput: {
              lexicographicResourceInput: {
                id: env.get("DDF_LEXICOGRAPHIC_RESOURCE_ID").required().asString()
              }
            },
            [grammaticalCategoryInputName]: {
              id: synaptixSession.extractIdFromGlobalId(grammaticalCategoryId)
            },
            ...extraLexicalEntryInput
          }
        };

        clj("getCreateMutation objectInput ", objectInput);

        return synaptixSession.createObject(
          {
            modelDefinition: LexicalSenseDefinition,
            objectInput,
            lang: synaptixSession.getContext().getLang()
          }
        )
      }
    })
  }

  /**
   * @inheritDoc
   */
  static getUpdateMutation() {
    return new GraphQLUpdateMutation({
      extraInputArgs: `
        """ Lexical sense definition """
        definition: String
        """ writtenRep """
        writtenRep: String
        """ Related grammatical category concept ID """
        grammaticalCategoryId: String
        """ Lexical sense usage example with source and example"""
        usageExample: UsageExampleInput
        """ semanticRelationInput """ 
        semanticRelationInput : SemanticRelationInput
        """ Related lexicalEntry inherited type name """
        lexicalEntryTypeName: String       
        """ Related transitivity concept ID """
        transitivityId: String
        """ Related mood concept ID """
        moodId: String
        """ Related tense concept ID """
        tenseId: String
        """ Related person concept ID """
        personId: String
        """ Related gender concept ID """
        genderId: String
        """ Related number concept ID """
        numberId: String
        """ Related geonames place ID """
        placeId: String,
        """ geonames place ID to delete """
        placeIdsToDelete: [String]
        """ register ID """
        registerId: String
        """ register ID to delete """
        registerIdToDelete : String
        """ Related domain ID """
        domainId: String
        """ domaine ID to delete """
        domainIdToDelete: String
        """ Related temporality ID """
        temporalityId: String
        """ temporality ID to delete """
        temporalityIdToDelete: String
        """ Related frequency ID """
        frequencyId: String
        """ frequency ID to delete """
        frequencyIdToDelete: String
        """ related connotation ID """
        connotationId: String
        """ connotation ID to delete """
        connotationIdToDelete: String
        """ Related sociolect ID """
        sociolectId: String
        """ sociolect ID to delete """
        sociolectIdToDelete: String
        """ Related grammaticalConstraint ID """
        grammaticalConstraintId: String
        """ grammaticalConstraint ID to delete"""
        grammaticalConstraintIdToDelete: String
        """ Related textualGenre ID """
        textualGenreId: String
        """ textualGenre ID to delete"""
        textualGenreIdToDelete: String
        """ Related glossary ID """
        glossaryId: String
        """ glossary ID to delete"""
        glossaryIdToDelete: String
      `,
      description: `
        Update a lexical sense.

        If a grammaticalCategoryId is passed it delete and create a new LexicalEntry so pass it only when need to change the grammatical category (or any of "transitivity", "gender", "number", "tense", "mood", "person")
        why ? example, a user choose a "conjucated verb" so after mutation we have for example  lexicalEntry.id => "verbal-inflection/32fasp738yg9qq", 
        now same user update the definition and choose "non conjucated verb", id should be updated to "verb/32fasp738yg9qq" but id cannot be update in API. 
        So we mark the previous LexicalEntry as deleted and create a new one and link it.
       



        This mutation is possible if one of following graphQL shield rules is passed :

        - isCreator() : If logged user initing the mutation is the lexicalSense object creator.
        - isAdmin()   : If logged user initing the mutation is in the admin group.

        If not passed, the following i18nkey errors can be sent : **USER_NOT_ALLOWED**
        
        This mutation input also is protected by the Synaptix.js [formValidationMiddleware](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#form-input-validation). Following i18nkey errors can be sent :

        - **GRAMMATICAL_CATEGORY_NOT_MATCH_FORM** : If "lexicalEntryTypeName" input field not match form written representation
        - **REQUIRED** : If an input field is missing.
        `,
      /**
       * @param {string} objectId lexicalSenseId
       * @param {object} objectInput 
       * @param {string} definition
       * @param {string} writtenRep
       * @param {string} grammaticalCategoryId
       * @param {bool}   processed
       * @param {object} usageExample 
       * @param {string} lexicalEntryTypeName

       * @param {string} transitivityId  // only if grammaticalCategory is a verb (no matter if conjugate)
       * @param {string} moodId          // only if a verbalInflection ( conjugated verb )
       * @param {string} tenseId         // only if a verbalInflection ( conjugated verb )
       * @param {string} personId        // only if a verbalInflection ( conjugated verb )
       * @param {string} genderId        // only if a InflectablePOS ( not a conjugated verb )
       * @param {string} numberId        // only if a InflectablePOS ( not a conjugated verb )
       * 
       * @param {string} placeId
       * @param {array}  placeIdsToDelete
       * @param {string} domainId 
       * @param {string} domainIdToDelete 
       * @param {string} temporalityId 
       * @param {string} temporalityIdToDelete 
       * @param {string} registerId 
       * 
       * @param {SynaptixDatastoreSession} synaptixSession
       */
      resolver: async ({objectId, objectInput, writtenRep, definition, processed, lexicalEntryTypeName,
        grammaticalCategoryId, transitivityId, moodId, tenseId, personId, genderId, numberId, usageExample, semanticRelationInput,
        ...otherProps
      } = {objectInput: {}}, synaptixSession) => {


        clj(
          'getUpdateMutation props',
          {
            objectId, objectInput, writtenRep, definition,
            lexicalEntryTypeName, grammaticalCategoryId, transitivityId, moodId, tenseId, personId, genderId, numberId, usageExample, semanticRelationInput,
            processed, otherProps
          }
        );

        if (!objectId || objectId.trim() === "") {
          throw new FormValidationError(`objectId is required, its the lexicalSenseId`, [{
            field: "objectId",
            errors: ["REQUIRED"]
          }]);
        }
        if (!writtenRep || writtenRep.trim() === "") {
          throw new FormValidationError(`writtenRep is required`, [{
            field: "writtenRep",
            errors: ["REQUIRED"]
          }]);
        }


        let lexicalSenseId = synaptixSession.extractIdFromGlobalId(objectId);


        let newObjectInput = {
          // parfois objectInput.processed  est utilisé, parfois processed. ca depend de la requete...
          processed: objectInput?.processed || processed,
        };

        if (definition) {
          newObjectInput.definition = definition;
        }

        if (usageExample?.bibliographicalCitation || usageExample?.value) {
          newObjectInput.usageExampleInput = usageExample;
        }
        if (semanticRelationInput) {
          const {semanticRelationType, semanticRelationOfLexicalSenseInput} = semanticRelationInput;

          if (!semanticRelationType?.id) {
            throw new FormValidationError(`semanticRelationInput.id is required. It describes the nature of the semantic relation.`, [{
              field: "semanticRelationInput.id",
              errors: ["REQUIRED"]
            }]);
          }

          if ((semanticRelationOfLexicalSenseInput || []).length < 1) {
            throw new FormValidationError(`semanticRelationOfLexicalSenseInput.id is required. It describes the targeted lexicalSense.`, [{
              field: "semanticRelationOfLexicalSenseInput.id",
              errors: ["REQUIRED"]
            }]);
          }

          for (const {id: semanticRelationOfLexicalSenseId} of semanticRelationOfLexicalSenseInput) {
            if (synaptixSession.extractIdFromGlobalId(semanticRelationOfLexicalSenseId) === lexicalSenseId) {
              throw new I18nError(`A lexical sense can't have a semantic relation with itself...`);
            }
          }

          newObjectInput.semanticRelationInput = semanticRelationInput
        }

        for (let value in allowedIdFields) {
          if (otherProps[value]) {
            newObjectInput[allowedIdFields[value]] = [{id: synaptixSession.extractIdFromGlobalId(otherProps[value])}]
          }
        }
        for (let value in allowedToDeleteFields) {
          if (otherProps[value]) {
            let val = otherProps[value];
            let arr = [];
            if (Array.isArray(val)) {
              arr = val.map((id) => synaptixSession.extractIdFromGlobalId(id));
            } else {
              arr.push(synaptixSession.extractIdFromGlobalId(val));
            }
            newObjectInput[allowedToDeleteFields[value]] = arr;
          }
        }

        if (grammaticalCategoryId) {
          let lexicalEntry = await synaptixSession.getLinkedObjectFor({
            object: {id: lexicalSenseId},
            linkDefinition: LexicalSenseDefinition.getLink("lexicalEntry")
          });

          let formWrittenRep = writtenRep;

          let form = await synaptixSession.getLinkedObjectFor({
            object: lexicalEntry,
            linkDefinition: LexicalEntryDefinition.getLink("canonicalForm")
          });

          // get the previousFormWrittenRep to check if user change writtenRep or not
          let previousFormWrittenRep = await synaptixSession.getLocalizedLabelFor({
            object: form,
            labelDefinition: FormDefinition.getLabel("writtenRep"),
            returnFirstOneIfNotExistForLang: true
          });

          if (!formWrittenRep) {
            formWrittenRep = previousFormWrittenRep;
          }

          let {lexicalEntryTypeName} = getLexicalEntryContribInputsFromFormWrittenRep(formWrittenRep);

          // First, validate that submitted grammaticalCategoryId is in right scheme
          await assertGrammaticalCategoryIdMatchFormWrittenRep({grammaticalCategoryId, formWrittenRep, synaptixSession});

          // depending on the user's choices we change the lexicalEntryTypeName 
          let {
            grammaticalCategoryInputName,
            extraLexicalEntryInput,
            newLexicalEntryTypeName
          } = setupInputsCatergories({
            lexicalEntryTypeName, grammaticalCategoryId, synaptixSession,
            transitivityId, moodId, personId, tenseId, genderId, numberId
          });
          lexicalEntryTypeName = newLexicalEntryTypeName;

          if (needToSave({
            previous: {writtenRep: previousFormWrittenRep, grammaticalProperties: lexicalEntry.grammaticalProperties, types: lexicalEntry.types},
            next: {writtenRep, grammaticalCategoryId, grammaticalCategoryInputName, extraLexicalEntryInput, lexicalEntryTypeName}
          })) {

            newObjectInput = {
              ...newObjectInput,
              lexicalEntryInput: {
                // if grammaticalCategoryId is filled, it mean that user changed it and we need to create a new lexicalEntry
                // the old lexicalEntry is marked as deleted, no need to do something else because it is a 1-1 link so creating a new one is enough 
                // so don't put old id like : id: lexicalEntry.id,
                inheritedTypename: lexicalEntryTypeName,
                entryInput: {
                  lexicographicResourceInput: {
                    id: env.get("DDF_LEXICOGRAPHIC_RESOURCE_ID").required().asString()
                  }
                },
                ...extraLexicalEntryInput
              }
            }

            if (grammaticalCategoryInputName && grammaticalCategoryId) {
              newObjectInput.lexicalEntryInput[grammaticalCategoryInputName] = {
                id: synaptixSession.extractIdFromGlobalId(grammaticalCategoryId)
              }
            }

            // if (writtenRep && previousFormWrittenRep !== writtenRep) {
            // the user changed the form writteRep 
            newObjectInput.lexicalEntryInput.canonicalFormInput = {
              id: form.id,
              writtenRep
            };
            // }
          }
        }


        clj("getUpdateMutation props for synaptixSession.updateObject()", {lexicalSenseId, newObjectInput});

        return synaptixSession.updateObject({
          modelDefinition: LexicalSenseDefinition,
          objectId: lexicalSenseId,
          updatingProps: newObjectInput,
          lang: synaptixSession.getContext().getLang()
        })
      }
    })
  }

}

/**
 * We tweak here the generic mechanism of mutation by merging some extra infos with hypothetical objectInput :
 *
 *  - The related lexicalEntry inherited type to create, precising it's typename because an ontolex:LexicalEntry is not instantiable. We must precise if it's a Word, Affix or MultiWordExpression
 *  - The related entry with lexicographicResource precision.
 *  - The related grammatical category concept, given that the link name differs among different lexicalEntry inherited types.
 */
function setupInputsCatergories({lexicalEntryTypeName, grammaticalCategoryId, synaptixSession, transitivityId, moodId, personId, tenseId, genderId, numberId}) {

  let newLexicalEntryTypeName = lexicalEntryTypeName;
  let grammaticalCategoryInputName;
  // while contain the grammaticalEntries with moodId , tenseID ...
  let extraLexicalEntryInput = {}
  //  newLexicalEntryTypeName define if it is an affix, a multiword or a word
  switch (newLexicalEntryTypeName) {
    case "MultiWordExpression":
      grammaticalCategoryInputName = "multiWordTypeInput";
      // lexicalEntryDefinition = MultiWordExpressionDefinition;

      if (genderId && _IDONTKNOW !== genderId && genderId !== "ddfa:gender/unspecifiedGender") {
        extraLexicalEntryInput.genderInput = {id: synaptixSession.extractIdFromGlobalId(genderId)}
      }
      if (numberId && _IDONTKNOW !== numberId) {
        extraLexicalEntryInput.numberInput = {id: synaptixSession.extractIdFromGlobalId(numberId)}
      }

      break;
    case "Affix":
      grammaticalCategoryInputName = "termElementInput";
      // lexicalEntryDefinition = AffixDefinition;
      break;
    default:
      grammaticalCategoryInputName = "partOfSpeechInput";
      // lexicalEntryDefinition = WordDefinition;

      // then grammaticalCategoryId define all the sub categories of a word, choosed by user in the UI
      switch (grammaticalCategoryId) {
        case "lexinfo:verb":
          if (transitivityId && _IDONTKNOW !== transitivityId) {
            extraLexicalEntryInput.transitivityInput = {id: synaptixSession.extractIdFromGlobalId(transitivityId)};
          }

          // if grammaticalCategoryId==lexinfo:verb and moodId && personId && tenseId so it is a conjugated verb , so we use the VerbalInflection class
          if (moodId && personId && tenseId) {
            newLexicalEntryTypeName = "VerbalInflection";
            if (_IDONTKNOW !== moodId) {
              extraLexicalEntryInput.moodInput = {id: synaptixSession.extractIdFromGlobalId(moodId)};
            }
            if (_IDONTKNOW !== personId) {
              extraLexicalEntryInput.personInput = {id: synaptixSession.extractIdFromGlobalId(personId)};
            }
            if (_IDONTKNOW !== tenseId) {
              extraLexicalEntryInput.tenseInput = {id: synaptixSession.extractIdFromGlobalId(tenseId)};
            }
          } else if (!moodId && !personId && !tenseId) {
            // it is a verb in the infinitive  
            newLexicalEntryTypeName = "Verb";
          } else {
            // this case is an error, moodId,personId,tenseId should be all true or all false
            throw new FormValidationError(`In a verb  moodId,personId,tenseId should be all true or all false`, [{
              field: "input.moodId, input.personId, input.tenseId",
              errors: [
                "VERB_MOODID_PERSONID_TENSEID_NOT_MATCHING"
              ]
            }]);
          }
          break;
        case "lexinfo:adverb":
        case "lexinfo:conjunction":
        case "lexinfo:interjection":
        case "lexinfo:particle":
          newLexicalEntryTypeName = "InvariablePOS";
          break;
        case "lexinfo:possessiveAdjective":
        case "lexinfo:definiteArticle":
        case "lexinfo:partitiveArticle":
        case "lexinfo:demonstrativePronoun":
        case "lexinfo:indefinitePronoun":
        case "lexinfo:interrogativePronoun":
        case "lexinfo:personalPronoun":
        case "lexinfo:possessivePronoun":
        case "lexinfo:relativePronoun":
        case "lexinfo:adjective":
        case "lexinfo:article":
        case "lexinfo:noun":
        case "lexinfo:properNoun":
        case "lexinfo:preposition":
        case "lexinfo:pronoun":
        default:
          if (genderId && numberId) {
            newLexicalEntryTypeName = "InflectablePOS";
            if (_IDONTKNOW !== genderId) {
              extraLexicalEntryInput.genderInput = {id: synaptixSession.extractIdFromGlobalId(genderId)}
            }
            if (_IDONTKNOW !== numberId) {
              extraLexicalEntryInput.numberInput = {id: synaptixSession.extractIdFromGlobalId(numberId)}
            }
          } else {
            throw new FormValidationError(`GenderId and numberId should be field`, [{
              field: "input.genderId and input.numberId",
              errors: [
                "GENDERID_AND_NUMBERID_NOT_FIELD"
              ]
            }]);
          }
          break;
      }
  }
  return {
    grammaticalCategoryInputName,
    extraLexicalEntryInput,
    newLexicalEntryTypeName
  }

}


// compare previous and next state to check if there is a change and need to be saved 
function needToSave({previous, next}) {
  let allIn = true;
  let nextGrammaticalProperties = [];
  let prevGrammaticalProperties = [];


  try {
    for (let value of previous.grammaticalProperties) {
      if (value?.id?.includes("data.dictionnairedesfrancophones.org") || value?.id?.includes("www.lexinfo.net")) {
        prevGrammaticalProperties.push(value?.id);
      }
    }

    // check if grammaticalCategoryId is different than previous one
    const sameCatGram = ({id}) => id.includes(next?.grammaticalCategoryId.replace("lexinfo:", "lexinfo#"));
    if (!previous.grammaticalProperties.some(sameCatGram)) {
      console.log('needToSave; grammaticalCategoryId is different than previous one', next?.grammaticalCategoryId);
      allIn = false;
    }

    // check if all next extraLexicalEntryInput are in prevGrammaticalProperties
    for (let value in next.extraLexicalEntryInput) {
      let nextId = next.extraLexicalEntryInput[value].id;
      nextGrammaticalProperties.push(nextId);
      if (!prevGrammaticalProperties.includes(nextId)) {
        console.log('needToSave; not in prevGrammaticalProperties', nextId);
        allIn = false;
      }
    }

    if (prevGrammaticalProperties.length !== nextGrammaticalProperties.length) {
      clj(
        'needToSave; not same length ' + prevGrammaticalProperties.length + " vs " + nextGrammaticalProperties.length,
        {
          error: 'needToSave; not same length ' + prevGrammaticalProperties.length + " vs " + nextGrammaticalProperties.length,
          prevGrammaticalProperties, nextGrammaticalProperties
        }
      )
      allIn = false;
    }

    // previous.types can be an array or a string
    if (Array.isArray(previous.types)) {
      if (previous.types.filter((type) => type.includes(next.lexicalEntryTypeName)).length < 1) {
        console.log('needToSave; types are differents', previous.types, next.lexicalEntryTypeName);
        allIn = false;
      }
    } else if (!previous.types.includes(next.lexicalEntryTypeName)) {
      console.log('needToSave; types are differents', previous.types, next.lexicalEntryTypeName);
      allIn = false;
    }

    if (previous.writtenRep != next.writtenRep) {
      console.log("needToSave; writtenRep are differents", previous.writtenRep, next.writtenRep);
      allIn = false;
    }
  } catch (error) {
    clj("needToSave catcherror", {previous, next});
    console.log(error)
  }


  return !allIn;

}


// just console log data with stringify so we can see it's content in console
function clj(name, data) {
  console.log(`---------------------- ${name} --------------------------`);
  console.log(
    JSON.stringify(data, null, 4)
  )
  console.log("|-------------------------------------------------------|");
}


