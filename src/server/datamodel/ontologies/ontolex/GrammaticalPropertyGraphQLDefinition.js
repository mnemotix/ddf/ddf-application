import {
  GraphQLInterfaceDefinition,
  generateConnectionArgs,
  getObjectsResolver,
  QueryFilter
} from "@mnemotix/synaptix.js";

import {GrammaticalPropertyDefinition} from "./GrammaticalPropertyDefinition";

export class GrammaticalPropertyGraphQLDefinition extends GraphQLInterfaceDefinition {
  static getExtraGraphQLCode() {
    return `
extend type Query{
  """ Search for grammatical properties starting with a specific letters, like autocomplete """ 
  grammaticalPropertiesStartWith(      
      first: Int
      startWith: String
      scheme: String
      isTopInScheme: Boolean
      scopeNote: String
      ${generateConnectionArgs()}
  ): String
}
`
  }



  static getExtraResolvers() {

    return {
      Query: {
        grammaticalPropertiesStartWith: async (parent, {startWith, scheme, isTopInScheme, scopeNote, first, ...args}, synaptixSession, info) => {
          let _source = ["prefLabel", "prefLabel_locales", "definition", "definition_locales"];
          let bool = {"filter": []};
          if (startWith?.length > 0) {
            bool.must = [{"wildcard": {"prefLabel.keyword": {"value": `*${startWith}*`}}}];
          }
          if (scheme?.length > 0) {
            bool.filter.push({"terms": {"inScheme": [scheme]}})
          }
          if (isTopInScheme) {
            bool.filter.push({"exists": {"field": "topConceptOf"}})
          }
          if (scopeNote?.length > 0) {
            bool.filter.push({"term": {"scopeNote.keyword": scopeNote}})
            _source.push("scopeNote");
          }

          const result = await synaptixSession.getIndexService().getNodes({
            modelDefinition: GrammaticalPropertyDefinition,
            limit: synaptixSession.getLimitFromArgs(args),
            offset: synaptixSession.getOffsetFromArgs(args),
            getExtraQueryParams: () => {
              let query = {
                "query": {
                  bool
                },
                "_source": {"includes": _source},
                "size": first || 50,
                "from": 0,
                "sort": [{"prefLabel.keyword": "asc"}],
                "track_total_hits": true
              };
              return query;
            },
            rawResult: true,
          });

          let grammaticalProperties = {
            edges: result?.hits?.map((hit) => {
              let source = getFrEntry(hit?.["_source"]);
              return {
                "node": {
                  id: prefixId(hit?._id),
                  ...source
                }
              }
            })
          }

          return JSON.stringify(grammaticalProperties);
        }
      }
    }
  }
}

/**
 *  get an uri with full link and return prefixed id 
 * exemple "http://data.dictionnairedesfrancophones.org/authority/gender/epicene" become "ddfa:gender/epicene"
 **/
function prefixId(id) {
  const mapping = {
    "mnx:": "http://ns.mnemotix.com/ontologies/2019/8/generic-model/",
    "sko:": "http://www.w3.org/2004/02/skos/core#",
    "ddf:": "http://data.dictionnairedesfrancophones.org/ontology/ddf#",
    "lexicog:": "http://www.w3.org/ns/lemon/lexicog#",
    "ontolex:": "http://www.w3.org/ns/lemon/ontolex#",
    "lexinfo:": "http://www.lexinfo.net/ontology/2.0/lexinfo#",
    "geonames:": "http://www.geonames.org/",
    "ddfa:": "http://data.dictionnairedesfrancophones.org/authority/"
  }
  for (const prefix in mapping) {
    id = id.replace(mapping[prefix], prefix)
  }
  return id;
}

/**
 * some prefLabel are string, some are array of string and can be fr or english. 
 * return the first FR one 
 * @param {*} prefLabel 
 * @param {*} prefLabel_locales 
 */
function getFrEntry(source) {
  let {prefLabel, prefLabel_locales, definition, definition_locales, ...otherArgs} = source
  if (Array.isArray(prefLabel)) {
    if (prefLabel_locales && prefLabel_locales?.length === prefLabel?.length) {
      let firstFrIndex = prefLabel_locales.findIndex(elem => elem.toLowerCase() === "fr")
      prefLabel = prefLabel[firstFrIndex];
    } else {
      prefLabel = prefLabel[0]
    }
  }

  if (Array.isArray(definition)) {
    if (definition_locales && definition_locales?.length === definition?.length) {
      let firstFrIndex = definition_locales.findIndex(elem => elem.toLowerCase() === "fr")
      definition = definition[firstFrIndex];
    } else {
      definition = definition[0]
    }
  }

  return {prefLabel, definition, ...otherArgs}

}