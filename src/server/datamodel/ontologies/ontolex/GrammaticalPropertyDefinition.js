/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {GraphQLTypeDefinition, LinkDefinition, FilterDefinition, LabelDefinition, MnxOntologies, ModelDefinitionAbstract} from "@mnemotix/synaptix.js";
import {LexicalEntryDefinition} from "./LexicalEntryDefinition";
import {GrammaticalPropertyGraphQLDefinition} from "./GrammaticalPropertyGraphQLDefinition";

export class GrammaticalPropertyDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GrammaticalPropertyGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "skos:Concept";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLType() {
    return "GrammaticalProperty";
  }


  static getLabels() {
    let superLabels = super.getLabels().filter(label => label._propertyName !== 'scopeNote');
    return [
      ...superLabels,
      new LabelDefinition({
        labelName: 'scopeNote',
        rdfDataProperty: "skos:scopeNote",
        isSearchable: true
      }),
    ]
  }


  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'lexicalEntries',
        symmetricLinkName: 'grammaticalProperties',
        rdfReversedObjectProperty: 'ddf:hasGrammaticalProperty',
        relatedModelDefinition: LexicalEntryDefinition,
        isPlural: true,
        excludeFromIndex: true
      }),
    ]
  }

  static getFilters() {
    return [
      ...super.getFilters(),
      new FilterDefinition({
        filterName: "isTopInScheme",
        indexFilter: () => ({
          "exists": {
            "field": MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition.getLink("topInScheme").getPathInIndex()
          }
        })
      })
    ];
  }
}

