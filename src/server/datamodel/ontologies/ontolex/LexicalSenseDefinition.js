/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  EntityDefinition,
  FilterDefinition,
  LabelDefinition,
  LinkDefinition, LinkPath,
  LiteralDefinition,
  MnxOntologies,
  ModelDefinitionAbstract,
  SortingDefinition
} from "@mnemotix/synaptix.js";

import {LexicalEntryDefinition} from "./LexicalEntryDefinition";
import {PlaceDefinition} from "../geonames/PlaceDefinition";
import {UsageExampleDefinition} from "../lexicog/UsageExampleDefinition";
import {SemanticRelationDefinition} from "../ddf/SemanticRelationDefinition";
import {PostDefinition} from "../ddf-contribution/PostDefinition";
import {ValidationRatingDefinition} from "../ddf-contribution/ValidationRatingDefinition";
import {SuppressionRatingDefinition} from "../ddf-contribution/SuppressionRatingDefinition";
import {ReportingRatingDefinition} from "../ddf-contribution/ReportingRatingDefinition";
import {SemanticPropertyDefinition} from "./SemanticPropertyDefinition";
import {AggregateRatingDefinition} from "../ddf-contribution/AggregateRatingDefinition";
import {LexicographicResourceDefinition} from "../lexicog/LexicographicResourceDefinition";
import {FormDefinition} from "./FormDefinition";
import {LexicalSenseGraphQLDefinition} from "./LexicalSenseGraphQLDefinition";
import {EntryDefinition} from "../lexicog/EntryDefinition";
import {WordDefinition} from "./WordDefinition";
import {MultiWordExpressionDefinition} from "./MultiWordExpressionDefinition";
import {AffixDefinition} from "./AffixDefinition";
import {VerbDefinition} from "../ddf/VerbDefinition";
import {InflectablePOSDefinition} from "../ddf/InflectablePOSDefinition";
import {VerbalInflectionDefinition} from "../ddf/VerbalInflectionDefinition";
import {InflectionDefinition} from "../ddf/InflectionDefinition";

const UserAccountDefinition = MnxOntologies.mnxAgent.ModelDefinitions.UserAccountDefinition;
const UserGroupDefinition = MnxOntologies.mnxAgent.ModelDefinitions.UserGroupDefinition;

export class LexicalSenseDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return LexicalSenseGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ontolex:LexicalSense";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'lexical-sense';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    const lexicalEntryLinkDefinition = new LinkDefinition({
      linkName: 'lexicalEntry',
      symmetricLinkName: "senses",
      pathInIndex: 'isSenseOf',
      rdfObjectProperty: 'ontolex:isSenseOf',
      relatedModelDefinition: LexicalEntryDefinition,
      relatedInputName: "lexicalEntryInput",
    });

    const validationRatingLinkDefinition = new LinkDefinition({
      linkName: 'hasValidationRating',
      symmetricLinkName: 'hasEntity',
      rdfObjectProperty: 'ddf:hasValidationRating',
      relatedModelDefinition: ValidationRatingDefinition,
      isCascadingUpdated: true,
      isCascadingRemoved: true,
      isPlural: true,
      graphQLPropertyName: "validationRatings"
    });

    const suppressionRatingLinkDefinition = new LinkDefinition({
      linkName: 'hasSuppressionRating',
      symmetricLinkName: 'hasEntity',
      rdfObjectProperty: 'ddf:hasSuppressionRating',
      relatedModelDefinition: SuppressionRatingDefinition,
      isCascadingUpdated: true,
      isCascadingRemoved: true,
      isPlural: true,
      graphQLPropertyName: "suppressionRatings"
    });

    const reportingRatingLinkDefinition = new LinkDefinition({
      linkName: 'hasReportingRating',
      symmetricLinkName: 'hasEntity',
      rdfObjectProperty: 'ddf:hasReportingRating',
      relatedModelDefinition: ReportingRatingDefinition,
      isCascadingUpdated: true,
      isCascadingRemoved: true,
      isPlural: true,
      graphQLPropertyName: "reportingRatings"
    });

    const semanticPropertyLinks = [
      new LinkDefinition({
        linkName: 'hasConnotation',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasConnotation',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "connotationInput",
        graphQLPropertyName: "connotations"
      }),
      new LinkDefinition({
        linkName: 'hasDomain',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasDomain',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "domainInput",
        graphQLPropertyName: "domains"
      }),
      new LinkDefinition({
        linkName: 'hasFrequency',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasFrequency',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "frequencyInput",
        graphQLPropertyName: "frequencies"
      }),
      new LinkDefinition({
        linkName: 'hasGrammaticalConstraint',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasGrammaticalConstraint',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "grammaticalConstraintInput",
        graphQLPropertyName: "grammaticalConstraints"
      }),
      new LinkDefinition({
        linkName: 'hasRegister',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasRegister',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "registerInput",
        graphQLPropertyName: "registers"
      }),
      new LinkDefinition({
        linkName: 'hasSociolect',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasSociolect',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "sociolectInput",
        graphQLPropertyName: "sociolects"
      }),
      new LinkDefinition({
        linkName: 'hasTemporality',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasTemporality',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "temporalityInput",
        graphQLPropertyName: "temporalities"
      }),
      new LinkDefinition({
        linkName: 'hasTextualGenre',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasTextualGenre',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "textualGenreInput",
        graphQLPropertyName: "textualGenres"
      })
    ];

    return [
      ...super.getLinks(),
      lexicalEntryLinkDefinition,
      ...semanticPropertyLinks,
      new LinkDefinition({
        linkName: 'hasGlossary',
        symmetricLinkName: "lexicalSenses",
        rdfObjectProperty: 'ddf:hasGlossary',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: true,
        relatedInputName: "glossaryInputs",
        graphQLPropertyName: "glossaries"
      }),
      new LinkDefinition({
        linkName: 'semanticRelations',
        symmetricLinkName: 'lexicalSenses',
        rdfObjectProperty: 'ddf:lexicalSenseHasSemanticRelationWith',
        relatedModelDefinition: SemanticRelationDefinition,
        isPlural: true,
        relatedInputName: "semanticRelationInput",
        graphQLPropertyName: "semanticRelations"
      }),
      new LinkDefinition({
        linkName: 'hasFormRestriction',
        rdfObjectProperty: 'lexicog:formRestriction',
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false,
        relatedInputName: "formRestrictionInput",
        graphQLPropertyName: "formRestrictions"
      }),
      new LinkDefinition({
        linkName: 'usageExample',
        symmetricLinkName: 'lexicalSense',
        rdfObjectProperty: 'lexicog:usageExample',
        relatedModelDefinition: UsageExampleDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true,
        relatedInputName: "usageExampleInput",
        graphQLPropertyName: "usageExamples"
      }),
      new LinkDefinition({
        linkName: 'hasPlace',
        pathInIndex: "places",
        rdfObjectProperty: 'ddf:hasLocalisation',
        relatedModelDefinition: PlaceDefinition,
        isPlural: true,
        graphQLPropertyName: "places",
        relatedInputName: "placeInputs"
      }),
      new LinkDefinition({
        linkName: 'contributions',
        symmetricLinkName: 'sense',
        rdfObjectProperty: 'ddf:hasItemAboutSense',
        relatedModelDefinition: PostDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'topPostAboutSense',
        symmetricLinkName: 'sense',
        rdfObjectProperty: 'ddf:hasItemAboutSense',
        relatedModelDefinition: PostDefinition,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'hasAggregationRating',
        symmetricLinkName: 'hasEntity',
        rdfReversedObjectProperty: 'schema:itemReviewed',
        relatedModelDefinition: AggregateRatingDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: true,
        graphQLPropertyName: "aggregationRatings"
      }),
      validationRatingLinkDefinition,
      suppressionRatingLinkDefinition,
      reportingRatingLinkDefinition,

      //
      // Index only shortcuts
      //
      new LinkDefinition({
        linkName: 'hasLexicographicResource',
        pathInIndex: "lexicographicResource",
        relatedModelDefinition: LexicographicResourceDefinition,
        inIndexOnly: true,
        linkPath: new LinkPath()
          .step({linkDefinition: lexicalEntryLinkDefinition})
          .step({linkDefinition: LexicalEntryDefinition.getLink("entry")})
          .step({linkDefinition: EntryDefinition.getLink("lexicographicResource")}),
        graphQLPropertyName: "lexicographicResource"
      }),
      new LinkDefinition({
        linkName: 'hasCanonicalForm',
        pathInIndex: 'canonicalForm',
        relatedModelDefinition: FormDefinition,
        inIndexOnly: true,
        linkPath: new LinkPath()
          .step({linkDefinition: lexicalEntryLinkDefinition})
          .step({linkDefinition: LexicalEntryDefinition.getLink("canonicalForm")}),
        graphQLPropertyName: "canonicalForm"
      }),
      new LinkDefinition({
        linkName: 'hasReviewerGroup',
        relatedModelDefinition: UserGroupDefinition,
        inIndexOnly: true,
        isPlural: true,
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: validationRatingLinkDefinition})
                .step({linkDefinition: AggregateRatingDefinition.getLink("hasRater")})
                .step({linkDefinition: UserAccountDefinition.getLink("hasUserGroup")}),
              new LinkPath()
                .step({linkDefinition: suppressionRatingLinkDefinition})
                .step({linkDefinition: AggregateRatingDefinition.getLink("hasRater")})
                .step({linkDefinition: UserAccountDefinition.getLink("hasUserGroup")})
            ]
          }),
        graphQLPropertyName: "reviewerGroups"
      }),
      new LinkDefinition({
        graphQLPropertyName: "semanticProperties",
        isPlural: true,
        relatedModelDefinition: SemanticPropertyDefinition,
        linkName: 'hasSemanticProperty',
        linkPath: new LinkPath()
          .union({
            linkPaths: semanticPropertyLinks.map((link) => new LinkPath().step({linkDefinition: link}))
          })
      })
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "definition",
        pathInIndex: "definition",
        rdfDataProperty: 'skos:definition',
        isRequired: true,
        isSearchable: true
      }),
      new LabelDefinition({
        labelName: 'scientificName',
        pathInIndex: "scientificNames",
        rdfDataProperty: 'ddf:hasScientificName',
      }),
      new LabelDefinition({
        labelName: 'canonicalFormWrittenRep',
        inIndexOnly: true,
        linkPath: this
          .getLink("hasCanonicalForm")
          .getLinkPath()
          .property({
            propertyDefinition: FormDefinition.getLabel("writtenRep")
          })
      }),

       

      new LabelDefinition({
        labelName: 'lexicographicResourceName',
        inIndexOnly: true,
        linkPath: this
          .getLink("hasLexicographicResource")
          .getLinkPath()
          .property({
            propertyDefinition: LexicographicResourceDefinition.getLabel("prefLabel")
          })
      }),
      new LabelDefinition({
        labelName: 'lexicographicResourceShortName',
        inIndexOnly: true,
        linkPath: this
          .getLink("hasLexicographicResource")
          .getLinkPath()
          .property({
            propertyDefinition: LexicographicResourceDefinition.getLabel("altLabel")
          })
      }),
      new LabelDefinition({
        labelName: 'lexicographicResourceDefinition',
        inIndexOnly: true,
        linkPath: this
          .getLink("hasLexicographicResource")
          .getLinkPath()
          .property({
            propertyDefinition: LexicographicResourceDefinition.getLabel("definition")
          })
      }),
      new LabelDefinition({
        labelName: 'lexicalEntryGrammaticalPropertyLabels',
        isPlural: true,
        inIndexOnly: true,
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: WordDefinition.getLabel('partOfSpeechLabels').getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: MultiWordExpressionDefinition.getLabel('multiWordTypeLabels').getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: AffixDefinition.getLabel('termElementLabels').getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: VerbDefinition.getLabel("transitivityLabels").getLinkPath()}),

              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: InflectablePOSDefinition.getLabel("genderLabels").getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: InflectablePOSDefinition.getLabel("numberLabels").getLinkPath()}),

              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: InflectionDefinition.getLabel("genderLabels").getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: InflectionDefinition.getLabel("numberLabels").getLinkPath()}),

              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: VerbalInflectionDefinition.getLabel("moodLabels").getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: VerbalInflectionDefinition.getLabel("tenseLabels").getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .append({linkPath: VerbalInflectionDefinition.getLabel("personLabels").getLinkPath()}),
            ]
          })
      }),
      new LabelDefinition({
        labelName: 'semanticPropertyLabels',
        isPlural: true,
        inIndexOnly: true,
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: this.getLink("hasDomain")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasTemporality")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasRegister")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasConnotation")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasFrequency")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasTextualGenre")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasSociolect")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasGrammaticalConstraint")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasGlossary")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
            ]
          })
      }),
      new LabelDefinition({
        labelName: "placesForAggs",
        isPlural: true,
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("hasPlace")})
          .property({
            propertyDefinition: PlaceDefinition.getLabel("name")
          })
      }),
      new LabelDefinition({
        labelName: "domainForAggs",
        isPlural: true,
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("hasDomain")})
          .property({
            propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
          }),
      }),
      new LabelDefinition({
        labelName: "glossaryForAggs",
        isPlural: true,
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: this.getLink("hasGlossary")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntry")})
                .step({linkDefinition: LexicalEntryDefinition.getLink("hasGlossary")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
            ]
          })
      }),
      new LabelDefinition({
        labelName: "lexicalMarksForAggs",
        isPlural: true,
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: this.getLink("hasTemporality")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasRegister")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasConnotation")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                }),
              new LinkPath()
                .step({linkDefinition: this.getLink("hasFrequency")})
                .property({
                  propertyDefinition: SemanticPropertyDefinition.getLabel("prefLabel")
                })
            ]
          })
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "processed",
        rdfDataProperty: "ddf:processed",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean",
        defaultValue: false
      }),
      new LiteralDefinition({
        literalName: "geoloc",
        inIndexOnly: true,
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("hasPlace")})
          .append({linkPath: PlaceDefinition.getLiteral("geoloc").getLinkPath()}),
        rdfDataType: "http://www.opengis.net/ont/geosparql#wktLiteral"
      })
    ];
  }

  static getFilters() {
    return [
      ...super.getFilters(),
      new FilterDefinition({
        filterName: "hasNearbyPlace",
        indexFilter: ({lat, lon, distance}) => ({
          "geo_distance": {
            "distance": distance || "3000km",
            "geoloc": {lat, lon}
          }
        })
      }),
      new FilterDefinition({
        filterName: "hasSuppressionRatings",
        indexFilter: () => ({
          "exists": {
            "field": LexicalSenseDefinition.getLink("hasSuppressionRating").getPathInIndex()
          }
        })
      }),
      new FilterDefinition({
        filterName: "isReviewed",
        indexFilter: ({userGroupIds}) => ({
          "bool": {
            "should": [{
              "terms": {
                "hasReviewerGroup": userGroupIds
              }
            }, {
              "term": {
                "processed": true
              }
            }]
          }
        })
      }),
      new FilterDefinition({
        filterName: "accurateFormWrittenRep",
        indexFilter: ({formQs}) => ({
          "bool": {
            "filter": [{
              "term": {
                [`${this.getLabel("canonicalFormWrittenRep").getPathInIndex()}.keyword_not_normalized`]: {
                  value: formQs,
                }
              }
            }]
          }
        })
      }),
      new FilterDefinition({
        filterName: "writtenRepStartWithLetter",
        indexFilter: ({startWithLetter}) => ({
          "wildcard": {
            [`${this.getLabel("canonicalFormWrittenRep").getPathInIndex()}.keyword`]: {
              value: startWithLetter + "*"
            }
          }
        })
      }),
      new FilterDefinition({
        filterName: "writtenRepStartWithNumber",
        indexFilter: () => ({
          "regexp": {
            [`${this.getLabel("canonicalFormWrittenRep").getPathInIndex()}.keyword`]: {
              value: "[0-9].*"
            }
          }
        })
      }),
      new FilterDefinition({
        filterName: "writtenRepStartWithNonAlphaNum",
        indexFilter: () => ({
          "regexp": {
            [`${this.getLabel("canonicalFormWrittenRep").getPathInIndex()}.keyword`]: {
              value: "[^a-z0-9].*"
            }
          }
        })
      }),
      new FilterDefinition({
        filterName: "hasCreator",
        indexFilter: () => ({
          "exists": {
            "field": LexicalSenseDefinition.getLink("hasCreator").getPathInIndex()
          }
        })
      }),
    ];
  }

  /**
   * @return {SortingDefinition[]}
   */
  static getSortings() {
    let generateRatingScript = ({ratingProperty, direction, factor}) =>
    ({
      "_script": {
        "type": "number",
        "script": {
          "lang": "painless",
          "source": `
              if(doc.containsKey('${ratingProperty}')) return doc['${ratingProperty}'].length * params.factor;
              return 0;
            `,
          "params": {
            "factor": factor || 1.1
          }
        },
        "order": direction || "asc"
      }
    });

    return [
      new SortingDefinition({
        sortingName: "nearby",
        indexSorting: ({lat, lon, direction, unit, mode, distanceType}) => ({
          "_geo_distance": {
            "geoloc": {
              lat,
              lon
            },
            "order": direction || "asc",
            "unit": unit || "km",
            "mode": mode || "min",
            "distance_type": distanceType || "arc",
            "ignore_unmapped": true
          }
        })
      }),
      new SortingDefinition({
        sortingName: "placesCountSort",
        indexSorting: ({direction, factor}) => ({
          "_script": {
            "type": "number",
            "script": {
              "lang": "painless",
              "source": "def placesCount  = doc.containsKey('places')  ? doc['places'].length  : 0;  return (placesCount) * params.factor ;",
              "params": {
                "factor": factor || 0.9
              }
            },
            "order": direction || "desc"
          }
        })
      }),
      new SortingDefinition({
        sortingName: "reportingRatingCount",
        indexSorting: ({direction}) => generateRatingScript({
          ratingProperty: 'hasReportingRating', direction
        })
      }),
      new SortingDefinition({
        sortingName: "suppressionRatingCount",
        indexSorting: ({direction}) => generateRatingScript({
          ratingProperty: 'hasSuppressionRating', direction
        })
      }),
      new SortingDefinition({
        sortingName: "validationRatingCount",
        indexSorting: ({direction}) => generateRatingScript({
          ratingProperty: 'hasValidationRating', direction
        })
      }),
      new SortingDefinition({
        sortingName: "ratingScore",
        indexSorting: ({direction, factor}) => ({
          "_script": {
            "type": "number",
            "script": {
              "lang": "painless",
              "source": `
              def validationRatings  = doc.containsKey('hasValidationRating')  ? doc['hasValidationRating'].length  : 0;
              def suppressionRatings = doc.containsKey('hasSuppressionRating') ? doc['hasSuppressionRating'].length : 0;
              def reportingRatings   = doc.containsKey('hasReportingRating')   ? doc['hasReportingRating'].length   : 0;
              
              return (validationRatings - suppressionRatings  - reportingRatings) * params.factor ;
            `,
              "params": {
                "factor": factor || 1.1
              }
            },
            "order": direction || "asc"
          }
        })
      })
    ]
  }
}

