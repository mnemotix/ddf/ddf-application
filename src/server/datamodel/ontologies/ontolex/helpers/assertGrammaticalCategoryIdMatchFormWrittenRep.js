/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {getLexicalEntryContribInputsFromFormWrittenRep} from "./getLexicalEntryContribInputsFromFormWrittenRep";
import {Model, FormValidationError, LinkPath, MnxOntologies} from "@mnemotix/synaptix.js";
import {GrammaticalPropertyDefinition} from "../GrammaticalPropertyDefinition";

/**
 * Check if grammatical category concept id match for form written rep
 *
 * @param grammaticalCategoryId
 * @param formWrittenRep
 * @param synaptixSession
 * @return {boolean}
 */
export async function assertGrammaticalCategoryIdMatchFormWrittenRep({grammaticalCategoryId, formWrittenRep, synaptixSession}) {
  let {grammaticalCategorySchemeId} = getLexicalEntryContribInputsFromFormWrittenRep(formWrittenRep);

  if (!await synaptixSession.isObjectExistsForLinkPath({
    object: new Model(synaptixSession.extractIdFromGlobalId(grammaticalCategoryId)),
    modelDefinition: GrammaticalPropertyDefinition,
    linkPath: new LinkPath()
      .step({
        linkDefinition: GrammaticalPropertyDefinition.getLink("inScheme"),
        targetId: grammaticalCategorySchemeId
      })
  })) {
    throw new FormValidationError(`${grammaticalCategoryId} is not in scheme ${grammaticalCategorySchemeId}`, [{
      field: "input.grammaticalCategoryId",
      errors: [
        "GRAMMATICAL_CATEGORY_NOT_MATCH_FORM"
      ]
    }]);
  }
}
