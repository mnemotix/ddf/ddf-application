/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  LinkDefinition,
  ModelDefinitionAbstract,
  MnxOntologies,
  GraphQLTypeDefinition,
  LabelDefinition, LinkPath
} from "@mnemotix/synaptix.js";

import { InflectablePOSDefinition } from "./InflectablePOSDefinition"
import {GrammaticalPropertyDefinition} from "../ontolex/GrammaticalPropertyDefinition";
import {WordDefinition} from "../ontolex/WordDefinition";

export class InflectionDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ddf:Inflection";
  }

  static getParentDefinitions(){
    return [WordDefinition];
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'inflectionHasGender',
        pathInIndex: 'inflectionHasGender',
        rdfObjectProperty: 'ddf:inflectionHasGender',
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'inflectionHasNumber',
        pathInIndex: 'inflectionHasNumber',
        rdfObjectProperty: 'ddf:inflectionHasNumber',
        relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      }),
      new LinkDefinition({
        linkName: 'isInflectionOf',
        pathInIndex: 'isInflectionOf',
        rdfObjectProperty: 'ddf:isInflectionOf',
        relatedModelDefinition: InflectablePOSDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      })
    ]
  }

  static getLabels(){
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "genderLabels",
        isPlural: false,
        linkPath: new LinkPath()
          .step({
            linkDefinition: this.getLink("inflectionHasGender")
          })
          .property({
            propertyDefinition: GrammaticalPropertyDefinition.getLabel("prefLabel")
          })
      }),
      new LabelDefinition({
        labelName: "numberLabels",
        isPlural: false,
        linkPath: new LinkPath()
          .step({
            linkDefinition: this.getLink("inflectionHasNumber")
          })
          .property({
            propertyDefinition: GrammaticalPropertyDefinition.getLabel("prefLabel")
          })
      })
    ]
  }
}

