/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  LinkDefinition,
  ModelDefinitionAbstract,
  FilterDefinition, EntityDefinition, LabelDefinition, LinkPath,
} from "@mnemotix/synaptix.js";

import {LexicalSenseDefinition} from "../ontolex/LexicalSenseDefinition";
import {EntryDefinition} from "../lexicog/EntryDefinition";
import {LexicalEntryDefinition} from "../ontolex/LexicalEntryDefinition";
import {SemanticPropertyDefinition} from "../ontolex/SemanticPropertyDefinition";
import {FormDefinition} from "../ontolex/FormDefinition";
import {SemanticRelationGraphQLDefinition} from "./SemanticRelationGraphQLDefinition";
import {LexicographicResourceDefinition} from "../lexicog/LexicographicResourceDefinition";

export class SemanticRelationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return SemanticRelationGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ddf:SemanticRelation";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "semantic-relation";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    const hasEntryLinkDefinition = new LinkDefinition({
      linkName: 'entries',
      symmetricLinkName: 'semanticRelations',
      rdfObjectProperty: 'ddf:semanticRelationOfEntry',
      relatedModelDefinition: EntryDefinition,
      isPlural: true,
      relatedInputName: 'semanticRelationOfEntryInput'
    });

    const hasLexicalEntryLinkDefinition = new LinkDefinition({
      linkName: 'lexicalEntries',
      symmetricLinkName: 'semanticRelations',
      rdfObjectProperty: 'ddf:semanticRelationOfLexicalEntry',
      relatedModelDefinition: LexicalEntryDefinition,
      isPlural: true,
      relatedInputName: 'semanticRelationOfLexicalEntryInput'
    });

    const hasLexicalSenseLinkDefinition = new LinkDefinition({
      linkName: 'lexicalSenses',
      symmetricLinkName: 'semanticRelations',
      rdfObjectProperty: 'ddf:semanticRelationOfLexicalSense',
      relatedModelDefinition: LexicalSenseDefinition,
      isPlural: true,
      relatedInputName: 'semanticRelationOfLexicalSenseInput'
    });

    return [
      ...super.getLinks(),
      hasEntryLinkDefinition,
      hasLexicalEntryLinkDefinition,
      hasLexicalSenseLinkDefinition,
      new LinkDefinition({
        linkName: 'semanticRelationType',
        rdfObjectProperty: 'ddf:hasSemanticRelationType',
        relatedModelDefinition: SemanticPropertyDefinition,
        isPlural: false,
        relatedInputName:'semanticRelationType'
      }),
      new LinkDefinition({
        linkName: 'hasGatheredLexicalSenses',
        relatedModelDefinition: LexicalSenseDefinition,
        isPlural: true,
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: hasEntryLinkDefinition})
                .step({linkDefinition: EntryDefinition.getLink("lexicalEntry")})
                .step({ linkDefinition: LexicalEntryDefinition.getLink("senses") }),
              new LinkPath()
                .step({ linkDefinition: hasLexicalEntryLinkDefinition })
                .step({ linkDefinition: LexicalEntryDefinition.getLink("senses") }),
              new LinkPath()
                .step({linkDefinition: hasLexicalSenseLinkDefinition})
            ]
          }),
        graphQLPropertyName: "gatheredLexicalSenses"
      }),
      // Useless
      // TODO : REMOVE THEM
      new LinkDefinition({
        linkName: 'hasCanonicalForm',
        relatedModelDefinition: FormDefinition,
        isPlural: true,
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: hasEntryLinkDefinition})
                .step({linkDefinition: EntryDefinition.getLink("lexicalEntry")})
                .step({linkDefinition: LexicalEntryDefinition.getLink("canonicalForm")}),
              new LinkPath()
                .step({linkDefinition: hasLexicalEntryLinkDefinition})
                .step({linkDefinition: LexicalEntryDefinition.getLink("canonicalForm")}),
              new LinkPath()
                .step({linkDefinition: hasLexicalSenseLinkDefinition})
                .append({linkPath: LexicalSenseDefinition.getLink("hasCanonicalForm").getLinkPath()})
            ]
          }),
        graphQLPropertyName: "canonicalForms"
      }),
    ]
  }

  static getLabels() {
    const formWrittenRepLabel = LexicalEntryDefinition.getLabel("canonicalFormWrittenRep");
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "semanticRelationTypeLabel",
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: this.getLink("semanticRelationType")})
                .property({propertyDefinition: SemanticPropertyDefinition.getProperty('prefLabel')})
            ]
          })
      }),
      new LabelDefinition({
        labelName: "semanticRelationTypeScopeNote",
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: this.getLink("semanticRelationType")})
                .property({propertyDefinition: SemanticPropertyDefinition.getProperty('scopeNote')})
            ]
          })
      }),
      new LabelDefinition({
        labelName: "gathetedFormWrittenRep",
        isPlural: true,
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: this.getLink("entries")})
                .step({linkDefinition: EntryDefinition.getLink("lexicalEntry")})
                .append({linkPath: formWrittenRepLabel.getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntries")})
                .append({linkPath: formWrittenRepLabel.getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalSenses")})
                .append({linkPath: LexicalSenseDefinition.getLabel("canonicalFormWrittenRep").getLinkPath()})
            ]
          })
      }),
      new LabelDefinition({
        labelName: "canonicalFormWrittenRep",
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: this.getLink("entries")})
                .step({linkDefinition: EntryDefinition.getLink("lexicalEntry")})
                .append({linkPath: formWrittenRepLabel.getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntries")})
                .append({linkPath: formWrittenRepLabel.getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalSenses")})
                .append({linkPath: LexicalSenseDefinition.getLabel("canonicalFormWrittenRep").getLinkPath()})
            ]
          })
      }),
      new LabelDefinition({
        labelName: "lexicalSenseFormWrittenRep",
        isPlural: true,
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalSenses")})
                .append({linkPath: LexicalSenseDefinition.getLabel("canonicalFormWrittenRep").getLinkPath()})
            ]
          })
      }),
      new LabelDefinition({
        labelName: "lexicalEntryFormWrittenRep",
        isPlural: true,
        linkPath: new LinkPath()
          .union({
            linkPaths: [
              new LinkPath()
                .step({linkDefinition: this.getLink("entries")})
                .step({linkDefinition: EntryDefinition.getLink("lexicalEntry")})
                .append({linkPath: formWrittenRepLabel.getLinkPath()}),
              new LinkPath()
                .step({linkDefinition: this.getLink("lexicalEntries")})
                .append({linkPath: formWrittenRepLabel.getLinkPath()})
            ]
          })
      })
    ]
  }

  static getFilters() {
    return [
      ...super.getFilters(),
      new FilterDefinition({
        filterName: "accurateFormWrittenRep",
        indexFilter: ({formQs}) => ({
          "bool": {
            "filter": [{
              "term": {
                "canonicalFormWrittenRep.keyword": formQs
              }
            }]
          }
        })
      })
    ];
  }
}

