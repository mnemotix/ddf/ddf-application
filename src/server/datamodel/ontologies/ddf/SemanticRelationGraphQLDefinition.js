import {
  generateConnectionArgs,
  getObjectsResolver,
  GraphQLTypeDefinition,
  QueryFilter,
  LinkFilter
} from "@mnemotix/synaptix.js";
import {SemanticRelationDefinition} from "./SemanticRelationDefinition";
import env from "env-var";


export let senseToSenseSemanticRelationTypes = [
  'analogicalRelation', 'antonym', 'approximateAntonym', 'approximateSynonym', 'associativeRelation', 'demonymicRelation', 'derivativeMultiWordExpression', 'diachronicRelation', 'euphemisticRelation', 'extendedFromRelation', 'figurativeRelation', 'graphicRelation', 'holonym', 'hyperbolicRelation', 'hypernym', 'hyponym', 'litoticRelation', 'meronym', 'metaphoricRelation', 'metonymicRelation', 'moreCommonThan', 'morphologicalRelation', 'oppositeSexRelation', 'peculiarRelation', 'properMeaningRelation', 'rarerThan', 'sameSemanticDivision', 'soundRelation', 'synonym', 'typographicalRelation'
]

export class SemanticRelationGraphQLDefinition extends GraphQLTypeDefinition {
  static getExtraGraphQLCode() {
    return `
""" This enum lists all semantic relation types """
enum SemanticRelationType {
  associativeRelation
  morphologicalRelation
  derivativeMultiWordExpression
  graphicRelation
}

""" This enum lists all sense-to-sense semantic relation types """
enum SenseToSenseSemanticRelationType{
  ${senseToSenseSemanticRelationTypes.join("\n")}
}


extend type Query{
  """ Search for semantic relations related to an accurate form """
  semanticRelationsForAccurateWrittenForm(formQs: String, filterOnType: SemanticRelationType, ${generateConnectionArgs()}): SemanticRelationConnection
}
`
  }

  static getExtraResolvers() {
    return {
      Query: {
        /**
         * @param _
         * @param {string} formQs
         * @param {string} filterOnType
         * @param {object} args
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         * @param {GraphQLResolveInfo} gqlInfo Gql information
         */
        semanticRelationsForAccurateWrittenForm: async (_, {formQs, filterOnType, ...args}, synaptixSession, gqlInfo) => {
          const semanticRelationTypeNamespace = env.get("AUTHORITY_NAMESPACE_URI").required().asString() ;

          return getObjectsResolver(SemanticRelationDefinition, _, {
            queryFilters: [
              new QueryFilter({
                filterDefinition: SemanticRelationDefinition.getFilter("accurateFormWrittenRep"),
                filterGenerateParams: {
                  formQs
                }
              })
            ],
            linkFilters: [
              new LinkFilter({
                linkDefinition: SemanticRelationDefinition.getLink("semanticRelationType"),
                id: `${semanticRelationTypeNamespace}sense-relation/${filterOnType}`
              })
            ]
          }, synaptixSession, gqlInfo)
        }
      }
    }
  }
}
