import {
  GraphQLTypeConnectionQuery,
  getObjectsResolver,
  getObjectResolver,
} from "@mnemotix/synaptix.js";
import { geonamesClient } from "./services/api/geonamesClient";
import { PlaceDefinition } from "./PlaceDefinition";

export class PlaceGraphQLTypeConnectionQuery extends GraphQLTypeConnectionQuery {
  constructor() {
    super({
      description: `
Search for Place 

@see https://www.geonames.org/export/codes.html for feature code details.

Params :
 - citiesOnly   : Only search for ADM3, ADM4, ADM5 feature codes
 - featureCodes : An array of feature codes
 - country      : Only search within a country - ISO-3166 format (FR, EN ...)
 - bbox         : Only search within bounding box - [north,west,south,east]
 - overrideName : Override name with name from alternateNames or toponymName to get the translated one
`,
      extraArgs: `citiesOnly: Boolean
 featureCodes: [String]
 country: String
 bbox: [Float]
 overrideName: Boolean
`,
    });
  }

  /**
   * @inheritdoc
   */
  generateType(modelDefinition) {
    const baseType = super.generateType(modelDefinition);
    const extraType = this._wrapQueryType(`
      """ Get place by Geo Coors (lat, lng) """
      placeByGeoCoords(lat:Float, lng:Float): PlaceInterface
    `);
    return `
      ${baseType}
      ${extraType}
    `;
  }

  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      /**
       * @param _
       * @param args
       * @param synaptixSession
       * @param gqlInfos
       * @return {}
       */
      places: async (_, args, synaptixSession, gqlInfos) => {
        const {
          qs,
          first,
          citiesOnly,
          featureCodes,
          country,
          bbox,
          overrideName,
        } = args;

        /** @var {Collection} */
        const places = await getObjectsResolver(
          PlaceDefinition,
          _,
          args,
          synaptixSession,
          gqlInfos,
          true
        );

        /** @var {array} */
        const remotePlaces = await geonamesClient.searchPlaces({
          qs,
          limit: first,
          citiesOnly,
          featureCodes,
          country,
          bbox,
          overrideName,
          lang: synaptixSession.getContext().getLang(),
        });

        places.appendObjects(remotePlaces);

        /** @var {Map} */
        const reconciledPlacesMap = places.objects.reduce(
          (placesMap, place) => {
            const normalizedId = synaptixSession.normalizeId(place.id);

            if (!placesMap.has(normalizedId)) {
              placesMap.set(normalizedId, place);
            }
            return placesMap;
          },
          new Map()
        );

        return synaptixSession.wrapObjectsIntoGraphQLConnection(
          [...reconciledPlacesMap.values()],
          args
        );
      },
      place: async (_, { id }, synaptixSession, gqlInfos) => {
        const localPlace = await getObjectResolver(
          PlaceDefinition,
          _,
          { id },
          synaptixSession
        );

        if (id.includes("geonames")) {
          const remotePlace = await geonamesClient.getPlaceById({
            id: id
              .replace("geonames:", "")
              .replace("https://www.geonames.org/", ""),
            lang: synaptixSession.getContext().getLang(),
          });

          return Object.assign({}, remotePlace, localPlace);
        }

        return localPlace;
      },
      placeByGeoCoords: (_, { lat, lng }, synaptixSession) => {
        return geonamesClient.getPlaceByGeoCoords({
          lat,
          lng,
          lang: synaptixSession.getContext().getLang(),
        });
      },
    });
  }
}
