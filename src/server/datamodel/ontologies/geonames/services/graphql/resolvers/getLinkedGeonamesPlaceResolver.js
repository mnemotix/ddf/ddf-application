/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {enhancePlaceWithGeonamesData} from "./enhancePlaceWithGeonamesData";

/**
 * A wrapper to a standard Synaptix.js GraphQL resolver to merge a single Geonames place.
 *
 * @param {Model} object
 * @param {ResolverArgs|object} args
 * @param {Session} synaptixSession
 * @param {LinkDefinition} linkDefinition
 */
export async function getLinkedGeonamesPlaceResolver(linkDefinition, object, args, synaptixSession){
  let place;

  if (object.place?.id) {
    place = object.place;
  } else {
    place = await synaptixSession.getLinkedObjectFor({
      object,
      linkDefinition,
      args,
      discardTypeAssertion: true
    });
  }

  if (place) {
    return enhancePlaceWithGeonamesData({place, synaptixSession});
  }
}