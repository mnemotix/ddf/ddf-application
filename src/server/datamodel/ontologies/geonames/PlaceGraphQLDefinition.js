import {GraphQLInterfaceDefinition} from '@mnemotix/synaptix.js';
import {PlaceGraphQLTypeConnectionQuery} from "./PlaceGraphQLTypeConnectionQuery";

export class PlaceGraphQLDefinition extends GraphQLInterfaceDefinition {

  /**
   * @inheritDoc
   */
  static getTypeConnectionQuery(){
    return new PlaceGraphQLTypeConnectionQuery();
  }

  /**
   * @inheritDoc
   */
  static getExtraResolvers(){
    return {
      PlaceInterface: {
        __resolveType: (place) => {
          if (!place.countryId){
            return "Country";
          }

          if (place.id === place.countryId){
            return "Country";
          } else if (place.fcode === "ADM1"){
            return "State";
          } else {
            return "City";
          }
        }
      }
    }
  }
}
