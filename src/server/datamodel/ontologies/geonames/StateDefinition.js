/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  LiteralDefinition,
  ModelDefinitionAbstract
} from "@mnemotix/synaptix.js";
import { CountryDefinition } from "./CountryDefinition";
import {StateGraphQLDefinition} from "./StateGraphQLDefinition";

export class StateDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return StateGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions(){
    return [CountryDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "skos:Concept";
  }

  static getGraphQLType(){
    return "State";
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "stateCode",
        description: "State code",
      }),
      new LiteralDefinition({
        literalName: "stateName",
        description: "State name",
      })
    ];
  }
}

