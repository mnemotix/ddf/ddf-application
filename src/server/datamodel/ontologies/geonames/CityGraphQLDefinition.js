import {GraphQLInterfaceDefinition} from '@mnemotix/synaptix.js';

export class CityGraphQLDefinition extends GraphQLInterfaceDefinition {
  /**
   * @inheritDoc
   */
  static getExtraResolvers(){
    return {
      City: {
        stateCode: (place) => place.adminCode1,
        stateName: (place) => place.adminName1,
      }
    }
  }
}
