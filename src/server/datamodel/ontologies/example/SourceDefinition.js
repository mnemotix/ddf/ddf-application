/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  LabelDefinition,
  LiteralDefinition,
  ModelDefinitionAbstract,
  LinkDefinition,
  GraphQLTypeDefinition,
  EntityDefinition
} from "@mnemotix/synaptix.js";

import {UsageExampleDefinition} from "../lexicog/UsageExampleDefinition";

export class SourceDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   *
   * Quels sont les model definitions parents.
   *  - EntityDefinition par default (permet de récupérer automatique un certain nombre de propriétés
   *  et de liens comme les actions de création/mise à jour...
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   *
   * Sert à générer automatiquement le schéma GraphQL.
   *  Si la classe est instantiable => GraphQLTypeDefinition (@mnemotix/synaptix.js)
   *  Si la classe est une classe abstraite (type ontolex:LexicalEntry) : GraphQLInterfaceDefinition (@mnemotix/synaptix.js)
   *  Si le comportement standard des deux classes précédentes n'est pas suffisant (ex: surcharger un resolver, ajout une propriété calculée à la volée...) => Créer un classe métier héritant une des deux.
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   *
   * Quel est le type RDF (URI prefixée ou absolue au choix sachant que si préfixée, le préfix doit être défini dans la variable
   * d'environnement SCHEMA_NAMESPACE_MAPPING)
   */
  static getRdfType() {
    return "example:Source";
  }

  /**
   * @inheritDoc
   *
   * Si volonté d'indexer les objets de ce type RDF, alors préciser le nom de l'index sachant qu'il sera préfixé par la valeur de
   * la variable d'environnement INDEX_PREFIX_TYPES_WITH
   */
  static getIndexType() {
    return 'source';
  }

  /**
   * @inheritDoc
   *
   * Ce sont les data properties RDF, à savoir types scalaires (bool, int, date, float) et string NON internationalisées.
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'isImported',
        rdfDataProperty: 'example:isImported',
      }),
      new LiteralDefinition({
        literalName: 'isManualInput',
        rdfDataProperty: 'example:isManualInput',
        rdfDataType: 'xsd:boolean'
      })
    ]
  }

  /**
   * @inheritDoc
   *
   * Ce sont les strings internationalisées
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'name',
        rdfDataProperty: "example:name"
      }),
    ];
  }

  /**
   * @inheritDoc
   *
   * Ce sont les liens entre les classes.
   * - Utiliser rdfObjectProperty si le lien va dans le sens "UsageExample" => "classe liée".
   * - Utiliser rdfReversedObjectProperty si le lien va dans le sens "classe liée" => "UsageExample".
   *
   *  /!\ Limitation connecteur GraphDB/ES, seules les rdfObjectProperty seront indexées. Donc si besoin, demander au modélisateur
   *  d'ajouter une relation rdfs:inverseOf
   *
   *  - Utiliser isPlural: true si la relation est N:N. L'impact sera la définition d'une Connection GraphQL.
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'isSourceOf',
        symmetricLinkName: "hasSource",
        rdfReversedObjectProperty: 'example:hasSource', // |-> Alternative, les deux fonctionnent. Seulement, le premier ne sera pas indexé à cause de la limitation des connecteurs GraphDB qui oblige à utiliser un lien "non reversed"
        // rdfObjectProperty: 'example:isSourceOf',     // |
        relatedModelDefinition: UsageExampleDefinition,
        relatedInputName: "hasUsageExample" // A rajouter pour pouvoir faire du nested input dans les mutations (create|update)Source
      }),
    ];
  }
}

