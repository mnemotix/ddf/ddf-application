import {DataModel} from "@mnemotix/synaptix.js";
import {SourceDefinition} from "./SourceDefinition";

export const exampleDataModel = new DataModel({
  modelDefinitions: {
    SourceDefinition,
  }
});