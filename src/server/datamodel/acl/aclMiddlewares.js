/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  replaceFieldValueIfRuleFailsMiddleware,
  isAdminRule,
  isCreatorRule,
  isInUserGroupRule,
  isAuthenticatedRule,
  isReadOnlyModeDisabledRule,
  isMutationEnabledRule as isMutationEnabledDefaultRule,
  EntityDefinition
} from "@mnemotix/synaptix.js";
import {and, or, allow, rule} from "graphql-shield";
import env from "env-var";
import {allowedIdFields, allowedToDeleteFields} from "../ontologies/ontolex/LexicalSenseGraphQLDefinition";

/**
 * This rule extends default isMutationEnabledRule adding a security on "who has the right to mutation a resource"
 * Response is:
 *   - Administrator
 *   - Resource creator
 *
 * @return {Rule}
 */
export const isMutationEnabledRule = () => and(isMutationEnabledDefaultRule(), or(isCreatorRule(), isAdminRule()));

/**
 * @return {Rule}
 */
export const isOperatorRule = () => and(isAuthenticatedRule(), or(isInUserGroupRule({
  userGroupId: () => env.get("OPERATOR_USER_GROUP_ID").required().asString()
}), isAdminRule()));

/**
 * To have a good comprehension of the following definitons, please explore the Synaptix.js documentation.
 *
 * https://mnemotix.gitlab.io/synaptix.js/securizing_data/#graphql-shield-middlewares
 */

/**
 * This middleware is used to set "false" value to a target type field if a shield rule fails.
 */
export const falsifyFieldIfRuleFailsMiddleware = replaceFieldValueIfRuleFailsMiddleware({
  rule: isMutationEnabledRule(),
  replaceValue: false
});

/**
 * This GraphQL middleware :
 *  1/ Iterates other model definitions registered in dataModel
 *  2/ Filters those which have a "canUpdate" field
 *    - Descendant of EntityDefinition
 *    - Exposed in GraphQL schema
 *    - Instantiable (not the interfaces)
 *  3/ Securize "canUpdate" field that take following values :
 *    - True if the requesting user is an Administrator or the creator of the object
 *    - False otherwise.
 * @param {DataModel} dataModel
 */
export function generateAclMiddlewares(dataModel) {
  return dataModel.getModelDefinitions().reduce((middlewares, modelDefinition) => {
    if (modelDefinition.isEqualOrDescendantOf(EntityDefinition) && modelDefinition.getGraphQLDefinition() && modelDefinition.isInstantiable()) {
      middlewares[modelDefinition.getGraphQLType()] = {
        canUpdate: falsifyFieldIfRuleFailsMiddleware
      }
    }
    return middlewares;
  }, {});
}

/**
 * This GraphQL shield rules ensures the global securization of the GraphQL API.
 *
 * @return {IRules}
 */
export function generateAclRules() {
  return {
    Query: {
      persons: isOperatorRule(),
      person: isOperatorRule(),
      contributedLexicalSenses: isAuthenticatedRule(),
    },
    Mutation: {
      /**
       * Securizing following mutations by asserting that logged user is EITHER an administrator OR the object creator.
       * Or another user than the creator who just add place or example data to contribution
       */
      updateLexicalSense: rule()(
        async (parent, args, ctx, info) => {
          let isAdminOrCreator = await isMutationEnabledRule().resolve(parent, args, ctx, info);
          // If "processed" property is mutated, check that user is an Administrator
          if (typeof args?.input?.objectInput?.processed !== "undefined") {
            return isReadOnlyModeDisabledRule().resolve() && isAdminRule().resolve(parent, args, ctx, info);
          } else  // if it's creator or admin it's ok
            if (isAdminOrCreator === true) {
              return true;
            } else // if it's not creator or admin it can "add" place or example data only or remove none reified links
              /*
              for now many links are not reified but users need to remove them even if we doesn't know who own what.
              we use allowAnyOneToDeleteLink_until_reification in front to disable ownership test
              */
              if (ctx.user !== null &&
                (checkIfNotAddingForbiddenData(args?.input) || checkIfRemovingNotReifiedLinkOnly(args?.input))
              ) { // ctx.user !== null  user is at least authentificated
                return true;
              } else { // error !! return the throw exception message
                return isAdminOrCreator;
              }
        },
      ),
      removeObject: isMutationEnabledRule(),
      removeEntity: isMutationEnabledRule(),
      removeEntityLink: isMutationEnabledRule(),
      /**
       * Only operator users can create suppression rating.
       */
      createSuppressionRating: isOperatorRule(),
    },
    // A non admin user can only query person nicknames. Nothing more.
    UserAccount: {
      '*': or(isAdminRule(), rule()(
        /**
         * @param {Model} userAccount
         * @param args
         * @param {SynaptixDatastoreRdfSession} synaptixSession
         * @param info
         */
        async (userAccount, args, synaptixSession, info) => {
          let loggedUserAccount = await synaptixSession.getLoggedUserAccount();

          return loggedUserAccount && loggedUserAccount?.id === userAccount.id;
        }
      ))
    },
  };
}

/**
 * This helper tests if a GraphQL input contains only allowed fields to mutation, nothing else except objectId & writtenRep & semanticRelationInput
 * allowed fields are from allowedIdFields
 * @param {*} input 
 * @returns 
 */
function checkIfNotAddingForbiddenData(input = {}) {

  let {objectId, writtenRep, usageExample, semanticRelationInput, ...otherProps} = input;
  // is there one valid data at least ? 
  let oneValidData = false;

  if (usageExample?.value/* && !usageExample?.id*/) {
    oneValidData = true
  }
  if (semanticRelationInput) {
    oneValidData = true
  }
  // remove allowedId to check after if otherProps is null or not
  for (let value in allowedIdFields) {
    if (otherProps[value]) {
      oneValidData = true;
      delete otherProps[value];
    }
  }

  // otherProps should not contain any prop
  if (Object.keys(otherProps).length > 0) {
    console.log("checkIfNotAddingForbiddenData : otherProps not null", otherProps)
    return false;
  }
  if (!oneValidData) {
    console.log("checkIfNotAddingForbiddenData : oneValidData is false")
    console.log(JSON.stringify({objectId, writtenRep, usageExample, semanticRelationInput, otherProps}, null, 3))
    return false;
  }

  return (!!objectId && !!writtenRep && oneValidData);
}

/**
 * @TODO remove this after adding reified links in model for following links:
 *  register, place, domain, temporality, frequency, sociolect, grammaticalConstraint, textualGenre, glossary 

 * this function check if user is only removing links for none reified links, if we don't do this we API throw "Not allowed !  (Blocked by `isCreatorRule` 🛡 rule)" 
 * @param {*} input 
 */
function checkIfRemovingNotReifiedLinkOnly(input = {}) {
  let {objectId, writtenRep, ...otherProps} = input;

  // is there one valid data at least ? 
  let oneValidData = false;

  // remove allowedId to check after if otherProps is null or not
  for (let value in allowedToDeleteFields) {    
    if (otherProps[value]) {
      oneValidData = true;
      delete otherProps[value];
    }
  }

  // otherProps should not contain any prop
  if (Object.keys(otherProps).length > 0) {
    console.log("checkIfRemovingNotReifiedLink : otherProps not null", otherProps)
    return false;
  }

  return true;
}