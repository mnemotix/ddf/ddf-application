/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import yargs from "yargs";
import ora from "ora";
import dotenv from "dotenv";
import {
  GraphQLContext,
  initEnvironment,
  LabelDefinition,
  LinkStep,
  EntityDefinition,
  PropertyStep,
  UnionStep,
  ConcatStep,
  logWarning,
} from "@mnemotix/synaptix.js";
import env from "env-var";

import {
  commonEntityFilter,
  commonFields,
  commonMapping,
} from "./connectors/commonFields";
import { generateDatastoreAdapater } from "../../middlewares/generateDatastoreAdapter";
import { Client } from "@elastic/elasticsearch";
import path from "path";
import { generateDataModel } from "../../datamodel/generateDataModel";
import fs from "fs";
import waait from "waait";

dotenv.config();

process.env.UUID = "index-data";
process.env.RABBITMQ_RPC_TIMEOUT = "3000";
process.env.DATALOADER_DISABLED = "1";

const connectorsKnowMapping = {
  "xsd:date": "date",
  "xsd:dateTime": "date",
  "xsd:int": "integer",
  "xsd:long": "long",
  "xsd:float": "float",
  "xsd:double": "double",
  "xsd:boolean": "boolean",
};

const typeMapping = {
  ...connectorsKnowMapping,
  "xsd:dateTimeStamp": "date",
  "http://www.opengis.net/ont/geosparql#wktLiteral": "geo_point",
  "http://www.opengis.net/ont/geosparql#gmlLiteral": "geo_point"
};

export let indexData = async () => {
  let {
    includedTypes,
    excludedTypes,
    allIndices,
    deleteConnector,
    deleteConnectorAndIndex,
    dataModelPath,
    environmentPath,
    stopIfExists
  } = yargs
    .usage("yarn data:index -dm ./src/server/datamodel/dataModel.js")
    .example("yarn data:index -dm ")
    .option("m", {
      alias: "dataModelPath",
      describe: "Datamodel file location",
      default: "src/server/datamodel/dataModel.js",
    })
    .option("e", {
      alias: "environmentPath",
      describe: "Environment file location",
      default: "src/server/config/environment.js",
    })
    .option("s", {
      alias: "stopIfExists",
      describe: "Don't recreate an existing connector",
      default: false,
      nargs: 0,
      type: "boolean",
    })
      .option("a", {
        alias: "allIndices",
        describe: "(Re-)index all indices",
        default: false,
        nargs: 0,
        type: "boolean",
      })
    .option("d", {
      alias: "deleteConnector",
      describe: "Delete connectors only. Leave indices.",
      default: false,
      nargs: 0,
      type: "boolean",
    })
    .option("D", {
      alias: "deleteConnectorAndIndex",
      describe: "Delete connectors and indices",
      default: false,
      nargs: 0,
      type: "boolean",
    })
    .option("i", {
      alias: "includedTypes",
      default: [],
      describe: "List types to include to (re-)index ",
      type: "array",
    })
    .option("x", {
      alias: "excludedTypes",
      default: [],
      describe: "List types to exclude to (re-)index ",
      type: "array",
    })
    .option("p", {
      alias: "createPercolators",
      describe: "Create percolators ",
      default: false,
      nargs: 0,
      type: "boolean",
    })
    .help("h")
    .alias("h", "help")
    .epilog("Copyright Mnemotix 2019")
    .help().argv;

  if(deleteConnectorAndIndex){
    deleteConnector = true;
    stopIfExists = false;
  }

  const environmentDefinition = require(path.resolve(
    process.cwd(),
    environmentPath
  )).default;
  let extraDataModels;
  let dataModelAbsolutePath = path.resolve(process.cwd(), dataModelPath);

  if (fs.existsSync(dataModelAbsolutePath)) {
    extraDataModels = [
      require(path.resolve(process.cwd(), dataModelAbsolutePath)).dataModel,
    ];
  }

  initEnvironment(environmentDefinition);

  const dataModel = generateDataModel({
    extraDataModels,
    environmentDefinition,
  });

  const elasticsearchExternalUri = env
    .get("INDEX_MASTER_URI")
    .required()
    .asString();
  const elasticsearchNode = env.get("INDEX_CLUSTER_NODE").default(elasticsearchExternalUri).asString();
  const elasticsearchBasicAuthUser = env.get("INDEX_CLUSTER_USER").asString();
  const elasticsearchBasicAuthPassword = env.get("INDEX_CLUSTER_PWD").asString();
  const indexPrefix = env.get("INDEX_PREFIX_TYPES_WITH").required().asString();
  const rdfStoreVersion = env.get("RDFSTORE_VERSION").default(10).asFloat();

  const esClient = new Client({
    node: elasticsearchExternalUri,
    auth: {
      username: elasticsearchBasicAuthUser,
      password: elasticsearchBasicAuthPassword,
    },
    maxRetries: 2,
    requestTimeout: 60000,
    sniffOnStart: false,
  });

  /** @type {SynaptixDatastoreRdfAdapter} */
  let { datastoreAdapter } = await generateDatastoreAdapater({
    graphMiddlewares: [],
    dataModel,
  });
  /** @type {SynaptixDatastoreRdfSession} */
  let synaptixSession = datastoreAdapter.getSession({
    context: new GraphQLContext({
      anonymous: true,
    }),
  });

  let spinner = ora().start();
  spinner.spinner = "clock";

  spinner.info("Indexation options :")

  if(allIndices){
    spinner.info(" - Iterate over all types.");
  }

  if(stopIfExists){
    spinner.info(" - Don't recreate an existing connector.");
  }

  if(deleteConnector){
    spinner.info(" - Delete connectors only. Don't delete indices.");
  } else if(deleteConnectorAndIndex){
    spinner.info(" - Delete connectors AND indices.");
  }



  let connectors = [];

  for (let modelDefinition of dataModel.getModelDefinitions()) {
    let included = includedTypes.includes(modelDefinition.getIndexType());
    let excluded = excludedTypes.includes(modelDefinition.getIndexType());

    if (
      modelDefinition.isIndexed() &&
      (allIndices || (included && !excluded))
    ) {
      let connector = connectors.find(
        ({ name }) => name === `${indexPrefix}${modelDefinition.getIndexType()}`
      );

      if (!connector) {
        connector = {
          name: `${indexPrefix}${modelDefinition.getIndexType()}`,
          fields: [
            {
              fieldName: "types",
              propertyChain: [
                "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
              ],
              analyzed: false,
              multivalued: true,
            },
          ],
          mappings: {
            types: {
              type: "keyword",
            },
            ...commonMapping,
          },
          types: [],
          elasticsearchNode,
          elasticsearchBasicAuthUser,
          elasticsearchBasicAuthPassword,
          elasticsearchClusterSniff: false,
          bulkUpdateBatchSize: 500,
          indexCreateSettings: {
            "index.blocks.read_only_allow_delete": null,
          },
          manageMapping: false,
          manageIndex: false,
        };

        if (modelDefinition.isEqualOrDescendantOf(EntityDefinition)) {
          connector.fields = [].concat(connector.fields, commonFields);
          if (rdfStoreVersion > 9) {
            connector.documentFilter = commonEntityFilter;
          } else {
            connector.entityFilter = commonEntityFilter;
          }
        }

        connectors.push(connector);
      }

      connector.types.push(
        synaptixSession.normalizeAbsoluteUri({
          uri: modelDefinition.getRdfType(),
        })
      );

      for (let property of modelDefinition.getProperties()) {
        let fieldName = property.getPathInIndex();

        if (typeof fieldName !== "string") {
          continue;
        }

        let dataProperty = property.getRdfDataProperty();
        let dataType = property.getRdfDataType();
        let defaultValue;
        let propertyChain = [];
        let composedField =
          property.getLinkPath()?.getLastStep() instanceof UnionStep;
        let propertyStepExists =
          property.getLinkPath()?.getLastStep() instanceof PropertyStep;
        let isLocalized = property instanceof LabelDefinition;
        let multivalued =
          isLocalized ||
          property.isPlural?.() ||
          property.getLinkPath()?.getLastStep() instanceof ConcatStep;
        let analyzed = isLocalized || property.isSearchable();

        if (
          property.getLinkPath() &&
          (propertyStepExists || composedField)
        ) {
          propertyChain = linkPathToPropertyChain({
            modelDefinition,
            linkPath: property.getLinkPath(),
            synaptixSession,
          });

          if (!dataType) {
            if (composedField) {
              dataType = property
                .getLinkPath()
                .getLastStep()
                .getLinkPaths()[0]
                .getLastPropertyStep()
                .getPropertyDefinition()
                .getRdfDataType();
            } else {
              dataType = property
                .getLinkPath()
                .getLastPropertyStep()
                .getPropertyDefinition()
                .getRdfDataType();
            }
          }
        } else if (property.getRdfDataProperty()) {
          propertyChain.push(
            synaptixSession.normalizeAbsoluteUri({ uri: dataProperty })
          );
        }

        if (propertyChain.length > 0) {
          dataType = dataType
            .replace("http://www.w3.org/2001/XMLSchema#", "xsd:")
            .replace("integer", "int");

          if (property.getDefaultValue() != null) {
            if (property instanceof LabelDefinition) {
              defaultValue = property.getDefaultValue();
            } else {
              defaultValue = `\\"${property.getDefaultValue()}\\"^^${dataType}`;
            }
          }

          // Multiple propertyChains
          if (composedField) {
            for (const [index, fragmentPropertyChain] of Object.entries(
              propertyChain
            )) {
              buildPropertyField({
                connector,
                fieldName,
                fieldNameSuffix: `$${parseInt(index) + 1}`,
                propertyChain: fragmentPropertyChain,
                property,
                analyzed,
                defaultValue,
                dataType,
                multivalued,
                isLocalized,
                isAggregable: property.isAggregable?.(),
              });
            }
          } else {
            buildPropertyField({
              connector,
              fieldName,
              propertyChain,
              property,
              analyzed,
              defaultValue,
              multivalued,
              isLocalized,
              dataType,
              isAggregable: property.isAggregable?.(),
              addNamedGraph: true,
            });
          }

          // if (property instanceof LabelDefinition) {
          //   connector.fields.push({
          //     fieldName: `${fieldName}_locales`,
          //     propertyChain: [...propertyChain, "lang()"],
          //     analyzed: false,
          //     multivalued: multivalued,
          //   });
          //
          //   connector.mappings[`${fieldName}_locales`] = {
          //     type: "keyword",
          //   };
          // }
        }
      }

      for (let link of modelDefinition
        .getLinks()
        .filter((link) => !link.isExcludedFromIndex?.() || true)) {
        const fieldName = link.getPathInIndex();
        const defaultValue = link.getDefaultValue()
          ? `<${synaptixSession.normalizeAbsoluteUri({
            uri: link.getDefaultValue(),
          })}>`
          : null;
        let composedField =
          link.getLinkPath()?.getLastStep() instanceof UnionStep;

        if (typeof fieldName !== "string") {
          continue;
        }

        let objectProperty =
          link.getRdfObjectProperty() ||
          link.getSymmetricLinkDefinition()?.getRdfReversedObjectProperty();
        let reversedObjectProperty = link.getRdfReversedObjectProperty();
        let linkPath = link.getLinkPath();
        let propertyChain = [];

        if (linkPath) {
          propertyChain = linkPathToPropertyChain({
            modelDefinition,
            linkPath,
            synaptixSession,
          });
        } else if (objectProperty) {
          propertyChain.push(
            synaptixSession.normalizeAbsoluteUri({
              uri: objectProperty,
            })
          );
        } else if(reversedObjectProperty && rdfStoreVersion >= 10){
          // propertyChain.push(`^${synaptixSession.normalizeAbsoluteUri({
          //   uri: reversedObjectProperty,
          // })}`);
        }

        if (propertyChain.length > 0) {
          if (composedField) {
            for (const [index, fragmentPropertyChain] of Object.entries(
              propertyChain
            )) {
              buildPropertyField({
                connector,
                fieldName,
                fieldNameSuffix: `$${parseInt(index) + 1}`,
                propertyChain: fragmentPropertyChain,
                analyzed: false,
                multivalued: link.isPlural(),
                defaultValue,
              });
            }
          } else {
            buildPropertyField({
              connector,
              fieldName,
              propertyChain,
              analyzed: false,
              multivalued: link.isPlural(),
              defaultValue,
            });
          }

          connector.mappings[fieldName] = {
            type: "keyword",
          };
        }
      }
    }
  }

  for (let connector of connectors) {
    let name = connector.name;
    let mappings = connector.mappings;
    delete connector.name;
    delete connector.mappings;

    spinner.info(`Switching to index: "${name}"`);

    let [fields] = connector.fields.reduce(
      ([fields, fieldNames], field) => {
        let fieldName = field.fieldName;
        if (!fieldNames.includes(fieldName)) {
          fields.push(field);
          fieldNames.push(fieldName);
        }
        return [fields, fieldNames];
      },
      [[], []]
    );

    connector.fields = fields;

    let {done, notExists, processing, estimatedEntities, processedEntities, etaSeconds}  = await waitForConnectorBuild({
      spinner,
      synaptixSession,
      connectorName: name
    });


    if(!processing) {
      if(!notExists){
        if(stopIfExists){
          spinner.succeed(`Connector already exists. Doesn't recreate it.`);
          continue;
        }

        spinner.info(`Removing existing connector if exists.`);

        // Connector first.
        try {
          await synaptixSession
            .getGraphControllerService()
            .getGraphControllerPublisher()
            .insertTriples({
              query: `PREFIX :<http://www.ontotext.com/connectors/elasticsearch#>
PREFIX inst:<http://www.ontotext.com/connectors/elasticsearch/instance#>
INSERT DATA {
  inst:${name} :dropConnector "" .
}
`,
            });
        } catch (e) {
        }
      }

      if (!(deleteConnector && !deleteConnectorAndIndex)) {
        try {
          spinner.info(`Removing index "${name}".`);
          await esClient.indices.delete({
            index: name,
          });
        } catch (e) {
        }
      }

      if (!deleteConnector) {
        try {
          spinner.info(`Creating index "${name}".`);

          await esClient.indices.create({
            index: name,
            body: {
              settings: {
                analysis: {
                  analyzer: {
                    french: {
                      type: "standard",
                      stopwords: "_french_",
                    },
                  },
                  normalizer: {
                    lowercase_no_accent: {
                      type: "custom",
                      filter: ["asciifolding", "lowercase"],
                    },
                  },
                },
              },
              mappings: {
                properties: mappings,
              },
            },
          });
        } catch (e) {
          spinner.fail(e.message);
        }

        try {
          spinner.info(`Creating connector "${name}".`);
          synaptixSession
            .getGraphControllerService()
            .getGraphControllerPublisher()
            .insertTriples({
              query: `PREFIX :<http://www.ontotext.com/connectors/elasticsearch#>
PREFIX inst:<http://www.ontotext.com/connectors/elasticsearch/instance#>
INSERT DATA {
  inst:${name} :createConnector '''
  ${JSON.stringify(connector, null, " ")}
'''.
}
`,
            });

          spinner.info(`Processing..."`);
          spinner = spinner.start();

          do{
            await waait(5000);

            try{
              ({done, estimatedEntities, processedEntities, etaSeconds}  = await waitForConnectorBuild({
                spinner,
                synaptixSession,
                connectorName: name
              }));
            } catch (e) {}


            spinner.text = `Indexing (${Math.floor(processedEntities/estimatedEntities*100)}%)  - ${processedEntities} / ${estimatedEntities} entities`;

          } while (!done);

          spinner.succeed("Indexing success !")
        } catch (e) {
          spinner.fail(e.message);
        }
      }
    }
  }
  process.exit(0);
};


/**
 * @param {string} connectorName
 * @param {SynaptixDatastoreRdfSession} synaptixSession
 * @return {Promise<{estimatedEntities: number, processedEntities: number, processing: boolean, notExisting: boolean, done: boolean, etaSeconds: number}>}
 */
async function waitForConnectorBuild({connectorName, synaptixSession}) {
  let rsp;

  try {
    rsp = await synaptixSession
        .getGraphControllerService()
        .getGraphControllerPublisher()
        .select({
          query: `SELECT ?status { <http://www.ontotext.com/connectors/elasticsearch/instance#${connectorName}> <http://www.ontotext.com/connectors/elasticsearch#connectorStatus> ?status }`
        })
  } catch (e){ }

  const statusRaw = rsp?.results?.bindings?.[0]?.status?.value;

  const {status = "NOT_EXISTS", estimatedEntities = 1, processedEntities = 0, etaSeconds = 0} = JSON.parse(statusRaw || "{}");

  return {
    notExists: status === "NOT_EXISTS",
    done: status === "BUILT",
    processing: status === "BUILDING",
    estimatedEntities,
    processedEntities,
    etaSeconds
  }
}


/**
 * Build connector a property field
 * @param connector
 * @param fieldName
 * @param fieldNameSuffix
 * @param dataType
 * @param analyzed
 * @param multivalued
 * @param propertyChain
 * @param defaultValue
 * @param isLocalized
 * @param isAggregable
 * @param addNamedGraph
 */
function buildPropertyField({
  connector,
  fieldName,
  fieldNameSuffix = "",
  dataType,
  analyzed,
  multivalued,
  propertyChain,
  defaultValue,
  isLocalized,
  isAggregable = false,
  addNamedGraph = false,
}) {
  let connectorField = {
    fieldName: `${fieldName}${fieldNameSuffix}`,
    propertyChain,
    analyzed,
    multivalued,
  };

  if (Object.keys(connectorsKnowMapping).includes(dataType)) {
    connectorField.datatype = dataType;
    if (defaultValue) {
      connectorField.defaultValue = defaultValue;
    }
  }

  connector.fields.push(connectorField);

  const keywordFieldMapping = {
    type: "keyword",
    ignore_above: 256,
    normalizer: "lowercase_no_accent",
  };

  const keywordRawFieldMapping = {
    type: "keyword",
    ignore_above: 256,
  };

  const analyzedFieldMapping = {
    type: "text",
    analyzer: "french",
    fields: {
      keyword: typeMapping[dataType]
        ? { type: typeMapping[dataType] }
        : keywordFieldMapping,
      keyword_not_normalized: keywordRawFieldMapping,
    },
  };

  if(isLocalized){
    connector.fields.push({
      fieldName: `${fieldName}_locales`,
      propertyChain: [...propertyChain, "lang()"],
      analyzed: false,
      multivalued: multivalued,
    });

    connector.mappings[`${fieldName}_locales`] = {
      type: "keyword",
    };
  }

  if (isLocalized || analyzed) {
    connector.mappings[fieldName] = analyzedFieldMapping;
  } else if (typeMapping[dataType]) {
    connector.mappings[fieldName] = {
      type: typeMapping[dataType],
    };
  } else {
    connector.mappings[fieldName] = keywordFieldMapping;
  }
}

function linkPathToPropertyChain({
  modelDefinition,
  linkPath,
  synaptixSession,
}) {
  return linkPath.getSteps().reduce((propertyChain, step) => {
    if (step instanceof LinkStep) {
      let objectProperty =
        step.getLinkDefinition().getRdfObjectProperty() ||
        step
          .getLinkDefinition()
          .getSymmetricLinkDefinition()
          ?.getRdfReversedObjectProperty();

      let reversedObjectProperty = step.getLinkDefinition().getRdfReversedObjectProperty();

      if (objectProperty) {
        propertyChain.push(
          synaptixSession.normalizeAbsoluteUri({
            uri: objectProperty,
          })
        );
      } else if(reversedObjectProperty){
        propertyChain.push(`^${synaptixSession.normalizeAbsoluteUri({
          uri: reversedObjectProperty,
        })}`);
      }
    } else if (step instanceof PropertyStep) {
      if (step.getPropertyDefinition().getRdfDataProperty()) {
        propertyChain.push(
          synaptixSession.normalizeAbsoluteUri({
            uri: step.getPropertyDefinition().getRdfDataProperty(),
          })
        );
      } else {
        logWarning(
          `Linkpath ${linkPath} might be not indexed because property ${step.getPropertyDefinition()} has no getRdfDataProperty() value`
        );
      }
    } else if (step instanceof UnionStep) {
      for (const linkPath of step.getLinkPaths()) {
        propertyChain.push(
          linkPathToPropertyChain({
            linkPath,
            synaptixSession,
            modelDefinition,
          })
        );
      }
    }

    return propertyChain;
  }, []);
}