import env from "env-var";
import {logDebug} from '@mnemotix/synaptix.js';
/**
 * Get all public named graphs defined in PUBLIC_NAMED_GRAPHS environement variable and
 * returns a list of SPARQL "FROM <graph>" selectors.
 *
 * @param {boolean} [isAdminQuery=false] - Is this query issued from a user belonging to Admin group ?
 * @return {string}
 */
function generateSparqlNamedGraphSelectors({isAdminQuery = false} = {}) {
  let publicNamedGraphs = env.get("PUBLIC_NAMED_GRAPHS")
    .required()
    .asJson();

  if (isAdminQuery) {
    publicNamedGraphs.push(`<${env.get("NODES_NAMED_GRAPH").asString()}>`);
  }

  return publicNamedGraphs
    .map(namedGraph => `FROM ${namedGraph}`)
    .join("\n");
}

/**
 * insert the FROM in the  SPARQL query to filter on public data only
 * @param {*} query - The SPARQL Query to securize.
 * @param {boolean} [isAdminQuery=false] - Is this query issued from a user belonging to Admin group ?
 */
export function protectSparqlQuery({query, isAdminQuery} = {}) {

  /**
   * We replace `\n` with ` \n ` because 
   * `PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      ASK
      #ontologies`
   * is received as 
   * '<http://www.w3.org/2004/02/skos/core#>\nASK\n#ontologies\nFROM', 
   * when splitting by space we don't have `ASK` alone
   */
  const newQuery = query.replaceAll('\n', ' \n ').replaceAll('  ', ' ');
  const terms = newQuery.split(' ');
  // we search for the first keyword to get what type of operation we are doing
  for (let i = 0; i < terms.length; i++) {
    let term = terms[i];
    switch (term.toUpperCase()) {
      case 'SELECT':
      case 'CONSTRUCT':
      case 'ASK':
        return addFromBeforeWhere({terms, position: i, isAdminQuery});
      case 'DESCRIBE':
        return newQuery + generateSparqlNamedGraphSelectors({isAdminQuery});
    }
  }

  throw new Error("Cannot found SELECT or CONSTRUCT or ASK or DESCRIBE in SPARQL query.");
}


/**
 * Add the FORM before the WHERE
 * @param {*} terms
 * @param {*} position: we search for the WHERE beginning at this position
 * @param {boolean} [isAdminQuery=false] - Is this query issued from a user belonging to Admin group ?
 */
function addFromBeforeWhere({terms, position, isAdminQuery}) {
  for (let i = position; i < terms.length; i++) {
    let term = terms[i];
    if (term.toUpperCase() === 'WHERE') {
      terms.splice(i, 0, `\n${generateSparqlNamedGraphSelectors({isAdminQuery})}\n`);
      return terms.join(' ');
    }
  }

  throw new Error("Cannot found WHERE clause in SPARQL query (try adding space between WHERE and '{' ).");
}
