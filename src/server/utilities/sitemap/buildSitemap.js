/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import yargs from "yargs";
import ora from "ora";
import dotenv from "dotenv";
import {
  initEnvironment
} from "@mnemotix/synaptix.js";
import env from "env-var";
import { Client } from "@elastic/elasticsearch";
import path from "path";
import fs from "fs";
import {buildSitemaps} from "express-sitemap-xml";
import {FormDefinition} from "../../datamodel/ontologies/ontolex/FormDefinition";
import {LexicalSenseDefinition} from "../../datamodel/ontologies/ontolex/LexicalSenseDefinition";

dotenv.config();
process.env.UUID = "build-sitemap";

export let buildSiteMap = async () => {
  let {
    dataModelPath,
    environmentPath
  } = yargs
    .usage("yarn sitemap:build")
    .option("m", {
      alias: "dataModelPath",
      describe: "Datamodel file location",
      default: "src/server/datamodel/dataModel.js"
    })
    .option("e", {
      alias: "environmentPath",
      describe: "Environment file location",
      default: "src/server/config/environment.js"
    })
    .help("h")
    .alias("h", "help")
    .epilog("Copyright Mnemotix 2019")
    .help().argv;

  const environmentDefinition = require(path.resolve(
    process.cwd(),
    environmentPath
  )).default;
  let extraDataModels;
  let dataModelAbsolutePath = path.resolve(process.cwd(), dataModelPath);

  if (fs.existsSync(dataModelAbsolutePath)) {
    extraDataModels = [
      require(path.resolve(process.cwd(), dataModelAbsolutePath)).dataModel
    ];
  }

  initEnvironment(environmentDefinition);

  const baseURL = env.get("APP_URL").required().asString();
  const elasticsearchNode = env
    .get("ES_CLUSTER_NODE")
    .required()
    .asString();
  const elasticsearchExternalUri = env
    .get("ES_MASTER_URI")
    .required()
    .asString();
  const elasticsearchBasicAuthUser = env.get("ES_CLUSTER_USER").asString();
  const elasticsearchBasicAuthPassword = env.get("ES_CLUSTER_PWD").asString();
  const indexPrefix = env
    .get("INDEX_PREFIX_TYPES_WITH")
    .required()
    .asString();

  const esClient = new Client({
    node: elasticsearchExternalUri,
    auth: {
      username: elasticsearchBasicAuthUser,
      password: elasticsearchBasicAuthPassword
    },
    maxRetries: 2,
    requestTimeout: 60000,
    sniffOnStart: false
  });

  let spinner = ora().start();
  spinner.spinner = "clock";

  let urls = [];

  spinner.start(`Fetching forms...`);
  // Index "forms" URLs
  const {body: {count: formsCount}} = await esClient.count({
    index: [indexPrefix + FormDefinition.getIndexType()]
  });
  const formWrittenRep = FormDefinition.getLabel("writtenRep").getPathInIndex();
  for await (const hit of scrollSearch(esClient,{
    index: [indexPrefix + FormDefinition.getIndexType()],
    _source: [formWrittenRep],
    scroll: '30s',
    body: {
      query: {
        match_all: {}
      }
    },
    sort: `${formWrittenRep}.keyword`,
    size: 9000
  })) {
    spinner.text = `Fetching forms... ${urls.length}/${formsCount} (${Math.floor(urls.length / formsCount * 100)}%)`

    if (
      hit._source[formWrittenRep].includes("/") || hit._source[formWrittenRep].includes("&") ||
      hit._source[formWrittenRep].includes("?") || hit._source[formWrittenRep].includes("%")
    ) {
      // console.log({a: hit._source[formWrittenRep], b: encodeURIComponent(hit._source[formWrittenRep])})
      urls.push(`${baseURL}/form/${encodeURIComponent(hit._source[formWrittenRep])}`);
    } else {
      urls.push(`${baseURL}/form/${hit._source[formWrittenRep]}`);
    }
  }

  spinner.succeed(`Forms fetched...`);
  spinner.info("Building sitemap");
  const sitemaps = await buildSitemaps(urls, baseURL);

  const sitemapsDir = getSitemapsDirPath();

  if (!fs.existsSync(sitemapsDir)){
    fs.mkdirSync(sitemapsDir);
  }

  for(const [filename, data] of Object.entries(sitemaps)){
    const filepath = path.join(sitemapsDir, filename);
    spinner.info(`Writing sitemap ${filepath}`);
    fs.writeFileSync(filepath, data);
  }
  //
  // // Index "lexical senses" URLs
  // const canonicalFormWrittenRep = LexicalSenseDefinition.getLabel("canonicalFormWrittenRep").getPathInIndex();
  //
  // for await (const hit of scrollSearch(esClient,{
  //   index: [indexPrefix + LexicalSenseDefinition.getIndexType()],
  //   _source: [canonicalFormWrittenRep],
  //   scroll: '30s',
  //   body: {
  //     query: {
  //       match_all: {}
  //     }
  //   },
  //   sort: `${canonicalFormWrittenRep}.keyword`,
  //   size: 1000
  // })) {
  //   for(let [prefix, ns] of Object.entries(nsMapping)){
  //     if(hit._id.includes(ns)){
  //       hit._id = hit._id.replace(ns, `${prefix}:`)
  //     }
  //   }
  //   urls.push(`${baseURL}/form/${hit._source[canonicalFormWrittenRep]}/sense/${encodeURIComponent(hit._id)}`)
  // }


  spinner.succeed();

  process.exit(0);
};

// Scroll utility
async function* scrollSearch(client, params) {
  let response = await client.search(params)

  while (true) {
    const sourceHits = response.body.hits.hits
    if (sourceHits.length === 0) {
      break;
    }

    for (const hit of sourceHits) {
      yield hit;
    }

    if (!response.body._scroll_id) {
      break;
    }

    response = await client.scroll({
      scrollId: response.body._scroll_id,
      scroll: params.scroll
    })
  }
}


export function getSitemapsDirPath(){
  return path.join(process.cwd(), "/sitemaps");
}