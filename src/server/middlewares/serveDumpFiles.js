
import env from "env-var";
import {ExpressApp, logError, attachDatastoreSessionExpressMiddleware as attachDatastoreSession} from "@mnemotix/synaptix.js";
import fse from "fs-extra";
import path from "path";
import orderBy from 'lodash/orderBy';

const BYTES_PER_MB = 1024 * 1024;
const BYTES_PER_KB = 1024;

/**
 * 
 *  Public api listing the DUMP files
 *
 * @param {ExpressApp} app - The expressJS application, wrapped in a synaptix.js ExpressApp instance
 */
export function serveDumpFiles({app, authenticate}) {
  const directory = env.get("DUMP_FILES_PATH").required().asString();

 

  app.get(
    "/api/dumpfiles",
    authenticate({
      acceptAnonymousRequest: true,
      disableAuthRedirection: true
    }),
    attachDatastoreSession({
      datastoreAdapter: app.getDefaultDatastoreAdapter(),
      acceptAnonymousRequest: true
    }),
    async function (req, res) {
      try {
        const files = await scanDir(directory);      
        try {
          let from = parseInt(req?.query?.after || 0);
          let to = from + parseInt(req?.query?.first || 20) ;         
          return res.status(200).send({files: files.slice(from, to), count: files.length});
        } catch (error) {
          return res.status(200).send({files, count: files.length});
        }
        
      } catch (error) {
        logError(error);
        return res.status(500).send({files: [], count: 0});
      }
    }
  );



  app.get(
    "/api/downloadfile/:path",
    authenticate({
      acceptAnonymousRequest: true,
      disableAuthRedirection: true
    }),
    attachDatastoreSession({
      datastoreAdapter: app.getDefaultDatastoreAdapter(),
      acceptAnonymousRequest: true
    }),
    async function (req, res) {
      try {
        const path = decodeURIComponent(req.params.path);
        res.download(path, (err) => {
          if (err) {
            res.status(500).send({
              message: `Could not download the file ${path} ` + err,
            });
          }
        });
      } catch (error) {
        logError(error);
        return res.status(500).send([]);
      }
    }
  );



}

const allowedExtensions = ["csv", "rar", "zip", "tar"];

async function scanDir(dir, fileList = []) {
  const files = fse.readdirSync(dir);
  for (const file of files) {
    const stat = await fse.stat(path.join(dir, file));
    const isDir = stat.isDirectory();
    if (isDir) {
      fileList = await scanDir(path.join(dir, file), fileList);
    }
    else {
      const extension = file.split('.').pop();
      if (allowedExtensions.includes(extension)) {
        fileList.push({
          filename: file,
          path: path.join(dir, file),
          size: stat.size / BYTES_PER_MB > 1 ? `${(stat.size / BYTES_PER_MB).toFixed(0)} Mb` : `${(stat.size / BYTES_PER_KB).toFixed(0)} Kb`,
          creation: stat.birthtime
        });
      }
    }
  };

  return orderBy(fileList, 'creation', 'desc');
}
