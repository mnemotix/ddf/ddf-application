import env from "env-var";
import fs from "fs";
import {
  ExpressApp,
  logInfo,
  logError
} from "@mnemotix/synaptix.js";
import {getSitemapsDirPath} from "../utilities/sitemap/buildSitemap";

/**
 * Prepare the API and setup the express middleware to serve it
 *
 * @param {ExpressApp} app - The expressJS application, wrapped in a synaptix.js ExpressApp instance
 */
export function serveSitemap({ app }) {
  const baseURL = env.get("APP_URL").required().asString();
  app.get(/(sitemap(?:-\d*)?\.xml)/, (req, res, next) => {
    const userAgent = req.headers['user-agent'];
    logInfo(`Robot is requesting /${req.params[0]}. User agent : ${userAgent}`);

    const sitemapsPath = `${getSitemapsDirPath()}/${req.params[0]}`;

    if(fs.existsSync(sitemapsPath)){
      fs.createReadStream(sitemapsPath).pipe(res);
    } else {
      logError(`Sitemap file does not exist at location ${sitemapsPath}`)
      res.sendStatus(404);
    }
  });
  app.get("/robots.txt", (req, res) => {
    const userAgent = req.headers['user-agent'];
    logInfo("Robot is requesting /robots.txt. User agent : " + userAgent);
    res.header('Content-Type', 'text/plain');
    res.send(`Sitemap: ${baseURL}/sitemap.xml`);
  });
}