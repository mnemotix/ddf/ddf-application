import got from "got";
import env from "env-var";
import {
  ExpressApp,
  logInfo,
  logDebug,
  logError,
  attachDatastoreSessionExpressMiddleware as attachDatastoreSession
} from "@mnemotix/synaptix.js";

import { protectSparqlQuery } from "../utilities/sparql/protectSparqlQuery";

/**
 * 
 *  Public Sparql endpoint is used by reactJs Yasguisparql component to create a SPARQL interface in front with input query and display results. 
 * 
 * Prepare the API and setup the express middleware to serve it
 *
 * @param {ExpressApp} app - The expressJS application, wrapped in a synaptix.js ExpressApp instance
 */
export function servePublicSparqlEndpoint({ app, authenticate }) {
  const uri = env
    .get("SPARQL_PUBLIC_ENDPOINT_URI")
    .required()
    .asString();
  const repoName = env
    .get("SPARQL_PUBLIC_ENDPOINT_REPOSITORY_NAME")
    .required()
    .asString();
  const username = env
    .get("SPARQL_PUBLIC_ENDPOINT_USER")
    .required()
    .asString();
  const password = env
    .get("SPARQL_PUBLIC_ENDPOINT_PWD")
    .required()
    .asString();

  logInfo(
    `SPARQL Public endpoint (${uri}/repositories/${repoName}) listening on ${env
      .get("APP_URL")
      .asString()}/api/sparql`
  );

  async function stream(query, req, res) {
    const options = {
      method: "POST",
      headers: {
        Accept: req.get("Accept") // Forward "Accept" client header, needed by server.
      },
      username,
      password,
      form: {
        query
      }
    };

    return got
      .stream(`${uri}/repositories/${repoName}`, options)
      .on("error", error => {
        logError("[SPARQL ERROR]", error.response?.body || error.message);
        res.status(500).send(error.response?.body || error.message);
      })
      .pipe(res);
  }

  async function prepareAndRunQuery(query, req, res) {
    /** @type {SynaptixDatastoreSession} datastoreSession */
    const datastoreSession = req.datastoreSession;

    const isAdminQuery = await datastoreSession?.isLoggedUserInAdminGroup();

    try {
      query = protectSparqlQuery({ query, isAdminQuery });

      if (env.get("DEBUG_SPARQL").asBool()) {
        logDebug(`[SPARQL Query] ${query}`);
      }

      return await stream(query, req, res);
    } catch (error) {
      logError("[PROTECT SPARQL QUERY ERROR]", error);
      return res.status(400).json({ success: false, data: error.message });
    }
  }

  app.get(
    "/api/sparql",
    authenticate({
      acceptAnonymousRequest: true,
      disableAuthRedirection: true
    }),
    attachDatastoreSession({
      datastoreAdapter: app.getDefaultDatastoreAdapter(),
      acceptAnonymousRequest: true
    }),
    async function(req, res) {
      if (req.query.query) {
        return await prepareAndRunQuery(req.query.query, req, res);
      } else {
        return res
          .status(400)
          .json({
            success: true,
            data: "ping ok, using get but no req.query.query"
          });
      }
    }
  );

  app.post(
    "/api/sparql",
    authenticate({
      acceptAnonymousRequest: true,
      disableAuthRedirection: true
    }),
    attachDatastoreSession({
      datastoreAdapter: app.getDefaultDatastoreAdapter(),
      acceptAnonymousRequest: true
    }),
    async function(req, res) {
      if (req.body.query) {
        return await prepareAndRunQuery(req.body.query, req, res);
      } else {
        return res
          .status(400)
          .json({
            success: true,
            data: "ping ok, using post but no req.body.query"
          });
      }
    }
  );
}
