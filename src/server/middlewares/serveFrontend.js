/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import env                   from 'env-var';
import path                   from 'path';
import webpack                from 'webpack';
import expressStaticGzip      from 'express-static-gzip';

import {ExpressApp, logInfo}  from '@mnemotix/synaptix.js';

import webpackConfig from '../../../webpack.config.js';

import isbot from "isbot";
import React from "react";
import {renderToString} from "react-dom/server";
import { StaticRouter } from "react-router";
import { ChunkExtractor } from "@loadable/server";
import {ApolloProvider} from "@apollo/client";
import {renderToStringWithData} from "@apollo/client/react/ssr";
import {getApolloClientForSSR} from "../../client/services/DDFApolloClient";
import fetch from "node-fetch";
import {Html} from "../../client/Html";
import {I18nextProvider} from "react-i18next";
import { HelmetProvider } from 'react-helmet-async';
import {ROUTES} from "../../client/routes";
import {FormSeoTags} from "../../client/components/ssr/FormSeoTags";
import {LexicalSenseSeoTags} from "../../client/components/ssr/LexicalSenseSeoTags";

/**
 * Prepare the frontend (source bundling with webpack) and setup the express middleware to serve it
 *
 * @param {ExpressApp} app - The expressJS application, wrapped in a synaptix.js ExpressApp instance
 */
export function serveFrontend({app}) {
  if (!['production', 'integration'].includes(env.get("NODE_ENV").asString())) {
    logInfo(`Building webpack resources...`);

    const compiler = webpack(webpackConfig);
    const middleware =  require("webpack-dev-middleware")(compiler, {
      publicPath: webpackConfig.output.publicPath,
      serverSideRender: true,
      writeToDisk: (filePath) => {
        return /^(?!.*(hot)).*/.test(filePath);
      }
    });

    app.use(middleware);
    app.use(require("webpack-hot-middleware")(compiler, {
      log: console.log,
      path: "/__webpack_hmr",
      heartbeat: 2000
    }));
  }

  app.use(expressStaticGzip("./dist", {
    enableBrotli: true,
    orderPreference: ['br']
  }));

  app.get(ROUTES.FORM_LEXICAL_SENSE, async (req, res, next) => {
    if(!isbot(req.get('user-agent'))){
      return next();
    }

    const formQuery = req.params.formQuery;
    const lexicalSenseId = req.params.lexicalSenseId;

    const url = `${env.get("APP_URL").asString()}${req.url}`;
    const Component =  <LexicalSenseSeoTags formQuery={formQuery} lexicalSenseId={lexicalSenseId} url={url}/>;

    return renderSSRComponent({Component, req, res, next});
  });

  app.get(ROUTES.FORM_SEARCH, async (req, res, next) => {
    if(!isbot(req.get('user-agent'))){
      return next();
    }

    const formQuery = req.params.formQuery;
    const url = `${env.get("APP_URL").asString()}${req.url}`;
    const Component = <FormSeoTags formQuery={formQuery} url={url}/>;
    return renderSSRComponent({Component, req, res, next});
  });

  // Fallback route to render default client rendering if SSR fail.
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(webpackConfig.output.path, 'index.html'));
  });
}

async function renderSSRComponent({req, res, next, Component}){
  const statsFile = path.resolve(
    __dirname,
    "../../../dist/loadable-stats.json"
  );
  const chunkExtractor = new ChunkExtractor({ statsFile });
  const context = {};

  const apolloClient = getApolloClientForSSR({
    uri: `${env.get("APP_URL").required().asString()}/graphql`,
    fetch: fetch,
    headers: {
      lang: req.get('Lang') || "fr"
    }
  });

  const helmetContext = {};

  try{
    const components = (
      <ApolloProvider client={apolloClient}>
        <StaticRouter location={req.url} context={context}>
          <HelmetProvider context={helmetContext}>
            <I18nextProvider i18n={req.i18n}>
              {Component}
            </I18nextProvider>
          </HelmetProvider>
        </StaticRouter>
      </ApolloProvider>
    );

    const ast = chunkExtractor.collectChunks(components);
    const content = await renderToStringWithData(ast);
    const state = apolloClient.extract();

    const html = (
      <Html content={content} state={state} helmet={helmetContext.helmet} chunkExtractor={chunkExtractor} />
    );

    res.status(200);
    res.send(`<!doctype html>
${renderToString(html)}`
    );
    res.end();

  } catch (e) {
    console.log("SSR error :", e);
    return next();
  }
}
