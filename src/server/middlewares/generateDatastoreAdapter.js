/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  NetworkLayerAMQP,
  SynaptixDatastoreRdfAdapter,
  MnxActionGraphMiddleware
} from '@mnemotix/synaptix.js';
import {dataModel} from '../datamodel/dataModel';
import kebabCase from "lodash/kebabCase";
import env from "env-var";

/**
 * A function to generate a datastore adapter with inited network layer.
 *
 * @param {ExpressApp} app - The synapix.js ExpressApp instance which will run the application server
 * @param {SSOApiClient} ssoApiClient
 *
 * @return {{datastoreAdapter: SynaptixDatastoreAdapter, networkLayer: NetworkLayerAMQP}}
 */
export async function generateDatastoreAdapater({ssoApiClient}) {
  let networkLayer;

  if(!env.get("RABBITMQ_DISABLED").asBool()) {
    /**
     * Connecting network layer.
     */
    const amqpURL = `amqp://${process.env.RABBITMQ_LOGIN}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`;

    networkLayer = new NetworkLayerAMQP(
      amqpURL,
      process.env.RABBITMQ_EXCHANGE_NAME,
      {},
      {
        durable: !!parseInt(process.env.RABBITMQ_EXCHANGE_DURABLE || 1)
      }
    );

    await networkLayer.connect();
  }

  /**
   * Initializing datastore adapter (data layer).
   */
  const datastoreAdapter = new SynaptixDatastoreRdfAdapter({
    networkLayer,
    modelDefinitionsRegister: dataModel.generateModelDefinitionsRegister(),
    ssoApiClient,
    graphMiddlewares: [
      new MnxActionGraphMiddleware()
    ],
    /**
     * This function tweaks a node type into a prefix with a uri-like form.
     *
     * For example :
     *
     * A type `ontolex:LexicalSense` is tranformed into `lexical-sense`.
     * And a  `ontolex:LexicalSense` node URI could be `http://data.dictionnairedesfrancophones.org/dict/contrib/lexical-sense/[generated_id]`
     *
     * @param {nodeType} nodeType
     */
    nodesTypeFormatter: (nodeType) => {
      return kebabCase(nodeType);
    }
  });

  return {
    networkLayer,
    datastoreAdapter
  };
}
