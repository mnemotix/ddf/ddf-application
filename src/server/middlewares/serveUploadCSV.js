
import env from "env-var";
import {ExpressApp, logError, attachDatastoreSessionExpressMiddleware as attachDatastoreSession} from "@mnemotix/synaptix.js";
import multer from "multer";

/**
 * 
 *  API to upload CSV
 *
 * @param {ExpressApp} app - The expressJS application, wrapped in a synaptix.js ExpressApp instance
 */
export function serveUploadCSV({app, authenticate}) {
  const upload = multer({
    dest: env.get("UPLOAD_CSV_FILES_PATH").required().asString()
  })

  app.post('/api/upload/csv', authenticate({
      acceptAnonymousRequest: false,
      disableAuthRedirection: true
    }),
    upload.single('csvFile'),
    async function (req, res) {
      if(req.file){
        return res.status(200).send(req.file.filename);
      } else {
        return res.sendStatus(500);
      }
    })
}