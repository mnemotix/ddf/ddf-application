/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const LoadablePlugin = require("@loadable/webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");


const fs = require('fs');
const dotenv = require('dotenv');

dotenv.config();

let isDev = process.env.NODE_ENV !== 'production';
let isProd = !isDev;
let isCI = !!process.env.CI;

/** mode **/
let mode = isDev ? 'development' : 'production';

/** devtool **/
let devtool = isDev ? "eval-source-map" : false;

/** entry **/
let entry = [
  ...(isDev ? ["webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000"] : []),
  path.resolve(__dirname, './src/client/main.js')
];

const publicPath = "/";
const assetsPath = "a/";

/** output **/
let output = {
  path: path.resolve(__dirname, 'dist'),
  filename: isDev ? '[name].js' : '[name].[hash].js',
  chunkFilename: isDev ? '[name].js' : '[name].[hash].js',
  publicPath: publicPath
};


/** plugins **/
let plugins = [
  ...(!isCI ? [new webpack.ProgressPlugin()] : []),
  new LoadablePlugin(),
  new FaviconsWebpackPlugin({
    logo: path.resolve(__dirname, './src/client/assets/favicon.png'),
    publicPath: "https://www.dictionnairedesfrancophones.org/",
    prefix: assetsPath,
    cache: true,
    mode: 'webapp',
    devMode: 'webapp',
    favicons: {
      appName: 'DDF',
      appDescription: 'Dictionnaire des francophones',
      developerName: 'Mnemotix',
      developerURL: "www.mnemotix.com",
    }
  }),
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, './src/client/index.tpl.html'),
    inject: 'body',
    filename: 'index.html'
  }),
  new CopyPlugin({
    patterns: [
      {
        from: path.resolve(__dirname, './src/client/assets/opensearch.xml'),
        to: path.resolve(__dirname, './dist/a/opensearch.xml')
      },
      {
        from: path.resolve(__dirname, './src/client/assets/images/ddf_logo_squared.png'),
        to: path.resolve(__dirname, './dist/a/ddf_logo_squared.png')
      },
      {
        from: path.resolve(__dirname, './src/client/assets/images/ddf_logo.svg'),
        to: path.resolve(__dirname, './dist/a/ddf_logo.svg')
      }

    ],
  }),
  ...(isDev ? [new webpack.HotModuleReplacementPlugin(), new ReactRefreshWebpackPlugin()] : []),
  ...(isProd ? [
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css',
      chunkFilename: '[id].[hash].css',
    }),
    new CompressionPlugin({
      filename: "[path][base].gz",
      algorithm: "gzip",
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8
    }),
    new CompressionPlugin({
      filename: "[path][base].br",
      algorithm: "brotliCompress",
      test: /\.(js|css|html|svg)$/,
      compressionOptions: {
        level: 11
      },
      threshold: 10240,
      minRatio: 0.8
    })
  ] : []),
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.EnvironmentPlugin({
    NODE_ENV: "development",
    SYNAPTIX_USER_SESSION_COOKIE_NAME: "SNXID"
  })
];

/** optimization **/
let optimization = {
  splitChunks: {
    cacheGroups: {
      reactVendor: {
        test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
        name: "reactvendor"
      },
      yasgui: {
        test: new RegExp(
          `[\\/]node_modules[\\/](@triply/yasgui})[\\/]`
        ),
        name: "yasgui"
      },
      vendors: {
        test: /[\\/]node_modules[\\/](!yasgui)[\\/]/,
        name: "vendors",
        chunks: "all"
      },
    }

  }
};

if (isProd) {
  optimization.minimizer = [
    new TerserPlugin({
      extractComments: true
    }),
    new OptimizeCSSAssetsPlugin({})
  ]
}

/** modules **/
let webpackModule = {
  rules: [
    {
      test: /\.m?js$/,
      resolve: {
        fullySpecified: false
      }
    },
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          rootMode: "upward",
          plugins: [
            isDev && require.resolve("react-refresh/babel")
          ].filter(Boolean)
        }
      }]
    },
    {
      /* For some libraries using ES6 modules (ex: react-relay-network-modern), this is needed to ensure
       * correct interpretation of .mjs files as javascript */
      test: /\.mjs$/,
      include: /node_modules/,
      type: "javascript/auto",
    },
    {
      /*
       * Also, CSS dependencies in node_modules are not used by this project
       *
       */
      test: /\.css$/,
      use: [
        // style-loader
        {loader: 'style-loader'},
        // css-loader
        {
          loader: 'css-loader',
          options: {
            modules: true,
          },
        },
      ],
    },
    {
      test: /\.(jpe?g|png|gif|ico|woff|woff2|eot|ttf)$/i,
      use: {
        loader: "file-loader",
        options: {
          name: assetsPath + '[name].[hash].[ext]'
        }
      }
    },
    {
      test: /\.svg$/,
      use: ['url-loader'],
    },
    {
      test: /\.md/,
      use: [
        'html-loader',
        'markdown-loader'
      ]
    }
  ],
};

const webpackConfig = {
  mode,
  devtool,
  entry,
  output,
  plugins,
  optimization,
  module: webpackModule,
  resolve: {
    alias: {
      process: "process/browser"
    },
    extensions: [
      '.js',
      '.json', /* needed because some dependencies use { import './Package' } expecting to resolve Package.json */
      '.mjs',  /* for libraries shipping ES6 module to work */
    ],
    fallback: {
      // This is to avoid the error "BREAKING CHANGE: webpack < 5 used to include polyfills for node.js core modules by default."
      "path": require.resolve("path-browserify"),
      "url": require.resolve("url"),
      "events": require.resolve("events"),

      /* "util": false,
       "assert": false,
       "buffer": false,
       "string_decoder": false,
       "crypto": false,
       "fs": false,
       */

    }
  },
  cache: {
    type: 'filesystem'
  },
  performance: {
    hints: false
  }
  // stats: 'minimal',
};

module.exports = webpackConfig;
exports.publicPath = publicPath;
exports.assetsPath = assetsPath;