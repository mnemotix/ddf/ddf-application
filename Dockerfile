FROM node:16.19.0-alpine3.16

ARG CI

RUN npm install pm2 -g
RUN mkdir -p /opt/app
WORKDIR /opt/app
ADD . /opt/app
ENV NODE_ENV production
RUN yarn install
RUN yarn build:prod
CMD if [ "$PM2_DISABLED" = "1" ] ; then yarn run start:prod ; else yarn run start:prod:pm2 ; fi
