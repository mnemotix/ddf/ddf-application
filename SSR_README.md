# Poc pour SSR

## Choses à penser côté Serveur :

- Initialiser i18next pour récupérer les locales côté serveur (serveLocale)
- Initialiser Helmet (serveFrontend)
- Initialiser Apollo en mode SSR (serveFrontend)


## Problèmes non résolus pour faire du Full SSR :

- YasGUI désactivé (solution potentielle => dynamic import)
- Compilation Babel SSR vs CRS :
  - Images désactivées via plugin babel ignore
- Object window / localstorage not found en SSR (workaround avec jsdom)

## Solution retenue pour faire fonctionner le SEO :

1. Initialiser les metas tags "fallback" sur le index.tpl.html. Ceci assure qu'un lien copié dans un réseau social ou un logiciel faisant du "URL unfurling" retourne un aperçu minimal.

2. Détecter le user-agent d'un robot sur deux routes particulières (serveFrontend) :
  - Recherche de forme
  - Détail d'une définition.

  2.1. Pour les robots, faire un SSR partiel sur des composants prévus spécialement pour ça (src/client/components/ssr) afin de récupérer un objet
       Helmet rempli avec des tags.

    2.1.1 Si le robot n'exécute pas de Javascript, il se retrouvera avec un document HTML ayant un <body> vide mais un <head> rempli.

    2.1.2 Si le robot exécute pas de Javascript, il se retrouvera avec un document HTML ayant un <body> vide mais un <head> rempli à la résulution.
          Puis il aura en plus la page complète après l'exécution du rendu JS.

  2.2 Pour les navigateurs, rendu normal sans SSR.


## Pistes d'évolution.

- (Difficile) Utiliser un framework faisant du SSR nativement type Next.
- (Facile)    Pousser le SSR partiel pour les robots en faisant un rendu sans style mais en validant toutes les règles HTML5.


